This is the Java part of the source code for DICOMscope 3.6.0
(DICOM toolkit dcmtk 3.5.3 also required for compilation).

Directories:

DICOMscope  Application folder
interface   Interface between C++ and Java
tkgui       Java GUI source code

A detailed description on how to compile this code on different platforms
is available in the user manual (see DICOMscope/docs/dsum360.pdf).

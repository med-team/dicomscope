/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package browser;

import J2Ci.*;


/**
 * This class represents a tree element with all possible contents.
 *
 * @author Andreas Schroeter
 * @since 30.04.1999
*/
public class TreeElement
{
    
    
    
    private String text;
    public String studyUID = null;
    public String seriesUID =null;
    public String instanceUID = null;
    public int instanceType = -1;
    /**
     * Contains the status of the TreeElement.
     * Object isn't new {@link jDVIFhierarchyStatus.DVIF_objectIsNotNew}
     * Object is new {@link jDVIFhierarchyStatus.DVIF_objectIsNotNew}
     * Object contains new subjects {@link jDVIFhierarchyStatus.DVIF_objectContainsNewSubobjects}
    */
    private int status = jDVIFhierarchyStatus.DVIF_objectIsNew;
    
    public static boolean showUID = false;
    public static boolean showModality = false;
    public static boolean showDescription = false;
    public static boolean showPatientInfo = false;
    public static boolean showDateTime = false;
    public static boolean showFilename = false;
    public static boolean showLabel = false;
    public static boolean showNewWithIcon = false;
    
    public String modality;
    private String description;
    private String patientInfo;
    private String dateTime;
    private String fileName;
    private String label;
    
    
    public TreeElement (String text, 
                        int instanceType, 
                        String studyUID, 
                        String seriesUID, 
                        String instanceUID, 
                        String modality,
                        String description,
                        String patientInfo,
                        String dateTime,
                        String fileName,
                        String label, 
                        int status)
                        
    {
        this.instanceType = instanceType;
        this.status = status;
        this.text = text;
        this.studyUID = studyUID; 
        this.seriesUID = seriesUID; 
        this.instanceUID = instanceUID;
        this.modality = modality;
        this.description = description;
        this.patientInfo = patientInfo;
        this.dateTime = dateTime;
        this.fileName = fileName;
        this.label = label;
        
        if (description == null) description = new String ("");
        if (modality == null) modality = new String ("");
        if (patientInfo == null) patientInfo = new String ("");
        if (dateTime == null) dateTime = new String ("");
        if (fileName == null) fileName = new String ("");
        if (label == null) label = new String ("");
        
        if (instanceType != -1)  text = new String(modality);
    
    }
    
    
    public TreeElement (int instanceType, 
                        String studyUID, 
                        String seriesUID, 
                        String instanceUID, 
                        String modality,
                        String description,
                        String dateTime,
                        String fileName,
                        String label, 
                        int status)
    {
        this( null,
               instanceType, 
               
               studyUID, 
               seriesUID, 
               instanceUID,
               modality,
               description,
               null,
               dateTime,
               fileName,
               label,
               status);
    
    
    }
    
    public TreeElement (String text, 
                        String studyUID, 
                        String seriesUID,
                        String modality,
                        String description,
                        String patientInfo,
                        String dateTime,
                        String fileName,
                        String label,
                        int status)
                        
    {
        
        this( text, -1, studyUID, seriesUID, null,modality,description,patientInfo,dateTime,fileName,label,status);
    }
    
    public TreeElement (String text, 
                        String studyUID,
                        String modality,
                        String description,
                        String patientInfo,
                        String dateTime,
                        String fileName,
                        String label,
                        int status)
    {
        this( text, -1,   studyUID ,null,null ,modality,description,patientInfo,dateTime,fileName,label,status);
        
    }
    
    public TreeElement (String text)
    {
        this( text, -1,null, null, null,null,null,null,null,null,null,0);
    }
    
    public boolean isST()
    {
        if (instanceType ==jDVPSInstanceType.DVPSI_storedPrint) return true;
        else return false;
    
    }
    public boolean isHC()
    {
        if (instanceType ==jDVPSInstanceType.DVPSI_hardcopyGrayscale) return true;
        else return false;
    
    }
    public boolean isSR()
    {
        if (instanceType ==jDVPSInstanceType.DVPSI_structuredReport) return true;
        else return false;
    
    }
    public boolean isRoot()
    {
        return !(isInstance()||isSeries()||isStudy());
    }
    public boolean isInstance()
    {
        return !(instanceUID == null);
    }
    public boolean isSeries()
    {
        return (!isInstance() && !(seriesUID == null));
    }
    public boolean isStudy()
    {
        return (!isSeries() && !(studyUID==null));
        
    }
    public boolean isPS ()
    {
        if (instanceType ==jDVPSInstanceType.DVPSI_presentationState) return true;
        else return false;
    }
    
    public int getStatus()
    {
        return status;
    }
    
    public void setToOld ()
    {
        status = jDVIFhierarchyStatus.DVIF_objectIsNotNew;
    }
    
    public void setStatus (int status)
    {
        this.status = status;
    }
    
    public boolean isOld ()
    {
        if (status == jDVIFhierarchyStatus.DVIF_objectIsNotNew) return true;
        else return false;
    }
    
        
    public String toString ()
    {
        String temp= null;
        
        
        if (isInstance()) // Bild
        {            
            //temp = appendStr(temp, "index: " + instanceIdx+ ", UID: " + instanceUID);
            temp = new String (modality + " ");
            if (TreeElement.showLabel) temp = appendStr (temp, label);
            if (TreeElement.showDescription) temp = appendStr (temp, description);
            if (TreeElement.showUID) temp = appendStr (temp, instanceUID);
            if (TreeElement.showFilename) temp = appendStr (temp, "[" + fileName + "]");
            
            if (!TreeElement.showLabel && !TreeElement.showDescription && !TreeElement.showUID && !showFilename) temp += " #instance#";
        }
        else if (isSeries()) // Serie
        {
            temp = new String (text + " ");
            if (TreeElement.showModality) temp = appendStr (temp, modality);
            if (TreeElement.showDateTime) temp = appendStr (temp, dateTime);
            if (TreeElement.showDescription) temp = appendStr (temp, description);
            if (TreeElement.showUID) temp = appendStr (temp, seriesUID);
            
            if (!TreeElement.showDateTime && !TreeElement.showModality && !TreeElement.showDescription && !TreeElement.showUID) temp += " #series#";
        }
        else if (isStudy()) // Studie
        {
            //temp=appendStr(temp, "index: " + studyIdx+ ", UID: " + studyUID);
            
             temp = new String (text + " ");
            if (TreeElement.showPatientInfo) temp = appendStr (temp, patientInfo);
            if (TreeElement.showDateTime) temp = appendStr (temp, dateTime);
            if (TreeElement.showDescription) temp = appendStr (temp, description);
            if (TreeElement.showUID) temp = appendStr (temp, studyUID);
            
            if (!TreeElement.showDateTime && !TreeElement.showPatientInfo && !TreeElement.showDescription && !TreeElement.showUID) temp += " #study#";
        }       
        if (temp == null) return text;
        return temp;
    }	
    
    
    private String appendStr (String dest, String what)
    {     
        if (what == null) return dest;
        if (what.equals ("")) return dest;
        
        if (dest.charAt(dest.length()-1) == ' ') return dest + what;        
        else return dest + " * " + what;
    }
    
}

/*
 *  CVS Log
 *  $Log: TreeElement.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package browser;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import jToolkit.gui.*;
import main.*;
/**
* This class contains the diaog for saving a presentation state. 
* @autor Klaus Kleber
* @since 01.06.2000
*/
public class SavePresentationStateDialog extends javax.swing.JDialog implements jToolkit.gui.CommandButtonListener
{
    /**
    * Action for pressing the OK-Button
    */
    public static final int ID_OK = 0;
    /**
    * Action for pressing the Cancel-Button
    */
    public static final int ID_CANCEL = 1;
    
    
    private CommandJButton okButton = new CommandJButton("OK", this, ID_OK);
    private CommandJButton cancelButton = new CommandJButton("CANCEL", this,ID_CANCEL);
    private int actionValue;
    private JTextField nameTextField;
    private JTextField creatorTextField;
    private JRadioButton implicitButton = new JRadioButton("Implicit VR Little Endian", false);
    private JRadioButton explicitButton= new JRadioButton("Explicit VR Little Endian", true);
    private JCheckBox displayAreaCheckBox = new JCheckBox("Set Displayed Area to Image Size", true);
    
    /**
    * Constructor.
    * @param parent Parent Frame.
    * @param chooseTransferSyntax If true the transfer syntax of the image is selectable.
    */
    public SavePresentationStateDialog(JFrame parent, boolean chooseTransferSyntax, String title, boolean savePS) 
    {
        super(parent,title , true);
        setLocationRelativeTo(parent);
        getContentPane().setSize(400,300);
        
        JPanel paintPanel = new JPanel();
        paintPanel.setLayout(new BoxLayout(paintPanel,BoxLayout.Y_AXIS));
        getContentPane().add(paintPanel);
        
        GuiComponents gui = GuiComponents.getInstance();
        nameTextField = gui.nameTextField;
        creatorTextField = gui.creatorTextField;
        
        JPanel psPanel = new JPanel(new BorderLayout());
        psPanel.setBorder(new TitledBorder("Presentation State"));
        JPanel nPanel = new JPanel(new GridLayout(2,2));
        nPanel.add(new JLabel("Label: "));
        nPanel.add(nameTextField);
        nPanel.add(new JLabel("Creator: "));
        nPanel.add(creatorTextField);
        psPanel.add(nPanel, BorderLayout.NORTH);        
        
        paintPanel.add(psPanel);        
        
        JPanel daPanel = new JPanel();
        daPanel.setBorder(new TitledBorder("Displayed Area"));
        daPanel.add(displayAreaCheckBox);
        paintPanel.add(daPanel);        
        
         
         
         if (chooseTransferSyntax)
         {
            JPanel trPanel = new JPanel();
            trPanel.setBorder(new TitledBorder("Transfer Syntax"));
            trPanel.setLayout(new GridLayout(1,2));
            trPanel.add(implicitButton);
            trPanel.add(explicitButton);
            ButtonGroup bg = new ButtonGroup();
            bg.add(implicitButton);
            bg.add(explicitButton);
            paintPanel.add(trPanel);
         }
         
         JPanel southPanel = new JPanel();
         southPanel.add(okButton);
         if (savePS) southPanel.add(cancelButton);
        
        paintPanel.add(southPanel);
        pack();
        
    }
    /**
    * Action handling.
    * @param id the id of the pressed JButton.
    */
    public void buttonClicked (int id)
    {
            if (id == ID_OK)  
            {
                actionValue = ID_OK;
                setVisible (false);
            }
            if (id == ID_CANCEL) 
            {
                actionValue = ID_CANCEL;
                setVisible (false);
            }
    }
    /**
    * Returns the Action applied to this JDialog. The following values are possible:
    * ID_OK for pressing the OK Button and ID_CANCEL for pressing the Cancel Button.
    * returns the Action applied to this JDialog.
    */
    public int getActionValue()
    {
        return actionValue;
    }
    /**
    * Returns the name of the presentation state.
    * @return the name of the presentation state.
    */
    public String getPresentationStateName()
    {
        if (nameTextField != null) return nameTextField.getText();
        else return new String("");
    }
    /**
    * Returns true if the display Area of the mage should be set to the image size
    * @return true if the display Area of the mage should be set to the image size
    */
    public boolean saveImageSize()
    {
        return displayAreaCheckBox.isSelected();
    }
    /**
    * Returns true if image and presentation state should be save in Explicit VR Little Endian
    * @return true if image and presentation state should be saved in Explicit VR Little Endian, 
    * if false Implicit Little VR Endian
    */
    public boolean saveAsExplicit()
    {
        return explicitButton.isSelected();
    }
}
/*
 *  CVS Log
 *  $Log: SavePresentationStateDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

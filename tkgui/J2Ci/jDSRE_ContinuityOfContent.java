/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;
import java.util.*;

/** 
 * The class <em>jDSRE_ContinuityOfContent</em> represents the C++ enumeration
 * DSRTypes::E_ContinuityOfContent.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_ContinuityOfContent
{
    /** internal type used to indicate an error
     */
    public static final int COC_invalid = 0;

    /** DICOM defined term: SEPARATE
     */
    public static final int COC_Separate = 1;

    /** DICOM defined term: CONTINUOUS
     */
    public static final int COC_Continuous = 2;
    
    /**
    * Contains for each ID the name.
    */
    public static Hashtable names;
    static
    {
         names = new Hashtable();
         names.put(new Integer(COC_invalid),    "<invalid>");
         names.put(new Integer(COC_Separate),   "Separate");
         names.put(new Integer(COC_Continuous), "Continuous");
    };

    /**
    * Returns the name of the specified jDSRE_ContinuityOfContent
    * @param id Id specifieying the djDSRE_ContinuityOfContent
    * @return The name of the specified jDSRE_ContinuityOfContent
    */
    public static final  String getName(int id)
    {
        return (String) names.get(new Integer(id));
    }
    
}


/*
 *  CVS Log
 *  $Log: jDSRE_ContinuityOfContent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

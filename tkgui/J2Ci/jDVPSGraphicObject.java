/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * A <em>jDVPSGraphicObject</em> is the Java-sided class for the C++-Class
 * DVPSGraphicObject.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPSGraphicObject
{
    /**
     * Constructor    
    */
    protected jDVPSGraphicObject()
    {
        createObjOfDVPSGraphicObject ();
    }
    
    
    /**
     * Constructor for attaching an existing C++-Object. FOR INTERNAL USE ONLY!
     * @param attachAdr Address of C++-Object
    */    
    public jDVPSGraphicObject (long attachAdr)
    {
        cppClassAddress = attachAdr;
    }
    
    
        // -------------------------------- Methods for C++-Class Binding
    
    /**
     * Address of C++-Object for access in the DLL. Never change manually!!
    */
    private long cppClassAddress = (long) 0; // never change!
    
    
    /**
     * Creates a C++-Object of class DVPSGraphicObject and attached it to this
     * current object. The address of the C++-object will be put into field 
     * cppClassAddress.
     *
     * @see J2Ci.jDVPSGraphicObject#cppClassAddress
    */
    private native void createObjOfDVPSGraphicObject ();
    
    
    
    // ----------------------------------- Methods of Class jDVPSGraphicObject
    
    /**
     * Copy-Constructor
     * @param copy DVPSGraphicObject-Object to copy
    */
    public jDVPSGraphicObject (jDVPSGraphicObject copy)
    {                
        CopyConstructor (copy.cppClassAddress);
    }

    private native void CopyConstructor (long fromCppObj);
    
     

    /** 
     * Gets the graphic annotation units.
     * @return annotation units (from jDVPSannotationUnit).
    */
    public native int getAnnotationUnits();

    
    /** 
     * Gets the number of graphic points.
     * @return number of graphic points
    */
    public native int getNumberOfPoints();

    
    /** 
     * Gets one point from the graphic data.
     * @param idx index of the graphic point, must be < getNumberOfPoints();
     * @param x upon success the x value of the point is returned in this parameter.
     *        Must be created before passing to this method!
     * @param y upon success the y value of the point is returned in this parameter   
     *        Must be created before passing to this method!
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int getPoint(int idx, jDoubleByRef x, jDoubleByRef y);

    
    /** 
     * Gets the graphic type of this graphic object.
     * @return graphic type (from jDVPSGraphicType).
    */
    public native int getGraphicType();

    
    /** 
     * Checks if the graphic is filled.
     * @return true if graphic is filled.
    */
    public native boolean isFilled();
  
  
    /** 
     * Sets the graphic data for this graphic object.
     * @param number number of graphic points in parameter "data"
     * @param data pointer to an array of Float32 values with a size of (at least)
     *      2*number. The values are copied into the graphic object.
     * @param unit the graphic annotation units for this data (from jDVPSannotationUnit).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setData(int number, float[] data, int unit);


    /** 
     * Sets the graphic type for the graphic object
     * @param gtype the graphic type (from jDVPSGraphicType).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setGraphicType(int gtype);


    /** 
     * Sets the graphic filled status for the graphic object.
     * @param isFilled true if graphic is filled, false otherwise.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setFilled(boolean filled);

}

/*
 *  CVS Log
 *  $Log: jDVPSGraphicObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jE_Condition</em> represents the C++-enumeration
 * E_Condition.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPScharacterSet
{
  /** ISO 646 (ISO-IR 6): ASCII
   */
  public static final int DVPSC_ascii = 0;

  
  /** ISO-IR 100: Latin alphabet No. 1
   */
  public static final int DVPSC_latin1 = 1;
  
  
  /** ISO-IR 101: Latin alphabet No. 2
   */
  public static final int DVPSC_latin2 = 2;
  
  
  /** ISO-IR 109: Latin alphabet No. 3
   */
  public static final int DVPSC_latin3 = 3;
  
  
  /** ISO-IR 110: Latin alphabet No. 4
   */
  public static final int DVPSC_latin4 = 4;
  
  
  /** ISO-IR 148: Latin alphabet No. 5
   */
  public static final int DVPSC_latin5 = 5;
  
  
  /** ISO-IR 144: Cyrillic
   */
  public static final int DVPSC_cyrillic = 6;
  
  
  /** ISO-IR 127: Arabic
   */
  public static final int DVPSC_arabic = 7;
  
  
  /** ISO-IR 126: Greek
   */
  public static final int DVPSC_greek = 8;
  
  
  /** ISO-IR 138: Hebrew
   */
  public static final int DVPSC_hebrew = 9;
  
  
  /** ISO-IR 13: Japanese (Katakana/Romaji)
   */
  public static final int DVPSC_japanese = 10;
  
  
  /** unrecognized term or code extension
   */
  public static final int DVPSC_other = 11;
}

/*
 *  CVS Log
 *  $Log: jDVPScharacterSet.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/


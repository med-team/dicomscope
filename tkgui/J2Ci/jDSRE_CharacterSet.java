/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/** 
 * The class <em>jDSRE_CharacterSet</em> represents the C++ enumeration
 * DSRTypes::E_CharacterSet.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_CharacterSet
{
    
    /** internal type used to indicate an error/unknown CS
     */
    public static final int CS_invalid = 0;
    
    /** ISO 646 (ISO-IR 6): ASCII
     */
    public static final int CS_ASCII = 1;
    
    /** ISO-IR 100: Latin alphabet No. 1
     */
    public static final int CS_Latin1 = 2;
    
    /** ISO-IR 101: Latin alphabet No. 2
     */
    public static final int CS_Latin2 = 3;
    
    /** ISO-IR 109: Latin alphabet No. 3
     */
    public static final int CS_Latin3 = 4;
    
    /** ISO-IR 110: Latin alphabet No. 4
     */
    public static final int CS_Latin4 = 5;
    
    /** ISO-IR 148: Latin alphabet No. 5
     */
    public static final int CS_Latin5 = 6;
    
    /** ISO-IR 144: Cyrillic
     */
    public static final int CS_Cyrillic = 7;
    
    /** ISO-IR 127: Arabic
     */
    public static final int CS_Arabic = 8;
    
    /** ISO-IR 126: Greek
     */
    public static final int CS_Greek = 9;
    
    /** ISO-IR 138: Hebrew
     */
    public static final int CS_Hebrew = 10;
    
    /** ISO-IR 166: Thai
     */
    public static final int CS_Thai = 11;
    
    /** ISO-IR 13: Japanese (Katakana/Romaji)
     */
    public static final int CS_Japanese = 12;
}


/*
 *  CVS Log
 *  $Log: jDSRE_CharacterSet.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

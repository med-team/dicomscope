/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRDocument</em> represents the relating C++ class DSRDocument in Java.
 *
 *  This class supports reading, writing, creation, printing and rendering of
 *  DICOM Structured Reporting documents (according to supplement 23 final text).
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRDocument
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRDocument()
    {
        // emtpy
    }

    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRDocument(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!


    // --------------------------- native methods ------------------------------

    /* --- DSRDocument --- */

    // --- misc routines ---

    /** check whether the current internal state is valid.
     *  The SR document is valid if the corresponding document tree is valid and
     *  the SOP instance UID as well as the SOP class UID are not "empty".
     ** @return OFTrue if valid, OFFalse otherwise
     */
    public native boolean isValid();

    /** check whether the document is finalized.
     *  A new document is originally not finalized but can be finalized using the method
     *  finalizeDocument().  This flag is e.g. used to indicate whether the entire document
     *  is digitally signed and, therefore, each newly added verifying observer would corrupt
     *  all previous signatures.
     ** @return OFTrue if finalized, OFFalse otherwise
     */
    public native boolean isFinalized();


    // --- input and output ---

    /** render current SR document in HTML format
     ** @param  outputString  output string to which the HTML document is written
     *  @param  flags         flag used to customize the output (see DSRTypes::HF_xxx).
     *                        The flag 'HF_version32Compatibility' is implicitly set.
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int renderHTML(jStringByRef outputString,
                                 int flags);


    // --- get misc attributes ---

    /** get the current SR document type
     ** @return document type (might be DT_invalid if read from dataset)
     */
    public native int getDocumentType();

    /** get document tree
     ** @return reference to the document tree
     */
    public jDSRDocumentTree getTree()
    {
        return new jDSRDocumentTree (getTreeN());
    }

    private native long getTreeN();

    /** get specific character set type.
     *  If the type is unknown the original DICOM defined term can be retrieved
     *  with the method getSpecificCharacterSet().
     ** @return character set (might be CS_invalid/unknown if not supported)
     */
    public native int getSpecificCharacterSetType();

    /** set specific character set type.
     *  The DICOM defined term (see SpecificCharacterSet) is set accordingly.
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setSpecificCharacterSetType(int characterSet);

    /** get document completion flag
     ** @return completion flag (might be CF_invalid if read from dataset)
     */
    public native int getCompletionFlag();

    /** get document completion flag description
     ** @return string value (might be NULL)
     */
    public native String getCompletionFlagDescription();

    /** get document verification flag
     ** @return verification flag (might be VF_invalid if read from dataset)
     */
    public native int getVerificationFlag();

    /** get number of verifying observers.
     *  A document can be verified more than once.  The verification flag should be VERIFIED
     *  if any verifying observer is specified.  The details on the observer can be retrieved
     *  using the getVerifyingObserver() methods.
     ** @return number of verifying observers (if any), 0 otherwise
     */
    public native int getNumberOfVerifyingObservers();

    /** get information about a verifying observer.
     *  All reference variables are cleared before the information is retrieved, i.e. if an error
     *  occurs (return value != EC_Normal) non-empty variables do contain correct data.
     ** @param  idx           index of the verifying observer to be retrieved (starting with 1).
     *                        Use getNumberOfVerifyingObservers() to get the maximum value.
     *  @param  dateTime      reference to variable where the date and time when this document
     *                        has been verified should be stored (required)
     *  @param  observerName  reference to variable where the name of the person who has verified
     *                        this document should be stored (required)
     *  @param  organization  reference to variable where the name of the organization to which
     *                        the observer belongs should be stored (required)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int getVerifyingObserver(int idx,
                                           jStringByRef dateTime,
                                           jStringByRef observerName,
                                           jStringByRef organization);

    /** get information about a verifying observer.
     *  All reference variables are cleared before the information is retrieved, i.e. if an error
     *  occurs (return value != EC_Normal) non-empty variables do contain correct data.
     ** @param  idx                             index of the verifying observer to be retrieved
     *                                          (starting with 1). Use getNumberOfVerifyingObservers()
     *                                          to get the maximum value.
     *  @param  dateTime                        reference to variable where the date and time when this
     *                                          document has been verified should be stored (required)
     *  @param  observerName                    reference to variable where the name of the person who
     *                                          has verified this document should be stored (required)
     *  @param  observerCodeValue               reference to variable where the code value of the code
     *                                          identifying the verifying observer should be stored.
     *                                          (optional, see previous method)
     *  @param  observerCodingSchemeDesignator  reference to variable where the coding scheme designator
     *                                          of the code identifying the verifying observer should be
     *                                          stored. (optional, see previous method)
     *  @param  observerCodingSchemeVersion     reference to variable where the coding scheme version
     *                                          of the code identifying the verifying observer should be
     *                                          stored. (optional, see previous method)
     *  @param  observerCodeMeaning             reference to variable where the code meaning of the code
     *                                          identifying the verifying observer should be stored.
     *                                          (optional, see previous method)
     *  @param  organization  reference to variable where the name of the organization to which
     *                        the observer belongs should be stored (required)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int getVerifyingObserver(int idx,
                                           jStringByRef dateTime,
                                           jStringByRef observerName,
                                           jStringByRef observerCodeValue,
                                           jStringByRef observerCodingSchemeDesignator,
                                           jStringByRef observerCodingSchemeVersion,
                                           jStringByRef observerCodeMeaning,
                                           jStringByRef organization);

    /** get number of predecessor documents.
     *  A document can have more than one (direct) predecessor document.  This is the case
     *  when two or more documents have been merged to created it.  The corresponding method
     *  createRevisedVersion() creates only one reference to the current document (and
     *  replaces any existing referenced).
     ** @return number of predecessor documents (if any), 0 otherwise
     */
    public native int getNumberOfPredecessorDocuments();


    // --- get DICOM string attributes (C string) ---

    /** get modality
     ** @return string value (might be NULL)
     */
    public native String getModality();

    /** get SOP class UID
     ** @return string value (might be NULL)
     */
    public native String getSOPClassUID();

    /** get study instance UID
     ** @return string value (might be NULL)
     */
    public native String getStudyInstanceUID();

    /** get series instance UID
     ** @return string value (might be NULL)
     */
    public native String getSeriesInstanceUID();

    /** get SOP instance UID
     ** @return string value (might be NULL)
     */
    public native String getSOPInstanceUID();

    /** get instance creator UID
     ** @return string value (might be NULL)
     */
    public native String getInstanceCreatorUID();

    /** get specific character set
     ** @return string value (might be NULL)
     */
    public native String getSpecificCharacterSet();

    /** get patient's name
     ** @return string value (might be NULL)
     */
    public native String getPatientsName();

    /** get patient's birth date
     ** @return string value (might be NULL)
     */
    public native String getPatientsBirthDate();

    /** get patient's sex
     ** @return string value (might be NULL)
     */
    public native String getPatientsSex();

    /** get referring physicians name
     ** @return string value (might be NULL)
     */
    public native String getReferringPhysiciansName();

    /** get study description
     ** @return string value (might be NULL)
     */
    public native String getStudyDescription();

    /** get series description
     ** @return string value (might be NULL)
     */
    public native String getSeriesDescription();

    /** get manufacturer
     ** @return string value (might be NULL)
     */
    public native String getManufacturer();

    /** get study date
     ** @return string value (might be NULL)
     */
    public native String getStudyDate();

    /** get study time
     ** @return string value (might be NULL)
     */
    public native String getStudyTime();

    /** get instance creation date
     ** @return string value (might be NULL)
     */
    public native String getInstanceCreationDate();

    /** get instance creation time
     ** @return string value (might be NULL)
     */
    public native String getInstanceCreationTime();

    /** get content date
     ** @return string value (might be NULL)
     */
    public native String getContentDate();

    /** get content time
     ** @return string value (might be NULL)
     */
    public native String getContentTime();

    /** get study ID
     ** @return string value (might be NULL)
     */
    public native String getStudyID();

    /** get patient ID
     ** @return string value (might be NULL)
     */
    public native String getPatientID();

    /** get series number
     ** @return string value (might be NULL)
     */
    public native String getSeriesNumber();

    /** get instance number
     ** @return string value (might be NULL)
     */
    public native String getInstanceNumber();

    /** get accession number
     ** @return string value (might be NULL)
     */
    public native String getAccessionNumber();


    // --- set DICOM string attributes ---

    /** set specific character set.
     *  The passed string must be a valid DICOM Code String (CS).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setSpecificCharacterSet(String string);

    /** set document completion flag description.
     *  The description can be removed from the DICOM dataset (type 3) by setting an empty string.
     ** @param  string  explanation of the value set for completion flag (optional, VR=LO)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setCompletionFlagDescription(String String);

    /** set patient's name.
     *  The passed string must be a valid DICOM Person Name (PN).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setPatientsName(String string);

    /** set patient's birth date.
     *  The passed string must be a valid DICOM Date (DA).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setPatientsBirthDate(String string);

    /** set patient's sex.
     *  The passed string must be a valid DICOM Code String (CS).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setPatientsSex(String string);

    /** set referring physicians name.
     *  The passed string must be a valid DICOM Person Name (PN).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setReferringPhysiciansName(String string);

    /** set study description.
     *  The passed string must be a valid DICOM Long String (LO).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setStudyDescription(String string);

    /** set series description.
     *  The passed string must be a valid DICOM Long String (LO).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setSeriesDescription(String string);

    /** set manufacturer.
     *  The passed string must be a valid DICOM Long String (LO).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setManufacturer(String string);

    /* NOT USED IN JNI
    E_Condition setContentDate(const OFString &string);
    E_Condition setContentTime(const OFString &string);
    */

    /** set study ID.
     *  The passed string must be a valid DICOM Short String (SH).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setStudyID(String string);

    /** set patient ID.
     *  The passed string must be a valid DICOM Long String (LO).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setPatientID(String string);

    /** set series number.
     *  The passed string must be a valid DICOM Short String (SH).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setSeriesNumber(String string);

    /** set instance number.
     *  The passed string must be a valid DICOM Integer String (IS).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setInstanceNumber(String string);

    /** set accession number.
     *  The passed string must be a valid DICOM Short String (SH).
     ** @param  string  character string specifying the value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setAccessionNumber(String string);


    // --- document management functions ---

    /** create new study.
     *  After generating a new study instance UID the method createNewSeries() is called,
     *  i.e. also a new series instance UID and SOP instance UID are generated.  This is
     *  a requirement according to DICOM Supplement 23.
     */
    public native void createNewStudy();

    /** create a new series.
     *  After generating a new series instance UID the method createNewSOPInstance() is
     *  called, i.e. also a SOP instance UID is generated.  This is a requirement according
     *  to DICOM Supplement 23.
     */
    public native void createNewSeries();

    /** create a new series within a given study.
     *  After generating a new series instance UID within the given study the method
     *  createNewSOPInstance() is called, i.e. also a SOP instance UID is generated.
     *  This is a requirement according to DICOM Supplement 23.
     ** @param  studyUID  study instance UID to be set (should be a valid UID)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int createNewSeriesInStudy(String studyUID);

    /** create a new SOP instance.
     *  Generate a new SOP instance UID, set the instance creation date/time and reset the
     *  finalized flag (OFFalse).
     *  This method is used internally for createNewDocument(), createRevisedVersion()
     *  and during object initialization.
     *  It could also be used explicitly from the calling application if a new UID should
     *  be created (this is the case if the study instance UID or series instance UID has
     *  changed as well as any other attribute within the SR Document General Module or
     *  SR Document Content Module, see DICOM Supplement 23).
     *  This method also updates the other DICOM header attributes (calling updateAttributes()).
     */
    public native void createNewSOPInstance();

    /** create a new document.
     *  A new SOP instance is only created if the current document type was valid/supported.
     *  Please note that the current document is deleted.
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int createNewDocument();

    /** create a new document of the specified type.
     *  A new SOP instance is only created if the current document type was valid/supported.
     *  Please note that the current document is deleted.
     ** @param  documentType  type of the SR document (BasicText, Enhanced, Comprehensive)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int createNewDocument(int documentType);

    /** create a revised version of the current document.
     *  A revised version can only be created if the current document is already completed
     *  (see completion flag).  If so a reference to the current document is included in the
     *  predecessor documents sequence (possible existing references are automatically
     *  replaced, i.e. there is never more than one reference in this sequence).  If all
     *  revised versions of a SR document are stored (written to datasets/files) it is
     *  possible to trace back the full chain of previous versions.
     *  A new SOP instance is created and the content date/time are set automatically.
     *  Furthermore the verifying observer sequence is deleted, the verifcation flag is set
     *  to UNVERIFIED, the completion flag is set to PARTIAL (i.e. not complete), the
     *  completion flag description is deleted, all digital signatures contained in the
     *  document tree are deleted and the finalized flag is reset (OFFalse).
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int createRevisedVersion();

    /** complete the current document.
     *  Sets the completion flag to COMPLETE if not already done (fails otherwise).
     *  The completion flag description is set to an empty string (i.e. absent in DICOM
     *  dataset).
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int completeDocument();

    /** complete the current document and set completion description.
     *  Sets the completion flag to COMPLETE if not already done (fails otherwise).
     *  The completion flag description can be modified independently from the flag by means
     *  of the method setCompletionFlagDescription() - see above.
     ** @param  description  explanation of the value set for completion flag (optional, see
     *                       previous method, VR=LO)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int completeDocument(String description);

    /** verify the current document by a specific observer.
     *  A document can be verified more than once.  The observer information is added to a
     *  sequence stored in the dataset.  The verification flag is automatically set to
     *  VERIFIED (if not already done) and the finalized flag is reset (OFFalse).
     *  Please note that only completed documents (see completion flag) can be verified and that
     *  a new SOP instance UID has to be generated (manually) according to the DICOM standard when
     *  creating a dataset/file from this document.
     ** @param  observerName  name of the person who has verified this document (required, VR=PN)
     *  @param  organization  name of the organization to which the observer belongs (required, VR=LO)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int verifyDocument(String observerName,
                                     String organization);

    /** verify the current document by a specific observer.
     *  A document can be verified more than once.  The observer information is added to a
     *  sequence stored in the dataset.  The verification flag is automatically set to
     *  VERIFIED (if not already done) and the finalized flag is reset (OFFalse).
     *  Please note that only completed documents (see completion flag) can be verified and that
     *  a new SOP instance UID has to be generated (manually) according to the DICOM standard when
     *  creating a dataset/file from this document.
     ** @param  observerName                    name of the person who has verified this document
     *                                          (required, VR=PN)
     *  @param  observerCodeValue               code value of the code identifying the observer
     *  @param  observerCodingSchemeDesignator  coding scheme designator of the code identifying the
     *                                          observer
     *  @param  observerCodingSchemeVersion     coding scheme version of the code identifying the
     *                                          observer
     *  @param  observerCodeMeaning             code meaning of the code identifying the observer
     *  @param  organization                    name of the organization to which the observer belongs
     *                                          (required, VR=LO)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int verifyDocument(String observerName,
                                     String observerCodeValue,
                                     String observerCodingSchemeDesignator,
                                     String observerCodingSchemeVersion,
                                     String observerCodeMeaning,
                                     String organization);

    /** remove verification information.
     *  The list of verifying observers is cleared, the verification flag is set to UNVERIFIED and
     *  the finalized flag is reset (OFFalse).
     *  Normally, there should be no need to use this method.  On the other hand it is useful to
     *  guarantee a consistent state when processing documents which have not been created with this
     *  toolkit.
     */
    public native void removeVerification();

    /** finalize the current state of the document.
     *  A new document is originally not finalized but can be finalized using the method
     *  finalizeDocument().  This flag is e.g. used to indicate whether the entire document
     *  is digitally signed and, therefore, each newly added verifying observer would corrupt
     *  all previous signatures.
     *  NB: A document needs to be completed first in order to be finalized.  Some of the above
     *      document management functions do reset the flag (i.e. set the FinalizedFlag to OFFalse),
     *      other methods (e.g. the setXXX) do not change the flag though the state of the document
     *      is not finalized any more after they have been called.
     ** @return status, EC_Normal if successful, an error code otherwise
     */
//    public native int finalizeDocument();
}


/*
 *  CVS Log
 *  $Log: jDSRDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

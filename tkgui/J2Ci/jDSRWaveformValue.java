/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRWaveformValue</em> represents the relating C++ class
 *  DSRWaveformReferenceValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRWaveformValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRWaveformValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRWaveformValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Waveform Value --- */

    /** get SOP class UID
     ** @return current SOP class UID (might be invalid or an empty string)
     */
    public native String getSOPClassUID();

    /** get SOP class name.
     *  The SOP class name as defined in the OFFIS dcmtk is used if available,
     *  an empty string if the SOP class UID is also empty, the static string
     *  "unknown SOP class" plus the SOP class UID otherwise.
     ** @return name of the current SOP class (should never be null or empty)
     */
    public native String getSOPClassName();

    /** get SOP instance UID
     ** @return current SOP instance UID (might be invalid or an empty string)
     */
    public native String getSOPInstanceUID();

    /** set SOP class UID and SOP instance UID value.
     *  Before setting the values they are checked (non-empty UIDs).  Currently
     *  all waveform SOP classes that are defined in supplement 30 are allowed.
     *  If the value pair is invalid the current value pair is not replaced and
     *  remains unchanged.
     ** @param  sopClassUID     SOP class UID to be set
     *  @param  sopInstanceUID  SOP instance UID to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setReference(String sopClassUID,
                                   String sopInstanceUID);

    /** clear the referenced channel list
     */
    public native void clearChannelList();

    /** get number of entries contained in the referenced channel list
     ** @return number of channels if any, 0 otherwise
     */
    public native int getNumberOfChannels();

    /** get copy of the specified entry from the referenced channel list
     ** @param  idx                   index of the entry to be returned (starting from 1)
     *  @param  multiplexGroupNumber  reference to variable where the multiplex group number
     *                                should be stored
     *  @param  channelNumber         reference to variable where the channel number should be
     *                                stored
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int getChannel(int idx,
                                 jIntByRef multiplexGroupNumber,
                                 jIntByRef channelNumber);

    /** add entry to the referenced channel list.
     *  Please note that only channels are added that are not already contained in the list.
     ** @param  multiplexGroupNumber  multiplex group number to be added
     *  @param  channelNumber         channel number to be added
     */
    public native void addChannel(int multiplexGroupNumber,
                                  int channelNumber);

    /** remove entry from the referenced channel list
     ** @param  idx  index of the entry to be removed (starting from 1)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int removeChannel(int idx);

    /** check whether the waveform reference applies to a specific channel.
     *  The waveform reference applies to a channel if the list of referenced waveform
     *  channels is empty or the group/channel pair is part of the list.
     ** @param  multiplexGroupNumber  multiplex group number of the referenced channel
     *  @param  channelNumber         channel number of the referenced channel
     ** @return OFTrue if reference applies to the specified channel, OFFalse otherwise
     */
    public native boolean appliesToChannel(int multiplexGroupNumber,
                                           int channelNumber);
}


/*
 *  CVS Log
 *  $Log: jDSRWaveformValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

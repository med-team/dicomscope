/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/**
 * A <em>jDVPSStoredPrint</em> represents the relating C++-Class DVPSStoredPrint
 * in Java.
 *
 * @author 	Andreas Schroeter, Joerg Riesmeier
 */
public class jDVPSStoredPrint
{
    /**
     * Constructor is disabled !!!
    */
    protected jDVPSStoredPrint()
    {
        // emtpy
    }

    /**
     * Constructor for attaching an existing C++-Object. FOR INTERNAL USE ONLY!
     * @param attachAdr Address of C++-Object
    */
    public jDVPSStoredPrint (long attachAdr)
    {
        cppClassAddress = attachAdr;
    }

    // -------------------------------- Methods for C++-Class Binding

    /**
     * Address of C++-Object for access in the DLL. Never change manually!!
    */
    private long cppClassAddress = (long) 0; // never change!


    // --------------------------- native Methods ------------------------------


    /**
     * Reset the object to initial state.
     * After this call, the object is in the same state as after
     * creation with the default constructor.
    */
    public native void clear();


    /** sets the name of the current printer.
     *  This name is identical to the unique entry used in the configuration file.
     *  @return name of the current printer
     */
    public native int setOriginator(String aetitle);


    /** sets the application entity title of the print SCU.
     *  @return application entity title of the print SCU
     */
    public native int setDestination(String aetitle);


    /** sets the application entity title of the print SCP.
     *  @return application entity title of the print SCP
     */
    public native int setPrinterName(String name);


    /**
     * Sets the image display format to 'STANDARD\columns,rows'.
     * The caller must make sure that the column and row values are
     * valid for the selected printer.
     * @param columns number of columns
     * @param rows number of rows
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setImageDisplayFormat(long columns, long rows);


    /**
     * Sets the (optional) film size ID.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setFilmSizeID(String value);


    /**
     * Sets the (optional) magnification type.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setMagnificationType(String value);


    /**
     * Sets the (optional) smoothing type.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setSmoothingType(String value);


    /**
     * Sets the (optional) configuration information.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setConfigurationInformation(String value);


    /**
     * Sets the (optional) requested resolution ID.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setResolutionID(String value);


    /**
     * Sets the (optional) film orientation.
     * @param value new enumerated value. The caller is responsible for
     *   making sure that the selected printer supports film orientation
     *   if a non-default value is set (from jDVPSFilmOrientation).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setFilmOrientation(int value);


    /**
     * Sets the (optional) trim (printing of borders).
     * @param value new enumerated value. The caller is responsible for
     *   making sure that the selected printer supports trim
     *   if a non-default value is set (from jDVPSTrimMode)
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setTrim(int value);


    /**
     * Sets the (optional) requested decimate/crop behaviour
     * for all image boxes managed by this stored print object.
     * @param value new enumerated value. The caller is responsible for
     *   making sure that the selected printer supports decimate/crop
     *   if a non-default value is set (from jDVPSDecimateCropBehaviour).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setRequestedDecimateCropBehaviour(int value);


    /**
     * Deletes all optional attribute values that might not be
     * supported by all printers. Film size ID, magnification and smoothing type,
     * configuration information, requested resolution ID, film orientation,
     * trim and requested decimate/crop behaviour are reset to default.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int newPrinter(); // short cut, delete all optional settings


    /** gets the the application entity title of the print SCU.
     *  @return application entity title of the print SCP
     */
    public native String getOriginator();


    /** gets the the application entity title of the print SCP.
     *  @return application entity title of the print SCP
     */
    public native String getDestination();


    /** gets the name of the current printer.
     *  @return name of the current printer
     */
    public native String getPrinterName();


    /**
     * Gets the number of columns of the current image display format.
     * @return number of columns.
    */
    public native long getImageDisplayFormatColumns();


    /**
     * Gets the number of rows of the current image display format.
     * @return number of rows.
    */
    public native long getImageDisplayFormatRows();


    /**
     * Gets the current film orientation.
     * @return film orientation (from jDVPSFilmOrientation).
    */
    public native int getFilmOrientation();


    /**
     * Gets the current trim mode.
     * @return trim mode (from jDVPSTrimMode).
    */
    public native int getTrim();


    /**
     * Gets the current requested decimate/crop behaviour setting
     * that is used for all image boxes managed by this object.
     * @return requested decimate/crop behaviour (from jDVPSDecimateCropBehaviour)
    */
    public native int getRequestedDecimateCropBehaviour();


    /**
     * Gets the (optional) film size ID.
     * @return film size ID, may be NULL.
    */
    public native String getFilmSizeID();


    /**
     * Gets the (optional) magnification type.
     * @return magnification type, may be NULL.
    */
    public native String getMagnificationType();


    /**
     * Gets the (optional) smoothing type.
     * @return smoothing type, may be NULL.
    */
    public native String getSmoothingType();


    /**
     * Gets the (optional) configuration information.
     * @return configuration information, may be NULL.
    */
    public native String getConfigurationInformation();


    /**
     * Gets the (optional) requestes resolution ID
     * @return requested resolution ID, may be NULL.
    */
    public native String getResolutionID();


    /**
     * Gets the number of images currently registered in this object.
     * @return number of images.
    */
    public native int getNumberOfImages();


    /**
     * Deletes one of the registered images.
     * @param idx index, must be < getNumberOfImages()
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int deleteImage(int idx);


    /**
     * Deletes multiple of the registered
     * images, starting with the first one.
     * @param number number of images to delete, must be <= getNumberOfImages()
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int deleteMultipleImages(int number);


    /**
     * Creates a new image box object and sets the content of this image box object.
     * @param retrieveaetitle retrieve AETITLE of the referenced image
     * @param refsopinstanceuid SOP instance UID of the referenced image
     * @param requestedimagesize requested images size for this image, default: absent
     * @param patientid patient ID for the referenced image, default: absent
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int addImageBox(String retrieveaetitle, String refsopinstanceuid, String requestedimagesize, String patientid);


    /**
     * Sets a new SOP Instance UID for the Stored Print object.
     * @param uid new SOP Instance UID
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setInstanceUID(String uid);


    /**
     * Checks if one of the registered images has additional settings that are not
     * default values on the image box level.
     * @param idx index, must be < getNumberOfImages()
     * @return true if yes.
    */
    public native boolean imageHasAdditionalSettings(int idx);


    /**
     * Sets the (optional) magnification type for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setImageMagnificationType(int idx, String value);


    /**
     * Sets the (optional) smoothing type for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setImageSmoothingType(int idx, String value);


    /**
     * Sets the (optional) configuration information for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setImageConfigurationInformation(int idx, String value);


    /**
     * Gets the (optional) magnification type for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @return magnification type, may be NULL.
    */
    public native String getImageMagnificationType(int idx);


    /**
     * Gets the (optional) smoothing type for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @return smoothing type, may be NULL.
    */
    public native String getImageSmoothingType(int idx);


    /**
     * Gets the (optional) configuration information for the given registered image box.
     * @param idx index, must be < getNumberOfImages()
     * @return configuration information, may be NULL.
    */
    public native String getImageConfigurationInformation(int idx);


    /** 
     * Resets the Presentation LUT to the default setting. i.e. the presentation LUT
     * which is specified separately for each image box is used.
     * @return EC_Normal if successful, an error code otherwise.
    */
    public native int setDefaultPresentationLUT();


    /**
     * Sets the current Presentation LUT shape (overrides the image box settings).
     * Only DVPSP_identity and DVPSP_lin_od are allowed.
     * @param shape the new presentation LUT shape (from jDVPSPresentationLUTType).
     * @return EC_Normal if successful, an error code otherwise.
    */
    public native int setPresentationLUTShape(int shape);


    /** converts an optical density (OD) value to an 8/12/16-bit P-value which is linear to luminance.
     *  The output is not calibrated according to the GSDF.  This can be done by convertPValueToDDL() in
     *  class DVPSPresentationState.  The attributes illumination, reflected ambient light and min/max
     *  density (default 20/300) from this stored print object are used for the calculation.
     *  @param density in hundreds of OD (e.g. 150 corressponds to 1.5 OD)
     *  @param bits number of bits used for the output value (8, 12, 16)
     *  @return P-Value, 0..0xFF, 0..0xFFF, 0..0xFFFF, < 0 if an error occurred.
     */
    public native int convertODtoPValue(int density, int bits);


    /**
     * Sets the (optional) border density.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setBorderDensity(String value);


    /**
     * Sets the (optional) empty image density.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setEmtpyImageDensity(String value);


    /**
     * Gets the (optional) border density.
     * @return border density, may be NULL.
    */
    public native String getBorderDensity();


    /**
     * Gets the (optional) empty image density.
     * @return empty image density, may be NULL.
    */
    public native String getEmtpyImageDensity();


    /**
     * Sets the (optional) max density.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise.
    */
    public native int setMaxDensity(String value);


    /**
     * Sets the (optional) min density.
     * @param value new attribute value, may be NULL.
     *   The caller is responsible for making sure
     *   that the value is valid for the selected printer.
     * @return EC_Normal if successful, an error code otherwise.
    */
    public native int setMinDensity(String value);


    /**
     * Gets the (optional) max density.
     * The string returned becomes invalid after the next
     * call to getMaxDensity or getMinDensity.
     * @return max density, may be NULL.
    */
    public native String getMaxDensity();


    /**
     * Gets the (optional) min density.
     * The string returned becomes invalid after the next
     * call to getMaxDensity or getMinDensity.
     * @return min density, may be NULL.
    */
    public native String getMinDensity();


    /** sets the illumination to be used
     *  with the print Presentation LUT SOP Class.
     *  @param value new attribute value, in cd/m2.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrintIllumination(int value);


    /** gets the current illumination setting
     *  used with the print Presentation LUT SOP Class.
     *  @return illumination in cd/m2
     */
    public native int getPrintIllumination();


    /** sets the reflected ambient light to be used
     *  with the print Presentation LUT SOP Class.
     *  @param value new attribute value, in cd/m2.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrintReflectedAmbientLight(int value);


    /** gets the current reflected ambient light setting
     *  used with the print Presentation LUT SOP Class.
     *  @return reflected ambient light in cd/m2
     */
    public native int getPrintReflectedAmbientLight();

}


/*
 *  CVS Log
 *  $Log: jDVPSStoredPrint.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

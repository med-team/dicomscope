/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/**
 * A <em>jDVInterface</em> represents the relating C++-Class DVInterface
 * in Java.
 *
 * @author 	Andreas Schroeter, Joerg Riesmeier, Marco Eichelberg
 */
public class jDVInterface
{
    /**
     * Constructor, creates a relating C++-Object.
     * @param configfile s String defining the name of the config file.
    */
    public jDVInterface(String configfile)
    {
        createObjOfDVInterface (configfile);
    }


    /**
     * Destructor, removes *this* relating C++-Object out of memory
    */
    public void finalize ()
    {
        removeObjOfDVInterface ();
    }

    // -------------------------------- Methods for C++-Class Binding

    /**
     * Address of relating C++-Object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!


    /**
     * Creates a C++-Object and write its address in field "cppClassAddress"
     *
     * @see J2Ci.jDVInterface#cppClassAddress
    */
    private native void createObjOfDVInterface (String indexfolder);


    /**
     * Removes a C++-Object with the address of field cppClassAddress out of memory.
     * @see J2Ci.jDVInterface#cppClassAddress
    */
    private native void removeObjOfDVInterface ();


    // --------------------------- native Methods ------------------------------

    /* --- DVInterface --- */

    /* load images and presentation states */

    /** loads an image which is contained in the database
     *  and creates a default presentation state for the image.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID study instance UID of the image
     *  @param seriesUID series instance UID of the image
     *  @param instanceUID SOP instance UID of the image
     *  @param changeStatus if true the image file is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise (from jE_Condition).
     */
    public native int loadImage(String studyUID, String seriesUID, String instanceUID, boolean changeStatus);


    /** loads an image (which need not be contained in the database)
     *  and creates a default presentation state for the image.
     *  This method does not acquire a database lock.
     *  @param filename path and filename of the image to be loaded
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadImage(String filename);


    /** loads an image which referenced by the current presentation
     *  state and needs to be contained in the database.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param idx index of the image to be loaded (< getNumberOfImageReferences())
     *  @param changeStatus if true the image file is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadReferencedImage(int idx, boolean changeStatus);


    /** loads a presentation state which is contained in the database.
     *  The first image referenced in presentation state is also looked up in the
     *  database, loaded, and attached to the presentation state.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID study instance UID of the presentation state
     *  @param seriesUID series instance UID of the presentation state
     *  @param instanceUID SOP instance UID of the presentation state
     *  @param changeStatus if true the pstate and (first) image file is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadPState(String studyUID, String seriesUID, String instanceUID, boolean changeStatus);


    /** loads a presentation state (which need not be contained in the database) and
     *  attaches the current image to the presentation state.
     *  This method does not acquire a database lock.
     *  @param pstName path and filename of the presentation state to be loaded
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadPState(String pstName);


    /** loads a presentation state and an image (which need not be contained in the database)
     *  and attaches the image to the presentation state (if specified, otherwise the current
     *  image will be used).
     *  This method does not acquire a database lock.
     *  @param pstName path and filename of the presentation state to be loaded
     *  @param imgName path and filename of the image to be loaded
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadPState(String pstName, String imgName);


    /** loads a structured report which is contained in the database.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID study instance UID of the structured report
     *  @param seriesUID series instance UID of the structured report
     *  @param instanceUID SOP instance UID of the structured report
     *  @param changeStatus if true the structured report file is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadStructuredReport(String studyUID, String seriesUID, String instanceUID, boolean changeStatus);


    /** loads a structured report (which need not be contained in the database).
     *  This method does not acquire a database lock.
     *  @param filename path and filename of the structured report to be loaded
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadStructuredReport(String filename);


    /** loads a structured reporting "template".
     *  This "template" is just a DICOM Structured Reporting file which resides in a
     *  special sub-folder and is referenced by the configuration file.  This meachanism
     *  should facilitate the creation of new reports since one does not have to start
     *  with a completely empty report.
     *  Please note that the current structured report is replaced by the specified
     *  "template".  New study/series/instance UIDs are generated automatically for the
     *  new report.
     *  This method does not acquire a database lock.
     *  @param reportID report identifier, as returned by getReportID().
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadSRTemplate(String reportID);


    /** saves the current presentation state in the same directory
     *  in which the database index file resides. The filename is generated automatically.
     *  A new SOP Instance UID is assigned whenever a presentation state is saved.
     *  After successfully storing the presentation state, the database index is updated
     *  to include the new object.
     *  This method releases under any circumstances the database lock if it exists.
     *  @param replaceSOPInstanceUID flag indicating whether the
     *    SOP Instance UID should be replaced by a new UID.
     *    If true, a new UID is always generated. If false, a new
     *    UID is generated only if no UID has been assigned before.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int savePState(boolean replaceSOPInstanceUID);


    /** saves the current presentation state in a file with the given path and filename.
     *  A new SOP Instance UID is assigned whenever a presentation state is saved.
     *  This method does not acquire a database lock and does not register
     *  the saved presentation state in the database.
     *  @param filename path and filename under which the presentation state is to be saved
     *  @param replaceSOPInstanceUID flag indicating whether the
     *    SOP Instance UID should be replaced by a new UID.
     *    If true, a new UID is always generated. If false, a new
     *    UID is generated only if no UID has been assigned before.
     *  @param explicitVR selects the transfer syntax to be written. True (the default) selects
     *    Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int savePState(String filename, boolean replaceSOPInstanceUID, boolean explicitVR);


    /** saves the DICOM image that is currently attached to the presentation state
     *  in a file with the given path and filename.
     *  This method does not acquire a database lock and does not register
     *  the saved presentation state in the database.
     *  @param filename path and filename under which the image is to be saved
     *  @param explicitVR selects the transfer syntax to be written. True (the default) selects
     *    Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveCurrentImage(String filename, boolean explicitVR);


    /** saves the current structured report in the same directory in which the database index
     *  file resides.  The filename is generated automatically.  A new SOP Instance UID is not
     *  assigned automatically unless the method getStructuredReport().createRevisedVersion()
     *  is called (see documentation in file dcmsr/dsrdoc.h).  This is not required for the
     *  first version of a document (i.e. directly after program start or calling the method
     *  getStructuredReport().createNewDocument()).
     *  After successfully storing the structured report, the database index is updated
     *  to include the new object.
     *  This method releases under any circumstances the database lock if it exists.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveStructuredReport();


    /** saves the current structured report in a file with the given path and filename.
     *  A new SOP Instance UID is not assigned automatically unless the method
     *  getStructuredReport().createRevisedVersion() is called (see documentation in file
     *  dcmsr/dsrdoc.h).  This is not required for the first version of a document (i.e.
     *  directly after program start or calling the method getStructuredReport().createNewDocument()).
     *  This method does not acquire a database lock and does not register the saved structured
     *  report in the database.
     *  @param filename path and filename under which the structured report is to be saved
     *  @param explicitVR selects the transfer syntax to be written. True (the default) selects
     *    Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveStructuredReport(String filename, boolean explicitVR);


    /** adds an image which is contained in the database
     *  to the list of referenced images of the current presentation state.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID study instance UID of the image
     *  @param seriesUID series instance UID of the image
     *  @param instanceUID SOP instance UID of the image
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int addImageReferenceToPState(String studyUID, String seriesUID, String instanceUID);


    /** gets the number of image references contained in the current presentation state.
     *  @return number of image references, 0 if an error occurred.
     */
    public native int getNumberOfImageReferences();


    /** returns a reference to the current presentation state.
     *  This reference will become invalid when the DVInterface object is deleted,
     *  a different image or presentation state is loaded
     *  (using loadPState or loadImage) or when resetPresentationState() is called.
     *  @return reference to the current presentation state
     */
    public jDVPresentationState getCurrentPState()
    {
        return new jDVPresentationState (getCurrentPStateN());
    }

    private native long getCurrentPStateN();


    /** returns a reference to the current structured report.
     *  This reference will become invalid when the DVInterface object is deleted or
     *  a different structured report is loaded (using loadStructuredReport).
     *  @return reference to the current structured report
     */
    public jDSRDocument getCurrentReport()
    {
        return new jDSRDocument (getCurrentReportN());
    }

    private native long getCurrentReportN();


    /** returns a reference to the print handler.
     *  This reference remains valid as long as the DVInterface object exists.
     *  @return reference to the current print handler
     */
    public jDVPSStoredPrint getPrintHandler()
    {
        return new jDVPSStoredPrint (getPrintHandlerN());
    }

    private native long getPrintHandlerN();


    /** resets the presentation state object to the status it had immediately after the
     *  last successful operation of "loadImage" or "loadPState". A state can also explicitly
     *  specified as such a "reset state" by using the method saveCurrentPStateForReset().
     *  Attention: The last reference returned by getCurrentPState() becomes invalid
     *  when this method is called.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int resetPresentationState();


    /** saves the current state of the presentation state object to be used for
     *  resetPresentationState(). This is e.g. useful after registration of additional images
     *  directly after a new images has been loaded.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveCurrentPStateForReset();


    /** removes any shared or exclusive lock on the database.
     *  This method should be called when a database transaction
     *  (i.e. reading all studies, series, instances etc.) is finished.
     *  As long as a lock exists on the database, no other application
     *  (i.e. the network receiver) can add new images to the database.
     *  This method also clears the index cache.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int releaseDatabase();


    /** searches in the database for a DICOM instance with the given
     *  study, series and instance UIDs and returns its pathname if found.
     *  If the given instance is not found in the database, NULL is returned.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID the DICOM study instance UID
     *  @param seriesUID the DICOM series instance UID
     *  @param instanceUID the DICOM SOP instance UID
     *  @return filename (path) if found, empty string otherwise
     */
    public native String getFilename(String studyUID, String seriesUID, String instanceUID);


    /** returns the number of studies currently contained in the database.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The number reported (returned) by this method remains valid as long
     *  as the database lock remains active and no function modifying the database is called.
     *  Functions that modify the database are: Storing new presentation states,
     *  deleting data, modifying the 'reviewed' status flag of IODs.
     *  @return number of studies in the database.
     */
    public native int getNumberOfStudies();


    /** selects the study with the given index in the database.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  Implicitly the first series and first instance within this study is selected, too.
     *  @param idx index to be selected, must be < getNumberOfStudies()
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectStudy(int idx);


    /** selects the study with the given UID in the database.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  Implicitly the first series and first instance within this study is selected, too.
     *  @param studyUID the DICOM study instance UID
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectStudy(String studyUID);


    /** returns the review status of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  Implicitly the first instance within this series is selected, too.
     *  @return study review status
     */
    public native int getStudyStatus();


    /** returns the Study Instance UID of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Study Instance UID or empty string if absent or not selected.
     */
    public native String getStudyUID();


    /** returns the Study Description of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Study Description or empty string if absent or not selected.
     */
    public native String getStudyDescription();


    /** returns the Study Date of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Study Date or empty string if absent or not selected.
     */
    public native String getStudyDate ();


    /** returns the Study Time of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Study Time or empty string if absent or not selected.
     */
    public native String getStudyTime ();


    /** returns the Referring Physicians Name of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Referring Physicians Name or empty string if absent or not selected.
     */
    public native String getReferringPhysiciansName ();


    /** returns the Accession Number of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Accession Number or empty string if absent or not selected.
     */
    public native String getAccessionNumber ();


    /** returns the Name Of Physicians Reading Study of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Name Of Physicians Reading Study or empty string if absent or not selected.
     */
    public native String getNameOfPhysiciansReadingStudy ();


    /** returns the Patient Name of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient Name or empty string if absent or not selected.
     */
    public native String getPatientName();


    /** returns the Patient ID of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient ID or empty string if absent or not selected.
     */
    public native String getPatientID();


    /** returns the Patient Birth Date of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient Birth Date or empty string if absent or not selected.
     */
    public native String getPatientBirthDate();


    /** returns the Patient Sex of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient Sex or empty string if absent or not selected.
     */
    public native String getPatientSex();


    /** returns the Patient Birth Time of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient Birth Time or empty string if absent or not selected.
     */
    public native String getPatientBirthTime();


    /** returns the Other Patient Names of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Other Patient Names or empty string if absent or not selected.
     */
    public native String getOtherPatientNames();


    /** returns the Other Patient ID of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Other Patient ID or empty string if absent or not selected.
     */
    public native String getOtherPatientID();


    /** returns the Patient Ethnic Group of the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  Note: Since the database uses the Study Root model, patient data appears
     *  on the study level.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Patient Ethnic Group or empty string if absent or not selected.
     */
    public native String getEthnicGroup();


    /** returns the number of series within the currently selected study.
     *  May be called only if a valid study selection exists - see selectStudy().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  See the comments for getNumberOfStudies() about the validity period
     *  of the returned number.
     *  @return number of series in the current study.
     */
    public native int getNumberOfSeries();


    /** selects the series with the given index within the currently selected study.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param idx index to be selected, must be < getNumberOfSeries()
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectSeries(int idx);


    /** selects the series with the given UID within the currently selected study.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param seriesUID series instance UID of the image
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectSeries(String seriesUID);


    /** returns the Series Instance UID of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Instance UID or empty string if absent or not selected.
     */
    public native String getSeriesUID();


    /** returns the review status of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return series review status
     */
    public native int getSeriesStatus();


    /** returns the type of all instances within the currently selected series.
     *  DICOM series always contain a single modality only, if not the modality of
     *  the first examined instance (which is no image) is used (e.g. PresentationState).
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return instance type
     */
    public native int getSeriesType();


    /** returns the type of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return instance type
     */
    public native int getInstanceType();


    /** returns the Series Number of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Number or empty string if absent or not selected.
     */
    public native String getSeriesNumber ();


    /** returns the Series Date of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Date or empty string if absent or not selected.
     */
    public native String getSeriesDate();


    /** returns the Series Time of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Time or empty string if absent or not selected.
     */
    public native String getSeriesTime ();


    /** returns the Series Description of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Description or empty string if absent or not selected.
     */
    public native String getSeriesDescription ();


    /** returns the Series Performing Physicians Name of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Performing Physicians Name or empty string if absent or not selected.
     */
    public native String getSeriesPerformingPhysiciansName ();


    /** returns the Series Protocol Name of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Protocol Name or empty string if absent or not selected.
     */
    public native String getSeriesProtocolName ();


    /** returns the Series Operators Name of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Series Operators Name or empty string if absent or not selected.
     */
    public native String getSeriesOperatorsName ();


    /** returns the Modality of the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Modality or empty string if absent or not selected.
     */
    public native String getModality();


    /** returns the number of instances (IODs) within the currently selected series.
     *  May be called only if a valid series selection exists - see selectSeries().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  See the comments for getNumberOfStudies() about the validity period
     *  of the returned number.
     *  @return number of instances in the current series.
     */
    public native int getNumberOfInstances();


    /** selects the instance with the given index within the currently selected series.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param idx index to be selected, must be < getNumberOfInstances()
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectInstance(int idx);


    /** selects the instance with the given UID within the currently selected series.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param instanceUID SOP instance UID of the image
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectInstance(String instanceUID);


    /** selects the instance with the given UID and SOP class over all studies and series.
     *  Please note that in worst case all studies, series and instances are examined.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param instanceUID SOP instance UID of the instance
     *  @param sopClassUID SOP class UID of the instance (might be NULL to be not compared)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectInstance(String instanceUID, String sopClassUID);


    /** selects the instance with the given UIDs.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The selection remains valid until the database lock is removed or the database
     *  is modified (see comments for getNumberOfStudies).
     *  @param studyUID study instance UID of the image
     *  @param seriesUID series instance UID of the image
     *  @param instanceUID SOP instance UID of the image
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectInstance(String studyUID, String seriesUID, String instanceUID);


    /** returns the SOP Instance UID of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return SOP Instance UID or empty string if absent or not selected.
     */
    public native String getInstanceUID();


    /** returns the Image Number of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Image Number or empty string if absent or not selected.
     */
    public native String getImageNumber();


    /** returns the Filename of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Filename or empty string if absent or not selected.
     */
    public native String getFilename();


    /** returns the review status of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return instance review status
     */
    public native int getInstanceStatus();


    /** returns the Instance Description of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Instance Description or empty string if absent or not selected.
     */
    public native String getInstanceDescription();


    /** returns the Presentation Label of the currently selected instance.
     *  May be called only if a valid instance selection exists - see selectInstance().
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @return Presentation Label or empty string if absent or not selected.
     */
    public native String getPresentationLabel();


    /* methods modifying the database */

    /** modifies the review flag for one instance in the database, which is set to
     *  'reviewed' state (DVIF_objectIsNotNew). The status of the corresponding series
     *  and study is updated automatically.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The database is modified - any study, series or instance selection
     *  and the number of studies, series and instances reported will become invalid since
     *  other processes may modify the database before the exclusive lock is granted to this method.
     *  @param studyUID study instance UID of the reviewed instance
     *  @param seriesUID series instance UID of the reviewed instance
     *  @param instanceUID SOP instance UID of the reviewed instance
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int instanceReviewed(String studyUID, String seriesUID, String instanceUID);


    /** deletes the given instance from the database. If the corresponding DICOM file
     *  resides in the same directory as the index file, it is also removed.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The database is modified - any study, series or instance selection
     *  and the number of studies, series and instances reported will become invalid since
     *  other processes may modify the database before the exclusive lock is granted to this method.
     *  @param studyUID study instance UID of the instance to be deleted
     *  @param seriesUID series instance UID of the instance to be deleted
     *  @param instanceUID SOP instance UID of the instance to be deleted
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int deleteInstance(String studyUID, String seriesUID, String instanceUID);


    /** deletes the given series from the database. Any of the corresponding DICOM files
     *  residing in the same directory as the index file are also removed.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The database is modified - any study, series or instance selection
     *  and the number of studies, series and instances reported will become invalid since
     *  other processes may modify the database before the exclusive lock is granted to this method.
     *  @param studyUID study instance UID of the series to be deleted
     *  @param seriesUID series instance UID of the series to be deleted
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int deleteSeries(String studyUID, String seriesUID);


    /** deletes the given study from the database. Any of the corresponding DICOM files
     *  residing in the same directory as the index file are also removed.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  The database is modified - any study, series or instance selection
     *  and the number of studies, series and instances reported will become invalid since
     *  other processes may modify the database before the exclusive lock is granted to this method.
     *  @param studyUID study instance UID of the study to be deleted
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int deleteStudy(String studyUID);


    /* here follow the Network interface methods */

    /** starts the network receiver process (Storage Service Class SCP).
     *  The receiver process will wait for incoming DICOM associations, receive images
     *  and presentation states. Data will be stored in file and registered in the
     *  database index file. Attention: Successful return of this method is no guarantee
     *  that the network receiver has successfully started, because certain errors
     *  (i.e. incorrect settings in the config file) will only be noted in the network
     *  receiver process when running. On Unix platform, successful return of this method
     *  means that the fork() used to start the receiver was successful.
     *  On Win32 platforms, it means that the CreateProcess() call was successful.
     *  @return EC_Normal if the receiver process could be started, an error code otherwise.
     */
    public native int startReceiver();


    /** terminates the network receiver process (Storage Service Class SCP).
     *  This method attempts to terminate the network receiver process by
     *  requesting a DICOM association with it and delivering a special "shutdown" command.
     *  If for some reason the network receiver cannot be found (i.e. because it has
     *  terminated abnormally), a TCP/IP timeout (several seconds) may occur before this method returns.
     *  @return EC_Normal if the receiver process could be terminated, an error code otherwise.
     */
    public native int terminateReceiver();


    /** starts the query/retrieve server process (Query/Retrieve Service Class SCP).
     *  The query/retrieve process will wait for incoming DICOM associations, serve queries and
     *  send the requested instances to the specified destination.  Data will be taken from the
     *  same local database used to store received instances and created presentation states,
     *  stored print objects and hardcopy grayscale images.
     *  The configuration file which is required for the query/retrieve process will be created
     *  automatically from the 'global' configuration file each time this method is called, unless
     *  this automatical creation is disabled (entry: [QUERYRETIUEVE] AutoCreateConfigFile = false).
     *  Attention: Successful return of this method is no guarantee that the query/retrieve server
     *  has successfully started, because certain errors (i.e. incorrect settings in the config file)
     *  will only be noted in the query/retrieve process when running. On Unix platform, successful
     *  return of this method means that the fork() used to start the receiver was successful.
     *  On Win32 platforms, it means that the CreateProcess() call was successful.
     *  @return EC_Normal if the query/retrieve process could be started, an error code otherwise.
     */
    public native int startQueryRetrieveServer();

    /** terminates the query/retrieve server process (Query/Retrieve Service Class SCP).
     *  This method attempts to terminate the query/retrieve process by requesting a
     *  DICOM association with it and delivering a special "shutdown" command.
     *  If for some reason the query/retrieve server cannot be found (i.e. because it has
     *  terminated abnormally), a TCP/IP timeout (several seconds) may occur before this
     *  method returns.
     *  @return EC_Normal if the query/retrieve process could be terminated,
     *     an error code otherwise.
     */
    public native int terminateQueryRetrieveServer();


    /** tests whether the database has been modified in any way since the last
     *  call to this method. Any write access to the database (adding, deleting, changing)
     *  is reported. This method works by modifying and checking the "modification date/time"
     *  of the database index file. This method is not affected by database locks and can be
     *  called at any time.
     *  Always returns OFTrue for the first call after construction of the interface.
     *  Also returns OFTrue if something goes wrong (i.e. if the method cannot make sure
     *  that nothing has changed).
     *  @return true if the database has been modified since the last call to this method.
     */
    public native boolean newInstancesReceived();


    /** sends a complete study over network to a different DICOM peer.
     *  A separate application or process is launched to handle the send operation.
     *  This call returns when the send operation has successfully been launched.
     *  No information about the status or success of the transfer itself is being made
     *  available.
     *  @param targetID symbolic identifier of the send target, must be one of the
     *     strings returned by getTargetID().
     *  @param studyUID Study Instance UID of the study to be sent. Must be a study
     *     contained in the database.
     *  @return EC_Normal when the send process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int sendStudy(String targetID, String studyUID);


    /** sends a complete series over network to a different DICOM peer.
     *  A separate application or process is launched to handle the send operation.
     *  This call returns when the send operation has successfully been launched.
     *  No information about the status or success of the transfer itself is being made
     *  available.
     *  @param targetID symbolic identifier of the send target, must be one of the
     *     strings returned by getTargetID().
     *  @param studyUID Study Instance UID of the series to be sent. Must be a series
     *     contained in the database.
     *  @param seriesUID Series Instance UID of the series to be sent. Must be a series
     *     contained in the database.
     *  @return EC_Normal when the send process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int sendSeries(String targetID, String studyUID, String seriesUID);


    /** sends a single instance (image or presentation state)
     *  over network to a different DICOM peer.
     *  A separate application or process is launched to handle the send operation.
     *  This call returns when the send operation has successfully been launched.
     *  No information about the status or success of the transfer itself is being made
     *  available.
     *  @param targetID symbolic identifier of the send target, must be one of the
     *     strings returned by getTargetID().
     *  @param studyUID Study Instance UID of the IOD to be sent. Must be an IOD
     *     contained in the database.
     *  @param seriesUID Series Instance UID of the IOD to be sent. Must be an IOD
     *     contained in the database.
     *  @param instanceUID SOP Instance UID of the IOD to be sent. Must be an IOD
     *     contained in the database.
     *  @return EC_Normal when the send process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int sendIOD(String targetID, String studyUID, String seriesUID, String instanceUID);


    /** creates a dump of the contents of a DICOM file and displays it on-screen.
     *  A separate application or process is launched to handle the dump and display.
     *  This call returns when the dump operation has successfully been launched.
     *  No information about the status or success of the process itself is being made
     *  available.
     *  This method does not acquire a database lock.
     *  @param filename path of file to be displayed.
     *  @return EC_Normal when the process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int dumpIOD(String filename);


    /** creates a dump of the contents of a DICOM file and displays it on-screen.
     *  A separate application or process is launched to handle the dump and display.
     *  This call returns when the dump operation has successfully been launched.
     *  No information about the status or success of the process itself is being made
     *  available.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID Study Instance UID of the IOD to be dumped. Must be an IOD
     *     contained in the database.
     *  @param seriesUID Series Instance UID of the IOD to be dumped. Must be an IOD
     *     contained in the database.
     *  @param instanceUID SOP Instance UID of the IOD to be dumped. Must be an IOD
     *     contained in the database.
     *  @return EC_Normal when the process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int dumpIOD(String studyUID, String seriesUID, String instanceUID);


    /** checks the contents of a DICOM file and displays an evaluation report on the screen.
     *  A separate application or process is launched to handle the evaluation and display.
     *  This call returns when the check operation has successfully been launched.
     *  No information about the status or success of the process itself is being made
     *  available.
     *  This method does not acquire a database lock.
     *  @param filename path of file to be checked.
     *  @return EC_Normal when the process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int checkIOD(String filename);


    /** checks the contents of a DICOM file and displays an evaluation report on the screen.
     *  A separate application or process is launched to handle the evaluation and display.
     *  This call returns when the check operation has successfully been launched.
     *  No information about the status or success of the process itself is being made
     *  available.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID Study Instance UID of the IOD to be checked. Must be an IOD
     *     contained in the database.
     *  @param seriesUID Series Instance UID of the IOD to be checked. Must be an IOD
     *     contained in the database.
     *  @param instanceUID SOP Instance UID of the IOD to be checked. Must be an IOD
     *     contained in the database.
     *  @return EC_Normal when the process has successfully been launched,
     *     an error condition otherwise.
     */
    public native int checkIOD(String studyUID, String seriesUID, String instanceUID);


    /** saves a monochrome bitmap as a DICOM Secondary Capture image.
     *  The bitmap must use one byte per pixel, left to right, top to bottom
     *  order of the pixels. 0 is interpreted as black, 255 as white.
     *  @param filename the file name or path under which the image is saved.
     *  @param pixelData a pointer to the image data. Must contain at least
     *    width*height bytes of data.
     *  @param width the width of the image, must be <= 0xFFFF
     *  @param height the height of the image, must be <= 0xFFFF
     *  @aspectRatio the pixel aspect ratio as width/height. If omitted, a pixel
     *    aspect ratio of 1/1 is assumed.
     *  @return EC_Normal upon success, an error code otherwise.
     */
	public native int saveDICOMImage(String filename, byte[] pixelData, long width,
	        long height, double aspectRatio);


    /** saves a monochrome bitmap as a DICOM Secondary Capture image.
     *  The bitmap must use one byte per pixel, left to right, top to bottom
     *  order of the pixels. 0 is interpreted as black, 255 as white.
     *  @param filename the file name or path under which the image is saved.
     *  @param pixelData a pointer to the image data. Must contain at least
     *    width*height bytes of data.
     *  @param width the width of the image, must be <= 0xFFFF
     *  @param height the height of the image, must be <= 0xFFFF
     *  @aspectRatio the pixel aspect ratio as width/height. If omitted, a pixel
     *    aspect ratio of 1/1 is assumed.
     *  @param explicitVR selects the transfer syntax to be written.
     *    True selects Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @param instanceUID parameter containing the SOP Instance UID to be written.
     *    This parameter should be omitted unless the SOP Instance UID needs to be controlled
     *    externally.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveDICOMImage(String filename, byte[] pixelData, long width,
            long height, double aspectRatio, boolean explicitVR, String instanceUID);


    /** saves a monochrome bitmap as a DICOM Secondary Capture image
     *  in the same directory in which the database index file resides.
     *  The filename is generated automatically.
     *  When the image is stored successfully, the database index is updated
     *  to include the new object.
     *  This method releases under any circumstances the database lock if it exists.
     *  @param pixelData a pointer to the image data. Must contain at least
     *    width*height bytes of data.
     *  @param width the width of the image, must be <= 0xFFFF
     *  @param height the height of the image, must be <= 0xFFFF
     *  @aspectRatio the pixel aspect ratio as width/height. If omitted, a pixel
     *    aspect ratio of 1/1 is assumed.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveDICOMImage(byte[] pixelData, long width, long height,
            double aspectRatio);


    /** saves a monochrome bitmap as a DICOM Hardcopy Grayscale image.
     *  The bitmap must use 16 bits per pixel, left to right, top to bottom
     *  order of the pixels. It is assumed that only values 0..4095 are used.
     *  @param filename the file name or path under which the image is saved.
     *  @param pixelData a pointer to the image data. Must contain at least
     *    width*height*2 bytes of data.
     *  @param width the width of the image, must be <= 0xFFFF
     *  @param height the height of the image, must be <= 0xFFFF
     *  @aspectRatio the pixel aspect ratio as width/height. If omitted, a pixel
     *    aspect ratio of 1/1 is assumed.
     *  @param explicitVR selects the transfer syntax to be written.
     *    True selects Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @param instanceUID parameter containing the SOP Instance UID to be written.
     *    This parameter should be omitted unless the SOP Instance UID needs to be controlled
     *    externally.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveGrayscaleHardcopyImage(String filename, short[] pixelData,
            long width, long height, double aspectRatio, boolean explicitVR, String instanceUID);


    /** saves a monochrome bitmap as a DICOM Hardcopy Grayscale image
     *  in the same directory in which the database index file resides.
     *  The filename is generated automatically.
     *  When the image is stored successfully, the database index is updated
     *  to include the new object.
     *  This method releases under any circumstances the database lock if it exists.
     *  @param pixelData a pointer to the image data. Must contain at least
     *    width*height*2 bytes of data.
     *  @param width the width of the image, must be <= 0xFFFF
     *  @param height the height of the image, must be <= 0xFFFF
     *  @aspectRatio the pixel aspect ratio as width/height. If omitted, a pixel
     *    aspect ratio of 1/1 is assumed.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveGrayscaleHardcopyImage(short[] pixelData, long width, long height,
            double aspectRatio);


    /** loads a Stored Print object which is contained in the database into memory.
     *  Attention: The current print job (Stored Print object) will be deleted by doing this.
     *  This method acquires a database lock which must be explicitly freed by the user.
     *  @param studyUID study instance UID of the Stored Print object
     *  @param seriesUID series instance UID of the Stored Print object
     *  @param instanceUID SOP instance UID of the Stored Print object
     *  @param changeStatus if true the stored print object is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadStoredPrint(String studyUID, String seriesUID, String instanceUID, boolean changeStatus);


    /** loads a Stored Print object (which need not be contained in the database) into memory.
     *  Attention: The current print job (Stored Print object) will be deleted by doing this.
     *  This method does not acquire a database lock.
     *  @param filename path and filename of the Stored Print object to be loaded
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int loadStoredPrint(String filename);


    /** saves the current print job as a Stored Print object.
     *  @param filename the file name or path under which the image is saved.
     *  @param writeRequestedImageSize if false, the Requested Image Size attributes are not written,
     *    e. g. because they are not supported by the target printer.
     *  @param explicitVR selects the transfer syntax to be written.
     *    True selects Explicit VR Little Endian, False selects Implicit VR Little Endian.
     *  @param instanceUID parameter containing the SOP Instance UID to be written.
     *    This parameter should be omitted unless the SOP Instance UID needs to be controlled
     *    externally.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveStoredPrint(String filename, boolean writeRequestedImageSize, boolean explicitVR,
            String instanceUID);


    /** saves the current print job as a Stored Print object
     *  in the same directory in which the database index file resides.
     *  The filename is generated automatically.
     *  When the image is stored successfully, the database index is updated
     *  to include the new object.
     *  This method releases under any circumstances the database lock if it exists.
     *  @param writeRequestedImageSize if false, the Requested Image Size attributes are not written,
     *    e. g. because they are not supported by the target printer.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int saveStoredPrint(boolean writeRequestedImageSize);


    /** gets the number of Hardcopy Grayscaleimages currently registered by the stored print object.
     *  @return number of images.
     */
    public native int getNumberOfPrintPreviews();


    /** loads a Hardcopy Grayscale image registered by the stored print object and creates a preview.
     *  The preview bitmap is implicitly scaled to fit into the rectangle specified by
     *  setMaxPrintPreviewWidthHeight().
     *  @param idx index of the image, must be < getNumberOfPrintPreviews()
     *  @param printLUT OFTrue if presentation LUT should be interpreted as a print presentation LUT
     *    (default, in this case there is no implicit scaling of the input width of the LUT and,
     *    therefore, the VOI transformation - which is absent for print - is used),
     *    OFFalse otherwise (softcopy interpretation of a presentation LUT)
     *  @param changeStatus if true the hardcopy grayscale image file is marked 'reviewed' (not new)
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int loadPrintPreview(int idx, boolean printLUT, boolean changeStatus);


    /** removes a currently loaded Hardcopy Grayscale image from memory.
     */
    public native void unloadPrintPreview();


    /** gets number of bytes used for the print preview bitmap.
     *  (depends on width, height and depth)
     *  @return number of bytes used for the preview bitmap
     */
    public native long getPrintPreviewSize();


    /** sets the maximum print preview bitmap width and height.
     *  Larger images are scaled down (according to the pixel aspect ratio) to fit into
     *  the specified rectangle.
     *  Attention: If the values differ from the the previous ones the currently loaded
     *  hardcopy grayscale image (preview) is automatically detroyed and has to be re-loaded.
     *  @param width maximum width of preview bitmap (in pixels)
     *  @param height maximum height of preview bitmap (in pixels)
     */
    public native void setMaxPrintPreviewWidthHeight(int width, int height);


    /** gets width and height of print preview bitmap.
     *  The return values depend on the current maximum preview bitmap width/height values!
     *  The object of class jIntByRef must be created before passing to this method!
     *  @param width upon success, the bitmap width (in pixels) is returned in this parameter
     *  @param height upon success, the bitmap height (in pixels) is returned in this parameter
     *  @return EC_Normal upon success, an error code otherwise
     */
    public native int getPrintPreviewWidthHeight(jIntByRef width, jIntByRef height);


    /** writes the bitmap data of the print preview image into the given buffer.
     *  The storage area must be allocated and deleted from the calling method.
     *  @param bitmap pointer to storage area where the pixel data is copied to (array of 8 bit values)
     *  @param size specifies size of the storage area in bytes
     *  @return EC_Normal upon success, an error code otherwise
     */
    public native int getPrintPreviewBitmap(byte[] bitmap, long size);


    /** stores the current presentation state in a temporary place
     *  and creates a new presentation state that corresponds with an
     *  image displayed "without" presentation state.
     *  If called twice, an error code is returned.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int disablePState();


    /** restores the stored presentation state (see disablePresentationState)
     *  and deletes the temporary presentation state.
     *  If no stored presentation state exists, returns an error.
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int enablePState();


    /** returns number of presentation states referencing the currently selected image.
     *  If no instance is currently selected or the selected instance is a presentation
     *  state, returns an error.
     *  @return number of presentation states, 0 if none available or an error occurred
     */
    public native int getNumberOfPStates();


    /** selects and loads specified presentation state referencing the currently selected
     *  image.
     *  @param idx index to be selected, must be < getNumberOfPStates()
     *  @param changeStatus if true the presentation state is marked 'reviewed' (not new)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int selectPState(int idx, boolean changeStatus);


    /** returns description of specified presentation state referencing the currently
     *  selected image.
     *  @param idx index to be selected, must be < getNumberOfPStates()
     *  @return presentation state description or empty string if idx is invalid
     */
    public native String getPStateDescription(int idx);


    /** returns label of specified presentation state referencing the currently
     *  selected image.
     *  @param idx index to be selected, must be < getNumberOfPStates()
     *  @return presentation state label or empty string if idx is invalid
     */
    public native String getPStateLabel(int idx);


    /** checks whether display correction is possible (in principle),
     *  i.e. a valid monitor characteristics description exists
     *  and current system is a low-cost system (without built-in
     *  calibration).
     *  @param transform display transform to be checked (default: GSDF)
     *  @return OFTrue if display transform is possible, OFFalse otherwise
     */
    public native boolean isDisplayTransformPossible(int transform);


    /** sets ambient light value for the display transformation.
     *  @param value ambient light value to be set
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int setAmbientLightValue(double value);


    /** returns ambient light value for the display transformation.
     *  @param value returned ambient light value. The object of class
     *    jDoubleByRef must be created before passing to this method!
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int getAmbientLightValue(jDoubleByRef value);


    /* print related methods */

    /** selects the current printer. Also adjusts the destination AE title and the
     *  printer name attribute within the Stored Print object.
     *  @param targetID one of the printer target IDs returned by getTargetID().
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setCurrentPrinter(String targetID);


    /** gets the current printer's target ID.
     *  @return printer target ID, can be empty string if no printer is defined
     *   in the configuration file.
     */
    public native String getCurrentPrinter();


    /** sets the (optional) print medium type.
     *  @param value new attribute value, may be empty string.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterMediumType(String value);


    /** gets the (optional) print medium type.
     *  @return medium type, may be empty string.
     */
    public native String getPrinterMediumType();


    /** sets the (optional) printer film destination.
     *  @param value new attribute value, may be empty string.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterFilmDestination(String value);


    /** gets the (optional) printer film destination.
     *  @return printer film destination, may be NULL or empty string.
     */
    public native String getPrinterFilmDestination();


    /** sets the (optional) printer film session label.
     *  @param value new attribute value, may be empty string.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterFilmSessionLabel(String value);


    /** gets the (optional) printer film session label.
     *  @return printer film session label, may be NULL or empty string.
     */
    public native String getPrinterFilmSessionLabel();


    /** sets the (optional) print priority.
     *  @param value new attribute value, may be NULL.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterPriority(String value);


    /** gets the (optional) print priority.
     *  @return print priority, may be NULL or empty string.
     */
    public native String getPrinterPriority();


    /** sets the (optional) print session owner ID.
     *  @param value new attribute value, may be empty string.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterOwnerID(String value);


    /** gets the (optional) print session owner ID.
     *  @return print session owner ID, may be empty string.
     */
    public native String getPrinterOwnerID();


    /** sets the (optional) print number of copies.
     *  @param value new attribute value, may be 0.
     *    The caller is responsible for making sure
     *    that the value is valid for the selected printer.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setPrinterNumberOfCopies(int value);


    /** gets the (optional) print number of copies.
     *  @return print number of copies, 0 if absent.
     */
    public native int getPrinterNumberOfCopies();


    /** resets the settings for basic film session (everything that
     *  is not managed by the Stored Print object) to initial state.
     *  Affects medium type, film destination, film session label,
     *  priority, owner ID, and number of copies.
     */
    public native void clearFilmSessionSettings();


    /** sets the LUT with the given identifier
     *  in the Presentation State as current Presentation LUT.
     *  @param lutID LUT identifier, as returned by getLUTID().
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int selectDisplayPresentationLUT(String lutID);


    /** if the Presentation State contains an active
     *  Presentation LUT that was set with selectDisplayPresentationLUT(),
     *  return the corresponding LUT identifier.
     *  @return lutID if found, empty string otherwise.
     */
    public native String getDisplayPresentationLUTID();


    /** sets the LUT with the given identifier in the Stored Print object
     *  as current Presentation LUT. This LUT overrides the settings made
     *  for the separate image boxes, it can be deactivated using the method
     *  DVPSStoredPrint::setDefaultPresentationLUT().
     *  @param lutID LUT identifier, as returned by getLUTID().
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int selectPrintPresentationLUT(String lutID);


    /** if the Stored Print object contains an active
     *  Presentation LUT that was set with selectPrintPresentationLUT(),
     *  return the corresponding LUT identifier.
     *  @return lutID if found, NULL or empty string otherwise.
     */
    public native String getPrintPresentationLUTID();


    /** start spooling of print job with current settings.
     *  @param deletePrintedImages if true, delete printed images from queue.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int spoolPrintJob(boolean deletePrintedImages);


    /** starts the print spooler process.
     *  The print spooler will wait for print jobs created with spoolPrintJob()
     *  and communicate them to the printer using the DICOM Print Management Service Class.
     *  Attention: Successful return of this method is no guarantee
     *  that the spooler has successfully started, because certain errors
     *  (i.e. incorrect settings in the config file) will only be noted in the spooler
     *  process when running. On Unix platform, successful return of this method
     *  means that the fork() used to start the spooler was successful.
     *  On Win32 platforms, it means that the CreateProcess() call was successful.
     *  @return EC_Normal if the spooler process could be started, an error code otherwise.
     */
    public native int startPrintSpooler();


    /** terminates the print spooler process. This method creates a "dummy"
     *  print job that request the print spooler to shutdown as soon as all other pending
     *  print jobs are finished.
     *  @return EC_Normal if the spooler process dummy print job could be written,
     *    an error code otherwise.
     */
    public native int terminatePrintSpooler();


    /** starts the print server process (Basic Grayscale Print Management SCP).
     *  The print server process will wait for incoming DICOM associations, handle the
     *  DICOM print protcol, store data in file and register stored print and grayscale
     *  image objects in the database index file.
     *  Attention: Successful return of this method is no guarantee that the print
     *  server has successfully started, because certain errors (i.e. incorrect settings
     *  in the config file) will only be noted in the print server process when running.
     *  On Unix platform, successful return of this method means that the fork() used to
     *  start the server was successful.
     *  On Win32 platforms, it means that the CreateProcess() call was successful.
     *  @return EC_Normal if the server process could be started, an error code otherwise.
     */
    public native int startPrintServer();


    /** terminates the print server process (Basic Grayscale Print Management SCP).
     *  This method attempts to terminate the print server process by requesting a DICOM
     *  association with it and delivering a special "shutdown" command.
     *  If for some reason the print server cannot be found (i.e. because it has terminated
     *  abnormally), a TCP/IP timeout (several seconds) may occur before this method returns.
     *  @return EC_Normal if the server process could be terminated, an error code otherwise.
     */
    public native int terminatePrintServer();


    /** adds an existing DICOM image (should be Hardcopy Grayscale)
     *  that is already present in the image database to the current print image queue
     *  without rendering it again.
     *  The "requested image size" option is not used - the bitmap is treated as if the
     *  presentation mode was "SCALE TO FIT".
     *  @param studyUID study instance UID of the image, as reported by getStudyUID()
     *  @param seriesUID series instance UID of the image, as reported by getSeriesUID()
     *  @param instanceUID SOP instance UID of the image, as reported by getInstanceUID()
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int addToPrintHardcopyFromDB(String studyUID, String seriesUID, String instanceUID);


    /** requests the spooler process to print an old print job that is stored
     *  in the database as a "stored print" object. The Stored Print that is printed
     *  does not contain all parameters of a print job. The following parameters are taken from the
     *  current settings in this object: Target printer, medium type,
     *  illumination and reflected ambient light.
     *  @param studyUID study instance UID of the Stored Print, as reported by getStudyUID()
     *  @param seriesUID series instance UID of the Stored Print, as reported by getSeriesUID()
     *  @param instanceUID SOP instance UID of the Stored Print, as reported by getInstanceUID()
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int spoolStoredPrintFromDB(String studyUID, String seriesUID, String instanceUID);


    /* annotation interface */

    /** gets the current setting of the annotation activity annotation flag.
     *  @return OFTrue if annotation is on, OFFalse otherwise.
     */
    public native boolean isActiveAnnotation();


    /** gets the current setting of the Prepend Date/Time annotation flag.
     *  @return OFTrue if Prepend Date/Time is on, OFFalse otherwise.
     */
    public native boolean getPrependDateTime();


    /** gets the current setting of the Prepend Printer Name annotation flag.
     *  @return OFTrue if Prepend Printer Name is on, OFFalse otherwise.
     */
    public native boolean getPrependPrinterName();


    /** gets the current setting of the Prepend Lighting annotation flag.
     *  @return OFTrue if Prepend Lighting is on, OFFalse otherwise.
     */
    public native boolean getPrependLighting();


    /** gets the current annotation text.
     *  @return annotation text, may be empty string.
     */
    public native String getAnnotationText();


    /** switches annotation printing on/off
     *  @param value OFTrue if annotation is switched on, OFFalse otherwise.
     */
    public native void setActiveAnnotation(boolean value);


    /** sets the Prepend Date/Time annotation flag.
     *  @param value OFTrue if Prepend Date/Time is switched on, OFFalse otherwise.
     */
    public native void setPrependDateTime(boolean value);


    /** sets the Prepend Printer Name annotation flag.
     *  @param value OFTrue if Prepend Printer Name is switched on, OFFalse otherwise.
     */
    public native void setPrependPrinterName(boolean value);


    /** sets the Prepend Lighting annotation flag.
     *  @param value OFTrue if Prepend Lighting is switched on, OFFalse otherwise.
     */
    public native void setPrependLighting(boolean value);


    /** sets the current annotation text.
     *  @param value new text, may be empty.
     */
    public native void setAnnotationText(String value);


    /* user management interface */

    /** verifies the given password for the given user ID. This method tries
     *  to load and decrypt the private key for the given user with the
     *  given password. If this fails, the password verification fails,
     *  otherwise the password verification succeeds.
     *  This method requires that DCMTK be configured and compiled with
     *  the WITH_OPENSSL flag, otherwise always returns false.
     *  @param userID symbolic user ID for given user, as returned by
     *    DVConfiguration::getUserID()
     *  @param password for user as entered in some GUI control
     *  @return true if password verification succeeds, false otherwise.
     */
    public native boolean verifyUserPassword(String userID, String passwd);


    /** verifies and digitally signs the current structured report.
     *  If the user ID is known (i.e. specified in the configuration file) the current
     *  structured report is verified (a verifying observer is added).  If the 'mode'
     *  parameter is set accordingly and the password is correct (see verifyUserPassword)
     *  the report is also digitally signed.
     *  Please note that after signing the report it should not be modified anymore before
     *  stored in the database or a file. Otherwise the digital signature would be corrupted.
     *  Therefore, the SOP instance UID should be generated before calling this method.
     *  To digitally sign the report it is required that DCMTK is configured and compiled
     *  with the WITH_OPENSSL flag, otherwise only verification is available (returns an
     *  error code if mode differs from DVPSY_verify).
     *  NB: Internally a new structured report object is created when the current report is
     *      successfully signed, i.e. the method getCurrentReport() should be used afterwards
     *      to access this new report.
     *  @param userID symbolic user ID for given user, as returned by
     *    DVConfiguration::getUserID()
     *  @param password for user as entered in some GUI control
     *  @param mode flag specifying whether to verify only, verify and sign or verify and
     *    sign and finalize the document (see jDVPSVerifyAndSignMode). The difference between
     *    the second and the third mode is that "finalize" always signs the entire document
     *    whereas the other mode only signs the marked items (if any, all items otherwise)
     *    and leaves out certain header attributes (e.g. the SOP instance UID and the verifying
     *    observer sequence).
     *  @return status, EC_Normal if successful, an error code otherwise.
     */
    public native int verifyAndSignStructuredReport(String userID, String passwd, int mode);


    /* log file interface */

    /** sets a filter to specify which messages are actually written to the application
     *  wide log file.
     *  There are five different levels (in ascending order): none, informational, warning,
     *  error, debug. All messages which belong to a 'lower' level are included in the
     *  higher levels, i.e. the level debug includes all messages (see jDVPSLogMessageLevel).
     *  @param level status level specifying the filter
     */
    public native void setLogFilter(int level);

    /** writes a message into the application wide log file.
     *  @param level status level of the message (also used to filter the messages),
     *    DVPSM_none should only be used for setLogFilter() and not to write a log message
     *    since it has no meaning for this method (see jDVPSLogMessageLevel).
     *  @param module name of the module which writes the message
     *  @param message (free) text of the log message ('\n' for newline)
     *  @return EC_Normal upon success, an error code otherwise.
     */
    public native int writeLogMessage(int level, String module, String message);


    /* digital signatures */

    /** returns a string containing a complete HTML page with the
     *  signature validation results for the last object of the
     *  given type.
     *  @param objtype object type (see jDVPSObjectType)
     *  @return string with HTML page (might be empty)
     */
    public native String getCurrentSignatureValidationHTML(int objtype);


    /** returns a string containing a complete HTML page with the
     *  signature validation overview.
     *  @return string with HTML page (might be empty)
     */
    public native String getCurrentSignatureValidationOverview();


    /** returns the status flag for the current object of given type.
     *  @param objtype object type (see jDVPSObjectType)
     *  @return digital signature status for object (see jDVPSSignatureStatus)
     */
    public native int getCurrentSignatureStatus(int objtype);


    /** returns the combined status flag for the current image and presentation state.
     *  @return digital signature status for image and presentation state (see jDVPSSignatureStatus)
     */
    public native int getCombinedImagePStateSignatureStatus();


    /** returns number of correct signatures for given object type.
     *  @param objtype object type (see jDVPSObjectType)
     *  @return number of digital signatures
     */
    public native int getNumberOfCorrectSignatures(int objtype);

  
    /** returns number of untrustworthy signatures for given object type.
     *  @param objtype object type (see jDVPSObjectType)
     *  @return number of digital signatures
     */
    public native int getNumberOfUntrustworthySignatures(int objtype);

  
    /** returns number of corrupt signatures for given object type.
     *  @param objtype object type (see jDVPSObjectType)
     *  @return number of digital signatures
     */
    public native int getNumberOfCorruptSignatures(int objtype);

  
    /** disables internal settings for image and presentation state.
     *  Called when a new SR object is loaded and the current
     *  image/presentation state are hidden consequently.
     */
    public native void disableImageAndPState();


    /* --- DVConfiguration --- */

    /* access to communication partner data */

    /** returns the number of communication partners (send targets)
     *  in the configuration file.
     *  @param peerType defines a filter for the peer service type. Default: handle only storage peers.
     *    (from jDVPSPeerType)
     *  @return number of communication partners
     */
    public native int getNumberOfTargets(int peerType);


    /** returns the target identifier of the communication partner
     *  with the given index. The target identifier is unique within the configuration file
     *  @param idx index, must be < getNumberOfTargets()
     *  @param peerType defines a filter for the peer service type. Default: handle only storage peers.
     *    (from jDVPSPeerType)
     *  @return target identifier if found, NULL otherwise.
     */
    public native String getTargetID (int idx, int peerType);


    /** returns the DESCRIPTION entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, NULL otherwise.
     */
    public native String getTargetDescription (String targetID);


    /** returns the HOSTNAME entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, NULL otherwise.
     */
    public native String getTargetHostname(String targetID);


    /** returns the PORT entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present and parsable in the config file, 0 otherwise.
     */
    public native int getTargetPort(String targetID);


    /** returns the TYPE entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present and parsable in the config file, DVPSE_storage otherwise.
     *    (from jDVPSPeerType)
     */
    public native int getTargetType(String targetID);


    /** returns the AETitle entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, empty string otherwise.
     */
    public native String getTargetAETitle(String targetID);


    /** returns the MAXPDU entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present and parsable in the config file, 0 otherwise.
     */
    public native int getTargetMaxPDU(String targetID);


    /** returns the IMPLICITONLY entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetImplicitOnly(String targetID);


    /** returns the DISABLENEWVRS entry for the communication partner with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetDisableNewVRs(String targetID);


    /** returns the BITPRESERVINGMODE entry for the storage peer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetBitPreservingMode(String targetID);


    /** returns the USETLS entry for the storage peer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetUseTLS(String targetID);


    /* NOT USED IN JNI
    public native String getTargetCertificate(String targetID);
    public native String getTargetPrivateKey(String targetID);
    public native String getTargetPrivateKeyPassword(String targetID);
    */


    /** returns the number of distinct values (separated by backslash characters)
     *  in the CIPHERSUITES entry for the storage peer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetNumberOfCipherSuites(String targetID);


    /** returns one value from the CIPHERSUITES entry for the storage peer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @param idx index of the value, must be < getTargetNumberOfCipherSuites(targetID)
     *  @return value if present, NULL otherwise.
     */
    public native String getTargetCipherSuite(String targetID, int idx);


    /** returns the PEERAUTHENTICATION entry for the communication partner with the given
     *  target ID from the configuration file.
     *  See definition of jDVPSCertificateVerificationType for return values.
     *  Current return values are:
     *  0 == DVPSQ_require: verify peer certificate, refuse transmission if absent
     *  1 == DVPSQ_verify : verify peer certificate if present
     *  2 == DVPSQ_ignore : don't verify peer certificate
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID().
     *  @return entry if present and parsable in the config file, DVPSQ_require otherwise.
     */
    public native int getTargetPeerAuthentication(String targetID);


    /* NOT USED IN JNI
    public native String getTargetDiffieHellmanParameters(String targetID);
    public native String getTargetRandomSeed(String targetID);
    */

    /** returns the SUPPORTSPRESENTATIONLUT entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetPrinterSupportsPresentationLUT(String targetID);

    /* NOT USED IN JNI
    OFBool getTargetPrinterPresentationLUTMatchRequired(const char *targetID);
    OFBool getTargetPrinterPresentationLUTPreferSCPRendering(const char *targetID);
    OFBool getTargetPrinterPresentationLUTinFilmSession(const char *targetID);
    */

    /** returns the SUPPORTS12BIT entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return entry if present in the config file, OFTrue otherwise
     *   (default is 12 bit supported).
     */
    public native boolean getTargetPrinterSupports12BitTransmission(String targetID);


    /** returns the SUPPORTSIMAGESIZE entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetPrinterSupportsRequestedImageSize(String targetID);


    /** returns the SUPPORTSDECIMATECROP entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetPrinterSupportsDecimateCrop(String targetID);


    /** returns the SUPPORTSTRIM entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return entry if present in the config file, OFFalse otherwise.
     */
    public native boolean getTargetPrinterSupportsTrim(String targetID);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the BORDERDENSITY entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfBorderDensities(String targetID);


    /** returns one value from the BORDERDENSITY entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfBorderDensities(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, NULL otherwise.
     */
    public native String getTargetPrinterBorderDensity(String targetID, int index);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the MAXDENSITY entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfMaxDensities(String targetID);


    /** returns one value from the MAXDENSITY entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfMaxDensities(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, NULL otherwise.
     */
    public native String getTargetPrinterMaxDensity (String targetID, int idx);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the MINDENSITY entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfMinDensities(String targetID);


    /** returns one value from the MINDENSITY entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfMinDensities(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, NULL otherwise.
     */
    public native String getTargetPrinterMinDensity(String targetID, int idx);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the DISPLAYFORMAT entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfPortraitDisplayFormats(String targetID);


    /** returns one row value from the DISPLAYFORMAT entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfPortraitDisplayFormats(targetID)
     *  @return number of rows for this display format if present, 0 otherwise
     */
    public native int getTargetPrinterPortraitDisplayFormatRows(String targetID, int idx);


    /** returns one columns value from the DISPLAYFORMAT entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfPortraitDisplayFormats(targetID)
     *  @return number of columns for this display format if present, 0 otherwise
     */
    public native int getTargetPrinterPortraitDisplayFormatColumns(String targetID, int idx);

    /** returns OFTrue if an ANNOTATION entry for the printer with the given
     *  target ID from the configuration file exists or if the
     *  SESSIONLABELANNOTATION flag is true for the printer.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return OFTrue if printer supports annotation, OFFalse otherwise.
     */
    public native boolean getTargetPrinterSupportsAnnotation(String targetID);

    /* NOT USED IN JNI
    OFBool getTargetPrinterSupportsAnnotationBoxSOPClass(const char *targetID);
    OFBool getTargetPrinterSessionLabelAnnotation(const char *targetID);
    const char *getTargetPrinterAnnotationDisplayFormatID(const char *targetID, OFString& value);
    Uint16 getTargetPrinterAnnotationPosition(const char *targetID);
    */

    /** returns the number of distinct values (separated by backslash characters)
     *  in the FILMSIZEID entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfFilmSizeIDs(String targetID);


    /** returns one value from the FILMSIZEID entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfFilmSizeIDs(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterFilmSizeID(String targetID, int idx);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the MEDIUMTYPE entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfMediumTypes(String targetID);


    /** returns one value from the MEDIUMTYPE entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfMediumTypes(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterMediumType(String targetID, int idx);


    /* NOT USED IN JNI
    Uint32 getTargetPrinterNumberOfFilmDestinations(const char *targetID);
    const char *getTargetPrinterFilmDestination(const char *targetID, Uint32 idx, OFString& value);
    */


    /** returns the number of distinct values (separated by backslash characters)
     *  in the RESOLUTIONID entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfPrinterResolutionIDs(String targetID);


    /** returns one value from the RESOLUTIONID entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfPrinterResolutionIDs(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterResolutionID(String targetID, int idx);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the MAGNIFICATIONTYPE entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfMagnificationTypes(String targetID);


    /** returns one value from the MAGNIFICATIONTYPE entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfMagnificationTypes(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterMagnificationType(String targetID, int idx);


    /** returns the number of distinct values (separated by backslash characters)
     *  in the SMOOTHINGTYPE entry for the printer with the given
     *  target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfSmoothingTypes(String targetID);


    /** returns one value from the SMOOTHINGTYPE entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfSmoothingTypes(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterSmoothingType(String targetID, int idx);


    /** returns the number of distinct values in the CONFIGURATION_x entries
     *  for the printer with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @return number of values if entry present in the config file, 0 otherwise.
     */
    public native int getTargetPrinterNumberOfConfigurationSettings(String targetID);

    /** returns the value from one of the CONFIGURATION_x entries for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfConfigurationSettings(targetID)
     *  @return value if present, empty string otherwise.
     */
    public native String getTargetPrinterConfigurationSetting(String targetID, int index);


    /** returns the value from one of the CONFIGURATION_x entries for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfConfigurationSettings(targetID)
     *  @return value if present, NULL otherwise.
     */
    public native int getTargetPrinterNumberOfEmptyImageDensities(String targetID);

    /** returns one value from the EMPTYIMAGEDENSITY entry for the printer
     *  with the given target ID from the configuration file.
     *  @param targetID communication target ID, must be one of the target
     *    identifiers returned by getTargetID() for peer type DVPSE_printer.
     *  @param idx index of the value, must be < getTargetPrinterNumberOfEmptyImageDensities(targetID)
     *  @param value the result is both stored in this object and returned as return value.
     *  @return value if present, NULL otherwise.
     */
    public native String getTargetPrinterEmptyImageDensity(String targetID, int index);

    /* NOT USED IN JNI
    OFBool getTargetPrintSCPOmitSOPClassUIDFromCreateResponse(const char *targetID);
    */

    /* general settings */

    /* NOT USED IN JNI
    const char *getLogFolder();
    const char *getLogFile();
    DVPSLogMessageLevel getLogLevel();
    OFBool getMessagePortKeepOpen();
    */

    /** returns the port on which the GUI application accepts notification
     *  messages from the network processes.
     *  Value is taken from the section GENERAL/APPLICATION/MESSAGEPORT
     *  in the config file.
     *  @return message port, default: 0 (no message exchange).
     */
    public native int getMessagePort();


    /** returns the AETitle with which this application should identify itself.
     *  The AETitle is taken from the section GENERAL/NETWORK in the
     *  config file. If absent, a default value is returned.
     *  @return AETitle for this application. Never returns NULL.
     */
    public native String getNetworkAETitle();


    /* NOT USED IN JNI
    OFBool getQueryRetrieveAutoCreateConfigFile();
    const char *getQueryRetrieveAETitle();
    unsigned short getQueryRetrievePort();
    unsigned long getQueryRetrieveMaxPDU();
    unsigned long getQueryRetrieveMaxAssociations();
    */


    /** returns the database folder to be used for sending/receiving/browsing.
     *  Value is taken from the section GENERAL/DATABASE/DIRECTORY
     *  in the config file. If absent, a default value is returned.
     *  @return database folder path.
     */
    public native String getDatabaseFolder();

    /* NOT USED IN JNI
    const char *getSpoolFolder();
    OFBool getDetailedLog();
    OFBool getBinaryLog();
    const char *getSenderName();
    const char *getReceiverName();
    const char *getQueryRetrieveServerName();
    const char *getSpoolerName();
    const char *getPrintServerName();
    const char *getDumpToolName();
    const char *getCheckToolName();
    unsigned long getSpoolerSleep();
    OFBool getSpoolerDeletePrintJobs();
    OFBool getSpoolerAlwaysDeleteTerminateJobs();
    */


    /** returns the filename (path) of the monitor characteristics file
     *  used to implement that Barten transform, as configured in section
     *  GENERAL/MONITOR/CHARACTERISTICS in the config file.
     *  @return monitor characteristics path name or empty string if absent.
     */
    public native String getMonitorCharacteristicsFile();


    /** returns the width (in mm) of one pixel on the current monitor
     *  @return pixel width, 0 if unknown
     */
    public native double getMonitorPixelWidth();


    /** returns the height (in mm) of one pixel on the current monitor
     *  @return pixel height, 0 if unknown
     */
    public native double getMonitorPixelHeight();


    /* NOT USED IN JNI
    Uint32 getMaxPreviewResolutionX();
    Uint32 getMaxPreviewResolutionY();
    */


    /** returns the value of configuration file entry key=value
     *  in the section GENERAL/GUI of the config file.
     *  Specified key must be upper case.
     *  If the entry is absent, empty string is returned.
     *  @param key the entry key
     *  @return entry value or empty string.
     */
    public native String getGUIConfigEntry(String key);


    /** returns the value of configuration file entry key=value
     *  in the section GENERAL/GUI of the config file.
     *  Specified key must be upper case.
     *  If the entry is absent or cannot be parsed, the default dfl
     *  is returned.
     *  @param key the entry key
     *  @param dfl the default to be used if the value is absent or incorrect
     *  @return entry value or default.
     */
    public native boolean getGUIConfigEntryBool(String key, boolean dfl);


    /* Presentation Look Up Tables (LUTs) */


    /* NOT USED IN JNI
    const char *getLUTFolder();
    */


    /** returns the number of Presentation LUTs in the configuration file.
     *  @return number of LUTs
     */
    public native int getNumberOfLUTs();


    /** returns the identifier of the Presentation LUT
     *  with the given index. The identifier is unique within the configuration file
     *  @param idx index, must be < getNumberOfLUTs()
     *  @return identifier if found, NULL otherwise.
     */
    public native String getLUTID(int index);


    /** returns the DESCRIPTION entry for the LUT with the given
     *  ID from the configuration file.
     *  @param lutID LUT ID, must be one of the identifiers returned by getLUTID().
     *  @return entry if present in the config file, empty string otherwise.
     */
    public native String getLUTDescription(String lutID);


    /* NOT USED IN JNI
    const char *getLUTFilename(const char *lutID);
    */


    /* Structured Reporting (SR) "templates" */


    /* NOT USED IN JNI
    const char *getReportFolder();
    */


    /** returns the number of SR "templates" in the configuration file.
     *  @return number of SR "templates"
     */
    public native int getNumberOfReports();


    /** returns the identifier of the SR "template"
     *  with the given index. The identifier is unique within the configuration file
     *  @param idx index, must be < getNumberOfReports()
     *  @return identifier if found, NULL otherwise.
     */
    public native String getReportID(int index);


    /** returns the DESCRIPTION entry for the SR "template" with the given
     *  ID from the configuration file.
     *  @param reportID SR "template" ID, must be one of the identifiers returned by
     *    getReportID().
     *  @return entry if present in the config file, NULL otherwise.
     */
    public native String getReportDescription(String reportID);


    /* NOT USED IN JNI
    const char *getReportFilename(const char *reportID);
    */


    /* general print settings */

    /* NOT USED IN JNI
    Uint32 getMinPrintResolutionX();
    Uint32 getMinPrintResolutionY();
    Uint32 getMaxPrintResolutionX();
    Uint32 getMaxPrintResolutionY();
    Uint16 getDefaultPrintIllumination();
    Uint16 getDefaultPrintReflection();
    */


    /* VOI settings */

    /** returns the number of VOI Presets defined for the given modality
     *  @param modality Modality, e.g. "CT", "MR", "DX" etc.
     *  @return number of VOI Presets
     */
    public native int getNumberOfVOIPresets(String modality);


    /** returns the description string for the given VOI Preset
     *  @param modality Modality, e.g. "CT", "MR", "DX" etc.
     *  @param idx index of the value, must be < getNumberOfVOIPresets(modality)
     *  @return description if present, NULL otherwise.
     */
    public native String getVOIPresetDescription(String modality, int idx);


    /** returns the window center for the given VOI Preset
     *  @param modality Modality, e.g. "CT", "MR", "DX" etc.
     *  @param idx index of the value, must be < getNumberOfVOIPresets(modality)
     *  @return window center if present, 0.0 otherwise.
     */
    public native double getVOIPresetWindowCenter(String modality, int idx);


    /** returns the window width for the given VOI Preset
     *  @param modality Modality, e.g. "CT", "MR", "DX" etc.
     *  @param idx index of the value, must be < getNumberOfVOIPresets(modality)
     *  @return window width if present, 1.0 otherwise.
     */
    public native double getVOIPresetWindowWidth(String modality, int idx);


    /* TLS settings */


    /* NOT USED IN JNI
    const char *getTLSFolder();
    const char *getTLSCACertificateFolder();
    OFBool getTLSPEMFormat();
    */


    /* User login settings */


    /* NOT USED IN JNI
    const char *getUserCertificateFolder();
    */


    /** returns the number of configured users in the USERS section.
     *  @return number of configured users
     */
    public native int getNumberOfUsers();


    /** returns the symbolic identifier of the user with the given index.
     *  The symbolic identifier is unique within the configuration file.
     *  @param idx index, must be < getNumberOfUsers()
     *  @return identifier if found, NULL otherwise.
     */
    public native String getUserID(int idx);


    /** returns the login for the given user. If absent in the config file,
     *  returns NULL.
     *  @param userID user ID as returned by getUserID()
     *  @return login for the given user
     */
    public native String getUserLogin(String userID);


    /** returns the human readable name for the given user.
     *  If absent in the config file, returns NULL.
     *  @param userID user ID as returned by getUserID()
     *  @return name for the given user
     */
    public native String getUserName(String userID);


    /** returns the name for the given user in DICOM Person Name (PN) format.
     *  If absent in the config file, returns NULL.
     *  @param userID user ID as returned by getUserID()
     *  @return DICOM PN name for the given user
     */
    public native String getUserDICOMName(String userID);


    /** returns the organization for the given user.
     *  If absent in the config file, returns NULL.
     *  @param userID user ID as returned by getUserID()
     *  @return organization for the given user
     */
    public native String getUserOrganization(String userID);


    /** returns the coding scheme designator of the user code for the given user if present.
     *  @param userID user ID as returned by getUserID()
     *  @return value if present, NULL otherwise.
     */
    public native String getUserCodingSchemeDesignator(String userID);


    /** returns the coding scheme version of the user code for the given user if present.
     *  @param userID user ID as returned by getUserID()
     *  @return value if present, NULL otherwise.
     */
    public native String getUserCodingSchemeVersion(String userID);


    /** returns the code value of the user code for the given user if present.
     *  @param userID user ID as returned by getUserID()
     *  @return value if present, NULL otherwise.
     */
    public native String getUserCodeValue(String userID);


    /** returns the code meaning of the user code for the given user if present.
     *  @param userID user ID as returned by getUserID()
     *  @return value if present, NULL otherwise.
     */
    public native String getUserCodeMeaning(String userID);


    /* NOT USED IN JNI
    const char *getUserCertificate(const char *userID);
    const char *getUserPrivateKey(const char *userID);
    */


    /* log settings */


    /* NOT USED IN JNI
    virtual void setLog(OFConsole *stream, OFBool verbMode, OFBool dbgMode);
    */
}


/*
 *  CVS Log
 *  $Log: jDVInterface.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

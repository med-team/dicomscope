/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPSLogMessageLevel</em> represents the C++-enumeration
 * DVPSLogMessageLevel.
 *
 * @author 	Joerg Riesmeier
*/
public class jDVPSLogMessageLevel
{
  /** no log messages (only used as a filter)
   */  
  public static final int DVPSM_none = 0;
  /** only error messages
   */  
  public static final int DVPSM_error = 1;
  /** warning messages (includes DVPSM_error)
   */  
  public static final int DVPSM_warning = 2;
  /** informational messages (includes DVPSM_warning)
   */  
  public static final int DVPSM_informational = 3;
  /** debug messages (includes DVPSM_informational)
   */  
  public static final int DVPSM_debug = 4;
}

/*
 *  CVS Log
 *  $Log: jDVPSLogMessageLevel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

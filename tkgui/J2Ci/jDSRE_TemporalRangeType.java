/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;
import java.util.*;

/** 
 * The class <em>jDSRE_TemporalRangeType</em> represents the C++ enumeration
 * DSRTypes::E_TemporalRangeType.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_TemporalRangeType
{
    /** internal type used to indicate an error
     */
    public static final int TRT_invalid = 0;

    /** DICOM Temporal Range Type: POINT
     */
    public static final int TRT_Point = 1;

    /** DICOM Temporal Range Type: MULTIPOINT
     */
    public static final int TRT_Multipoint = 2;

    /** DICOM Temporal Range Type: SEGMENT
     */
    public static final int TRT_Segment = 3;

    /** DICOM Temporal Range Type: MULTISEGMENT
     */
    public static final int TRT_Multisegment = 4;

    /** DICOM Temporal Range Type: BEGIN
     */
    public static final int TRT_Begin = 5;

    /** DICOM Temporal Range Type: END
     */
    public static final int TRT_End = 6;
    
    /**
    * Contains the name for each ID.
    */
    public static Hashtable names; 
    static   
    {
        names = new Hashtable();
        names.put(new Integer(TRT_invalid),       "<invalid>");
        names.put(new Integer(TRT_Point),        "Point");
        names.put(new Integer(TRT_Multipoint),      "Multipoint");
        names.put(new Integer(TRT_Segment), "Segment");
        names.put(new Integer(TRT_Multisegment), "Multisegment");
        names.put(new Integer(TRT_Begin), "Begin");
        names.put(new Integer(TRT_End), "End");
    };
    
    /**
    * Returns the name of the specified message
    * @param id Id specifying the message
    * @return The name of the specified message
    */
    public static final  String getName(int id)
    {
        return (String) names.get(new Integer(id));
    }
    
}


/*
 *  CVS Log
 *  $Log: jDSRE_TemporalRangeType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

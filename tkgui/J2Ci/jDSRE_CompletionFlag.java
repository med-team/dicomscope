/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/** 
 * The class <em>jDSRE_CompletionFlag</em> represents the C++ enumeration
 * DSRTypes::E_CompletionFlag.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_CompletionFlag
{
    /** internal type used to indicate an error
     */
    public static final int CF_invalid = 0;

    /** DICOM defined term: PARTIAL
     */
    public static final int CF_Partial = 1;

    /** DICOM defined term: COMPLETE
     */
    public static final int CF_Complete = 2;
}


/*
 *  CVS Log
 *  $Log: jDSRE_CompletionFlag.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

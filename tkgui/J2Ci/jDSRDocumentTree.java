/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRDocumentTree</em> represents the relating C++ class
 *  DSRDocumentTree in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRDocumentTree
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRDocumentTree()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRDocumentTree(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Document Tree --- */

    /** check whether specified content item can be added to the current one.
     *  If the tree is currently empty only a CONTAINER with the internal relationship
     *  type RT_isRoot is allowed (as the new root node).  This method can be used
     *  to decide which type of content items can be added prior to really do so.
     ** @param  relationshipType  relationship type of node to be checked with regard
     *                            to the current one
     *  @param  valueType         value type of node to be checked
     *  @param  addMode           flag specifying at which position the new node would
     *                            be added.
     *                            (AM_afterCurrent, AM_beforeCurrent, AM_belowCurrent)
     ** @return OFTrue if specified node can be added, OFFalse otherwise
     */
    public native boolean canAddContentItem(int relationshipType,
                                            int valueType,
                                            int addMode);

    /** add specified content item to the current one.
     *  If possible this method creates a new node as specified and adds it to the current
     *  one.  The method canAddContentItem() is called internally to check parameters first.
     ** @param  relationshipType  relationship type of node to be added with regard
     *                            to the current one
     *  @param  valueType         value type of node to be added
     *  @param  addMode           flag specifying at which position to add the new node.
     *                            (AM_afterCurrent, AM_beforeCurrent, AM_belowCurrent)
     ** @return ID of new node if successful, 0 otherwise
     */
    public native int addContentItem(int relationshipType,
                                     int valueType,
                                     int addMode);

    /** remove current content item from tree.
     *  Please note that not only the specified node but also all of his child nodes are
     *  removed from the tree and then deleted.  The internal cursor is set automatically
     *  to a new valid position.
     ** @return ID of the node which became the current one after deletion, 0 if an error
     *          occured or the tree is now empty.
     */
    public native int removeCurrentContentItem();
    
    /** remove digital signatures from the document tree.
     *  This method clears the MACParametersSequence and the DigitalSignaturesSequence for all
     *  content items which have been filled during reading.
     */
    public native void removeSignatures();  

    /** unmark all content items in the document tree.
     *  Use method 'setCurrentItemMark' to mark and unmark a single content item.
     *  Pointers to the DICOM dataset/item of marked content items are added to the optional
     *  stack when calling the 'write' method.  This mechanism can e.g. be used to digitally
     *  sign particular content items.
     */
    public native void unmarkAllContentItems();

    /** goto root node of the tree
     ** @return ID of the root node if successful, 0 otherwise
     */
    public native int gotoRootNode();

    /** goto previous node on the same level
     ** @return ID of the previous node if successful, 0 otherwise
     */
    public native int gotoPreviousNode();

    /** goto next node on the same level
     ** @return ID of the next node if successful, 0 otherwise
     */
    public native int gotoNextNode();

    /** goto parent node (one level up)
     ** @return ID of the parent node if successful, 0 otherwise
     */
    public native int gotoParentNode();

    /** goto first child node (one level down)
     ** @return ID of the first child node if successful, 0 otherwise
     */
    public native int gotoFirstChildNode();

    /** iterate over all nodes.
     *  This method visits first the children (if any) and then the siblings ("deep search").
     ** @return ID of the next node if successful, 0 otherwise
     */
    public native int iterateNodes();

    /** set cursor to specified node
     ** @param  searchID  ID of the node to set the cursor to
     ** @return ID of the new current node if successful, 0 otherwise
     */
    public native int gotoNode(int searchID);


    /* --- Current Content Item --- */

    /** check whether current item is valid (complete)
     *  Applicable to all content items.
     ** @return OFTrue if current item is valid, OFFalse otherwise
     */
    public native boolean isCurrentItemValid();

    /** check for mark flag.
     *  Applicable to all content items.
     ** @return OFTrue if current content item is marked, OFFalse otherwise
     */
    public native boolean isCurrentItemMarked();

    /** mark/unmark item. Used to digitally sign particular content items.
     *  Applicable to all content items.
     ** @param  flag  mark item if OFTrue, unmark otherwise
     */
    public native void setCurrentItemMark(boolean flag);

    /** get value type of the current content item.
     *  Applicable to all content items.
     ** @return value type of current content item if valid, VT_invalid otherwise
     */
    public native int getCurrentValueType();

    /** get relationship type of the current content item.
     *  Applicable to all content items.
     ** @return relationship type of current content item if valid, RT_invalid otherwise
     */
    public native int getCurrentRelationshipType();

    /** get current node ID.
     *  The node ID uniquely identifies a content item in the document tree.  Most of the
     *  navigation methods above do return this ID too.
     *  Applicable to all content items.
     ** @return ID of the current node if valid, 0 otherwise
     */
    public native int getCurrentNodeID();

    /** get ID of the referenced node.
     *  Applicable to: byReference relationships
     ** @return ID of the referenced node if valid, 0 otherwise
     */
    public native int getCurrentReferencedNodeID();

    /** get current level.
     *  The level starts with 1 for the root node, then 2 for its child nodes, etc.
     ** @return number of the current level if valid, 0 otherwise
     */
    public native int getCurrentLevel();

    /** get string value of current content item.
     *  Applicable to: TEXT, DATETIME, DATE, TIME, UIDREF, PNAME
     ** @return string value of current content item if valid, EmptyString otherwise
     */
    public native String getCurrentStringValue();
    
    /** set string value of current content item.
     *  Please use the correct format for the string value depending on the corresponding
     *  content item (value type).
     *  Applicable to: TEXT, DATETIME, DATE, TIME, UIDREF, PNAME
     ** @param  stringValue  value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setCurrentStringValue(String stringValue);

    /** get reference to current code value.
     *  If the current content item is not a code value or the cursor is not valid NULL
     *  is returned.
     *  Applicable to: CODE
     ** @return reference to CODE value if successful, NULL otherwise 
     */
    public jDSRCodeValue getCurrentCodeValue()
    {
        long addr = getCurrentCodeValueN();
        if (addr == 0) return null;
        return new jDSRCodeValue (addr);
    }
    
    private native long getCurrentCodeValueN();

    /** get reference to current numeric measurement value.
     *  If the current content item is not a numeric measurement value or the cursor
     *  is not valid NULL is returned.
     *  Applicable to: NUM
     ** @return reference to NUM value if successful, NULL otherwise 
     */
    public jDSRNumericValue getCurrentNumValue()
    {
        long addr = getCurrentNumValueN();
        if (addr == 0) return null;
        return new jDSRNumericValue (addr);
    }
    
    private native long getCurrentNumValueN();

    /** get reference to current spatial coordinates value.
     *  If the current content item is not a spatial coordinates value or the cursor is
     *  not valid NULL is returned.
     *  Applicable to: SCOORD
     ** @return reference to SCOORD value if successful, NULL otherwise 
     */
    public jDSRSCoordValue getCurrentSCoordValue()
    {
        long addr = getCurrentSCoordValueN();
        if (addr == 0) return null;
        return new jDSRSCoordValue (addr);
    }
    
    private native long getCurrentSCoordValueN();

    /** get reference to current temporal coordinates value.
     *  If the current content item is not a temporal coordinates value or the cursor is
     *  not valid NULL is returned.
     *  Applicable to: TCOORD
     ** @return reference to TCOORD value if successful, NULL otherwise 
     */
    public jDSRTCoordValue getCurrentTCoordValue()
    {
        long addr = getCurrentTCoordValueN();
        if (addr == 0) return null;
        return new jDSRTCoordValue (addr);
    }
    
    private native long getCurrentTCoordValueN();

    /** get reference to current composite reference value.
     *  If the current content item is not a composite reference value or the cursor is
     *  not valid NULL is returned.
     *  Applicable to: COMPOSITE
     ** @return reference to COMPOSITE value if successful, NULL otherwise 
     */
    public jDSRCompositeValue getCurrentCompositeValue()
    {
        long addr = getCurrentCompositeValueN();
        if (addr == 0) return null;
        return new jDSRCompositeValue (addr);
    }
    
    private native long getCurrentCompositeValueN();

    /** get reference to current image reference value.
     *  If the current content item is not an image reference value or the cursor is
     *  not valid NULL is returned.
     *  Applicable to: IMAGE
     ** @return reference to IMAGE value if successful, NULL otherwise 
     */
    public jDSRImageValue getCurrentImageValue()
    {
        long addr = getCurrentImageValueN();
        if (addr == 0) return null;
        return new jDSRImageValue (addr);
    }
    
    private native long getCurrentImageValueN();

    /** get reference to current waveform reference value.
     *  If the current content item is not a waveform reference value or the cursor is
     *  not valid NULL is returned.
     *  Applicable to: WAVEFORM
     ** @return reference to WAVEFORM value if successful, NULL otherwise 
     */
    public jDSRWaveformValue getCurrentWaveformValue()
    {
        long addr = getCurrentWaveformValueN();
        if (addr == 0) return null;
        return new jDSRWaveformValue (addr);
    }
    
    private native long getCurrentWaveformValueN();

    /** get continuity of content flag of current content item.
     *  This flag specifies whether or not its contained content items (child nodes) are
     *  logically linked in a continuous textual flow, or are sparate items.
     *  Applicable to: CONTAINER
     ** @return continuity of content flag if successful, COC_invalid otherwise
     */
    public native int getCurrentContinuityOfContentFlag();

    /** set continuity of content flag.
     *  This flag specifies whether or not its contained content items (child nodes) are
     *  logically linked in a continuous textual flow, or are sparate items.
     *  Applicable to: CONTAINER
     ** @param  continuityOfContent  value to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setCurrentContinuityOfContentFlag(int continuityOfContent);

    /** get reference to current concept name value.
     *  Code describing the concept represented by this content item.  Also conveys the value
     *  of document title and section headings in documents.
     *  Applicable to all content items (by-value only, optional/conditional for some value types).
     ** @return reference to concept name value if successful, NULL otherwise 
     */
    public jDSRCodeValue getCurrentConceptName()
    {
        long addr = getCurrentConceptNameN();
        if (addr == 0) return null;
        return new jDSRCodeValue (addr);
    }
    
    private native long getCurrentConceptNameN();

    /** check whether the current content item requires a non-empty concept name.
     *  The following value types require a concept name: TEXT, NUM, CODE, DATETIME, DATE, TIME,
     *  UIDREF, PNAME, CONTAINER (only required for root node).
     *  @return OFTrue if current conten items requires a concept name, OFFalse otherwise
     */
//    public native int currentItemRequiresConceptName();

    /** get observation date and time of current content item.
     *  This is the date and time on which this content item was completed.  Might be empty
     *  if the date and time do not differ from the content date and time, see DSRDocument.
     *  Applicable to all content items (by-value only, optional attribute).
     ** @return observation date and time if valid, EmptyString otherwise
     */
    public native String getCurrentObservationDateTime();

    /** set observation date time of current content item.
     *  This is the date and time on which this content item was completed.  Might be empty
     *  if the date and time do not differ from the content date and time, see DSRDocument.
     *  Please use the correct DICOM format (VR=DT).
     *  Applicable to all content items (by-value only).
     ** @param  observationDateTime  value to be set (might be an empty string)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setCurrentObservationDateTime(String observationDateTime);
}


/*
 *  CVS Log
 *  $Log: jDSRDocumentTree.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

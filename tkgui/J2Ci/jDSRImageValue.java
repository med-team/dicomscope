/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRImageValue</em> represents the relating C++ class
 *  DSRImageReferenceValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRImageValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRImageValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRImageValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Image Value --- */

    /** get image SOP class UID
     ** @return current image SOP class UID (might be invalid or an empty string)
     */
    public native String getImageSOPClassUID();

    /** get image SOP class name.
     *  The SOP class name as defined in the OFFIS dcmtk is used if available,
     *  an empty string if the SOP class UID is also empty, the static string
     *  "unknown SOP class" plus the SOP class UID otherwise.
     ** @return name of the current image SOP class (should never be null or empty)
     */
    public native String getImageSOPClassName();

    /** get image SOP instance UID
     ** @return current image SOP instance UID (might be invalid or an empty string)
     */
    public native String getImageSOPInstanceUID();

    /** set image SOP class UID and SOP instance UID value.
     *  Before setting the values they are checked (non-empty UIDs).  If the value
     *  pair is invalid the current value pair is not replaced and remains unchanged.
     ** @param  sopClassUID     SOP class UID of the image to be set
     *  @param  sopInstanceUID  SOP instance UID of the image to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setImageReference(String sopClassUID,
                                        String sopInstanceUID);

    /** get presentation state SOP class UID
     ** @return current pstate SOP class UID (might be invalid or an empty string)
     */
    public native String getPStateSOPClassUID();

    /** get presentation state SOP class name.
     *  The SOP class name as defined in the OFFIS dcmtk is used if available,
     *  an empty string if the SOP class UID is also empty, the static string
     *  "unknown SOP class" plus the SOP class UID otherwise.
     ** @return name of the current pstate SOP class (should never be null or empty)
     */
    public native String getPStateSOPClassName();

    /** get presentation state SOP instance UID
     ** @return current pstate SOP instance UID (might be invalid or an empty string)
     */
    public native String getPStateSOPInstanceUID();

    /** set presentation state SOP class UID and SOP instance UID value.
     *  Before setting the values they are checked.  The presentation state object is
     *  "valid" if both UIDs are empty or both are not empty and SOP class UID equals
     *  to "GrayscaleSoftcopyPresentationStateStorage".  If the value pair is invalid
     *  the current value pair is not replaced and remains unchanged.
     ** @param  sopClassUID     SOP class UID of the pstate to be set
     *  @param  sopInstanceUID  SOP instance UID of the pstate to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setPStateReference(String sopClassUID,
                                         String sopInstanceUID);

    /** clear the referenced frame list
     */
    public native void clearFrameList();

    /** get number of entries contained in the referenced frame list
     ** @return number of frames if any, 0 otherwise
     */
    public native int getNumberOfFrames();

    /** get copy of the specified entry from the referenced frame list
     ** @param  idx          index of the entry to be returned (starting from 1)
     *  @param  frameNumber  reference to variable where the frame number should be stored
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int getFrame(int idx,
                               jIntByRef frameNumber);

    /** add entry to the referenced frame list.
     *  Please note that only frames are added that are not already contained in the list.
     ** @param  frameNumber  frame number to be added
     */
    public native void addFrame(int frameNumber);

    /** remove entry from the referenced frame list
     ** @param  idx  index of the entry to be removed (starting from 1)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int removeFrame(int idx);

    /** check whether the image reference applies to a specific frame.
     *  The image reference applies to a frame (of multiframe images) if the list of
     *  referenced frame numbers is empty or the frame number is part of the list.
     ** @param  frameNumber  number of the frame to be checked
     ** @return OFTrue if reference applies to the specified frame, OFFalse otherwise
     */
    public native boolean appliesToFrame(int frameNumber);
}


/*
 *  CVS Log
 *  $Log: jDSRImageValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

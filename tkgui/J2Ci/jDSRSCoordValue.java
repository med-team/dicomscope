/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRSCoordValue</em> represents the relating C++ class
 *  DSRSpatialCoordinatesValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRSCoordValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRSCoordValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRSCoordValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- SCoord Value --- */

    /** get current graphic type.
     *  The graphic type specifies the geometry of the coordinates stored in the graphic data
     *  list.
     ** @return graphic type (might be GT_invalid)
     */
    public native int getGraphicType();

    /** set current graphic type.
     *  The graphic type specifies the geometry of the coordinates stored in the graphic data
     *  list.
     ** @param  graphicType  graphic type to be set (GT_invalid is not allowed)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setGraphicType(int graphicType);

    /** clear the graphic data list
     */
    public native void clearGraphicData();

    /** get number of pixels contained in the graphic data list
     ** @return number of pixels if any, 0 otherwise
     */
    public native int getNumberOfPixels();

    /** get copy of the specified pixel
     ** @param  idx     index of the value pair to be returned (starting from 1)
     *  @param  column  reference to variable where the column value should be stored
     *  @param  row     reference to variable where the row value should be stored
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int getPixel(int idx,
                               jFloatByRef column,
                               jFloatByRef row);

    /** add pixel to the graphic data list
     ** @param  column  column value to be added
     *  @param  row     row value to be added
     */
    public native void addPixel(float column,
                                float row);

    /** remove pixel from the list
     ** @param  idx  index of the pixel to be removed (starting from 1)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int removePixel(int idx);
}


/*
 *  CVS Log
 *  $Log: jDSRSCoordValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

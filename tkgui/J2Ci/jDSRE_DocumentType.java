/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;

import java.util.*;
/**
 * The class <em>jDSRE_DocumentType</em> represents the C++ enumeration
 * DSRTypes::E_DocumentType.
 *
 * @author 	Joerg Riesmeier, Klaus Kleber
*/
public class jDSRE_DocumentType
{
    /** internal type used to indicate an error
     */
    public static final int DT_invalid = 0;

    /** DICOM SOP Class: Basic Text SR
     */
    public static final int DT_BasicTextSR = 1;

    /** DICOM SOP Class: Enhanced SR
     */
    public static final int DT_EnhancedSR = 2;

    /** DICOM SOP Class: Comprehensive SR
     */
    public static final int DT_ComprehensiveSR = 3;

    /**
    * Contains for each ID the name.
    */
    public static Hashtable vtNames;
    static
    {
         vtNames = new Hashtable();
         vtNames.put(new Integer(DT_invalid),         "<invalid>");
         vtNames.put(new Integer(DT_BasicTextSR),     "Basic Text SR");
         vtNames.put(new Integer(DT_EnhancedSR),      "Enhanced SR");
         vtNames.put(new Integer(DT_ComprehensiveSR), "Comprehensive SR");
    };

    /**
    * Returns the name of the specified document type
    * @param id Id specifieying the document type
    * @return The name of the specified document type
    */
    public static final  String getDocumentTypeName(int id)
    {
        return (String) vtNames.get(new Integer(id));
    }

}


/*
 *  CVS Log
 *  $Log: jDSRE_DocumentType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/

/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPrStateParam_GetScreenData</em> is responsible for parameters
 * passed to the method getScreenData().
 *
 * @author 	Andreas Schr�ter
 */
public class jDVPrStateParam_GetScreenData
{
    /**
     * IN/OUT: Pixeldata as byte-Array. Pixel output will be put in this array. 
     * The array must be created before passing an object of this class to that
     * method getScreenData().
    */
    public byte[] pixelArray = null;
    
    /**
     * IN: Width of array / window width.
    */
	public int width = 0;
	
	/**
     * IN: Height of array / window height.
    */
	public int height = 0;
	
	/**
     * IN: Offset for X to fill array. From this point the array will be filled. All before
     * this point will be set to black. MUST be positive.
    */
	public int offsetX = 0;
	
	/**
     * IN: Offset for Y to fill array. From this point the array will be filled. All before
     * this point will be set to black. MUST be positive.
    */
	public int offsetY = 0;
    
    // -------------------------------------------
    
    /**
     * IN: Top left hand corner, x-coordinate of image.
    */
	public int TLHC_x = 0;
	
	/**
     * IN: Top left hand corner, y-coordinate.
    */
	public int TLHC_y = 0;
	
	/**
     * OUT: Bottom right hand corner, x-coordinate. This element is set to the coordinate
     * of the lower right pixel shown in this returned array of the original image data.
    */
	public int BRHC_x = 0;
	
	/**
     * OUT: Bottom right hand corner, y-coordinate. This element is set to the coordinate
     * of the lower right pixel shown in this returned array of the original image data.
    */
	public int BRHC_y = 0;
	
    // --------------------------------------------
	
    /**
     * IN: Zoom factor. (1.0) means no maximizing or minimizing. The value MUST NOT be 0.
    */
	public double zoomfactor = 1.0;
	
	/**
     * IN: scale factor for x. (1.0) means no maximizing or minimizing. The value MUST NOT be 0.
    */
	public double scale_x = 1.0;
	
	/**
     * IN: scale factor for y. (1.0) means no maximizing or minimizing. The value MUST NOT be 0.
    */
	public double scale_y = 1.0;
	
    /**
     * IN: interpolate pixel for maximizing the image. Set to false if pixels should be doubled.
    */
	public boolean interpolate = true;
	
	
	
}

/*
 *  CVS Log
 *  $Log: jDVPrStateParam_GetScreenData.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;


/** 
 * A <em>jDVPSTextObject</em> is the Java-sided class for the C++-Class
 * DVPSTextObject.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPSTextObject
{
    /**
     * Constructor - not allowed!
    */
    protected jDVPSTextObject()
    {
        createObjOfDVPSTextObject ();
    }
    
    
    /**
     * Constructor for attaching an existing C++-Object. FOR INTERNAL USE ONLY!
     * @param attachAdr Address of C++-Object
    */    
    public jDVPSTextObject (long attachAdr)
    {
        cppClassAddress = attachAdr;
    }
    
    // -------------------------------- Methods for C++-Class Binding
    
    /**
     * Address of C++-Object for access in the DLL. Never change manually!!
    */
    private long cppClassAddress = (long) 0; // never change!
    
    
    /**
     * Creates a C++-Object of class DVPSTextObject and attached it to this
     * current object. The address of the C++-object will be put into field 
     * cppClassAddress.
     *
     * @see J2Ci.jDVPSTextObject#cppClassAddress
    */
    private native void createObjOfDVPSTextObject ();
    
    
    // ----------------------------------- Methods of Class jDVPSTextObject
    
    /**
     * Copy-Constructor
     * @param copy TextObject-Object to copy
    */
    public jDVPSTextObject (jDVPSTextObject copy)
    {                
        CopyConstructor (copy.cppClassAddress);
    }

    private native void CopyConstructor (long fromCppObj);

    // ------------------------------------------------------------
    
    /** 
     * Checks if this text object contains an anchor point.
     * @return true if anchor point present.
    */
    public native boolean haveAnchorPoint();


    /** 
     * Checks if this text object contains bounding box.
     * @return true if bounding box present.
    */
    public native boolean haveBoundingBox();

   
    /** 
     * Sets an anchor point for this text object.
     * @param x anchor point X value
     * @param y anchor point Y value
     * @param unit anchor point annotation units (pixel/display) (from jDVPSannotationUnit).
     * @param isVisible anchor point visibility
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setAnchorPoint(double x, double y, int unit, boolean isVisible);


    /** 
     * Sets bounding box for this text object.
     * @param TLHC_x bounding box top-lefthand corner X value
     * @param TLHC_y bounding box top-lefthand corner Y value
     * @param BRHC_x bounding box bottom-righthand corner X value
     * @param BRHC_y bounding box bottom-righthand corner Y value
     * @param unit bounding box annotation units (pixel/display) (from jDVPSannotationUnit)
     * @param justification bounding box horizontal justification (left/right/center) (from jDVPSTextJustification)
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setBoundingBox(double TLHC_x, double TLHC_y, double BRHC_x, double BRHC_y, int unit, int justification); 


    /** 
     * Assigns a new "unformatted text value" for this text object.
     * @param text unformatted text value. Must not be NULL or empty string.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setText(String text);


       
    /** 
     * Removes any anchor point from the text object.
     * Attention: A text object must always contain either anchor point, bounding box
     * or both. This property is not asserted by the text object itself.
    */
    public native void removeAnchorPoint();


    /** 
     * Removes any bounding box from the text object.
     * Attention: A text object must always contain either anchor point, bounding box
     * or both. This property is not asserted by the text object itself.
    */
    public native void removeBoundingBox();
 
 
    /** 
     * Gets the unformatted text value for this text object.
     * @return unformatted text value
    */
    public native String getText();

    /** 
     * Gets the bounding box TLHC x value.
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)
     * @return bounding box TLHC x value
    */
    public native double getBoundingBoxTLHC_x();


    /** 
     * Gets the bounding box TLHC y value.
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)
     * @return bounding box TLHC y value
    */
    public native double getBoundingBoxTLHC_y();


    /** 
     * Gets the bounding box BRHC x value.
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)
     * @return bounding box BRHC x value
    */
    public native double getBoundingBoxBRHC_x();


    /** 
     * Gets the bounding box BRHC y value.
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)
     * @return bounding box BRHC y value
    */
    public native double getBoundingBoxBRHC_y();
   
   
    /** 
     * Gets the bounding box annotation units.
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)
     * @return bounding box annotation units (from jDVPSannotationUnit).
    */
    public native int getBoundingBoxAnnotationUnits();
 
 
    /** 
     * Gets the anchor point x value.
     * May only be called when an anchor point is present (haveAnchorPoint()==OFTrue)
     * @return anchor point x value
    */
    public native double getAnchorPoint_x();  
 
 
    /** 
     * Gets the anchor point y value.
     * May only be called when an anchor point is present (haveAnchorPoint()==OFTrue)
     * @return anchor point y value
    */
    public native double getAnchorPoint_y();
 
 
    /** 
     * Gets the anchor point visibility
     * May only be called when an anchor point is present (haveAnchorPoint()==OFTrue)
     * @return true if anchor point is visible
    */
    public native boolean anchorPointIsVisible();
 
 
    /** 
     * Gets the anchor point annotation units.
     * May only be called when an anchor point is present (haveAnchorPoint()==OFTrue)
     * @return anchor point annotation units (from jDVPSannotationUnit).
    */
    public native int getAnchorPointAnnotationUnits();
    
        
    /** 
     * Gets the bounding box horizontal justification.    
     * May only be called when a bounding box is present (haveBoundingBox()==OFTrue)  
     * @return bounding box horizontal justification (from jDVPSTextJustification)
    */
    public native int getBoundingBoxHorizontalJustification();

}

/*
 *  CVS Log
 *  $Log: jDVPSTextObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

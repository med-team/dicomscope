/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import J2Ci.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class contains the names and the IDs of all messages  to be
 * exchanged between DICOMscope processes.
 * The name of each ID can be requested by {@link #getIdName}
 *
 * @author Klaus Kleber
 * @since 16.10.2000
 */
public class ProcessMessageIDs
{   
    /**
    * ID for Respond Ok message
    */
    public static final int RESPOND_OK = 0;
    
    /**
    * ID for Assign Application Identification Number
    */
    public static final int ASSIGN_APPLICATION_IDENTIFICATION_NUMBER = 2;
    
    /**
    * ID for Request Application Identification Number
    */
    public static final int REQUEST_APPLICATION_IDENTIFICATIONNUMBER = 1;
    
    /**
    * ID for Application terminates
    */
    public static final int APPLICATION_TERMINATES = 3;
    /**
    * ID for Received unencrypted DICOM connection
    */
    public static final int RECEIVED_UNENCRYPTED_DICOM_CONNECTION =  5;
    /**
    * ID for Received encrypted DICOM connection
    */
    public static final int RECEIVED_ENCRYPTED_DICOM_CONNECTION =  7;
    /**
    * ID for DICOM connection closed
    */
    public static final int DICOM_CONNECTION_CLOSED = 9;
    /**
    * ID for DICOM connection aborded
    */
    public static final int DICOM_CONNECTION_ABORDED = 11;
    /**
    * ID for Requested unencrypted DICOM connection
    */
    public static final int REQUESTED_UNENCRYPTED_DICOM_CONNECTION =  13;
    /**
    * ID for Requested encrypted DICOM connection
    */
    public static final int REQUESTED_ENCRYPTED_DICOM_CONNECTION =  15;
    /**
    * ID for Received DICOM object
    */
    public static final int RECEIVED_DICOM_OBJECT =  17;
    /**
    * ID for SENT DICOM object
    */
    public static final int SENT_DICOM_OBJECT =  19;
    
    
    /**
    * Contains for each ID the name of the Message
    */
    public static Hashtable names; 
    static   
    {
           names = new Hashtable();
           names.put(new Integer(RESPOND_OK),  "Respond ok");
           names.put(new Integer(ASSIGN_APPLICATION_IDENTIFICATION_NUMBER),  "Assign Application Identification Number");
           names.put(new Integer(REQUEST_APPLICATION_IDENTIFICATIONNUMBER),  "Request Application Identification Number");
           names.put(new Integer(APPLICATION_TERMINATES),  "Application terminates");
           names.put(new Integer(RECEIVED_UNENCRYPTED_DICOM_CONNECTION),  "Received unencrypted DICOM connection");
           names.put(new Integer(RECEIVED_ENCRYPTED_DICOM_CONNECTION),  "Received encrypted DICOM connection");
           names.put(new Integer(DICOM_CONNECTION_CLOSED),   "DICOM connection closed");
           names.put(new Integer(DICOM_CONNECTION_ABORDED),   "DICOM connection aborted");
           names.put(new Integer(REQUESTED_UNENCRYPTED_DICOM_CONNECTION),   "Requested unencrypted DICOM connection");
           names.put(new Integer(REQUESTED_ENCRYPTED_DICOM_CONNECTION),   "Requested encrypted DICOM connection");
           names.put(new Integer(RECEIVED_DICOM_OBJECT),   "Received DICOM object");
           names.put(new Integer(SENT_DICOM_OBJECT),   "Sent DICOM object");
    };
    /**
    * Returns the name of the specified message
    * @param id Id specifieying the message
    * @return The name of the specified message
    */
    public static String getMessageName(int id)
    {
        return (String) names.get(new Integer(id));
    }
}

/*
 *  CVS Log
 *  $Log: ProcessMessageIDs.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;
import de.microtherapy.tools.text.document.general.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import main.*;
import javax.swing.table.*;
import java.awt.*;

/**
 * This class logs all received DicomScopeMessage. 
 * @author Klaus Kleber
 * @since 16.10.2000
 */
public class ProcessComponent extends JPanel implements ProcessLogListener, ActionListener
{   
    
    /**
    * TextArea for display the DicomScopeMessage
    */ 
    private JTextArea textArea = new JTextArea(5,5);
    
    /**
    * List containing the DicomScopeMessage.
    */ 
    private Vector messageList = new Vector();
    
    /**
    * Max lenght of messageList.
    */
    private int maxLength= 1500;
    
    /**
    * Number of DicomScopeMessages to be deleted if maxLenght is reached
    */
    private int deleteNumberOfMessage = 500;
    
    /**
    * ParentFrame
    */
    private JFrame parent;
    
    /**
    * If selected for each new application id a new ProcesIDDisplay will be initialized
    */
    private JCheckBox openBox = new JCheckBox("Open Log Window for new Process");
    
    
    /**
    * Highest application id. This value will be used to identify new application ids 
    */
    private int highestApplicationID = -1;
    
    ProcessTable table;
    
    
    /**
    * Constructor
    * @param parent Parent frame
    */
    public ProcessComponent(JFrame parent, Font f)
    {
       this.parent = parent;
       ProcessController.instance().addProcessLogListener(this);
       setLayout(new BorderLayout(5,5));
       
       //Text
       textArea.setWrapStyleWord(true);
       textArea.setEditable(false);
       
       JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
       table=  new ProcessTable(textArea,messageList, f);
       splitPane.setTopComponent(new JScrollPane(table));
       splitPane.setBottomComponent(new JScrollPane(textArea));
       add(splitPane, BorderLayout.CENTER);
       splitPane.setDividerLocation(250);
       
       //Buttons
       JPanel buttonPanel = new JPanel();
       JButton clearButton = new JButton("Clear");
       clearButton.addActionListener(this);
       clearButton.setToolTipText("clear the message display");
       clearButton.setActionCommand("clear");
       buttonPanel.add(clearButton);
       /*
       JButton selectButton = new JButton("Filter");
       selectButton.setToolTipText("Show process related messages ");
       selectButton.addActionListener(this);
       selectButton.setActionCommand("select");
       buttonPanel.add(selectButton);
       
       buttonPanel.add(openBox);
       */
       add(buttonPanel, BorderLayout.SOUTH);
    }
    /**
    * Received DicomScopeMessages
    */
    public  synchronized void logProcess (DicomScopeMessage e)
    {
        if (maxLength < messageList.size()) 
        {
            for (int i = 0 ; i <deleteNumberOfMessage; i++)messageList.removeElementAt(0);
            textArea.setText("");
            for (int i = 0 ; i <messageList.size(); i++)textArea.append(messageList.elementAt(i).toString() + "\n");
            
        }
        messageList.add(e);
        //textArea.append(e.toString()+"\n");
        //textArea.setCaretPosition(textArea.getText().length()-1);
        if (e.getProcessId() > highestApplicationID)
        {
            highestApplicationID = e.getProcessId();
            if (openBox.isSelected())openFrame(highestApplicationID);
        }
        SwingUtilities.invokeLater(new InsertInTable(table, e));
        
    }
    public class InsertInTable implements Runnable
    {
        ProcessTable t;
        DicomScopeMessage m;
        public InsertInTable(ProcessTable t,DicomScopeMessage m)
        {
            this.t = t;
            this.m = m;
            
        }
        
        public void run()
        {
            t.insertData(m);
        }
    }
    public class DeleteInTable implements Runnable
    {
        ProcessTable t;
        public DeleteInTable(ProcessTable t)
        {
            this.t = t;
        }
        
        public void run()
        {
            t.deleteAll();
        }
    }
    
    /**
    * Handle Button Events Button Evebts
    */
    public synchronized void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        if (command.equals("clear"))
        {
            messageList = new Vector();
            textArea.setText("");
            SwingUtilities.invokeLater(new DeleteInTable(table));
        }
        else if (command.equals("select"))
        {
	    JPanel p = new JPanel();
	    JTextField t = new JTextField(2);
	    t.setDocument(new IntegerDocument(Integer.MAX_VALUE));
	    p.add(t);
	    int result = JOptionPane.showConfirmDialog (parent,p, "Select Process ID", 
					        JOptionPane.OK_CANCEL_OPTION,
					        JOptionPane.PLAIN_MESSAGE);
					        
            if (result == JOptionPane.OK_OPTION)
            {
                try
                {
                    int id = new Integer (t.getText()).intValue();
                    openFrame(id);
                }
                catch (Exception ex)
                {
		    JOptionPane.showMessageDialog (parent, "Invalid ID!", 
					        "Invalid", JOptionPane.ERROR_MESSAGE);
                    
                }
            }
        }
    }
    
    /**
    * Opens a ProcessIDDisplay with a copy of the  DicomScopeMessage related to 
    * the specified 
    */
    private void openFrame(int applicationID)
    {
            Vector init = new Vector();
            DicomScopeMessage dsm; 
            for (int i = 0; i < messageList.size(); i++)
            {
                dsm = (DicomScopeMessage)messageList.elementAt(i);
                if (dsm.getProcessId() == applicationID) init.addElement(dsm);
            }
            new ProcessIDDisplay(init, applicationID).show();
        
    }
}

/*
 *  CVS Log
 *  $Log: ProcessComponent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

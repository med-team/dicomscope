/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;
import viewer.main.*;

/**
 * This class ist the super class for all MouseEventListners and MouseMotionListeners of
 * the ImageCanvas.
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 */
public   class ImageCanvasMouseEventListener implements MouseHandleListener
{
   
	Point windowPoint;
	public static  int numb = 0;
	/**
	 * Contains the current ScreenImageHandler
	 * 
	 * @since 30.04.1999
	 */
	protected ScreenImageHandler screenImageHandler;


	/**
	 * Construct an EventImageCanvas.
	 * 
	 * @param screenImageHandler Contains the current ScreenImageHandler
	 * @since 30.04.1999
	 */
	public ImageCanvasMouseEventListener(ScreenImageHandler screenImageHandler)
	{
	    this.screenImageHandler = screenImageHandler;
	    numb++;
    }
	public void mouseEntered(MouseEvent e)
	{}
	public void mouseExited(MouseEvent e)
	{}
	
	
	/**
	 * Handles the mouse pressed action  and triggers a PopupMenu.
	 * 
	 * @param e MouseEvent
	 * @since 30.04.1999
	 */
	public void mousePressed(MouseEvent e)
	{
         //PopUpTrigger
         showPopup(e); 
    }

    /**
	 * Handles the MouseEvent in the same way as the mouseMove action.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseDragged(MouseEvent e)
    {
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==InputEvent.BUTTON3_MASK) 
        {
            screenImageHandler.setNewWindow(windowPoint, e.getPoint(),true);
            windowPoint = e.getPoint();
        }
        //mouseMoved(e);
    }

    /**
     * Empty action. Should be overwritten form a derived class
     *
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseMoved(MouseEvent e)
    {
        
    }

    /**
	 * Handles the mouse released action and triggers a PopupMenu.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseReleased(MouseEvent e)
    {
         //Popuptrigger
         showPopup(e); 
        
    }

    /**
	 * Handles the mouse clicekd action and triggers a PopupMenu.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    
    public void mouseClicked(MouseEvent e)
    {
         showPopup(e); 
    }

    /**
     * Calls a PopupMenu if the specified MouseEvent is a PopupTrigger.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    
    void showPopup(MouseEvent e)
    {
        if(e.isPopupTrigger()) 
        {
            handlePopup(e);
        }
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==InputEvent.BUTTON3_MASK) 
        {
            windowPoint = e.getPoint();
        }
    } 

   /**
    * Shows a PopupMenu depending form the derived class.
    * 
    * @param e MouseEvent
    * @since 30.04.1999
    */
   
    void handlePopup(MouseEvent e)
   {}
}

/*
 *  CVS Log
 *  $Log: ImageCanvasMouseEventListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

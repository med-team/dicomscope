/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;

import viewer.gui.*;
import viewer.presentation.*;
import viewer.paint.*;
import viewer.main.*;

/**
 * Listener for handling mouse based zooming function
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see GUI.ImageCanvas
 * @see ImageCanvasMouseEventListener
 */
public  class MouseZoomListener implements MouseHandleListener
{
	
	/**
	 * Contains the current ScreenImageHandler
	 * 
	 * @since 30.04.1999
	 */
	protected ScreenImageHandler screenImageHandler;
	    Rectangle2DObject rect;
	boolean moved = false;
	
	/**
	 * Construct an EventImageCanvas.
	 * 
	 * @param screenImageHandler Contains the current ScreenImageHandler.
	 * @since 30.04.1999
	 */
	public MouseZoomListener(ScreenImageHandler screenImageHandler)
	{
        this.screenImageHandler = screenImageHandler;
        screenImageHandler.imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
    }
	public void mouseEntered(MouseEvent e)
	{}
	public void mouseExited(MouseEvent e)
	{}
	public void mouseClicked(MouseEvent e)
	{}
	public void mouseDragged(MouseEvent e)
	{
	}
	public void mousePressed(MouseEvent e)
	{
	    
	}

    /**
     * Handles the mouse move action. If the mouse is moving the new Annotation must be paint.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseMoved(MouseEvent e)
    {
	   if (moved == true) screenImageHandler.paintStructure.setPaintPoint(new Point2D.Float(e.getPoint().x,e.getPoint().y));
        
    }

    /**
     * Handles the mouse released action. If the mouse will be released the new Annotation gets a new point.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseReleased(MouseEvent e)
    {
	    if (!moved)
	    {
	        rect = new Rectangle2DObject();
                screenImageHandler.paintStructure.setCurrentObject(null,true);  
                screenImageHandler.paintStructure.deleteCurrentPaintObject();
                screenImageHandler.paintStructure.oldPaintObject = null;
            
	        screenImageHandler.paintStructure.setCurrentPaintObject(rect);
	        screenImageHandler.setNewPoint(new Point2D.Float(e.getPoint().x,e.getPoint().y));
	        moved = true;
	    }
	    else
	    {
	        rect.setNewPoint(screenImageHandler.paintStructure.getInverseTransformedPoint(new Point2D.Float(e.getPoint().x,e.getPoint().y)));
            screenImageHandler.setNewDisplayArea(rect);
            moved = false;
        }
    }

}

/*
 *  CVS Log
 *  $Log: MouseZoomListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

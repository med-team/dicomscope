/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;

import viewer.main.*;
import viewer.gui.*;
import viewer.presentation.*;

/**
 * This class handles the MouseEvents for moving a PresentationStateTextObject.
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see GUI.ImageCanvas
 * @see ImageCanvasMouseEventListener
 */
public  class ImageCanvasMoveTextListener extends ImageCanvasMouseEventListener
{
	
	
	
	/**
	 * Construct an EventImageCanvas.
	 * 
	 * @param screenImageHandler Contains the current ScreenImageHandler
	 * @since 30.04.1999
	 */
	public ImageCanvasMoveTextListener(ScreenImageHandler screenImageHandler)
	{
	    super(screenImageHandler);
	    screenImageHandler.imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
   }
	   
    /**
     * Moves the PresentationStateTextObject.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseMoved(MouseEvent e)
    {
        screenImageHandler.paintStructure.movePresentationStateTextObject(new Point2D.Float((float)(e.getX()),(float)( e.getY())));
    }
    
    /**
    * Overwrites the function form the superclass.
    * 
    * @param e MouseEvent
    * @since 30.04.1999
     */
    
    public void mouseDragged(MouseEvent e)
    {
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==InputEvent.BUTTON3_MASK) 
        {
            screenImageHandler.setNewWindow(windowPoint, e.getPoint(),true);
            windowPoint = e.getPoint();
        }
        
    }

	/**
	 * Stops moving the PresentationStateTextObject.
	 *
	 * @param e MouseEvent
	 * @since 30.04.1999
	 */
	
    public void mousePressed(MouseEvent e)
    {
        screenImageHandler.stopMoveText();
    }
    
   /**
    * Overwrites the function form the superclass.
    * 
    * @param e MouseEvent
    * @since 30.04.1999
    */
    
   void handlePopup(MouseEvent e)
   {
   }
}

/*
 *  CVS Log
 *  $Log: ImageCanvasMoveTextListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

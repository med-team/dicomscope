/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.paint;


import java.awt.*;
import java.util.*;
import java.awt.geom.*;
/**
 * This class contains methods for creating a bezier curve.
 * <br>
 * These objects can be display or image relative.
 * <br> 
 * These objects can be a shutter.
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PaintObject
 */
public class CurveObject extends PaintObject 
{
    /**
     * Contains the geometic form of the circle.
     * 
     * @since 30.04.1999
     */
    CubicCurve2D.Float thisShape = null;
 
    
    public  int count = 0; 
    Point2D.Float point1;
    Point2D.Float point2;
    Point2D.Float firstPoint;
    Point2D.Float nextPoint;
    
    /**
     * Constucts a new object.
     * 
     * @since 30.04.1999
     */
    public CurveObject()
    {
        super();
        System.out.println("CurveObject");
        
    }
    
        

    
    public CurveObject(boolean isDisplayRelative, int count,Point2D.Float startPoint,Point2D.Float point1,Point2D.Float point2,Point2D.Float nextPoint)
    {
        super();
        System.out.println("CurveObject");
        this.isDisplayRelative = isDisplayRelative;
        this.count = count;
        this.point1 = point1;
        this.point2 = point2;
        this.firstPoint = startPoint;
        this.nextPoint = nextPoint;
         thisShape = new CubicCurve2D.Float();
         thisShape.setCurve(startPoint.x,startPoint.y,startPoint.x,startPoint.y,startPoint.x,startPoint.y,startPoint.x,startPoint.y);
        
         if (count == 1) 
         {
            thisShape.setCurve(  startPoint.x,startPoint.y,nextPoint.x, 
                                nextPoint.y, 
                                nextPoint.x, 
                                nextPoint.y, 
                                nextPoint.x, 
                                nextPoint.y);
         }   
         if (count == 2) 
         {
            thisShape.setCurve(  startPoint.x,startPoint.y,point1.x, 
                                point1.y, 
                                nextPoint.x, 
                                nextPoint.y, 
                                nextPoint.x, 
                                nextPoint.y);
         }   
         if (count == 3) 
         {
            thisShape.setCurve(  startPoint.x,startPoint.y,point1.x, 
                                point1.y, 
                                point2.x, 
                                point2.y, 
                                nextPoint.x, 
                                nextPoint.y);
         }   
        
    }
    
    
    /**
     * Not implemented yet
     * 
     * @since 30.04.1999
     */
    public  PaintObject getMovePaintObject(Point2D.Float nextPoint)
    {
        return null;
    } 
    
    /**
     * Not implemented yet
     * 
     * @since 30.04.1999
     */
    public  void setNewPoint(Point2D.Float newPoint)
    {
    }
    
    /**
     * Gets the Shape of the Circle2DObject.
     * 
     * @return The Shape of the Circle2DObject.
     * @since 30.04.1999
     */

    public  Shape getShape()
    {
        return thisShape;
    }
   /**
    * Draws the Shape of the CurveObject  in the specified Graphics context.
    *
    * @param g The Graphics context in which the Shape should be drawn.
    * @since 30.04.1999
    */
   public  void drawShape(Graphics2D g)
   {
      if ((g!=null) && (thisShape!= null))
      {
         
         g.draw(thisShape);
         
      }
   }
      
    /**
    * Returns a real copy of the CurveObject.
    *
    * @return A real copy of this CurveObject.
    * @since 30.04.1999
    */
    public PaintObject copy()
    {
     return new CurveObject(isDisplayRelative,  count, firstPoint, point1, point2, nextPoint);
        
    }
    /**
     * Not implemented yet
     * 
     * @since 30.04.1999
     */
    public  PaintObject getNewPaintObject()
    {
      return null;
    }


}
/*
 *  CVS Log
 *  $Log: CurveObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.paint;

import java.awt.*;
import java.awt.geom.*;

/**
 * This class contains methods for creating a rectangle.
 * <br>
 * These objects can be display or image relative.
 * <br> 
 * These objects can be a shutter.
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PaintObject
 */
public class Rectangle2DObject extends PaintObject
{
    
    /**
     * Contains the geometic form of the circle.
     * 
     * @since 30.04.1999
     */
    public Rectangle2D.Float thisShape = null;
    
    /**
     * Contructs an new Rectangle2DObject form the specified parameters. 
     * 
     * @param tlhc Top left hand coner of the rectangle
     * @param brhc Bottom right hand coner of the rectangle
     * @since 30.04.1999
     */
    public Rectangle2DObject(Point2D.Float tlhc , Point2D.Float brhc)
    {
        super();
        setNewPoint(tlhc);
        setNewPoint(brhc);
        thisShape = buildRectangle(tlhc,brhc);
    }
    
    /**
     * Gets the Shape of the Rectangle2DObject.
     * 
     * @return The Shape of the Rectangle2DObject.
     * @since 30.04.1999
     */
    public  Shape getShape()
    {
        return thisShape;
    }
    
    
    /**
     * Constructs a new object.
     * 
     * @since 30.04.1999
     */
    public Rectangle2DObject()
    {
        super();
    }
    /**
     * Constucts a new object with the specified parameter.
     * 
     * @param filled true if the object should be filled
     * @param isDisplayRelative true if the object should be display relative.
     * @param isShutter true if the object should be a shutter.
     * @since 30.04.1999
     */
    public Rectangle2DObject(boolean filled , boolean isDisplayRelative,boolean isShutter)
    {
        super();
        this.filled = filled;
        this.isShutter = isShutter;
        
        this.isDisplayRelative = isDisplayRelative;
    }
    
    /**
     * Insert a new point to the objectPoints. You can only insert 2 points.
     * 
     * @param The new point of the circle
     * @since 30.04.1999
     */
    public  void setNewPoint(Point2D.Float newPoint)
    {
      
      objectPoints.add(newPoint);
      if (objectPoints.size() == 1)
      {
         status = STATUS_BEGIN;
         
      }
      else
      {
         status = STATUS_STOP;
         thisShape = buildRectangle(((Point2D.Float)objectPoints.elementAt(0)),((Point2D.Float)objectPoints.elementAt(1)));
      }
    }
    /**
    * Returns the part of the Rectangle2DObject which should be redraw if the Rectangle2DObject will be created. 
    *
    * @param nextPoint The next drawing Point. 
    * @return The part of the PaintObject which should be redraw. 
    * @see PaintObject#getMovePaintObject
    */
    public  PaintObject getMovePaintObject(Point2D.Float nextPoint)
    {
      return new Rectangle2DObject(getCopyPointAt(0), nextPoint);
    } 
    
    /**
    * Returns a real copy of the Rectangle2DObject.
    *
    * @return a real copy of this Rectangle2DObject.
    * @since 30.04.1999
    */
    
    public PaintObject copy()
    {
      Rectangle2DObject returnAnnotation = new Rectangle2DObject();
      for (int i = 0; i< objectPoints.size(); i++)
      {
         returnAnnotation.setNewPoint(((Point2D.Float)objectPoints.elementAt(i)));
      }
      returnAnnotation.setStatus(this.getStatus());
      returnAnnotation.setFilled(filled);
      returnAnnotation.isDisplayRelative = isDisplayRelative;
      
      returnAnnotation.thisShape=buildRectangle(((Point2D.Float)objectPoints.elementAt(0)),((Point2D.Float)objectPoints.elementAt(1)));
      return returnAnnotation;
    }
    /**
    * Draws the Shape of the Rectangle2DObject  in the specified Graphics context.
    *
    * @param g The Graphics context in which the Shape should be drawn.
    * @since 30.04.1999
    */
   public  void drawShape(Graphics2D g)
   {
      if ((g!=null) && (thisShape!= null))
      {
         g.draw(thisShape);
         if (filled) g.fill(thisShape);
      }
   }
    /**
     * Contructs a Rectangle2D form the specified parameters. 
     * 
     * @param tlhc Top left hand coner of the rectangle
     * @param brhc Bottom right hand coner of the rectangle
     * @since 30.04.1999
     */
   public Rectangle2D.Float buildRectangle(Point2D.Float tlhc, Point2D.Float brhc)
   {
        if (tlhc.x >brhc.x)
        {
            float change = tlhc.x;
            tlhc.x = brhc.x;
            brhc.x = change;
        }
        if (tlhc.y >brhc.y)
        {
            float change = tlhc.y;
            tlhc.y = brhc.y;
            brhc.y = change;
        }
         
        float x = tlhc.x;
        float y = tlhc.y;
        float w = brhc.x-x;
        float h = brhc.y-y;
         
        return new Rectangle2D.Float(x,y,w,h);
    }
    /**
    * Returns new Rectangle2DObject with the  same properties as the current object.
    *
    * @return A new Rectangle2DObject with the same properties.
    * @since 30.04.1999
    */
    public  PaintObject getNewPaintObject()
    {
      return new Rectangle2DObject(filled,isDisplayRelative,isShutter);
    }
    
}

/*
 *  CVS Log
 *  $Log: Rectangle2DObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

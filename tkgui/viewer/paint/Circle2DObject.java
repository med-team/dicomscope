/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.paint;

import java.awt.*;
import java.awt.geom.*;

/**
 * This class contains methods for creating a circle.
 * <br>
 * These objects can be display or image relative.
 * <br> 
 * These objects can be a shutter.
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PaintObject
 */

public class Circle2DObject extends PaintObject 
{
    
    float scaleX = 1f;
    float scaleY = 1f;
    /**
     * Contains the geometic form of the circle.
     * 
     * @since 30.04.1999
     */
    public Ellipse2D.Float thisShape = null;
    
    /**
     * Contructs an new Circle2DObject form the specified parameters. 
     * 
     * @param center Center of the circle
     * @param point Point of the border.
     * @since 30.04.1999
     */
    public Circle2DObject(Point2D.Float center, Point2D.Float point, float scaleX, float scaleY)
    {
        super();
        setNewPoint(center);
        setNewPoint(point);
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        
        thisShape = build(center,point,scaleX, scaleX);
    }
    
    /**
     * Contructs an new Circle2DObject form the specified parameters. 
     * 
     * @param center Center of the circle
     * @param point Point of the border.
     * @since 30.04.1999
     */
    public Circle2DObject( float scaleX, float scaleY)
    {
        super();
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        
    }
    
    
    /**
     * Constructs a new object.
     * 
     * @since 30.04.1999
     */
    private Circle2DObject()
    {
        super();
    }
    
    /**
     * Constucts a new object with the specified parameter.
     * 
     * @param filled true if the object should be filled
     * @param isDisplayRelative true if the object should be display relative.
     * @param isShutter true if the object should be a shutter.
     * @since 30.04.1999
     */
    public Circle2DObject(boolean filled , boolean isDisplayRelative,boolean isShutter, float scaleX, float scaleY)
    {
        super();
        this.filled = filled;
        this.isShutter = isShutter;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        
        this.isDisplayRelative = isDisplayRelative;
    }
    
    /**
     * Gets the Shape of the Circle2DObject.
     * 
     * @return The Shape of the Circle2DObject.
     * @since 30.04.1999
     */
    public  Shape getShape()
    {
        return thisShape;
    }
    
    /**
     * Insert a new point to the objectPoints. You can only insert 2 points.
     * 
     * @param The new point of the circle
     * @since 30.04.1999
     */
    public  void setNewPoint(Point2D.Float newPoint)
    {
      
      objectPoints.add(newPoint);
      if (objectPoints.size() == 1)
      {
         status = STATUS_BEGIN;
         
      }
      else
      {
         status = STATUS_STOP;
         thisShape = build(((Point2D.Float)objectPoints.elementAt(0)),((Point2D.Float)objectPoints.elementAt(1)),scaleX, scaleY);
      }
    }
    
    
    /**
    * Returns the part of the Circle2DObject which should be redraw if the Circle2DObject will be created. 
    *
    * @param nextPoint The next drawing Point. 
    * @return The part of the PaintObject which should be redraw. 
    * @see PaintObject#getMovePaintObject
    */
    public  PaintObject getMovePaintObject(Point2D.Float nextPoint)
    {
      return new Circle2DObject(getCopyPointAt(0), nextPoint, scaleX, scaleY);
    } 
    
    /**
    * Returns a real copy of the Circle2DObject.
    *
    * @return a real copy of this Circle2DObject.
    * @since 30.04.1999
    */
    public PaintObject copy()
    {
      Circle2DObject returnObject = new Circle2DObject(scaleX, scaleY);
      for (int i = 0; i< objectPoints.size(); i++)
      {
         returnObject.setNewPoint(((Point2D.Float)objectPoints.elementAt(i)));
      }
      returnObject.setStatus(this.getStatus());
      returnObject.setFilled(filled);
      returnObject.isDisplayRelative = isDisplayRelative;
      
      returnObject.thisShape=build(((Point2D.Float)objectPoints.elementAt(0)),((Point2D.Float)objectPoints.elementAt(1)),scaleX, scaleY);
      return returnObject;
    }
    
    /**
    * Draws the Shape of the Circle2DObject  in the specified Graphics context.
    *
    * @param g The Graphics context in which the Shape should be drawn.
    * @since 30.04.1999
    */
   public  void drawShape(Graphics2D g)
   {
      if ((g!=null) && (thisShape!= null))
      {
         g.draw(thisShape);
         if (filled) g.fill(thisShape);
      }
   }
    /**
     * Contructs a cirular Ellipse2D form the specified parameters. 
     * 
     * @param center Center of the circle
     * @param point Point of the border.
     * @since 30.04.1999
     */
   public Ellipse2D.Float build(Point2D.Float center, Point2D.Float point, float scaleX, float scaleY)
   {
         
        float radius = (float)center.distance(point);
        float x = center.x-radius;
        float y = center.y-radius;
        float w = 2*radius;
        float h = 2*radius;
         
        return new Ellipse2D.Float(x,y,w,h);
    }
    /**
    * Returns new Circle2DObject with the  same properties as the current object.
    *
    * @return A new Circle2DObject with the same properties.
    * @since 30.04.1999
    */
    public  PaintObject getNewPaintObject()
    {
      return new Circle2DObject(filled,isDisplayRelative,isShutter, scaleX, scaleY);
    }


}

/*
 *  CVS Log
 *  $Log: Circle2DObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.paint;

import J2Ci.*;

import java.awt.*;
import java.util.*;
import java.awt.geom.*;

/**
 * This class contains methods for creating a polyline.
 * <br>
 * These objects can be display or image relative.
 * <br> 
 * These objects can be a shutter.
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PaintObject
 */

public class InterpolatedObject extends PaintObject 
{
    /**
     * Contains the geometic form of the polyline.
     * 
     * @since 30.04.1999
     */
    GeneralPath thisShape = null;
    /**
     * Constructs a new object.
     * 
     * @since 30.04.1999
     */
    public InterpolatedObject()
    {
        super();
        
    }
    /**
     * Constucts a new object with the specified parameter.
     * 
     * @param filled true if the object should be filled
     * @param isDisplayRelative true if the object should be display relative.
     * @param isShutter true if the object should be a shutter.
     * @since 30.04.1999
     */
    public InterpolatedObject(boolean filled, boolean isDisplayRelative,boolean isShutter)
    {
        super();
        this.filled = filled;
        this.isShutter = isShutter;
        this.isDisplayRelative = isDisplayRelative;
    }
    
        

    /**
     * Constucts a new object form the specified parameter
     * 
     * @param anPolylineObject Constructor copies the specified object.
     * @param isDisplayRelative true if the object should be display relative.
     * @param isShutter true if the object should be a shutter.
     * @since 30.04.1999
     */
    public InterpolatedObject(InterpolatedObject anPolylineObject)
    {
        super();
        for (int i = 0; i < anPolylineObject.getObjectPoints().size(); i++)
        {
            setNewPoint((Point2D.Float) (anPolylineObject.getObjectPoints().elementAt(i)));
        }
        this.setStatus( anPolylineObject.getStatus());
        this.setFilled(anPolylineObject.filled);
    }
    
    
    /**
    * Returns the part of the PolylineObject which should be redraw if the Circle2DObject will be created. 
    *
    * @param nextPoint The next drawing Point. 
    * @return The part of the PaintObject which should be redraw. 
    * @see PaintObject#getMovePaintObject
    */
    
    public  PaintObject getMovePaintObject(Point2D.Float nextPoint)
    {
      InterpolatedObject il = new InterpolatedObject (this);
      il.setNewPoint(nextPoint);
      //return new Line2DObject(getCopyPointAt(objectPoints.size()-1), nextPoint);
       return il;
    } 
    
    /**
     * Insert a new point to the objectPoints. 
     * 
     * @param The new point of the circle
     * @since 30.04.1999
     */
    public  void setNewPoint(Point2D.Float newPoint)
    {
      if (objectPoints.size() == 0)
      {
         status = STATUS_BEGIN;
      }
      
      if (objectPoints.size() == 1) status = STATUS_WORK;
      objectPoints.add(newPoint);
      int size = objectPoints.size();
      Point2D.Double[] pts = new Point2D.Double[size];
      for (int i = 0; i < size; i++)
      {
        Point2D.Float p = (Point2D.Float)objectPoints.elementAt(i);
        pts[i] = new Point2D.Double(p.x,p.y);
      }
      thisShape = GeometryTool.getCubicSpline(pts, 1d);
    }
    /**
     * Gets the Shape of the PolylineObject.
     * 
     * @return The Shape of the Circle2DObject.
     * @since 30.04.1999
     */
    public  Shape getShape()
    {
        return thisShape;
    }
    
    /**
    * Draws the Shape of the Circle2DObject  in the specified Graphics context.
    *
    * @param g The Graphics context in which the Shape should be drawn.
    * @since 30.04.1999
    */
   public  void drawShape(Graphics2D g)
   {
      if ((g!=null) && (thisShape!= null))
      {
         
         g.draw(thisShape);
         if ((filled) && isClosed() && (status == PaintObject.STATUS_STOP)) g.fill(thisShape);
      }
   }
    /**
    * Returns a real copy of the PolylineObject.
    *
    * @return A real copy of this PolylineObject.
    * @since 30.04.1999
    */
      
    public PaintObject copy()
    {
      return  new InterpolatedObject(this);
    }

    /**
    * Returns new PolylineObject with the  same properties as the current object.
    *
    * @return A new PolylineObject with the same properties.
    * @since 30.04.1999
    */
    
    public  PaintObject getNewPaintObject()
    {
      return new InterpolatedObject(filled,isDisplayRelative,isShutter);
    }
    
    
    

}

/*
 *  CVS Log
 *  $Log: InterpolatedObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

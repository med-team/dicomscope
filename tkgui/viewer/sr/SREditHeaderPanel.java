/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;
import  de.microtherapy.tools.text.document.dicom.*;
import  de.microtherapy.tools.text.document.general.*;

import java.util.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.tree.*;

import main.*;
import J2Ci.*;
/**
 * This class contains the GUI vizualizing a Dicom Structured Report 
 * as an HTML document.
 * 
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SREditHeaderPanel extends JPanel 
{
    //Patient
    private JSizedTextField patientBirthDateText;
    private JTextField patientIdText;
    private JTextField nameTextField;
    private JComboBox patientSexCombo ;
     
    //Study
    private JTextField studyDescriptionText ;
    private JSizedTextField studyIdText ;
    private JSizedTextField accessionText ;
    private JTextField referringText;
     
    //Series
    private JSizedTextField seriesNumberText;
    private JTextField seriesDescriptionText ;
     
     //Report
    private JSizedTextField instanceNumberText;
        
    boolean isListenerEnabled = false;
    
    /**
    * Title
    */
    private TitledBorder titledBorder = new TitledBorder("Header Information");
   
    /**
    * Current jDVInterface
    */ 
    private jDVInterface dvi;
    
    /**
    * Size for the Struts
    */ 
    private int strutSize = 5;
    
    /**
    * Fonts for the header
    */
    private Font headerFont;
    
    
    /**
    * Dimension for the label
    */
    private Dimension labelDimension;
    
    
    
    /**
     * Constructor
     * 
     * @param dvi jDVInterface
     */
    public SREditHeaderPanel(jDVInterface dvi)
    {
        this.dvi = dvi;
        setBorder(titledBorder);
        setLayout(new BorderLayout(5,5));
        EditListener editListener = new EditListener();
        
        
        //Patient Name
        nameTextField = new JTextField(30);
        nameTextField.setDocument(new PNDocument());
        nameTextField.addActionListener(editListener);
        nameTextField.addFocusListener(editListener);
        
        //Patient ID
        patientIdText = new JTextField(30);
        patientIdText.setDocument(new LODocument());
        patientIdText.addActionListener(editListener);
        patientIdText.addFocusListener(editListener);
        
        //Patient Birth Date
        patientBirthDateText = new JSizedTextField(10);
        patientBirthDateText.setDocument(new DateDocument());
        patientBirthDateText.addActionListener(editListener);
        patientBirthDateText.addFocusListener(editListener);
        
        //Patient Sex
        String v[] = {"M", "F", "O"};
        patientSexCombo = new JComboBox(v);
        patientSexCombo.addActionListener(editListener);
        patientSexCombo.addFocusListener(editListener);
        
        
        //Study Description
        studyDescriptionText = new JTextField(30);
        studyDescriptionText.setDocument(new LODocument());
        studyDescriptionText.addActionListener(editListener);
        studyDescriptionText.addFocusListener(editListener);
        
        //Stud ID
        studyIdText = new JSizedTextField(10);
        studyIdText.setDocument(new SHDocument());
        studyIdText.addActionListener(editListener);
        studyIdText.addFocusListener(editListener);
        
        //Referring Physicains Name
        referringText = new JTextField(30);
        referringText.setDocument(new PNDocument());
        referringText.addActionListener(editListener);
        referringText.addFocusListener(editListener);
        
        //Accession Number
        accessionText = new JSizedTextField(10);
        accessionText.setDocument(new SHDocument());
        accessionText.addActionListener(editListener);
        accessionText.addFocusListener(editListener);
       
        //Series Description
        seriesDescriptionText = new JTextField(30);
        seriesDescriptionText.setDocument(new LODocument());
        seriesDescriptionText.addActionListener(editListener);
        seriesDescriptionText.addFocusListener(editListener);
        
        
        //Series Number
        seriesNumberText = new JSizedTextField(10);
        seriesNumberText.setDocument(new SHDocument());
        seriesNumberText.addActionListener(editListener);
        seriesNumberText.addFocusListener(editListener);
        
        //Instance Number
        instanceNumberText = new JSizedTextField(10);
        instanceNumberText.setDocument(new ISDocument());
        instanceNumberText.addActionListener(editListener);
        instanceNumberText.addFocusListener(editListener);
        
        
        labelDimension = (new JLabel("Referring Physician ")).getPreferredSize();
        Box  box = new Box(BoxLayout.Y_AXIS);
        add(box, BorderLayout.NORTH);
        
        box.add(getPatientPanel());
        box.add(getStudyPanel());
        box.add(getSeriesPanel());
        box.add(getReportPanel());
        update();
    }
    
    
    /**
    * Updates alle fields from the interface
    */
    private void update()
    {
        //System.err.println("update");
        isListenerEnabled = false;
        jDSRDocument sr = dvi.getCurrentReport();
        nameTextField.setText(sr.getPatientsName());
        patientIdText.setText(sr.getPatientID());
        patientSexCombo.setSelectedItem(sr.getPatientsSex());
        patientBirthDateText.setText(sr.getPatientsBirthDate());
        referringText.setText(sr.getReferringPhysiciansName());
        accessionText.setText(sr.getAccessionNumber());
        seriesDescriptionText.setText(sr.getSeriesDescription());
        seriesNumberText.setText(sr.getSeriesNumber());
        
        studyDescriptionText.setText(sr.getStudyDescription());
        studyIdText.setText(sr.getStudyID());
        
       
        instanceNumberText.setText( sr.getInstanceNumber());
        isListenerEnabled = true;
        
    }
    
    /**
    * Changes the values in the interface
    */
    private void changeInterface()
    {
        
        jDSRDocument sr = dvi.getCurrentReport();
	sr.setSpecificCharacterSetType(jDSRE_CharacterSet.CS_Latin1);
        
        //Patient Name
        int status = sr.setPatientsName(nameTextField.getText());
        if (status != 0) System.err.println("Error while setting patients name. Value: " +
                                nameTextField.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Patient Birth date
        status = sr.setPatientsBirthDate(patientBirthDateText.getText());
        if (status != 0) System.err.println("Error while setting patients birth date. Value: " +
                                patientBirthDateText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Patient ID
        sr.setPatientID(patientIdText.getText());
        if (status != 0) System.err.println("Error while setting patients id. Value: " +
                                patientIdText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Patient Sex
        sr.setPatientsSex((String)patientSexCombo.getSelectedItem());
        if (status != 0) System.err.println("Error while setting patients Sex. Value: " +
                                (String)patientSexCombo.getSelectedItem()+ 
                                ",  Status: " + 
                                status);
        
        //Study description
        sr.setStudyDescription(studyDescriptionText.getText());
        if (status != 0) System.err.println("Error while setting study description. Value: " +
                                studyDescriptionText.getText()+ 
                                ",  Status: " + 
                                status);
        
        
        //Study Id
        sr.setStudyID(studyIdText.getText());
        if (status != 0) System.err.println("Error while setting study Id. Value: " +
                                studyIdText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Referring Physician
        sr.setReferringPhysiciansName(referringText.getText());
        if (status != 0) System.err.println("Error while setting referring physicians name. Value: " +
                                referringText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Accession number
        sr.setAccessionNumber(accessionText.getText());
        if (status != 0) System.err.println("Error while setting accession number. Value: " +
                                accessionText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Series Description
        sr.setSeriesDescription(seriesDescriptionText.getText());
        if (status != 0) System.err.println("Error while setting series description. Value: " +
                                seriesDescriptionText.getText()+ 
                                ",  Status: " + 
                                status);
        
        //Series Id
        sr.setSeriesNumber(seriesNumberText.getText());
        if (status != 0) System.err.println("Error while setting series number. Value: " +
                                seriesNumberText.getText()+ 
                                ",  Status: " + 
                                status);
        //Instance Number
        sr.setInstanceNumber(instanceNumberText.getText());
        if (status != 0) System.err.println("Error while setting instance number. Value: " +
                                instanceNumberText.getText()+ 
                                ",  Status: " + 
                                status);
       
       
        
    }
    
    
    public JPanel getPatientPanel()
    {
        JPanel namePanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        namePanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        namePanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel patientLabel = new JLabel("Patient");
        
        
        Font f = patientLabel.getFont();
        headerFont = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        patientLabel.setFont(headerFont);
        namePanel.add(patientLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        namePanel.add(new JSeparator(),  gbc);
        
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        
        JLabel nl = new JLabel("Name");
        nl.setPreferredSize(labelDimension);
        nl.setMinimumSize(labelDimension);
        namePanel.add(nl, gbc);
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        namePanel.add(nameTextField, gbc);
        
        //namePanel.add(Box.createHorizontalStrut(strutSize));
        
        
        
        //add(Box.createVerticalStrut(vStrut));
        
        gbc.fill = GridBagConstraints.NONE;
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth=1;
        gbc.weightx = 0;
        JLabel idl = new JLabel("Identification");
        idl.setPreferredSize(labelDimension);
        namePanel.add(idl, gbc);
        
        //namePanel.add(new JLabel(""));
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.weightx = 1;
        
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        namePanel.add(patientIdText, gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        namePanel.add(new JLabel("Birth Date"), gbc);
        
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = 1;
        //gbc.fill = GridBagConstraints.NONE;
        namePanel.add(patientBirthDateText, gbc);
        namePanel.add(Box.createHorizontalStrut(2*strutSize));
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        namePanel.add(new JLabel("Sex"), gbc);
        namePanel.add(Box.createHorizontalStrut(strutSize));
        
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        namePanel.add(patientSexCombo, gbc);
        
        return namePanel;
    }
    public JPanel getReportPanel()
    {
        JPanel generalPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        generalPanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        generalPanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel patientLabel = new JLabel("Report");
        
        
        Font f = patientLabel.getFont();
        headerFont = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        patientLabel.setFont(headerFont);
        generalPanel.add(patientLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        generalPanel.add(new JSeparator(),  gbc);
        
        //Instance Number Label
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        JLabel l = new JLabel("Number");
        l.setPreferredSize(labelDimension);
        l.setMinimumSize(labelDimension);
        
        generalPanel.add(l, gbc);
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        
        //Instance Number
        //gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=1;
        gbc.weightx = 1;
        generalPanel.add(instanceNumberText, gbc);
        //Instance Number Label
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 0;
        generalPanel.add(new JLabel("   "), gbc);
        
        
        return generalPanel;
    }
    
    public JPanel getSeriesPanel()
    {
        JPanel returnPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        returnPanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        returnPanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel headerLabel = new JLabel("Series");
        
        
        Font f = headerLabel.getFont();
        f = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        headerLabel.setFont(headerFont);
        returnPanel.add(headerLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        returnPanel.add(new JSeparator(),  gbc);
        
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        JLabel desLabel = new JLabel("Description");
        desLabel.setPreferredSize(labelDimension);
        desLabel.setMinimumSize(labelDimension);
        returnPanel.add(desLabel, gbc);
        
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        returnPanel.add(seriesDescriptionText, gbc);
        
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        returnPanel.add(new JLabel("Number"), gbc);
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        //gbc.fill = GridBagConstraints.NONE;
        
        returnPanel.add(seriesNumberText, gbc);
        
        return returnPanel;
    }
    public JPanel getStudyPanel()
    {
        JPanel returnPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        returnPanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        returnPanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel headerLabel = new JLabel("Study");
        
        
        Font f = headerLabel.getFont();
        f = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        headerLabel.setFont(headerFont);
        returnPanel.add(headerLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        returnPanel.add(new JSeparator(),  gbc);
        
        //Description Label
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        gbc.insets = new Insets(0,0,0,0);
        JLabel desLabel = new JLabel("Description");
        desLabel.setPreferredSize(labelDimension);
        desLabel.setMinimumSize(labelDimension);
        returnPanel.add(desLabel, gbc);
        
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        
        //Description
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        returnPanel.add(studyDescriptionText, gbc);
        
        
        
        //Id Label
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        returnPanel.add(new JLabel("Identification"), gbc);
        
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        
        //Id
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        returnPanel.add(studyIdText, gbc);
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        gbc.insets = new Insets(0,0,0,0);
        
        
        
        returnPanel.add(new JLabel("Referring Physician "), gbc);
        
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        returnPanel.add(referringText, gbc);
        
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        returnPanel.add(new JLabel("Accession Number"), gbc);
        returnPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        //gbc.fill = GridBagConstraints.NONE;
        
        returnPanel.add(accessionText, gbc);
        
        return returnPanel;
    }
    
    
    public class EditListener implements ActionListener, FocusListener
    {
        
        
        public void actionPerformed(ActionEvent e)
        {
             if (isListenerEnabled)changeInterface();
        }
        public void focusGained(FocusEvent e)
        {
            
        }
        public void focusLost(FocusEvent e)
        {
             if (isListenerEnabled)changeInterface();
        }
        
    }
   /* 
   public JPanel getGeneralPanel()
    {
        JPanel generalPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        generalPanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        generalPanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel patientLabel = new JLabel("General");
        
        
        Font f = patientLabel.getFont();
        headerFont = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        patientLabel.setFont(headerFont);
        generalPanel.add(patientLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        generalPanel.add(new JSeparator(),  gbc);
        
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        generalPanel.add(new JLabel("Referring Physician"), gbc);
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=1;
        gbc.weightx = 1;
        TextField referringTextField = new TextField(30);
        referringTextField.setText(sr.getReferringPhysiciansName());
        generalPanel.add(referringTextField, gbc);
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        generalPanel.add(new JLabel("Accession Number"), gbc);
        
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        //gbc.fill = GridBagConstraints.NONE;
        TextField accessionField = new TextField(10);
        accessionField.setText(sr.getAccessionNumber());
        
        generalPanel.add(accessionField, gbc);
        
        
        //add(Box.createVerticalStrut(vStrut));
        
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth=1;
        generalPanel.add(new JLabel("Series description"));
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth=1;
        gbc.weightx = 1;
        
        TextField seriesDescrptionTextField = new TextField(30);
        seriesDescrptionTextField.setText(sr.getSeriesDescription());
        gbc.fill = GridBagConstraints.HORIZONTAL;
        generalPanel.add(seriesDescrptionTextField, gbc);
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        generalPanel.add(new JLabel("Series ID"), gbc);
        
        
        generalPanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        TextField seriesIDTextField = new TextField(10);
        
        generalPanel.add(seriesIDTextField, gbc);
        
        return generalPanel;
    }
    
    public JPanel getPatientPanel()
    {
        JPanel namePanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        namePanel.setBorder(new EtchedBorder());
        GridBagConstraints gbc = new GridBagConstraints();
        namePanel.setLayout(gbl);
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        JLabel patientLabel = new JLabel("Patient");
        
        
        Font f = patientLabel.getFont();
        headerFont = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
        patientLabel.setFont(headerFont);
        namePanel.add(patientLabel, gbc);
        
        gbc.anchor= GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0,0,10,0);
        namePanel.add(new JSeparator(),  gbc);
        
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        namePanel.add(new JLabel("Name"), gbc);
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth=1;
        gbc.weightx = 1;
        TextField nameTextField = new TextField(30);
        nameTextField.setText(sr.getPatientsName());
        namePanel.add(nameTextField, gbc);
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        namePanel.add(new JLabel("Birth Date"), gbc);
        
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        //gbc.fill = GridBagConstraints.NONE;
        TextField dateField = new TextField(10);
        namePanel.add(dateField, gbc);
        
        
        //add(Box.createVerticalStrut(vStrut));
        
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth=1;
        namePanel.add(new JLabel("Identification"));
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth=1;
        gbc.weightx = 1;
        
        TextField idTextField = new TextField(30);
        idTextField.setText(sr.getPatientID());
        gbc.fill = GridBagConstraints.HORIZONTAL;
        namePanel.add(idTextField, gbc);
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        namePanel.add(new JLabel("Sex"), gbc);
        
        
        namePanel.add(Box.createHorizontalStrut(strutSize));
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        String v[] = {"M", "F"};
        JComboBox sexField = new JComboBox(v);
        namePanel.add(sexField, gbc);
        
        return namePanel;
    }
    */
    public class JSizedTextField extends JTextField
    {
        public JSizedTextField(int length)
        {
            super(length);
        }
        public Dimension getMinimumSize()
        {
            Dimension d = super.getMinimumSize();
            return new Dimension(100,(int)d.getHeight());
        }

        public Dimension getMaximumSize()
        {
            Dimension d = super.getMaximumSize();
            return new Dimension(100,(int)d.getHeight());
        }

        
    }
}
/*
 *  CVS Log
 *  $Log: SREditHeaderPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;

import java.util.*;
import viewer.gui.*;
import java.net.*;
import viewer.sr.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.tree.*;

import main.*;
import J2Ci.*;
/**
 * This class contains the GUI vizualizing a Dicom Structured Report 
 * as an HTML document.
 * 
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SREditPanel extends JPanel 
{
    
    
    /**
    * Tilted Boder containing the name of the SR Object
    */
    //private TitledBorder titledBorder = new TitledBorder("Structured Report");
    
    /**
    * Current DVI Interface
    */
    private jDVInterface dvi;
    JSplitPane split ;
    SRTreeElement srTreeElement;
    SREditTreePanel treePanel;
    MainImageViewerPanel mainView;
    String mode = "";
    public static final String HACKER_MODE = "Hacker Mode";
    /**
     * Constructor
     * 
     * @param dvi jDVInterface
     */
    public SREditPanel(jDVInterface dvi, MainImageViewerPanel mainView  )
    {
        this.dvi = dvi;
        this.mainView = mainView;
        setBorder(new TitledBorder("Structured Report Editor " + mode));
        setLayout(new BorderLayout());
        
        
        JPanel buttonPanel = new JPanel();
        //buttonPanel.setBorder(new EtchedBorder());
        add(buttonPanel, BorderLayout.NORTH);
        
        GuiComponents gui = GuiComponents.getInstance();
    
        JButton okButton = gui.srOkButton;
        okButton.setToolTipText("Back to Viewer");
        okButton.addActionListener(new CloseAction());
        buttonPanel.add(okButton);
        
        
        
        
    }
    public void update(boolean hackermode)
    {
        if (hackermode) 
        {
            mode = HACKER_MODE;
        }
        else 
        {
            mode = "";
		    Controller.instance().fireStatus(new SignedStatusEvent(this,
		    SignedStatusEvent.LOADSR,
		    dvi.getCombinedImagePStateSignatureStatus(), 
		    dvi.getCurrentSignatureStatus(jDVPSObjectType.DVPSS_structuredReport), false));
            
        }
        setBorder(new TitledBorder("Structured Report Editor " + mode));
        if (split != null) remove(split);
        //CenterPanel
        jDSRDocument sr = dvi.getCurrentReport();
        String name = jDSRE_DocumentType.getDocumentTypeName(sr.getDocumentType());
        
        split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setOneTouchExpandable(true);
        treePanel = new SREditTreePanel(dvi,name, new SRTreeSelectionListener());
        split.setTopComponent(new JScrollPane(treePanel));
        split.setDividerLocation(300);
        add(split, BorderLayout.CENTER);
        split.setBottomComponent(new SREditHeaderPanel(dvi));
    }
    public class SRTreeSelectionListener implements TreeSelectionListener
    {
        public void valueChanged(TreeSelectionEvent e)
        {
            
            srTreeElement =(SRTreeElement) ((DefaultMutableTreeNode)e.getPath().getLastPathComponent()).getUserObject();
            int div = split.getDividerLocation();
                jDSRDocument sr = dvi.getCurrentReport();
                jDSRDocumentTree documentTree = sr.getTree();
                
            if (srTreeElement.name.equals("Header")) 
            {
                split.setBottomComponent(new SREditHeaderPanel(dvi));
               
            }
            else if(srTreeElement.name.equals("Text"))
            {
                 split.setBottomComponent(new SRTextContentItemEditPanel(documentTree,srTreeElement.srNodeId));
                                                                    
            }
            
            else if(srTreeElement.name.equals("Person Name"))
            {
                 split.setBottomComponent(new SRPNameContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("UID Reference"))
            {
                 split.setBottomComponent(new SRUIDRefContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Date"))
            {
                 split.setBottomComponent(new SRDateContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Time"))
            {
                 split.setBottomComponent(new SRTimeContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Datetime"))
            {
                 split.setBottomComponent(new SRDateTimeContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Container"))
            {
                 split.setBottomComponent(new SRContainerContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Numeric Value"))
            {
                 split.setBottomComponent(new SRNumericValueContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Code"))
            {
                 split.setBottomComponent(new SRCodeValueContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Temporal Coordinates"))
            {
                 split.setBottomComponent(new SRTemporalCoordinateContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Image"))
            {
                 split.setBottomComponent(new SRImageContentItemEditPanel(documentTree,srTreeElement.srNodeId,dvi));
            }
            else if(srTreeElement.name.equals("Waveform"))
            {
                 split.setBottomComponent(new SRWaveformContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            else if(srTreeElement.name.equals("Composite"))
            {
                 split.setBottomComponent(new SRCompositeContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            
            else if(srTreeElement.name.equals("Spatial Coordinates"))
            {
                 split.setBottomComponent(new SRSpatialCoordinateContentItemEditPanel(documentTree,srTreeElement.srNodeId));
            }
            
            
            else if ( srTreeElement.name.equals("by-reference"))
            {
                JPanel p = new JPanel(new BorderLayout());
                p.setBorder (new TitledBorder("by-reference"));
                JPanel buttonPanel = new JPanel();
                
                JButton b = new JButton(new GotoAction(srTreeElement.srNodeId));
                
                buttonPanel.add(b);
                p.add(buttonPanel, BorderLayout.CENTER);
                
                split.setBottomComponent(p);
            }
            else split.setBottomComponent(new JPanel());
            split.setDividerLocation(div);
        }

    }
    /**
    * CloseAction
    */
    public class GotoAction extends AbstractAction
    {
        int nodeId;
        public GotoAction(int nodeId)
        {
            super("Show Target");
            this.nodeId = nodeId;
        }
        public void actionPerformed(ActionEvent e)
        {
            jDSRDocument sr = dvi.getCurrentReport();
            jDSRDocumentTree documentTree = sr.getTree();
            documentTree.gotoNode(nodeId);
            int nextNode = documentTree.getCurrentReferencedNodeID();
            treePanel.gotoSRNode (nextNode);
        }
    }
    
    /**
    * CloseAction
    */
    public class CloseAction extends AbstractAction
    {
        public CloseAction()
        {
            super("Close");
        }
        public void actionPerformed(ActionEvent e)
        {
            mainView.editSRClose();
		   // if ( mode.equals(HACKER_MODE))
		                        Controller.instance().fireStatus(new SignedStatusEvent(this,
		                        SignedStatusEvent.LOADSR,
		                        dvi.getCombinedImagePStateSignatureStatus(), 
		                        dvi.getCurrentSignatureStatus(jDVPSObjectType.DVPSS_structuredReport),false));
            
        }
    }
    
    
}
/*
 *  CVS Log
 *  $Log: SREditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;


import java.awt.*;
import java.awt.event.*;
import  de.microtherapy.tools.text.document.dicom.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.border.*;
import main.*;
import J2Ci.*;
/**
 * This class contains a class
 * for editing an Image Content Item in a SR.
 * @see SRGeneralContentItemEditPanel
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SRImageContentItemEditPanel extends SRGeneralContentItemEditPanel
{
    /**
    *  Content 
    */
     /**
    * Current DVI Interface
    */
    private jDVInterface dvi;
    JTextField imageSOPClassUid;
    JTextField imageSOPInstanceUid;
    JTextField psSOPClassUid;
    JTextField psSOPInstanceUid;
    JPanel  buttonPanel;
    JButton showButton ;
    JButton applyImage;
    JButton applyPs ;
    
    public SRImageContentItemEditPanel(jDSRDocumentTree documentTree,int nodeId,jDVInterface dvi)
    {
        super(documentTree,nodeId,jDSRE_ValueType.VT_Image,SRCode.CONTEXT_GROUP_NAME_IMAGE_REFERENCE);
        this.dvi = dvi;
        updateContentItem();
    }
    
    /**
    * Updates the value from the jDSRDocumentTree.
    */
    private void updateContentItem()
    {
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        jDSRImageValue imageRef = documentTree.getCurrentImageValue();
        buttonPanel.removeAll();
        if (imageRef != null)
        {
                imageSOPClassUid.setText(imageRef.getImageSOPClassName());
                imageSOPInstanceUid.setText(imageRef.getImageSOPInstanceUID());
                psSOPClassUid.setText(imageRef.getPStateSOPClassUID());
                psSOPInstanceUid.setText(imageRef.getPStateSOPInstanceUID());
                buttonPanel.add(showButton);
                
        }
        buttonPanel.add(applyImage);
        buttonPanel.add(applyPs);
        
        isListenerEnabled= true;
    }
    
    /**
    * Changes the value in the jDSRDocumentTree
    */
    private void changeContentItem()
    {
        
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        isListenerEnabled= true;
        
    }
    /**
    * Initialise and returns  a JComponent containting a GUI for editing the 
    * value of the Content Item. 
    * @return JScrollbar with JTextArea containing  the 
    * value of the Content Item.
    */
    public JComponent initContentItemGUI()
    {
        imageSOPClassUid= new JTextField();
        imageSOPInstanceUid= new JTextField();
        psSOPClassUid= new JTextField();
        psSOPInstanceUid= new JTextField();
        imageSOPClassUid.setEditable(false);
        imageSOPInstanceUid.setEditable(false);
        psSOPClassUid.setEditable(false);
        psSOPInstanceUid.setEditable(false);
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBackground(Color.lightGray);
        JPanel textPanel = new JPanel(new BorderLayout(10,10));
        textPanel.setBorder(new EtchedBorder());
        
        textPanel.add(new JLabel(" "), BorderLayout.NORTH);
        textPanel.add(new JLabel(" "), BorderLayout.SOUTH);
        
        JPanel centerPanel = new JPanel (new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        //Description Label
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        
        centerPanel.add(new JLabel("Image SOP Class Name: " ), gbc);
        
        centerPanel.add(Box.createHorizontalStrut(5));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        centerPanel.add(imageSOPClassUid, gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        
        centerPanel.add(new JLabel("Image SOP Instance UID: " ), gbc);
        
        centerPanel.add(Box.createHorizontalStrut(5));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        centerPanel.add(imageSOPInstanceUid, gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        centerPanel.add(new JLabel("PS SOP Instance UID: " ), gbc);
        
        centerPanel.add(Box.createHorizontalStrut(5));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        centerPanel.add(psSOPInstanceUid, gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        showButton = new JButton(new ShowAction());
        applyImage = new JButton(new ApplyImageAction());
        applyPs = new JButton(new ApplyPSAction());
        buttonPanel = new JPanel();
        textPanel.add(centerPanel,BorderLayout.CENTER);
        p.add(textPanel, BorderLayout.NORTH);
        p.add(buttonPanel, BorderLayout.SOUTH);
        
        return p;
    }
    public class ShowAction extends AbstractAction
    {
        public ShowAction()
        {
            super("Show");
            putValue(AbstractAction.SHORT_DESCRIPTION, "Show"); 
        }
        public void actionPerformed(ActionEvent e)
        {
            documentTree.gotoNode(getNodeId());
                jDSRImageValue imageRef = documentTree.getCurrentImageValue();
                if (imageRef != null)
                {
                        
                        int frames[] = new int[imageRef.getNumberOfFrames()];
                        for (int i = 0; i < frames.length; i++)
                        {
                            jIntByRef value = new jIntByRef();
                            imageRef.getFrame(i, value);
                            frames[i] = value.value;
                        }
                        
                        if (psSOPInstanceUid.getText() == null||psSOPInstanceUid.getText().trim().equals(""))
                        {
                            Controller.instance().fireEvent(new DbActionEvent(
                                                        this,
                                                        DbActionEvent.LOAD_IMAGE_FOR_SR,
                                                        imageRef.getImageSOPClassUID(),
                                                        imageRef.getImageSOPInstanceUID(),
                                                        frames));
                        }
                        else
                        {
                            Controller.instance().fireEvent(new DbActionEvent(
                                                        this,
                                                        DbActionEvent.LOAD_IMAGE_FOR_SR,
                                                        imageRef.getImageSOPClassUID(),
                                                        imageRef.getImageSOPInstanceUID(),
                                                        imageRef.getPStateSOPClassUID(),
                                                        imageRef.getPStateSOPInstanceUID(),frames));
                            
                        }
                }
            
        }
    }
    public class ApplyImageAction extends AbstractAction
    {
        public ApplyImageAction()
        {
            super("Set Image");
            putValue(AbstractAction.SHORT_DESCRIPTION, "Set Reference to current Image"); 
        }
        public void actionPerformed(ActionEvent e)
        {
            jDVPresentationState ps = dvi.getCurrentPState();
            if (ps != null)
            {
                String instanceSOP = ps.getAttachedImageSOPInstanceUID(); 
                String imageSOP = ps.getAttachedImageSOPClassUID();
                if (instanceSOP != null &&imageSOP != null)
                {
                    documentTree.gotoNode(getNodeId());
                    jDSRImageValue imageRef = documentTree.getCurrentImageValue();
                    imageRef.clearFrameList();
                    imageRef.setPStateReference(null, null);
                    imageRef.setImageReference(imageSOP, instanceSOP);
                    updateContentItem();
                }
                else JOptionPane.showMessageDialog(null,"Please load Image");
            }
            else JOptionPane.showMessageDialog(null,"Please load Image");
             
        }
    }
    public class ApplyPSAction extends AbstractAction
    {
        public ApplyPSAction()
        {
            super("Set PState");
            putValue(AbstractAction.SHORT_DESCRIPTION, "Set Reference to current Presentation State"); 
        }
        public void actionPerformed(ActionEvent e)
        {
            jDVPresentationState ps = dvi.getCurrentPState();
            if (ps != null)
            {
                String instanceSOP = ps.getAttachedImageSOPInstanceUID(); 
                String imageSOP = ps.getAttachedImageSOPClassUID();
                String instancePSSOP = ps.getInstanceUID(); 
                String psSOP = ps.getSOPClassUID();
                if (instanceSOP != null &&imageSOP != null && instancePSSOP!= null &&psSOP != null)
                {
                    Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.SAVE_DB,false));
                    documentTree.gotoNode(getNodeId());
                    jDSRImageValue imageRef = documentTree.getCurrentImageValue();
                    ps = dvi.getCurrentPState();
                    instancePSSOP = ps.getInstanceUID(); 
                    psSOP = ps.getSOPClassUID();
                    imageRef.clearFrameList();
                    imageRef.setImageReference(imageSOP, instanceSOP);
                    imageRef.setPStateReference(psSOP, instancePSSOP);
                    updateContentItem();
                }
                else JOptionPane.showMessageDialog(null,"Please load Presentation State");
            }
            else JOptionPane.showMessageDialog(null,"Please load Presentation State");
             
        }
            
        
    }
    
}
/*
 *  CVS Log
 *  $Log: SRImageContentItemEditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 10:17:26 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
*/

package viewer.main;

import viewer.gui.*;
import main.*;

import viewer.controller.*;
import viewer.paint.*;
import viewer.presentation.*;
import J2Ci.*;

import java.awt.font.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * This class manages the the image viewing and processing part of 
 * DICOMScope. This class contains all important objects of the GUI
 * and the data level.
 * <BR>
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 */

public class ScreenImageHandler implements MainListener
{

	private int applyTo = jDVPSObjectApplicability.DVPSB_currentFrame;
	private Hashtable actionTable;
	boolean convertAnnotationScreenRelative = false;
	public StatusInformation statusInformation= new StatusInformation();
	/**
	 * Contains the current PresentationStateGraphicsHandler.
	 * 
	 * @since 30.04.1999
	 */
	public PresentationStateGraphicsHandler presentationStateGraphicsHandler;
	
    /**
     * Contains the current ImageCanvas. This is the image drawing part of the GUI.
     * 
     * @since 30.04.1999
     */
    public ImageCanvas imageCanvas;
	
	
    public JPopupMenu popup;
    /**
     * Contains the main GUI object.
     * 
     * @since 30.04.1999
     */
    public MainImageViewerPanel mainImageViewerPanel;
	
	
	/**
	 * MouseListener and MouseMotionListener which handles the MouseEvents.
	 * 
	 * @since 30.04.1999
	 */
	public MouseHandleListener mouseHandleListener = null;
    
    
    private double scaling= 3;
    /**
     * Object which transforms the visible part of the image to device space
     * 
     * @since 30.04.1999
     */
    public TransformationConverter transformationConverter;
    
    /**
     * Contains the visible part of the image in device space.
     * 
     * @since 30.04.1999
     */ 
    BufferedImage bufferedScreenImage = null;

    /**
     * Contains the size of the display.
     * 
     * @since 30.04.1999
     */
    Dimension screenSize;
    

	/**
	 * Contains the current PaintStructure
	 * 
	 * @since 30.04.1999
	 */
	public PaintStructure paintStructure ;

    boolean clear = true;
    /**
     * Builds the screenImageHandler.
     * 
     * @param mainImageViewerPanel Contains the main GUI object
     * @param presentationStateGraphicsHandler Contains the current ScreenImageHandler.
     * @since 30.04.1999
     */
    public  ScreenImageHandler(MainImageViewerPanel mainImageViewerPanel, PresentationStateGraphicsHandler  presentationStateGraphicsHandler, Hashtable config)
    {
        this.mainImageViewerPanel = mainImageViewerPanel;
        this.presentationStateGraphicsHandler= presentationStateGraphicsHandler;
        Controller.instance().addMainListener(this);
        //Builds the new image in User Space.
        screenSize = new Dimension(0,0);
        createActionTable();
        setConfiguration(config, true);
    }
    
    
    /**
     * This function draws the visible part of the image in the 
     * specified Graphics2D object.
     * 
     * @param g2 Specifies the Graphics object.
     * @since 30.04.1999
     */
    public void drawScreenImage(Graphics2D g2)
    {
            g2.setBackground(Color.black);
            g2.setColor(Color.black);
           //imageCanvas.setColor(Color.black);
            imageCanvas.setBackground(Color.black);
            if (clear) 
            {
                g2.clearRect(0,0,(int)imageCanvas.getSize().getWidth(),(int)imageCanvas.getSize().getHeight());
                if ((presentationStateGraphicsHandler.havePresentationState))
                {
                    if (!(screenSize.getSize().equals(imageCanvas.getSize())))
                    {
                        presentationStateGraphicsHandler.setScreenSize(imageCanvas.getSize());
                    }
                }
            }
            else
            {
                if ((presentationStateGraphicsHandler.havePresentationState))
                {
                    if (!(screenSize.getSize().equals(imageCanvas.getSize())))
                    {
                        presentationStateGraphicsHandler.setScreenSize(imageCanvas.getSize());
                        if (!clear)setNewImage();
                    }
                    //Drawing the bufferedImage 
                    if (bufferedScreenImage != null)
                    {
                        g2.drawImage(bufferedScreenImage,0,0, imageCanvas);
                        paintStructure.aff = transformationConverter.getTransformation();
                        paintStructure.drawState(g2);
                    }
                    else
                    {
                        g2.clearRect(0,0,(int)screenSize.getSize().getWidth(),(int)screenSize.getSize().getHeight());
                    }
	        }
	    }
        
    }//drawScreenImage
        
        
        
	/**
	 * Resets the all objects of the ScreenImageHandler.
	 *
	 * @since 30.04.1999
	 */
	public void resetState()
	{
        //removes the active event listeners
        if (mouseHandleListener != null)
        {
            imageCanvas.removeMouseListener(mouseHandleListener);
            imageCanvas.removeMouseMotionListener(mouseHandleListener);
               
        }  
        mouseHandleListener = new ImageCanvasMouseEventListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
	    popup = null;
	    
	    //set cursor to default
	    imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	    
	    //resets the state of the paintStructure
	    if (paintStructure!=null)paintStructure.reset();
	    
	    //reset the state of the presentationStateGraphicsHandler
	    if (presentationStateGraphicsHandler!=null)presentationStateGraphicsHandler.reset();
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Image Viewer"));
	    
	    
	    //mainImageViewerPanel.annotationPanel.setActiveButton(null);
	}

	/**
	 * Builds the visible part of the image in the bufferedScreenImage.
	 * 
	 * @param first if true builds the bufferedScreenImage at first time.
	 * @param loadNewPixels if ture - load the pixels form the c++ part
	 * @param newScrollbarValue if true sets the scrollbar properties of the PanleScollImage.
	 * @param resetState if true resets all objects to their initial state.
	 * @since 30.04.1999
	 */
	    
	public void buildImageBuffer(boolean first,boolean loadNewPixels,boolean newScrollbarValue, boolean resetState, boolean newBackground)
	{
        if (resetState)resetState();
        if (!imageCanvas.getSize().equals(new Dimension(0,0)))
        {
        
            
            
                
            if (! first) 
            {
                
                transformationConverter.drawInDeviceSpace(  bufferedScreenImage,
                                                            presentationStateGraphicsHandler.getZoomValue(),
                                                            loadNewPixels, 
                                                            newBackground, applyTo);
                                                            
            }
            else 
            {
                DisplayArea imageArea = presentationStateGraphicsHandler.getDisplayArea();
                
                transformationConverter.drawInDeviceSpaceFirst(  bufferedScreenImage,
                                                        presentationStateGraphicsHandler.getZoomValue(),
                                                        loadNewPixels, applyTo);
                DisplayArea newArea = presentationStateGraphicsHandler.getDisplayArea();
                
                if (convertAnnotationScreenRelative )presentationStateGraphicsHandler.convertDisplayedAreas(imageArea,newArea);
                convertAnnotationScreenRelative = false;
            }
            
            
            presentationStateGraphicsHandler.drawPaintStructure(bufferedScreenImage,transformationConverter.getTransformation(), transformationConverter.getOverlayTransformation(),false,8, false);
                
                
	        mainImageViewerPanel.setNewScrollbarValues(     transformationConverter.getScollValueWidth(), 
	                                                        transformationConverter.getScollValueHeight(),
	                                                        transformationConverter.getScrollbarValue());
                
        
           
            
            imageCanvas.repaint();
        }
	    
	}
	
	
    /**
     * Sets the ImageCanvas.
     * 
     * @param imageCanvas New ImageCanas.
     * @since 30.04.1999
     */
    public void setImageCanvas(ImageCanvas imageCanvas)
    {
        this.imageCanvas = imageCanvas;
        
    }
    
    
    
    
    /**
     * Sets a MouseEventListenerEditAnnotation the the imageCanvas.
     * 
     * @since 30.04.1999
     */
    public void setEditAction()
    {
        resetState();        
        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Select Annotation with Mouse"));
        
        mouseHandleListener = new ImageCanvasEditListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
        
    }  
   /**
    * Initialize objects for drawing a new Annotation. The paintStructure will be reset and
    * the ImageCanvasNewListener will be set.
    * 
    * @param theAnnotation The new Annotation.
    * @since 30.04.1999
    */
    public void setNewPaintObjectAction(PaintObject thePaintObject)
    {
        paintStructure.setCurrentObject(null,true);  
        paintStructure.deleteCurrentPaintObject();
        if (mouseHandleListener != null)
        {
            imageCanvas.removeMouseListener(mouseHandleListener);
            imageCanvas.removeMouseMotionListener(mouseHandleListener);
               
        }   
        paintStructure.oldPaintObject = null;
        
        paintStructure.setCurrentPaintObject(thePaintObject);
        
        mouseHandleListener = new ImageCanvasNewListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
          
    }//setNewAnnotationAction
    public void setNewDisplayArea(int tlhcX,int  tlhcY,int  brhcX,int brhcY)
    {
        
        presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode(), tlhcX, tlhcY, brhcX, brhcY, presentationStateGraphicsHandler.calculateZoomValue(new DisplayArea(tlhcX, tlhcY, brhcX, brhcY)), applyTo);
	    presentationStateGraphicsHandler.calculateFirstZoomValue();
	    Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ZOOMINPUT, new Double(presentationStateGraphicsHandler.getZoomValue())));
	    
	    buildImageBuffer(true,false,true,true, true);
	   statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
            
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
        
    }
	public void setNewDisplayArea(Rectangle2DObject rect)
	{
        Point.Float point0 = rect.getPoint(0);
        Point.Float point1 = rect.getPoint(1);
        if ((Math.abs(point0.x-point1.x)>2 )&&(Math.abs(point0.y-point1.y)>2 ))
        {
            setNewDisplayArea((int)point0.x, (int)point0.y,(int)point1.x,(int)point1.y);
        }
        else      buildImageBuffer(true,false,true,true, true);
	}
	
    /**
     * Sets the image on a new position of the display.  
     * 
     * @param newHeight Specifies the new virtualTLHC.y of the image. 
     * @since 30.04.1999
     * @see TransformationConverter
     */
    public void setNewImagePartHeight(int newHeight)
    {
	if (transformationConverter != null)
        {    resetState();
        
            transformationConverter.setVirtualTHLCHeight(newHeight);
                transformationConverter.drawInDeviceSpace(bufferedScreenImage,false,true, applyTo);
	    presentationStateGraphicsHandler.drawPaintStructure(bufferedScreenImage,transformationConverter.getTransformation(),transformationConverter.getOverlayTransformation(),false,8, false);
	    //mainImageViewerPanel.funktionBasisTabPanel.presentationStatePanel.setValue();
            imageCanvas.repaint();
	   statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
   
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
        }
    }

    /**
     * Sets the image on a new position of the display.  
     * 
     * @param newWidth Specifies the new virtualTLHC.x of the image. 
     * @since 30.04.1999
     * @see TransformationConverter
     */
    public void setNewImagePartWidth(int newWidth)
    {
        if (transformationConverter != null)
        {
            resetState();
            transformationConverter.setVirtualTHLCWidth(newWidth);
            transformationConverter.drawInDeviceSpace(bufferedScreenImage,false,true,applyTo);
	    presentationStateGraphicsHandler.drawPaintStructure(bufferedScreenImage,transformationConverter.getTransformation(),transformationConverter.getOverlayTransformation(),false,8, false);

	    //mainImageViewerPanel.funktionBasisTabPanel.presentationStatePanel.setValue();
            imageCanvas.repaint();
	   statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());

		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
        }
    }

/////////////////////////////////////////////////////////////////////////
//windowing
////////////////////////////////////////////////////////////////////////
	/**
	 * Sets a new center value for the image and draws the image.
	 *
	 * @param center the new center value.
	 * @since 30.04.1999
	 */
	public void setNewCenter(double center)
	{
	    double actualWidth = 0;
	    //System.out.println("Screen bin ich im center *: " +center);
        if (presentationStateGraphicsHandler.ps.haveActiveVOIWindow())
        {
            jDoubleByRef widthValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getCurrentWindowWidth(widthValue);
            actualWidth = (widthValue.value);
        }   
        else if (!presentationStateGraphicsHandler.ps.haveActiveVOILUT())
        {
            jDoubleByRef minValue = new jDoubleByRef();
            jDoubleByRef maxValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getImageMinMaxPixelRange(minValue,maxValue);
            actualWidth = ((maxValue.value-minValue.value+1));
        }
	    setNewWindow(center,actualWidth,true );
	}
	
	/**
	 * Sets a new widht value for the image and draws the image.
	 *
	 * @param width the new width value.
	 * @since 30.04.1999
	 */
	public void setNewWidth(double width)
	{
	    double actualCenter =0;
	    //System.out.println("Screen bin ich im Width *: " +width);
        if (presentationStateGraphicsHandler.ps.haveActiveVOIWindow())
        {
            jDoubleByRef centerValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getCurrentWindowCenter(centerValue);
            actualCenter = (centerValue.value);
        }   
        else if (!presentationStateGraphicsHandler.ps.haveActiveVOILUT())
        {
            jDoubleByRef minValue = new jDoubleByRef();
            jDoubleByRef maxValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getImageMinMaxPixelRange(minValue,maxValue);
            actualCenter = ((minValue.value+(maxValue.value-minValue.value+1)/2));
        }
	    
	    setNewWindow(actualCenter, width,true);
	}
	
	public void initVOI()
	{
            double width = getAutoWidth();
            double center = getAutoCenter();
            //gets the maximum pixel range
            scaling = (center+width/2)/500;
            
            
            if (presentationStateGraphicsHandler.ps.haveActiveVOILUT())
            {
	            String name = presentationStateGraphicsHandler.ps.getCurrentVOIDescription();
	            if (name == null) name = new String("VOI/LUT " );
	            statusInformation.setLUT(name);
		    Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT1,DSComponentType.VIEWER,statusInformation.getWinInfo()));
	            Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_VOILUT));
                
            }
            else 
            {
                if (presentationStateGraphicsHandler.ps.haveActiveVOIWindow())
                {
                    jDoubleByRef centerValue = new jDoubleByRef();
                    jDoubleByRef widthValue = new jDoubleByRef();
                    presentationStateGraphicsHandler.ps.getCurrentWindowCenter(centerValue);
                    presentationStateGraphicsHandler.ps.getCurrentWindowWidth(widthValue);
                    
                    center = centerValue.value;
                    width = widthValue.value;
                    presentationStateGraphicsHandler.ps.setVOIWindow( center, width ,"",applyTo);
                }
                else
                {
                    
                    presentationStateGraphicsHandler.ps.setVOIWindow( center, width ,"",applyTo);
	            
	        }
	        statusInformation.setWindow(center, width);
                Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT1,DSComponentType.VIEWER,statusInformation.getWinInfo()));
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_WINDOW, new Double(width), new Double(center)));
    	    
	    }
	}
	/**
	 * Changes the window after a mouse moving action. 
	 * 
	 * @param op Start moving point. 
	 * @param np End moving point.
	 * @since 30.08.1999
	 */
	public void setNewWindow(Point op, Point np, boolean viewImage)
	{
	    
	    double width = Math.ceil((presentationStateGraphicsHandler.getCurrentWindowWidth())+(double)((np.x-op.x)*scaling));
	    double center = Math.ceil((presentationStateGraphicsHandler.getCurrentWindowCenter())-(double)((np.y-op.y)*scaling));
	    setNewWindow( center,width,viewImage);
	    
	}
	
	/**
	 * Gets the width of the pixel range in the image
	 * 
	 * @since 30.08.1999
	 */
    public double getAutoWidth()
    {
            jDoubleByRef minValue = new jDoubleByRef();
            jDoubleByRef maxValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getImageMinMaxPixelValue(minValue, maxValue);
            return (maxValue.value-minValue.value+1);
    }
	
	/**
	 * Gets the center of the pixel range in the image
	 * 
	 * @since 30.08.1999
	 */
    public double getAutoCenter()
    {
            jDoubleByRef minValue = new jDoubleByRef();
            jDoubleByRef maxValue = new jDoubleByRef();
            presentationStateGraphicsHandler.ps.getImageMinMaxPixelValue(minValue, maxValue);
            
            return (minValue.value+(maxValue.value-minValue.value+1)/2);
    }
	public void setVOILUT(int index)
	{
	    presentationStateGraphicsHandler.ps.setVOILUTFromImage(index,applyTo);
	    
	    String name = presentationStateGraphicsHandler.ps.getDescriptionOfVOILUTsInImage(index);
	    if (name == null) name = new String("VOI/LUT " + index);
	    statusInformation.setLUT(name);
		    Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT1,DSComponentType.VIEWER,statusInformation.getWinInfo()));
	            Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_VOILUT));
        buildImageBuffer(false,true, false,false, false);
    }
	/**
	 * Sets the VOI window with the specified Index
	 * 
	 * @since 30.08.1999
	 */
	public void setVOIWin(int index)
	{
	    presentationStateGraphicsHandler.ps.setVOIWindowFromImage(index,applyTo);
        jDoubleByRef centerValue = new jDoubleByRef();
        jDoubleByRef widthValue = new jDoubleByRef();
        presentationStateGraphicsHandler.ps.getCurrentWindowCenter(centerValue);
        presentationStateGraphicsHandler.ps.getCurrentWindowWidth(widthValue);
        setNewWindow(centerValue.value, widthValue.value, true);
        
        
    }
	
	
	/**
	 * Sets a automatical window. 
	 * 
	 * @since 30.08.1999
	 */
	public void setAutoWindow()
	{
        
        jDoubleByRef minValue = new jDoubleByRef();
        jDoubleByRef maxValue = new jDoubleByRef();
        presentationStateGraphicsHandler.ps.getImageMinMaxPixelValue(minValue, maxValue);
        
        setNewWindow( (minValue.value+(maxValue.value-minValue.value+1)/2), (maxValue.value-minValue.value+1),true);
	}
	/**
	 * Sets a automatical window. 
	 * 
	 * @since 30.08.1999
	 */
	public void setMaxWindow()
	{
        
        jDoubleByRef minValue = new jDoubleByRef();
        jDoubleByRef maxValue = new jDoubleByRef();
        presentationStateGraphicsHandler.ps.getImageMinMaxPixelRange(minValue, maxValue);
        setNewWindow( (minValue.value+(maxValue.value-minValue.value+1)/2), (maxValue.value-minValue.value+1),true);
	}
    public void handleVOI(DSEvent e)
    {
        JPopupMenu m= new JPopupMenu("VOI LUT/WIN Settings");
        m.add( new AutoWindowAction());
        m.add( new MaxRangeWindowAction());
        
        popup = m;
        //popup.setVisible(true);
        if (presentationStateGraphicsHandler.ps.getNumberOfVOIWindowsInImage()>0) m.addSeparator();
        
        for (int i = 0; i < presentationStateGraphicsHandler.ps.getNumberOfVOIWindowsInImage(); i++)
        {
        
            if (presentationStateGraphicsHandler.ps.getDescriptionOfVOIWindowsInImage(i)!= null) m.add(new SetWindowingInImageAction("VOI/WIN: " +presentationStateGraphicsHandler.ps.getDescriptionOfVOIWindowsInImage(i),i));
            else m.add(new SetWindowingInImageAction("VOI/WIN: " +i,i));        
        }
        
        if (presentationStateGraphicsHandler.ps.getNumberOfVOILUTsInImage()>0) m.addSeparator();
        for (int i = 0; i < presentationStateGraphicsHandler.ps.getNumberOfVOILUTsInImage(); i++)
        {
        
            if (presentationStateGraphicsHandler.ps.getDescriptionOfVOILUTsInImage(i)!= null) m.add(new SetVOILUTAction("VOI/LUT: " +presentationStateGraphicsHandler.ps.getDescriptionOfVOILUTsInImage(i),i));
            else m.add(new SetVOILUTAction("VOI/LUT: " +i,i));        
        }
        String mod = presentationStateGraphicsHandler.ps.getCurrentImageModality();
        int number = presentationStateGraphicsHandler.dvi.getNumberOfVOIPresets(mod);
        if (number != 0) m.addSeparator();
        double center;
        double width;
        for (int i = 0; i < number; i++)
        {
            center = presentationStateGraphicsHandler.dvi.getVOIPresetWindowCenter(mod, i);
            width = presentationStateGraphicsHandler.dvi.getVOIPresetWindowWidth(mod,i);
            m.add(new SetWindowingAction((presentationStateGraphicsHandler.dvi.getVOIPresetDescription(mod, i)+ "(c/w) " + center + "/" + width),
                                            center,
                                            width));
        }
        m.addSeparator();
        m.add(new InsertGammaVOILUTAction());
        popup.pack();
        popup.show((Component)e.getSource(),0,30);
        
    }
    public void handlePresentationLUT(DSEvent e)
    {
        JPopupMenu m= new JPopupMenu("Presentation LUT");
        popup = m;
        int numberOfLUTs = presentationStateGraphicsHandler.dvi.getNumberOfLUTs();
        
        ButtonGroup bGroup = new ButtonGroup();
        
        for (int i = 0; i< numberOfLUTs; i++)
        {
            String lutName = presentationStateGraphicsHandler.dvi.getLUTID(i);
            JRadioButtonMenuItem rb = new JRadioButtonMenuItem(presentationStateGraphicsHandler.dvi.getLUTDescription(lutName));
            rb.addActionListener(new SetPresentationLutAction(lutName, i, m));
            bGroup.add(rb);
            m.add(rb);
            if (presentationStateGraphicsHandler.selectedLutIndex == i) rb.setSelected(true);
        }
        if (numberOfLUTs>0)m.addSeparator();
        
        JMenuItem identityButton = new JMenuItem("IDENTITY");
        identityButton.addActionListener(new SetPresentationLUTIdentityAction(m));
        m.add(identityButton);
        
        popup.pack();
        popup.show((Component)e.getSource(),0,30);
        
    }
	
	/**
	 * Set a new window.
	 * 
	 * @param width The new window with.
	 * @param center The new window center.
	 * @since 30.04.1999
	 */
	public void setNewWindow(double center, double width, boolean viewImage)
	{
	    if (width <1d)  width =1d;
	    int status = presentationStateGraphicsHandler.ps.setVOIWindow( center, width ,"",applyTo);
        
        /*
        jDoubleByRef centerValue = new jDoubleByRef();
        presentationStateGraphicsHandler.ps.getCurrentWindowCenter(centerValue);
        double actualCenter = (centerValue.value);
        System.out.println("actualCenter: " + actualCenter);
        
        jDoubleByRef widthValue = new jDoubleByRef();
        presentationStateGraphicsHandler.ps.getCurrentWindowWidth(widthValue);
        double actualWidth = (widthValue.value);
        System.out.println("actualWidth: " + actualWidth);
        */   
	    
	    
	    //Send notifications
	    if (viewImage)
	    {
	        statusInformation.setWindow(center, width);
                Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT1,DSComponentType.VIEWER,statusInformation.getWinInfo()));
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_WINDOW, new Double(width), new Double(center)));
    	    
	        buildImageBuffer(false,true,false,false,false);
	    }
	 }
	
////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////
	/**
	 * If a new jDVPresentationState object is available, this methode updates all object.
	 * 
	 * @since 30.04.1999
	 */
    public void setNewPresentationState()
    {
        if (!imageCanvas.getSize().equals(new Dimension(0,0)))
        {
            //applyTo = jDVPSObjectApplicability.DVPSB_currentFrame;
            presentationStateGraphicsHandler.setNewPresentationState(imageCanvas.getSize());            
       
        
            
            
            
            boolean haveLut = true;
            Double win = null;
            Double lev = null;
            Controller.instance().fireNotification(new PresentationStateFixInfoEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       win,
                                                                       lev,
                                                                       haveLut,
                                                                       new Double(presentationStateGraphicsHandler.getZoomValue()),
                                                                       presentationStateGraphicsHandler.ps.getPresentationLabel(),
                                                                       presentationStateGraphicsHandler.ps.getPresentationCreatorsName(),
                                                                       presentationStateGraphicsHandler.ps.getPresentationDescription(),
                                                                       presentationStateGraphicsHandler.getImageWidth(),
                                                                       presentationStateGraphicsHandler.getImageHeight(),
                                                                       presentationStateGraphicsHandler.getStandardDisplayedArea(),
                                                                       presentationStateGraphicsHandler.haveDisplayedAreaPresentationPixelSpacing(),
                                                                       presentationStateGraphicsHandler.ps.canUseDisplayedAreaTrueSize(),
                                                                       presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode(),
                                                                       presentationStateGraphicsHandler.getDisplayedAreaPresentationPixelSpacingX(),
                                                                       presentationStateGraphicsHandler.getDisplayedAreaPresentationPixelSpacingY(),
                                                                       presentationStateGraphicsHandler.getDisplayedAreaPresentationPixelAspectRatio(),
                                                                       presentationStateGraphicsHandler.getDisplayedAreaPresentationPixelMagnificationRatio(),
                                                                       presentationStateGraphicsHandler.getNumberOfImages(),
                                                                       presentationStateGraphicsHandler.getSelectedImageNumber(),
                                                                       presentationStateGraphicsHandler.getImageNumberOfFrames(),
                                                                       presentationStateGraphicsHandler.getSelectedImageFrameNumber(),
                                                                       applyTo));
            Controller.instance().fireNotification(new SendLayerEvent(this,getLayers(),presentationStateGraphicsHandler.getCurrentLayerIndex()));
            
            transformationConverter = new JavaTransformationConverter(presentationStateGraphicsHandler);            
            paintStructure = new PaintStructure(presentationStateGraphicsHandler);            
            paintStructure.setImageCanvas(imageCanvas);            
            setNewImage();
        }
        
        
    }
    
    /**
     * Initialize the image data struture for new image. Called if a new image width other 
     * dimensions than the old image should be displayed.
     * 
     * @since 30.04.1999
     */
    public boolean setNewImage()
    {
        DataBuffer dbb;
        int bandOffsets[] = {0};
        WritableRaster wr ;
        //initial immagebuffer 
        if (!screenSize.equals(imageCanvas.getSize()) || bufferedScreenImage == null)
        {
            
            bufferedScreenImage = null;
            wr= null;
            dbb= null;
            System.gc();
                
            dbb = new DataBufferByte( new byte[imageCanvas.getSize().height*imageCanvas.getSize().width],imageCanvas.getSize().height*imageCanvas.getSize().width);
            wr = Raster.createInterleavedRaster(dbb,imageCanvas.getSize().width,imageCanvas.getSize().height,imageCanvas.getSize().width,1, bandOffsets,null);
           // bufferedScreenImage = new BufferedImage(MainContext.instance().getUsedColorModel(), wr, MainContext.instance().getUsedColorModel().isAlphaPremultiplied(), null);
            bufferedScreenImage = new BufferedImage(MainContext.instance().getIndexColorModel(), wr, MainContext.instance().getIndexColorModel().isAlphaPremultiplied(), null);
            screenSize = new Dimension(imageCanvas.getSize());
                            
        }
	    resetState();
            initVOI();
            convertAnnotationScreenRelative = true;
	    buildImageBuffer(true,true,true,true, true);
	    
	    Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ZOOMINPUT, new Double(presentationStateGraphicsHandler.getZoomValue())));
	    if (presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode() == jDVPSPresentationSizeMode.DVPSD_magnify)
	    {  
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_MAGNIFY, true));
	    }
	    if (presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode() == jDVPSPresentationSizeMode.DVPSD_scaleToFit)
	    {
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_SCALETOFIT, true));
	    }
	    if (presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode() == jDVPSPresentationSizeMode.DVPSD_trueSize)
	    {
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_TRUESIZE, true));
            }
            int curve = jDVPSDisplayTransform.DVPSD_none;
            if (presentationStateGraphicsHandler.dvi.isDisplayTransformPossible(presentationStateGraphicsHandler.ps.getDisplayTransform()))curve=presentationStateGraphicsHandler.ps.getDisplayTransform();
        
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       curve,
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
            
            Controller.instance().fireNotification(new SendLayerEvent(this,getLayers(),presentationStateGraphicsHandler.getCurrentLayerIndex()));
	statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
            
            //Paints the image 
        return true;
    }//setNewImage

   
    /**
     * Stops creating a new Annoatation and 
     * transforms the Annotation to a PresentationStateGraphicObject.
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     */
   public void setStopNew()
   {
      if (paintStructure.getCurrentPaintObject().getStatus() >=PaintObject.STATUS_WORK)
      {
            
            paintStructure.setWorkingShape(null);
            paintStructure.setCurrentStatus(PaintObject.STATUS_STOP, applyTo);
            paintStructure.setCurrentPaintObject( paintStructure.getCurrentPaintObject().getNewPaintObject());
            buildImageBuffer(false,false,false,true, true);
        
      }
      else paintStructure.deleteCurrentPaintObject();
   
   }

    /**
     * Closes a new Annoatation and 
     * transforms the Annotation to a PresentationStateGraphicObject.
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     */
   
   public void setCloseNew()
   {
      if (paintStructure.getCurrentPaintObject().getStatus() >=PaintObject.STATUS_WORK)
      {
         paintStructure.setWorkingShape(null);
         paintStructure.getCurrentPaintObject().setNewPoint(paintStructure.getCurrentPaintObject().getCopyPointAt(0));
         paintStructure.setCurrentStatus(PaintObject.STATUS_STOP, applyTo);
         if (!paintStructure.getCurrentPaintObject().isShutter)
         {
            paintStructure.setCurrentPaintObject(paintStructure.getCurrentPaintObject().getNewPaintObject());
            buildImageBuffer(false,false,false,true, true);
            setEditAction();
            //mainImageViewerPanel.funktionBasisTabPanel.layerPanel.setValue();
            
         }
         else
         {
            boolean repaint = false ;
                
            
            paintStructure.setCurrentPaintObject(paintStructure.getCurrentPaintObject().getNewPaintObject());
            
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
                buildImageBuffer(false,false, false,true, true);
            
            //mainImageViewerPanel.funktionBasisTabPanel.shutterPanel.setValue();
         }
        }
      
   }
    
    

   /**
    * Sets the specified PresentationStateLayerObject to the active PresentationStateLayerObject.
    *
    * @param index The  index of the PresentationStateLayerObject
    * @since 30.04.1999
    */
   public void setCurrentLayerObject(int index)
   {
      presentationStateGraphicsHandler.setCurrentLayerObjectIndex(index);
      paintStructure.setCurrentObject(presentationStateGraphicsHandler.getCurrentLayerObject(),true);  
   }

    /**
     * Deletes the active PresentationStateGraphicObject in the presentationStateGraphicsHandler.
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     * @see PresentationStateGraphicsHandler
     */
   public void deleteActivePresentationStateGraphicObject()
   {
      if (presentationStateGraphicsHandler.getCurrentLayerObjectIndex() != -1)
      {
        presentationStateGraphicsHandler.deleteCurrentObject();
	    buildImageBuffer(false,false,false,true, true);
        
      }
      else 
      {
        paintStructure.deleteCurrentPaintObject();
	    buildImageBuffer(false,false,false,true, true);
      }
      
      setEditAction(); 
   }
   
   /**
    * This function search for the first PresentationStateLayerObject which contains the specified point.
    * 
    * @param clickedPoint The specified point.
    * @since 30.04.1999
    */
   public void setClick(Point2D.Float clickedPoint)
   {
        
        //
        paintStructure.setMoveAnn( clickedPoint);
        
        presentationStateGraphicsHandler.containsObject(clickedPoint);
          
        paintStructure.setCurrentObject(presentationStateGraphicsHandler.getCurrentLayerObject(),true);  
        //mainImageViewerPanel.funktionBasisTabPanel.layerPanel.setValue();
        //mainImageViewerPanel.annotationPanel.setActiveButton();
   }

    /**
     * Sets a new Point to the current Annotation in the paintStructure.
     * 
     * @param newPoint Contains the new Point. 
     * @since 30.04.1999
     */
    public void setNewPoint(Point2D.Float newPoint)
    {
        
         if (!paintStructure.getCurrentPaintObject().isShutter)
         {
            paintStructure.setNewPoint(newPoint, applyTo);
            if (paintStructure.getCurrentPaintObject().status == PaintObject.STATUS_NULL)
            {
                buildImageBuffer(false,false,false,true, true);
            }
            
         }
         else
         {
               
            paintStructure.setNewPoint(newPoint, applyTo);
            
            if (paintStructure.getCurrentPaintObject().status == PaintObject.STATUS_NULL)
            {
                paintStructure.setCurrentPaintObject(null);
            
                Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
                buildImageBuffer(false,false, false,true, true);
            }
         }
        
    }
    
    /**
     * Sets an new presentationStateTextObject form the specified parameters.
     * 
     * @param text Contains the text of the new presentationStateTextObject.
     * @param isBoundingBox Contains true if the new presentationStateTextObject have only a bounding box and no anchor point.
     * @param isAnchor Contains true if the new presentationStateTextObject have only an anchor point and no  bounding box .
     * @param isboth Contains true if the new presentationStateTextObject have  an achor point and a  bounding box .
     * @param isAnchorDisplay Contains true if the anchor point is display relative.
     * @param isBoxDisplay Contains true if the bounding box is display relative.
     * @param isAnchorVisible Contains ture if the anchor point is visible.
     * @since 30.04.1999
     */
    public void setNewText( String text, 
                            boolean isBoundingBox, 
                            boolean isAnchor,
                            boolean isAnchorDisplay, 
                            boolean isBoxDisplay,
                            boolean isAnchorVisible, 
                            int just, 
                            int rot)
    {
        
       
        resetState();
        
        //creates a new presentationStateTextObject.
        presentationStateGraphicsHandler.setNewText(text,isBoundingBox,isAnchor,isAnchorDisplay,isBoxDisplay,isAnchorVisible, just, rot, applyTo);
        
        //set the new current Object
        paintStructure.currentObject = presentationStateGraphicsHandler.getCurrentLayerObject();
        if (isBoundingBox)  Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Text"));
         else   Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Anchor Point"));
         
        //
        mouseHandleListener = new ImageCanvasMoveTextListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
        
    }
    
    /**
     * Sets either an new anchor or a new bounding box to an existing PresentationStateTextObject and
     * deletes the old if exist.
     * 
     * @param anchor If true insert anchor point, if false insert bounding box.
     * @param isDisplayRelative If true the anchor point is display relative.
     * @since 30.04.1999
     */
    public void setNewTextPart(boolean anchor, boolean isDisplayRelative)
    {
       
        PresentationStateTextObject currentText =(PresentationStateTextObject) paintStructure.currentObject;
        int layerIndex = presentationStateGraphicsHandler.getCurrentLayerIndex();
        int textIndex =presentationStateGraphicsHandler.getCurrentLayerObjectIndex();
        currentText.firstDraw = false;
        if (anchor)
        {
            Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Anchor Point"));
            currentText.needAnchorPoint= true;
            currentText.neededAnchorVisible = true;
            currentText.needBox= false;
            if ((currentText.getAnchorPointAnnotationUnits() == 1)||(isDisplayRelative))currentText.neededAnchorIsDisplayRelative = true;
            else currentText.neededAnchorIsDisplayRelative = false;
                
            //removes the anchor point
            currentText.removeAnchorPoint();
            
        }
        else
        {
            Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Text"));
            currentText.needAnchorPoint= false;
            currentText.needBox= true;
            if ((currentText.getBoundingBoxAnnotationUnits() == 1)||(isDisplayRelative))currentText.neededBoxIsDisplayRelative = true;
            else currentText.neededBoxIsDisplayRelative = false;
            //removes hte actual bounding box
            currentText.removeBoundingBox();
        }
	    currentText.setActive ( false);
	    //builds the new screen
	    buildImageBuffer(false,false, false,true, true);
        //sets the actual state
        presentationStateGraphicsHandler.setCurrentLayerObjectIndex(textIndex);
        presentationStateGraphicsHandler.setCurrentLayerIndex(layerIndex);
        
        
        paintStructure.currentObject= currentText;  
        
        if (mouseHandleListener != null)
        {
            imageCanvas.removeMouseListener(mouseHandleListener);
            imageCanvas.removeMouseMotionListener(mouseHandleListener);
        }    
        mouseHandleListener = new ImageCanvasMoveTextListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
        
    }

    /**
     * Initializes the active PresentationStateGraphicObject in the 
     * presentationStateGraphicsHandler for moving. Sets the ImageCanvasMoveListener.
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     */
    public void startMoveAnnotation()
    {
        
        presentationStateGraphicsHandler.getCurrentLayerObject().setActive ( false);
        int layerIndex = presentationStateGraphicsHandler.getCurrentLayerIndex();
        int graphicIndex =presentationStateGraphicsHandler.getCurrentLayerObjectIndex();
            
        //removes hte actual bounding box
	    //builds the new screen
	    buildImageBuffer(false,false, false,true, true);
                
        //sets the actual state
        presentationStateGraphicsHandler.setCurrentLayerIndex(layerIndex);
        presentationStateGraphicsHandler.setCurrentLayerObjectIndex( graphicIndex) ;
        paintStructure.currentObject=presentationStateGraphicsHandler.getCurrentLayerObject();  
        
        if (mouseHandleListener != null)
        {
            imageCanvas.removeMouseListener(mouseHandleListener);
            imageCanvas.removeMouseMotionListener(mouseHandleListener);
                       
        }   
                
        mouseHandleListener = new ImageCanvasMoveListener(this);
        imageCanvas.addMouseListener(mouseHandleListener);
        imageCanvas.addMouseMotionListener(mouseHandleListener);
        
    }

    /**
     * Stop moving the active PresentationStateGraphicObject in the
     * presentationStateGraphicsHandler.      
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     */
    public void stopMoveAnnotation()
    {
	    presentationStateGraphicsHandler.getCurrentLayerObject().setActive ( true);
	    buildImageBuffer(false,false,false,true, true);
	    setEditAction();
        
    }
    
    
    /**
     * Sets an new text value to an existing PresentationStateTextObject.
     * 
     * @param text The new text value.
     * @since 30.04.1999
     */
    public void setNewTextValue( String text , int just, int rot)
    {
        PresentationStateTextObject editText = (PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject();
        editText.setNewValues(text, just,rot);
        buildImageBuffer(false,false, false,true, true);
        setEditAction();        
    }

    /**
     * Stop moving the active PresentationStateTextObject in the
     * presentationStateGraphicsHandler.      
     *
     * @since 30.04.1999
     * @see Controller.PopupMenuController
     */
    public void stopMoveText()
    {
        PresentationStateTextObject textObject =(PresentationStateTextObject)paintStructure.currentObject;
        if (textObject.needBox &&textObject.needAnchorPoint)
        {
            textObject.needBox = false;
            Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Anchor Point"));
        }
        else
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
                   
            }   
	        textObject.setActive ( true);
            
            buildImageBuffer(false,false, false, true, true);
            
            //mainImageViewerPanel.funktionBasisTabPanel.layerPanel.setValue();
            setEditAction();   
            
        }
        
    }

    /**
     * Returns the current ImageCanvas.
     * 
     * @return The current ImageCanvas.
     * @since 30.04.1999
     */
    public ImageCanvas getImageCanvas()
    {
        return imageCanvas;
    }
    
    /**
    * Handles the received MoveImageEvents.
    */
    public void  handleMoveImage(MoveImageEvent e)
    {
        
         switch(e.type)
        {
               
            case MoveImageEvent.IMAGE_FIRST:
                presentationStateGraphicsHandler.selectImageNumber(1);
                setNewPresentationState();
                break;
            case MoveImageEvent.IMAGE_LAST:
                presentationStateGraphicsHandler.selectImageNumber(presentationStateGraphicsHandler.getNumberOfImages());
                setNewPresentationState();
                break;
            case MoveImageEvent.IMAGE_NEXT:
                presentationStateGraphicsHandler.selectNextImage();
                setNewPresentationState();
                break;
            case MoveImageEvent.IMAGE_PREVIOUS:
                presentationStateGraphicsHandler.selectPreviousImage();
                setNewPresentationState();
                break;
            case MoveImageEvent.IMAGE_SET:
                presentationStateGraphicsHandler.selectImageNumber(e.value);
                setNewPresentationState();
                break;
            case MoveImageEvent.FRAME_FIRST:
                presentationStateGraphicsHandler.selectImageFrameNumber(1);
                setNewPresentationState();
                break;
            case MoveImageEvent.FRAME_LAST:
                presentationStateGraphicsHandler.selectImageFrameNumber(presentationStateGraphicsHandler.getImageNumberOfFrames());
                setNewPresentationState();
                break;
            case MoveImageEvent.FRAME_NEXT:
                presentationStateGraphicsHandler.selectNextFrame();
                setNewPresentationState();
                break;
            case MoveImageEvent.FRAME_PREVIOUS:
                presentationStateGraphicsHandler.selectPreviousFrame();
                setNewPresentationState();
                break;
            case MoveImageEvent.FRAME_SET:
                presentationStateGraphicsHandler.selectImageFrameNumber(e.value);
                setNewPresentationState();
            case MoveImageEvent.APPLYFRAME:
                System.err.println(applyTo);
                applyTo = jDVPSObjectApplicability.DVPSB_currentFrame;
                System.err.println(applyTo);
                break;
            case MoveImageEvent.APPLYIMAGE:
                System.err.println(applyTo);
                applyTo = jDVPSObjectApplicability.DVPSB_currentImage;
                System.err.println(applyTo);
                break;
            case MoveImageEvent.APPLYALL:
                System.err.println(applyTo);
                applyTo = jDVPSObjectApplicability.DVPSB_allImages;
                System.err.println(applyTo);
                break;
                
        }
            
    }
    
    
    /**
     * Returns the content of the Image Canvas.
     * 
     * @return Returns the content of the Image Canvas.
     * @since 30.04.1999
     */
    public BufferedImage getBufferedScreenImage()
    {
        return bufferedScreenImage;
    }
	public boolean processEvent (DSEvent e)
	{
	    
	    if (e instanceof MoveImageEvent)
	    {
	        handleMoveImage((MoveImageEvent)e);
	        return true;
	    }
	    if (e instanceof SetShutterEvent)
	    {
	        handleShutter((SetShutterEvent) e);
	        return true;
	    }
	    if (e instanceof ImageActionEvent)
	    {
	        ImageActionEvent ae = (ImageActionEvent)e;
	        switch (ae.type)
	        {
	            case ImageActionEvent.ACTION_SETNEW_SR:
	                        //clear();
	                        return true;
	            case ImageActionEvent.ACTION_COMPOSITE_SR:
	                        //clear();
	                        return true;
	            
	            case ImageActionEvent.ACTION_SELECTLAYER:
	                        handleSelectLayer((Component)ae.getSource());
	                        return true;
	            case ImageActionEvent.PAINT_EDIT:
	                        setEditAction();
	                        return true;
	            case ImageActionEvent.PAINT_CIRCLE:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_TEXT:
	            
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_RECT:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_LINE:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_INTERPOLATED:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_ELLIPSE:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_POINT:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.PAINT_POLYLINE:
	                        handleNewAnnotation(ae.type);
	                        return true;
	            case ImageActionEvent.SET_IMAGERELATIVE:
	                        paintStructure.setImageRelative();
	                        break;
	            case ImageActionEvent.SET_FILLED:
	                        paintStructure.setFilled();
	                        
	                        break;
	            case ImageActionEvent.ACTION_SETNEWIMAGE:
	                        clear = false;
	                        presentationStateGraphicsHandler.selectedImageNumber = 1;
	                        setNewPresentationState();
	                        return true;
	                        
	            case ImageActionEvent.ACTION_EDITSHUTTER:
	                        editShutterColor();
	                        break;
	            case ImageActionEvent.ACTION_EDITLAYER:
	                        editLayer((Component)ae.getSource());
	                        break;
	            case ImageActionEvent.ACTION_INVERT:
	                        presentationStateGraphicsHandler.ps.invertImage();  
	                        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_INVERT, presentationStateGraphicsHandler.ps.isInverse()));
	                        break;
	            case ImageActionEvent.ACTION_FLIP:
	                        presentationStateGraphicsHandler.setFlip();
	                        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_FLIP, presentationStateGraphicsHandler.ps.getFlip()));
	                        break;
	            case ImageActionEvent.ACTION_ROT:
	                        presentationStateGraphicsHandler.setRotation();
	                        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_FLIP, presentationStateGraphicsHandler.ps.getFlip()));
	                        if (presentationStateGraphicsHandler.ps.getRotation() == 0)Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ROT0, false));
	                        if (presentationStateGraphicsHandler.ps.getRotation() == 1)Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ROT90, false));
	                        if (presentationStateGraphicsHandler.ps.getRotation() == 2)Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ROT180, false));
	                        if (presentationStateGraphicsHandler.ps.getRotation() == 3)Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ROT270, false));
	                        break;
	            case ImageActionEvent.ACTION_RESET:
                            presentationStateGraphicsHandler.resetPresentationState();
                            setNewPresentationState();
	                        break;
	            case ImageActionEvent.ACTION_PS:
                            if (presentationStateGraphicsHandler.enablePresentationState) 
                            {
                                presentationStateGraphicsHandler.psSelectedLUTIndex =presentationStateGraphicsHandler.selectedLutIndex;
                                presentationStateGraphicsHandler.dvi.disablePState();
                                presentationStateGraphicsHandler.enablePresentationState = false;
                                  presentationStateGraphicsHandler.updatePresentationState();
                                setNewImage(); 
                            }
                            else
                            {
                                presentationStateGraphicsHandler.dvi.enablePState();            
                                
                                presentationStateGraphicsHandler.enablePresentationState = true;
                                presentationStateGraphicsHandler.updatePresentationState();
                                setNewImage();   
                                presentationStateGraphicsHandler.selectedLutIndex =presentationStateGraphicsHandler.psSelectedLUTIndex;
                            }
                    
                    
                                  
	                        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_PS, presentationStateGraphicsHandler.enablePresentationState));
	                        break;
	            case ImageActionEvent.ACTION_BARTEN:
                            handleDisplayCurve((Component)ae.getSource());
	                        return true;
	            case ImageActionEvent.ACTION_ZOOM:
                            
	                        resetState();
                            
                            if (mouseHandleListener != null)
                            {
                                imageCanvas.removeMouseListener(mouseHandleListener);
                                imageCanvas.removeMouseMotionListener(mouseHandleListener);
                                   
                            }  
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Selecet new Display Area"));
                            mouseHandleListener = new MouseZoomListener(this);
                            imageCanvas.addMouseListener(mouseHandleListener);
                            imageCanvas.addMouseMotionListener(mouseHandleListener);
                            
	                        popup = null;
	                        return true;
	            case ImageActionEvent.ACTION_ZOOMFIT:
	                        setNewDisplayArea(1,1,presentationStateGraphicsHandler.getImageWidth(),presentationStateGraphicsHandler.getImageHeight() );
	                        return true;
	            case ImageActionEvent.ACTION_ZOOM1TO1:
	                        
	                        handleZoom(1d);
	                        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ZOOMINPUT, new Double(presentationStateGraphicsHandler.getZoomValue())));
	                        
	                        return true;
	            case ImageActionEvent.ACTION_VOI:
	                        handleVOI(ae);
	                        return true;
	            case ImageActionEvent.ACTION_WININFRAME:
	                        WindowingPreviewDialog windowingPreviewDialog = new WindowingPreviewDialog(mainImageViewerPanel.parent, this);
                                if ((windowingPreviewDialog.previewHeight >0) &&(windowingPreviewDialog.previewWidth >0)) windowingPreviewDialog.setVisible(true);
	                        
	                        return true;
	                        
	            case ImageActionEvent.ACTION_PRESENTATIONLUT:
	                        handlePresentationLUT(ae);
	                        return true;
	            case ImageActionEvent.MODE_MAGNIFY:
	                        
	                        //Caluclates the dipslay Area
	                        DisplayArea displayAreaM = presentationStateGraphicsHandler.getDisplayArea();
	                      
	                        int lastPresentationSizeModeM = presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode();
	                        
	                        //Sets a new Presentation Size Mode
	                        presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(jDVPSPresentationSizeMode.DVPSD_magnify,
	                                                                             displayAreaM.getTlhcX(),
	                                                                             displayAreaM.getTlhcY(),
	                                                                             displayAreaM.getBrhcX(),
	                                                                             displayAreaM.getBrhcY(),
	                                                                             presentationStateGraphicsHandler.getZoomValue(),
	                                                                             applyTo);
	                         //build a new Presntation State 
	                         presentationStateGraphicsHandler.buildPresentationState();
	                         
	                         //Calcaultes the zoom value
	                         if (lastPresentationSizeModeM  ==  jDVPSPresentationSizeMode.DVPSD_trueSize)presentationStateGraphicsHandler.setZoomValue(presentationStateGraphicsHandler.calculateZoomValue(displayAreaM));
	                         else presentationStateGraphicsHandler.calculateFirstZoomValue();
	                         
	                         //Notification, drawing
	                         Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ZOOMINPUT, new Double(presentationStateGraphicsHandler.getZoomValue())));
	                         buildImageBuffer(false,true,true,false, true);
	                        statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
		                     statusInformation.presentationSizeMode = "mgn";
		                     Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
	                         return true;
	            case ImageActionEvent.MODE_SCALE:
	                        DisplayArea displayAreaS = presentationStateGraphicsHandler.getDisplayArea();
	                        
                                int lastPresentationSizeMode = presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode();
	                        double lastZoom = presentationStateGraphicsHandler.getZoomValue();                                                   
	                        presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(jDVPSPresentationSizeMode.DVPSD_scaleToFit,
	                                                                             displayAreaS.getTlhcX(),
	                                                                             displayAreaS.getTlhcY(),
	                                                                             displayAreaS.getBrhcX(),
	                                                                             displayAreaS.getBrhcY(),
	                                                                             presentationStateGraphicsHandler.getZoomValue(),
	                                                                             applyTo);
	                         presentationStateGraphicsHandler.buildPresentationState();
	                         
	                         if (lastPresentationSizeMode  ==  jDVPSPresentationSizeMode.DVPSD_magnify)presentationStateGraphicsHandler.setZoomValue(lastZoom);
	                         else presentationStateGraphicsHandler.calculateFirstZoomValue();
	                         
	                         Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_ZOOMINPUT, new Double(presentationStateGraphicsHandler.getZoomValue())));
	                         buildImageBuffer(false,true,true,false, true);
	                        statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
		                     statusInformation.presentationSizeMode = "fit";
                          
		                     Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
	                        return true;
	            case ImageActionEvent.MODE_TRUESIZE:
	                        DisplayArea displayAreaT = presentationStateGraphicsHandler.getDisplayArea();
                                
	                        presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(jDVPSPresentationSizeMode.DVPSD_trueSize,
	                                                                             displayAreaT.getTlhcX(),
	                                                                             displayAreaT.getTlhcY(),
	                                                                             displayAreaT.getBrhcX(),
	                                                                             displayAreaT.getBrhcY(),
	                                                                             presentationStateGraphicsHandler.getZoomValue(),
	                                                                             applyTo);
	                         presentationStateGraphicsHandler.buildPresentationState();
	                         presentationStateGraphicsHandler.calculateFirstZoomValue();
	                         buildImageBuffer(false,true,true,false, true);
	                       statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
		                 
                               statusInformation.presentationSizeMode = "true";
		                     Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
	                         return true;
	            case ImageActionEvent.PRINT:
	                        print();
	                        return true;
	                         
	                        
	        }
	        
	       
	        buildImageBuffer(false,true,true,false, true);
	      statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
               
	        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
	        
	    
	    }
	    if (e instanceof RequestEvent)
	    
	    {
	        if (((RequestEvent) e).type == RequestEvent.REQUEST_SCREEN)
	        {
	            Controller.instance().fireEvent(new SendScreenEvent(this, ((DataBufferByte) (getBufferedScreenImage().getRaster().getDataBuffer())).getData(),
	                                                                    imageCanvas.getSize().width, 
	                                                                    imageCanvas.getSize().height,
	                                                                    presentationStateGraphicsHandler.ps.createInstanceUID()));
            }
            return false;
        }
        if (e instanceof ImageChangeEvent)
        {
            ImageChangeEvent ice = (ImageChangeEvent)e;
            if(ice.type == GuiComponents.ID_WINDOW) this.setNewWidth(ice.newDouble.doubleValue());
            if(ice.type == GuiComponents.ID_CENTER) this.setNewCenter(ice.newDouble.doubleValue());
            if(ice.type == GuiComponents.ID_ZOOM) handleZoom(ice.newDouble.doubleValue());
            if(ice.type == GuiComponents.ID_NAME) presentationStateGraphicsHandler.ps.setPresentationLabel(ice.s);
            if(ice.type == GuiComponents.ID_CREATOR) presentationStateGraphicsHandler.ps.setPresentationCreatorsName(ice.s);
            if(ice.type == GuiComponents.ID_DESCRIPTION) presentationStateGraphicsHandler.ps.setPresentationDescription(ice.s);
            if(ice.type == GuiComponents.ID_LAYERLIST) presentationStateGraphicsHandler.setCurrentLayerIndex(ice.intValue);
            return false;
        }
	    if (e instanceof ChangeOptionsEvent)
	    {
            setConfiguration(((ChangeOptionsEvent)e).getConfig(), false);
            
        }
 	    
	    return false;
	}
    public void handleDisplayCurve(Component source)
    {
        JPopupMenu m= new JPopupMenu("Selecte Display Curve");
        ButtonGroup bGroup = new ButtonGroup();
        JRadioButtonMenuItem rb;
        rb = new JRadioButtonMenuItem("None", true);
        rb.addActionListener(new SetDisplayTransformAction("None", jDVPSDisplayTransform.DVPSD_none));
        bGroup.add(rb);
        m.add(rb);
        if (presentationStateGraphicsHandler.dvi.isDisplayTransformPossible(jDVPSDisplayTransform.DVPSD_CIELAB)||presentationStateGraphicsHandler.dvi.isDisplayTransformPossible(jDVPSDisplayTransform.DVPSD_GSDF))
        {
            
            int selectedDisplayCurve = presentationStateGraphicsHandler.ps.getDisplayTransform();
            
            rb = new JRadioButtonMenuItem("Barten");
            rb.addActionListener(new SetDisplayTransformAction("Barten", jDVPSDisplayTransform.DVPSD_GSDF));
            bGroup.add(rb);
            m.add(rb);
            if (selectedDisplayCurve ==jDVPSDisplayTransform.DVPSD_GSDF) rb.setSelected(true);
            rb = new JRadioButtonMenuItem("CIE-lab");
            rb.addActionListener(new SetDisplayTransformAction("CIE-lab", jDVPSDisplayTransform.DVPSD_CIELAB));
            bGroup.add(rb);
            m.add(rb);
            if (selectedDisplayCurve ==jDVPSDisplayTransform.DVPSD_CIELAB) rb.setSelected(true);
            
        }
        m.pack();
        m.show(source,0,30);
       
    }
    
    
    public void handleSelectLayer(Component source)
    {
        JPopupMenu m= new JPopupMenu("Selecte Active Layer");
        ButtonGroup bGroup = new ButtonGroup();
        int numberOfLayers = presentationStateGraphicsHandler.getListPresentationStateGraphicLayer().size();
        PresentationStateGraphicLayer layer;
        JRadioButtonMenuItem rb;
        for (int i = 0; i< numberOfLayers; i++)
        {
            layer =  presentationStateGraphicsHandler.getLayerAtIndex(i);
            rb = new JRadioButtonMenuItem(layer.getName());
            rb.addActionListener(new SetCurrentLayer(layer.getName(), i, m));
            bGroup.add(rb);
            m.add(rb);
            if (presentationStateGraphicsHandler.getCurrentLayerIndex() == i) rb.setSelected(true);
        }
        
        m.pack();
        m.show(source,0,30);
        
        
    }
    public void setConfiguration(Hashtable config, boolean init)
    {
        if (config.containsKey("AmbientLight"))
        {
            //update ambientLightTextField
            String amb = (String)config.get("AmbientLight");
            if ((amb != null) && (!amb.equals("")))
            {
                presentationStateGraphicsHandler.dvi.setAmbientLightValue((new Double((String)config.get("AmbientLight"))).doubleValue());
                if (presentationStateGraphicsHandler.havePresentationState)buildImageBuffer(false,true, false, true, false);
       
            }
        }
        
        if (config.containsKey("PixelSizeY")&&config.containsKey("PixelSizeX"))
        {
            presentationStateGraphicsHandler.trueSizeX = (new Double((String)config.get("PixelSizeX"))).doubleValue();
            presentationStateGraphicsHandler.trueSizeY= (new Double((String)config.get("PixelSizeY"))).doubleValue();
            //System.out.println("PixelSizeY: " + presentationStateGraphicsHandler.trueSizeY);
            //System.out.println("PixelSizeX: " + presentationStateGraphicsHandler.trueSizeX);
            if (presentationStateGraphicsHandler.havePresentationState)
            {
                presentationStateGraphicsHandler.buildPresentationState();
                buildImageBuffer(false,true, false, true, true);
            }
            
        }
        if (config.containsKey("TextFont"))
        {
            Hashtable attributeMap = new Hashtable();
            attributeMap.put(TextAttribute.FONT,config.get("TextFont"));
            PresentationStateTextObject.setTextAttributeMap(attributeMap);
        }
        
        
    }
    public void handleZoom(double newZoom)
    {
            double oldZoom = presentationStateGraphicsHandler.getZoomValue();
            presentationStateGraphicsHandler.setZoomValue(newZoom);
            transformationConverter.setNewZoom(oldZoom);
	        buildImageBuffer(false,false,true,true, true);
	      statusInformation.setStandardDisplayedArea(presentationStateGraphicsHandler.getStandardDisplayedArea());
		    statusInformation.zoomValue = newZoom;
              
		    Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT2,DSComponentType.VIEWER,statusInformation.getPresentationInfo()));
        
    }
    public void handleShutter(SetShutterEvent e)
    {
         //Button events
         if (e.getType()==  SetShutterEvent.POLY)
         {
            //create polyline 
            if (e.getSelected()) 
            {
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Polygonal Shutter"));
                PaintObject ann = new PolylineObject();
   		        ann.isShutter = true;
   		        setNewPaintObjectAction(ann);
                    
            }
            else
            {
                presentationStateGraphicsHandler.shutterList.deletePolygonal();
                buildImageBuffer(false,false,false,true, true);
	           
            }
            
         }
         if (e.getType()==  SetShutterEvent.CIRCLE)
         {
            if (e.getSelected()) 
            {
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Circular Shutter"));
                PaintObject ann = new Circle2DObject((float)presentationStateGraphicsHandler.getCurrentScalingX(),(float)presentationStateGraphicsHandler.getCurrentScalingY());
   		        
   		        ann.isShutter = true;
   		        setNewPaintObjectAction(ann);
            }
            else
            {
                presentationStateGraphicsHandler.shutterList.deleteCircular();
                buildImageBuffer(false,false,false,true, true);
            }
         }
         if (e.getType()==  SetShutterEvent.RECT)
         {
            if (e.getSelected()) 
            {
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Rectangular Shutter"));
                PaintObject ann = new Rectangle2DObject();
   		        ann.isShutter = true;
   		        setNewPaintObjectAction(ann);
                
            }
            else
            {
                presentationStateGraphicsHandler.shutterList.deleteRectangular();
                buildImageBuffer(false,false,false,true, true);
                
            }
         }
         if (e.getType()==  SetShutterEvent.BMP)
         {
            editBmpShutter(e);
         }
            
            
    }
    public void editShutterColor()
    {
            JPanel shPanel = new JPanel();
            shPanel.setBorder(new TitledBorder("Set Shutter Color"));
            
            JTextField txtValue = new JTextField(new Float(presentationStateGraphicsHandler.shutterList.getShutterColorValue()).toString());
            txtValue.setColumns(4);
            shPanel.add(new JLabel("Color"));
            shPanel.add(txtValue);
            
            while (true)
            {
            
                int ret = JOptionPane.showOptionDialog(null, shPanel, "Shutter Presentation Value", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,null,null,null); 
                if (ret == JOptionPane.CANCEL_OPTION) return;
                try
                {
                    float value = (new Float(txtValue.getText().trim())).floatValue();
                    if (!((value <0.0f) || (value > 1f)))
                    {
                        presentationStateGraphicsHandler.shutterList.setShutterColor(value);
                        buildImageBuffer(false,true,false,true, true);
                        return;
                    }
                }
                catch(NumberFormatException t)
                {
                    
                }
                JOptionPane.showMessageDialog(null,"Value must be between 0 und 1");
            }
        }
            
        public void editBmpShutter(SetShutterEvent e)
        {
    
            ShutterList shutterList = presentationStateGraphicsHandler.shutterList;
            if (shutterList.numberOfSuitableBitmapShutter ==0)
            {
                JOptionPane.showMessageDialog(null,"No bitmap display shutter available");
                ((JToggleButton) e.getSource()).setSelected(false);
                return;
            }
            else
            {
                JPanel shPanel = new JPanel();
                shPanel.setBorder(new TitledBorder("Bitmap Display Shutter"));
                JComboBox bitmapShutterChoice = new JComboBox();
                shPanel.add(bitmapShutterChoice);
                bitmapShutterChoice.addItem("no bitmap activated");
                for (int i = 0; i <  shutterList.numberOfSuitableBitmapShutter; i++)
                {
                    bitmapShutterChoice.addItem(shutterList.overlayIsShutterLabelList[i]); 
                }
                if (shutterList.haveBitmapShutter())
                {
                    bitmapShutterChoice.setSelectedIndex(shutterList.activeBitmapShutterIndex+1);
                    
                }
                else 
                {
                    bitmapShutterChoice.setSelectedIndex(0);
                }
            
            
            
                int ret = JOptionPane.showOptionDialog(null, shPanel, "Bitmap Display Shutter", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,null,null,null); 
                if (ret == JOptionPane.CANCEL_OPTION) 
                {
                    ((JToggleButton) e.getSource()).setSelected(false);
                    return;
                }
                presentationStateGraphicsHandler.shutterList.activateBitmapShutter(bitmapShutterChoice.getSelectedIndex()-1);
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
                
                buildImageBuffer(false,true, false,true, true);
                
            }
    }
    public void editLayer(Component source)
    {
        
       (new LayerJPanel(this, this.mainImageViewerPanel.parent, source)).setVisible(true);
        
    }
    
    
    public void handleNewAnnotation(int type)
    {
	 buildImageBuffer(false,true,true,false, true);
        
        //If there is no layer you have to insert one layer before you can create a new Annotation
        if (presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size()<= 0)
        {
            (new EditLayerPanel(this.mainImageViewerPanel.parent, this ,false)).show();
            Controller.instance().fireNotification(new SendLayerEvent(this,getLayers(),presentationStateGraphicsHandler.getCurrentLayerIndex()));
                    
        }
         
         
        if (presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size()> 0)
        {
   		    
   		    PaintObject ann = null;
   		    switch (type)
   		    {
   		        case ImageActionEvent.PAINT_RECT:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Rectangle"));
   		        
   		                ann = new Rectangle2DObject();
                        ann.setFilled(paintStructure.filled);
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        
   		        case ImageActionEvent.PAINT_CIRCLE:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Circle"));
                        ann = new Circle2DObject((float)presentationStateGraphicsHandler.getCurrentScalingX(),(float)presentationStateGraphicsHandler.getCurrentScalingY());
                        ann.setFilled(paintStructure.filled);
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        
   		        case ImageActionEvent.PAINT_LINE:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Line"));
   		                ann = new Line2DObject();
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        
   		        case ImageActionEvent.PAINT_POLYLINE:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Polyline"));
   		                ann = new PolylineObject();
                        ann.setFilled(paintStructure.filled);
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        case ImageActionEvent.PAINT_ELLIPSE:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Ellipse"));
   		                ann = new Ellipse2DObject();
                        ann.setFilled(paintStructure.filled);
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		                
   		        case ImageActionEvent.PAINT_INTERPOLATED:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Interpolated"));
   		                ann = new InterpolatedObject();
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        case ImageActionEvent.PAINT_POINT:
		        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Insert Point"));
   		                ann = new Point2DObject();
                        ann.isDisplayRelative = !paintStructure.imageRelative;
   		                setNewPaintObjectAction(ann);
   		                break;
   		        
   		        case ImageActionEvent.PAINT_TEXT:
   		                TextAnnotationDialog dialog = new  TextAnnotationDialog(mainImageViewerPanel.parent,this, false,null);
   		                dialog.setVisible(true);
   		                break;
   		    
   		    }
   		}
        
    }
            
            
            
    public String [] getLayers()
    {
        
        String[] layers = new String[presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size()];
        //Fill layerChoice.
        for (int i = 0; i < presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size(); i++)
        {
            layers[i] = new String(i + " " +((PresentationStateGraphicLayer) (presentationStateGraphicsHandler.listPresentationStateGraphicLayer.elementAt(i))).getName());
        }
        return layers;
    }
    public AbstractAction getAction(String key)
    {
        return (AbstractAction) actionTable.get(key);
    }
    public void createActionTable()
    {
        actionTable = new Hashtable();
        actionTable.put("delete", new DeleteAnnotationAction());
        actionTable.put("deleteNew", new DeleteNewAnnotationAction());
        actionTable.put("stop", new StopAction());
        actionTable.put("close", new CloseAction());
        actionTable.put("filled", new FilledAction());
        actionTable.put("image relative", new ImageRelativeAction());
        actionTable.put("move", new MoveAction());
        actionTable.put("editText", new EditTextAction());
        actionTable.put("insert anchor point display relative", new SetNewTextPartAction("Insert Display Relative Anchor Point",true,true));
        actionTable.put("insert anchor point image relative", new SetNewTextPartAction("Insert Image Relative Anchor Point",true,false));
        actionTable.put("move anchor point", new SetNewTextPartAction("Move Anchor Point",true,false));
        actionTable.put("move bounding box", new SetNewTextPartAction("Move Bounding Box",false,false));
        actionTable.put("insert bounding box display relative", new SetNewTextPartAction("Insert Display Relative Bounding Box",false,true));
        actionTable.put("insert bounding box image relative", new SetNewTextPartAction("Insert Image Relative Bounding Box",false,false));
        actionTable.put("remove bounding box", new RemoveTextPartAction("Remove Bounding Box",false));
        actionTable.put("remove anchor point", new RemoveTextPartAction("Remove Anchor Point",true));
        actionTable.put("set bounding box display relative", new ConvertAnnotationUnitAction("Set Display Relative Bounding Box",false));
        actionTable.put("set bounding box image relative", new ConvertAnnotationUnitAction("Set Image Relative Bounding Box ",false));
        actionTable.put("set anchor point image relative", new ConvertAnnotationUnitAction("Set Image Relative Anchor Point",true));
        actionTable.put("set anchor point display relative", new ConvertAnnotationUnitAction("Set Display Relative Anchor Point",true));
        actionTable.put("set anchor point invisible", new VisibleAnchorAction("Set Anchor Point Invisible",false));
        actionTable.put("set anchor point visible", new VisibleAnchorAction("Set Anchor Point Visible",true));
    }
    
    class DeleteAnnotationAction extends AbstractAction
    {
        public DeleteAnnotationAction()
        {
            super("Delete");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
	        deleteActivePresentationStateGraphicObject();
        }
    }
    
    class AutoWindowAction extends AbstractAction
    {
        public AutoWindowAction()
        {
            super("Min - Max");
        }
        public void actionPerformed(ActionEvent e)
        {
            setAutoWindow();
        }
    }
    class MaxRangeWindowAction extends AbstractAction
    {
        public MaxRangeWindowAction()
        {
            super("Max Range");
        }
        public void actionPerformed(ActionEvent e)
        {
            setMaxWindow();
        }
    }
    class SetWindowingAction extends AbstractAction
    {
        double width;
        double center;
        public SetWindowingAction(String name, double center, double width)
        {
            super(name);
            this.width = width;
            this.center = center;
        }
        public void actionPerformed(ActionEvent e)
        {
            setNewWindow( center,width,true );
        }
    }
    class SetWindowingInImageAction extends AbstractAction
    {
        int index;
        public SetWindowingInImageAction(String name, int index)
        {
            super(name);
            this.index = index;
        }
        public void actionPerformed(ActionEvent e)
        {
            setVOIWin(index);           
        }
    }
    class SetPresentationLutAction extends AbstractAction
    {
        int index;
        String n; 
        JPopupMenu m;
        public SetPresentationLutAction(String name, int index, JPopupMenu m)
        {
            super(name);
            this.index = index;
            n = name;
            this.m = m;
        }
        public void actionPerformed(ActionEvent e)
        {
              m.setVisible(false); 
              presentationStateGraphicsHandler.selectedLutIndex = index;
              presentationStateGraphicsHandler.dvi.selectDisplayPresentationLUT(n);
              buildImageBuffer(false,true, false,false, false);
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
              
        }
    }
    class SetCurrentLayer extends AbstractAction
    {
        int index;
        String n; 
        JPopupMenu m;
        public SetCurrentLayer(String name, int index, JPopupMenu m)
        {
            super(name);
            this.index = index;
            n = name;
            this.m = m;
        }
        public void actionPerformed(ActionEvent e)
        {
              m.setVisible(false); 
              presentationStateGraphicsHandler.setCurrentLayerIndex(index);
              buildImageBuffer(false,false, false,false, false);
              
        }
    }
    
    
    class SetPresentationLUTIdentityAction extends AbstractAction
    {
        JPopupMenu m;
        public SetPresentationLUTIdentityAction( JPopupMenu m)
        {
            super();
            this.m = m;
        }
        public void actionPerformed(ActionEvent e)
        {
              m.setVisible(false); 
              presentationStateGraphicsHandler.ps.setCurrentPresentationLUT(jDVPSPresentationLUTType.DVPSP_identity);
              presentationStateGraphicsHandler.ps.setDefaultPresentationLUTShape();
              presentationStateGraphicsHandler.selectedLutIndex = -1;
              buildImageBuffer(false,true, false,false, false);
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
              
        }
    }
    class SetVOILUTAction extends AbstractAction
    {
        int index;
        public SetVOILUTAction(String name, int index)
        {
            super(name);
            this.index = index;
        }
        public void actionPerformed(ActionEvent e)
        {
	        setVOILUT(index);
        }
    }
    
    class DeleteNewAnnotationAction extends AbstractAction
    {
        public DeleteNewAnnotationAction()
        {
            super("Delete");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
            paintStructure.deleteCurrentPaintObject();
	    buildImageBuffer(false,false,false,true, true);
            setEditAction();   
        }
    }
    class StopAction extends AbstractAction
    {
        public StopAction()
        {
            super("Stop");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
                setStopNew();
                setEditAction();   
        }
    }
    class CloseAction extends AbstractAction
    {
        public CloseAction()
        {
            super("Close");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
	    setCloseNew();
            
        }
    }
    class MoveAction extends AbstractAction
    {
        public MoveAction()
        {
            super("Move");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
	        startMoveAnnotation();
        }
    }
    class FilledAction extends AbstractAction
    {
        public FilledAction()
        {
            super("Filled");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
            if (popup.isVisible()) popup.setVisible(false);
            PresentationStateGraphicObject filledObject =  ((PresentationStateGraphicObject)presentationStateGraphicsHandler.getCurrentLayerObject());
            if (filledObject != null) 
            {
                if (filledObject.isfilled() == false)filledObject.setFilled(true);
                else filledObject.setFilled(false);
                buildImageBuffer(false,false,false,true, true);
            }
            setEditAction();
        }
    }
    class ImageRelativeAction extends AbstractAction
    {
        public ImageRelativeAction()
        {
            super("Image Relative");
        }
        public void actionPerformed(ActionEvent e)
        {
            if (mouseHandleListener != null)
            {
                imageCanvas.removeMouseListener(mouseHandleListener);
                imageCanvas.removeMouseMotionListener(mouseHandleListener);
            }    
            if (popup.isVisible()) popup.setVisible(false);
            ((PresentationStateGraphicObject)presentationStateGraphicsHandler.getCurrentLayerObject()).convert();
    	    
	            buildImageBuffer(false,false,false,true, true);
	            setEditAction();
	        }
    }
    public void convert(int units)
    {
        ((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).convert(units);
        setEditAction();   
    
    }
    public void clear()
    {
       //reset();
       MainImageViewerPanel.setEnabled(false, mainImageViewerPanel.getImageCanvas() );
       bufferedScreenImage = null;
        //screenSize = new Dimension(0,0);
        clear = true;
       
    }
    public void print()
    {
        resetState();
        int status = JOptionPane.showConfirmDialog(mainImageViewerPanel,"Reset Displayed Area to Image Size?","Add Image to Print Job", OptionDialog.ID_OK);
        DisplayArea oldDisplaydArea = presentationStateGraphicsHandler.getDisplayArea();
        if (status == 1) presentationStateGraphicsHandler.getPrintImageBuffer(transformationConverter);
        else
        {
            double zoomValue = presentationStateGraphicsHandler.getZoomValue();
            presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode(), 1, 1, presentationStateGraphicsHandler.getImageWidth(),  presentationStateGraphicsHandler.getImageHeight(),zoomValue , applyTo);
            presentationStateGraphicsHandler.setScreenSize(new Dimension(presentationStateGraphicsHandler.getImageWidth(),presentationStateGraphicsHandler.getImageHeight()));
            presentationStateGraphicsHandler.getPrintImageBuffer(transformationConverter);
            presentationStateGraphicsHandler.ps.setImageRelativeDisplayedArea(presentationStateGraphicsHandler.ps.getDisplayedAreaPresentationSizeMode(), oldDisplaydArea.getTlhcX(), oldDisplaydArea.getTlhcY(), oldDisplaydArea.getBrhcX(),  oldDisplaydArea.getBrhcY(), zoomValue, applyTo);
            
            
        }
        presentationStateGraphicsHandler.setScreenSize(screenSize);
    }
    
    class EditTextAction extends AbstractAction
    {
        public EditTextAction()
        {
            super("Edit");
        }
        public void actionPerformed(ActionEvent e)
        {
   		    TextAnnotationDialog dialog = new  TextAnnotationDialog(mainImageViewerPanel.parent,ScreenImageHandler.this, true,(PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject());
   		    dialog.setVisible(true);
            
        }
    }
    class SetDisplayTransformAction extends AbstractAction
    {
        
        int index =0;
        public SetDisplayTransformAction(String name,int index)
        {
            super(name);
            this.index = index;
        }
        public void actionPerformed(ActionEvent e)
        {
             presentationStateGraphicsHandler.ps.setDisplayTransform(index);
             buildImageBuffer(false,true, false, true, true);
            Controller.instance().fireNotification(new SetPresentationStateEvent(this,
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_rectangular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_circular),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_polygonal),
                                                                       presentationStateGraphicsHandler.ps.haveShutter(jDVPSShutterType.DVPSU_bitmap),
                                                                       presentationStateGraphicsHandler.ps.getFlip(),
                                                                       presentationStateGraphicsHandler.ps.isInverse(),
                                                                       presentationStateGraphicsHandler.ps.getRotation(),
                                                                       presentationStateGraphicsHandler.enablePresentationState,
                                                                       presentationStateGraphicsHandler.ps.getDisplayTransform(),
                                                                      presentationStateGraphicsHandler.ps.getPresentationLUT()));
	     
   		    
            
        }
    }
    class InsertAnchorPointAction extends AbstractAction
    {
        boolean imageRel;
        public InsertAnchorPointAction(boolean imageRel, String name)
        {
            super(name);
            this.imageRel = imageRel;
        }
        public void actionPerformed(ActionEvent e)
        {
            setNewTextPart(true,imageRel);
        }
    }
    class SetNewTextPartAction extends AbstractAction
    {
        boolean anchor;
        boolean isDisplayRelative;
        public SetNewTextPartAction( String name, boolean anchor, boolean isDisplayRelative)
        {
            super(name);
            this.anchor = anchor;
            this.isDisplayRelative = isDisplayRelative;
        }
        public void actionPerformed(ActionEvent e)
        {
            setNewTextPart(anchor,isDisplayRelative);
            if (anchor) Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Anchor Point"));
            else Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Bounding Box"));
        }
    }
    class RemoveTextPartAction extends AbstractAction
    {
        boolean anchor;
        public RemoveTextPartAction( String name, boolean anchor)
        {
            super(name);
            this.anchor = anchor;
        }   
        public void actionPerformed(ActionEvent e)
        {
	            if (anchor)((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).removeAnchorPoint();
	            else((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).removeBoundingBox();
	            
	            buildImageBuffer(false,false,false, false,true);
                setEditAction();   
            
        }
    }
    class VisibleAnchorAction extends AbstractAction
    {
        boolean isVisible;
        public VisibleAnchorAction( String name, boolean isVisible)
        {
            super(name);
            this.isVisible = isVisible;
        }   
        public void actionPerformed(ActionEvent e)
        {
                ((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).setAnchorPointVisible(isVisible);
	            buildImageBuffer(false,false,false, false,true);
                setEditAction();   
            
        }
    }
    class ConvertAnnotationUnitAction extends AbstractAction
    {
        boolean anchor;
        public ConvertAnnotationUnitAction( String name, boolean anchor)
        {
            super(name);
            this.anchor = anchor;
        }   
        public void actionPerformed(ActionEvent e)
        {
        if (anchor)((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).convert(1);
        else ((PresentationStateTextObject)presentationStateGraphicsHandler.getCurrentLayerObject()).convert(0);
        setEditAction();   
                         
        }
    }
    class InsertGammaVOILUTAction extends AbstractAction
    {
        public InsertGammaVOILUTAction()
        {
            super("Create VOI LUT with Gamma");
        }
        public void actionPerformed(ActionEvent e)
        {
            CreateGammaVOILUTDialog d = new CreateGammaVOILUTDialog(mainImageViewerPanel.parent);
            d.setVisible(true);
            if (d.getActionValue() == d.ID_OK)
            {
                int status = presentationStateGraphicsHandler.ps.setGammaVOILUT(d.getGamma(),applyTo);
	        statusInformation.setLUT("Gamma: "+ d.getGamma());
		Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_TEXT1,DSComponentType.VIEWER,statusInformation.getWinInfo()));
	        Controller.instance().fireNotification(new ImageChangeEvent(this, GuiComponents.ID_VOILUT));
                buildImageBuffer(false,true, false,false, false);
            }
        }
    }
    public int getApplyTo()
    {
        return applyTo;
    }
	      
}
/*
 *  CVS Log
 *  $Log: ScreenImageHandler.java,v $
 *  Revision 1.2  2003/09/08 10:17:26  kleber
 *  Bugfix: The Displayed Area is defined after the spatial transformation.
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

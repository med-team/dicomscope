/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 10:13:44 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
 */
package viewer.main;

import viewer.presentation.*;
/**
 * This class contains status information about the image
 */
public class StatusInformation {
    public double width;
    public double center;
    public boolean haveLut= false;
    public String voiLut ;
    public DisplayArea displayArea;
    public double zoomValue;
    public String presentationSizeMode;
    public String info;
    public String layerName;
    
    
    public StatusInformation() {
        
    }
    public String toString() {
        String returnString = new String();
        if (displayArea != null) returnString = returnString.concat(displayArea.toString());
        if (haveLut) returnString = returnString.concat("LUT: " + voiLut);
        else returnString = returnString.concat(" c: = " + center + ", w: = " + width  );
        return returnString;
    }
    public void  setWindow(double center, double width) {
        this.center = center;
        this.width = width;
        haveLut = false;
    }
    
    public void setStandardDisplayedArea(DisplayArea displayArea) {
      
        this.displayArea = displayArea;
       
    }
    public void setLUT(String voiLut) {
        haveLut = true;
        this.voiLut = voiLut;
    }
    public String getWinInfo() {
        if (haveLut) return("LUT: = " + voiLut);
        else return new String("c/w: " + center + "/" + width);
    }
    public String getLayer() {
        return layerName;
    }
    public String getPresentationInfo() {
        
        //return new String(presentationSizeMode + ", z: " + zoomValue + ", da: (" + displayArea.getTlhcX()+ "/" + displayArea.tlhc_y+ "),("+displayArea.getBrhcX()+"/"+displayArea.getTlhcY()+")");
        return new String("da: (" + displayArea.getTlhcX()+ "/" + displayArea.getTlhcY()+ "),("+displayArea.getBrhcX()+"/"+displayArea.getBrhcY()+")");
    }
    
}
/*
 *  CVS Log
 *  $Log: StatusInformation.java,v $
 *  Revision 1.2  2003/09/08 10:13:44  kleber
 *  Rename setDisplayedArea to setStandardDisplayedArea
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 */

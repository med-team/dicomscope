/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import de.microtherapy.tools.text.document.general.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import jToolkit.gui.*;
import main.*;
/**
* This class contains the dialog creating a VOI LUT with a Gamma 
* @autor Klaus Kleber
* @since 01.06.2000
*/
public class CreateGammaVOILUTDialog extends javax.swing.JDialog implements jToolkit.gui.CommandButtonListener
{
    /**
    * Action for pressing the OK-Button
    */
    public static final int ID_OK = 0;
    /**
    * Action for pressing the Cancel-Button
    */
    public static final int ID_CANCEL = 1;
    
    
    private CommandJButton okButton = new CommandJButton("OK", this, ID_OK);
    private CommandJButton cancelButton = new CommandJButton("CANCEL", this,ID_CANCEL);
    private int actionValue;
    private JTextField gammaTextField;
    
    /**
    * Constructor.
    * @param parent Parent Frame.
    */
    public CreateGammaVOILUTDialog(JFrame parent) 
    {
        super(parent, "Create VOI LUT Dialog", true);
        setLocationRelativeTo(parent);
        getContentPane().setSize(400,300);
        
        JPanel paintPanel = new JPanel();
        paintPanel.setLayout(new BorderLayout());
        getContentPane().add(paintPanel);
        paintPanel.setBorder(new TitledBorder("Select Gamma"));
        gammaTextField = new JTextField(10);
        gammaTextField.setDocument(new DoubleDocument());
        
        JPanel p = new JPanel();
        p.add(gammaTextField);
        paintPanel.add(p,BorderLayout.CENTER);
         
         JPanel southPanel = new JPanel();
         southPanel.add(okButton);
         southPanel.add(cancelButton);
        
        paintPanel.add(southPanel, BorderLayout.SOUTH);
        pack();
        
    }
    /**
    * Action handling.
    * @param id the id of the pressed JButton.
    */
    public void buttonClicked (int id)
    {
            if (id == ID_OK)  
            {
                actionValue = ID_OK;
                setVisible (false);
            }
            if (id == ID_CANCEL) 
            {
                actionValue = ID_CANCEL;
                setVisible (false);
            }
    }
    /**
    * Returns the Action applied to this JDialog. The following values are possible:
    * ID_OK for pressing the OK Button and ID_CANCEL for pressing the Cancel Button.
    * returns the Action applied to this JDialog.
    */
    public int getActionValue()
    {
        return actionValue;
    }
    /**
    * Returns the gamma value
    * @return the gamma value
    */
    public double getGamma()
    {
        return (new Double(gammaTextField.getText()).doubleValue());
    }
}
/*
 *  CVS Log
 *  $Log: CreateGammaVOILUTDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

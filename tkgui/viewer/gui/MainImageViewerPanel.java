/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.gui;
import viewer.sr.*;
import java.awt.*;
import java.awt.image.*;

import javax.swing.border.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import viewer.main.*;
import viewer.presentation.*;
import viewer.controller.*;
import viewer.paint.*;
import main.*;
import jToolkit.gui.*;
import J2Ci.*;

/**
* This class contains the main panel of the image viewer. All other 
* components are integrated in this panel.
* 
* @author Klaus Kleber
* @since 30.04.1999
*/
public class MainImageViewerPanel extends JPanel implements MainListener
{

    
    public JFrame parent;
    
	/**
	 * @since 30.04.1999
	 */
	public ScreenImageHandler screenImageHandler;

    private ScrollListener scrollListener = new ScrollListener();
    SRViewPanel srViewPanel;
    SREditPanel srEditPanel;
    private FunctionPanel functionPanel;
    boolean stackPanelOn = false;
    
    boolean srPanelOn = false;
    
    private String functionPanelOrientation = BorderLayout.NORTH;
    private JPanel main = new JPanel();
    /**
     * Contains the parentFrame of this Panel
     * 
     * @since 30.04.1999
     */
     JScrollBar heightScrollbar;
     JScrollBar widthScrollbar;
     ImageCanvas imageCanvas;
     private StackFunctionPanel stackFunctionPanel = new StackFunctionPanel();
        JSplitPane splitPane = new JSplitPane();
	JPanel scroll = new JPanel();
    /**
     * Constructs a new object.
     * 
     * @param presentationStateGraphicsHandler Contains the current PresentationStateGraphicsHandler.
     * @param parentFrame Contains the parent frame of this Panel
     * @param sl Contains reference of a status bar
     * @since 30.04.1999
     */
    public  MainImageViewerPanel(PresentationStateGraphicsHandler presentationStateGraphicsHandler,Hashtable config, JFrame parent)
    {
         super();
         this.parent = parent;
           setBackground(Color.black);
		   
		   Controller.instance().addMainListener(this);
		   //Init screenImageHandler
		   screenImageHandler = new ScreenImageHandler(this,presentationStateGraphicsHandler, config);
		   srViewPanel = new SRViewPanel(presentationStateGraphicsHandler.dvi,this);
		   srEditPanel = new SREditPanel(presentationStateGraphicsHandler.dvi,this);
		   
		   setBackground(Color.lightGray);
		   setLayout(new BorderLayout(5,5));
           //setFont(MainContext.instance().getUsedFont());
	       
            
            scroll.setLayout(new BorderLayout());
            heightScrollbar = new JScrollBar(JScrollBar.VERTICAL);
            scroll.add(heightScrollbar, BorderLayout.EAST);
            heightScrollbar.setName("height");
            heightScrollbar.addAdjustmentListener(scrollListener);
            widthScrollbar = new JScrollBar(JScrollBar.HORIZONTAL);
            widthScrollbar.setName("width");
            widthScrollbar.addAdjustmentListener(scrollListener);
            scroll.setBorder (new TitledBorder("Image Viewer"));
            scroll.add(widthScrollbar, BorderLayout.SOUTH);
            imageCanvas = new ImageCanvas(screenImageHandler);
             scroll.add(imageCanvas,BorderLayout.CENTER);
            
            main.setLayout(new BorderLayout());
            main.add(scroll,BorderLayout.CENTER);
            splitPane.setRightComponent(srViewPanel);
            splitPane.setOneTouchExpandable(true);
            //splitPane.setLeftComponent(main);
            //add(splitPane, BorderLayout.CENTER);
            add(main, BorderLayout.CENTER);
            int width = parent.getSize().width;
            splitPane.setDividerLocation(width/2);
            
            functionPanel = new FunctionPanel();
           
           setConfiguration(config, true);
           
           //setEnabled(false, this);
    
    }
    public void setNewScrollbarValues(int width,int height, Point values)
    {
        setListeners(false);
        widthScrollbar.setMaximum(width);
        heightScrollbar.setMaximum(height);
        widthScrollbar.setValue(values.x);
        heightScrollbar.setValue(values.y);
        setListeners(true);
    }
    public void setListeners(boolean set)
    {
        if (set)
        {
            heightScrollbar.addAdjustmentListener(scrollListener);
            widthScrollbar.addAdjustmentListener(scrollListener);
        } 
        else
        {
            heightScrollbar.removeAdjustmentListener(scrollListener);
            widthScrollbar.removeAdjustmentListener(scrollListener);
        }
    }

    /**
     * Returns the ImageCanvas which contains the GUI of the image.
     * 
     * @return The imageCanvas which contains the image
     * @since 30.04.1999
     */
    public ImageCanvas getImageCanvas()
    {
        return imageCanvas;
    }
    
	public boolean processEvent (DSEvent e)
	{
	    if (e instanceof ChangeOptionsEvent)
	    {
                setConfiguration(((ChangeOptionsEvent)e).getConfig(), false);
	    }
	    if (e instanceof ImageActionEvent)
	    {
	        ImageActionEvent  ima = (ImageActionEvent)e;
	        if (ima.type == ImageActionEvent.ACTION_SETNEWIMAGE)
	        {
	            if (ima.numberOfFrames > 1 || ima.numberOfImages > 1)
	            {
                    if (!stackPanelOn)
                    {
                        main.add(stackFunctionPanel, BorderLayout.NORTH);
                        stackPanelOn = true;
                    }
                }
                else
                {
                    if (stackPanelOn)
                    {
                        main.remove(stackFunctionPanel);
                        stackPanelOn = false;
                    }
                            
                }
	            
	        }
	        
	        if (ima.type == ImageActionEvent.ACTION_SETNEW_SR)
	        {
	            setSRPanel(true);
	            removeEditSR();
	            screenImageHandler.clear();
	            srViewPanel.setNewSR();
	            
	        }
	        if (ima.type == ImageActionEvent.ACTION_COMPOSITE_SR)
	        {
	            removeEditSR();
	            screenImageHandler.clear();
	            srViewPanel.setNewCompositeSR();
	            
	        }
	        
	        return false;
	    }
	    return false;
	}
	
	
	
	public void setConfiguration(Hashtable config, boolean init)
	{
        String paintPanelPlacement=null;
        if (config.containsKey("PaintPanelPlacement")) paintPanelPlacement  = (String)config.get("SeparateImagePanelPlacement");
        if (!functionPanel.imagePanelOrientation.equals("None")) remove (functionPanel.paintPanel);
        if (config.containsKey("FunctionPanelPlacement")) 
        {
            if (!init) 
            {   
                main.remove(functionPanel);
            }
            String orient = (String)config.get("FunctionPanelPlacement");
            if (!orient.equals("None")) 
            {
                functionPanel.setConfiguration(config,orient);
                if (orient.equals(BorderLayout.EAST)) 
                {
                    main.add(functionPanel,BorderLayout.EAST);
                }
                else add(functionPanel,orient);
            }
            if (!functionPanel.imagePanelOrientation.equals("None"))add(functionPanel.paintPanel,functionPanel.imagePanelOrientation);
            
	}
	//config.put("NavigationPanelOn", new Boolean(true));
       /*
        if (config.containsKey("NavigationPanelOn")) 
        {
            if (((Boolean)(config.get("NavigationPanelOn"))).booleanValue())
            {
                if (!stackPanelOn)
                {
                    main.add(stackFunctionPanel, BorderLayout.NORTH);
                    stackPanelOn = true;
                }
            }
            else
            {
                if (stackPanelOn)
                {
                    main.remove(stackFunctionPanel);
                    stackPanelOn = false;
                }
                
            }
        }
        */
        updateUI();
    }
    
    public class ScrollListener implements AdjustmentListener
    {
        public void adjustmentValueChanged(AdjustmentEvent e)
        {  
            JScrollBar sb = (JScrollBar)e.getSource();
            if (sb.getName().equals("width"))screenImageHandler.setNewImagePartWidth(e.getValue());
            else screenImageHandler.setNewImagePartHeight(e.getValue());
        }
    }

        public static void setEnabled(boolean flag, Container container)
        {
            if (container!= null)
            {
                container.setEnabled(flag);
                int count = container.getComponentCount(); 
                for (int i = 0; i < count ; i++)
                {
                    Component c = container.getComponent(i);
                    if (c instanceof Container) setEnabled(flag, (Container) c);
                    else c.setEnabled(flag);
                }
                
            }
        }
    public void enabledImageView(boolean flag)
    {
        setEnabled(flag, functionPanel);
        setEnabled(flag, stackFunctionPanel);
        setEnabled(flag, scroll);
    }
    public void addEditSR(boolean hackerMode)
    {
        
        if (splitPane.getRightComponent() != srEditPanel)
        {
            int dividerLocation = splitPane.getDividerLocation();
            splitPane.remove(splitPane.getRightComponent());
            splitPane.setRightComponent(srEditPanel);
            splitPane.setDividerLocation(dividerLocation);
            srEditPanel.update(hackerMode);
            
        }
    }
   
    public void editSRClose()
    {
        removeEditSR();
        srViewPanel.setChangedSR();
    }
   
    public void removeEditSR()
    {
        if (splitPane.getRightComponent() == srEditPanel)
        {
            int dividerLocation = splitPane.getDividerLocation();
            splitPane.remove(srEditPanel);
            splitPane.setRightComponent(srViewPanel);
            splitPane.setDividerLocation(dividerLocation);
        }
    }
    public void setSRPanel (boolean enabled)
    {
        if (enabled)
        {
            if(srPanelOn)return;
            else
            {
                remove(main);
                splitPane.setLeftComponent(main);
                add(splitPane, BorderLayout.CENTER);
                splitPane.setRightComponent(srViewPanel);
                int width = parent.getSize().width;
                splitPane.setDividerLocation(width/2);
                
            }
        }
        else
        {
            if (!srPanelOn) return;
            else
           {
                splitPane.remove(main);
                remove(splitPane);
                add(main, BorderLayout.CENTER);
            
            }
        }
        updateUI();
    }
}

/*
 *  CVS Log
 *  $Log: MainImageViewerPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

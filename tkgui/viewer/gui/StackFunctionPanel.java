/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import main.*;
    import java.util.*;

/**
 * This class contains the GUI for the stack function panel
 * 
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class StackFunctionPanel extends JPanel 
{
    
    public static final int SINGLE_IMAGE_MODE = 0;
    
    public static final int MULIT_MODE = 1;
    
    public static final int MULIT_FRAME_MODE = 2;
    
    public static final int MULIT_IMAGE_MODE = 3;
    private JPanel mainPanel = new JPanel();
    private int mode = 0;
    private GuiComponents gui;
     private   JPanel framePanel = new JPanel();
     private    JPanel applyPanel = new JPanel();
      private  JPanel imagePanel = new JPanel();
    
    /**
     * Constructor
     * 
     * @param orientation
     */
    public StackFunctionPanel()
    {
        gui =  GuiComponents.getInstance();
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new BorderLayout());
        
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.X_AXIS));
        add(mainPanel, BorderLayout.WEST);
           //setLayout(new FlowLayout(FlowLayout.LEFT));
         imagePanel.setBorder(BorderFactory.createTitledBorder("Move in Image Stack"));
         imagePanel.setLayout(new BoxLayout(imagePanel,BoxLayout.X_AXIS));
         imagePanel.add(gui.stackLastImageButton);
         imagePanel.add(gui.stackImageRightButton);
         
         imagePanel.add(gui.currentImageTextField);
         imagePanel.add(new JLabel(" out of "));
         imagePanel.add(gui.maxImageTextField);
         imagePanel.add(gui.stackImageLeftButton);
         imagePanel.add(gui.stackFirstImageButton);
         
         framePanel.setBorder(BorderFactory.createTitledBorder("Move in Frame Stack"));
         framePanel.setLayout(new BoxLayout(framePanel,BoxLayout.X_AXIS));
         framePanel.add(gui.stackLastFrameButton);
         framePanel.add(gui.stackFrameRightButton);
         
         framePanel.add(gui.currentFrameTextField);
         framePanel.add(new JLabel(" out of "));
         framePanel.add(gui.maxFrameTextField);
         framePanel.add(gui.stackFrameLeftButton);
         framePanel.add(gui.stackFirstFrameButton);
         
         applyPanel.setBorder(BorderFactory.createTitledBorder("Apply Mode"));
         applyPanel.setLayout(new BoxLayout(applyPanel,BoxLayout.X_AXIS));
         setMode(MULIT_MODE);
    }
    public void setMode(int newMode)
    {
        mainPanel.removeAll();
        applyPanel.removeAll();
        this.mode = newMode;
        switch(newMode)
        {
            case MULIT_MODE: 
                    mainPanel.add(imagePanel);
                    mainPanel.add(framePanel);
                    applyPanel.add(gui.applyFrameRadio);
                    applyPanel.add(gui.applyImageRadio);
                    applyPanel.add(gui.applyAllRadio);
                    mainPanel.add(applyPanel);
                    
                    break;
            case MULIT_FRAME_MODE: 
                    mainPanel.add(framePanel);
                    applyPanel.add(gui.applyFrameRadio);
                    applyPanel.add(gui.applyAllRadio);
                    mainPanel.add(applyPanel);
                    
                    break;
            case MULIT_IMAGE_MODE: 
                    mainPanel.add(imagePanel);
                    applyPanel.add(gui.applyImageRadio);
                    applyPanel.add(gui.applyAllRadio);
                    mainPanel.add(applyPanel);
                    
                    break;
                    
                    
        }
    }
	public Dimension getMinimumSize()
	{
	    return new Dimension (10,10);
	}
	//getPreferredSize()
	public void setConfiguration(Hashtable config, String orient)
	{
            
	    
	}
    
}
/*
 *  CVS Log
 *  $Log: StackFunctionPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

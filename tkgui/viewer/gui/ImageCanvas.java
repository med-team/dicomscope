/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.gui;

import java.awt.*;

import viewer.main.*;
import viewer.presentation.*;
import javax.swing.*;

/**
 * This class contains the GUI in which the image will be drawn.
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 */
public class ImageCanvas extends JPanel
{

    
    /**
     * Contains the current ScreenImageHandler.
     * 
     * @since 30.04.99
     */
    
    public ScreenImageHandler screenImageHandler;
    
    /**
     * Constructs an ImagaCanvas
     * 
     * @param screenImageHandler Contains the current ScreenImageHandler
     * @since 30.04.99
     */
    public ImageCanvas(ScreenImageHandler screenImageHandler)
    {
       super();
       this.screenImageHandler = screenImageHandler;
       setBackground(Color.black);
	   screenImageHandler.setImageCanvas(this);
		setDoubleBuffered(false);
		//{{REGISTER_LISTENERS
		//}}
	}
    
    /**
     * Standard paint
     * 
     * @param g graphic context
     * @since 30.04.99
     */
    /*public void paint(Graphics g) 
    {
        update(g);
    }*/
    /**
     * Draws the image on the GUI.
     * 
     * @param g graphic context
     * @since 30.04.99
     */
  /*  public void update(Graphics g) 
    {
         Graphics2D g1 = null;
         //Erstellen des Graphic
        System.out.println("''''''''''ImageCanvas.update");
        
        try
        {
           g1 = (Graphics2D) g;    
            if (g1 != null)
            {
                g1.setColor(Color.black);
                screenImageHandler.drawScreenImage(g1);
            }
        }
        finally 
        {
            g1.dispose() ;
        }
    }
    */
    public void paintComponent(Graphics g) 
    {
         Graphics2D g1 = null;
         //Erstellen des Graphic
        System.out.println("''''''''''ImageCanvas.update: " + getSize());
        
        try
        {
           g1 = (Graphics2D) g;    
            if (g1 != null)
            {
                g1.setColor(Color.black);
                screenImageHandler.drawScreenImage(g1);
            }
        }
        finally 
        {
            g1.dispose() ;
        }
    }

}    
/*
 *  CVS Log
 *  $Log: ImageCanvas.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
   


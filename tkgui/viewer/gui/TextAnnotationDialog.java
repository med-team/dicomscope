/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import jToolkit.gui.*;
import viewer.main.*;
import main.*;
import java.util.*;
import viewer.presentation.*;
import java.awt.event.*;
import J2Ci.*;
import de.microtherapy.tools.text.document.general.*;

/**
* Contains GUI for creating textual annotations
*/
public class TextAnnotationDialog extends JDialog
{
    JCheckBox anchorUnitCheckBox = new JCheckBox("Image Relative",true);
    JCheckBox boundingBoxUnitCheckBox = new JCheckBox("Image Relative",true);
    JCheckBox anchorVisibleCheckBox = new JCheckBox("Anchor Point Visible", true);
    
    JCheckBox boundingBox = new JCheckBox("Have Bounding Box",true);
    JCheckBox anchorBox = new JCheckBox("Have Anchor Point");
    
    JTextArea textArea = new JTextArea(new LimitedSizeDocument(1024),null,80,5);
    
    JRadioButton rot0 = new JRadioButton("0 degree",true);
    JRadioButton rot90 = new JRadioButton("90 degree");
    JRadioButton rot180 = new JRadioButton("180 degree");
    JRadioButton rot270 = new JRadioButton("270 degree");
    JButton okButton = new JButton("OK");
    JButton cancelButton = new JButton("Cancel");
    
    JRadioButton left = new JRadioButton("Left",true);
    JRadioButton right = new JRadioButton("Right");
    JRadioButton center = new JRadioButton("Center");
    
    boolean edit;
    ScreenImageHandler screenImageHandler;
    public TextAnnotationDialog(Frame parent, ScreenImageHandler screenImageHandler, boolean edit, PresentationStateTextObject textObject)
    {
        
        super(parent,true);
        this.edit = edit;
        setLocation(100,100);
        this.screenImageHandler = screenImageHandler;
        if (!edit)setTitle("Create Textual Annotation");
        else setTitle("Edit Textual Annotation");
        getContentPane().setLayout(new BorderLayout());
        
        JPanel textPanel = new JPanel(new BorderLayout());
        textPanel.setBorder(new TitledBorder("Text"));
        
        ScrollPane scroll = new ScrollPane();
        scroll.add(textArea);
        textPanel.add(scroll,BorderLayout.CENTER);
        
        JPanel orientationPanel = new JPanel();
        orientationPanel.add(new JLabel("Orientation"));
        ButtonGroup obg = new ButtonGroup();
        obg.add(left);
        orientationPanel.add(left);
        obg.add(center);
        orientationPanel.add(center);
        obg.add(right);
        orientationPanel.add(right);
        textPanel.add(orientationPanel,BorderLayout.SOUTH);
        
        getContentPane().add(textPanel, BorderLayout.CENTER);
        JPanel settingPanel = new JPanel();
        settingPanel.setLayout(new BoxLayout(settingPanel,BoxLayout.Y_AXIS));
        
        
        JPanel boxPanel = new JPanel(new GridLayout(2,1));
        boxPanel.setBorder(new TitledBorder("Bounding Box"));
        
        
        ButtonGroup bg = new ButtonGroup();
        bg.add(rot0);
        bg.add(rot90);
        bg.add(rot180);
        bg.add(rot270);
        
        
        JPanel rotPanel = new JPanel();
        rotPanel.add(new JLabel("Rotation: "));
        rotPanel.add(rot0);
        rotPanel.add(rot90);
        rotPanel.add(rot180);
        rotPanel.add(rot270);
        boxPanel.add(rotPanel);
        
        
        JPanel anchorPanel = new JPanel();
        anchorPanel.setBorder(new TitledBorder("Anchor Point"));
        anchorPanel.add(anchorBox);
        anchorPanel.add(anchorUnitCheckBox);
        anchorPanel.add(anchorVisibleCheckBox);
        
        
        if (!edit)
        {
            JPanel hPanel = new JPanel();
            hPanel.add(boundingBox);
            hPanel.add(boundingBoxUnitCheckBox);
            boxPanel.add(hPanel);
            settingPanel.add(boxPanel);
            settingPanel.add(anchorPanel);
        }
        else
        {
            if (textObject.haveBoundingBox()) settingPanel.add(boxPanel);
            int rot = textObject.getRot();
            if (rot == 1) rot90.setSelected(true);
            if (rot == 2) rot180.setSelected(true);
            if (rot == 3) rot270.setSelected(true);
             
            int just = textObject.getJust();
            if (just == 2) center.setSelected(true);
            if (just == 1) right.setSelected(true);
            
            textArea.setText(textObject.getTextValue());
            
            
        }
        
        
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        okButton.addActionListener(new OKAction());
        buttonPanel.add(cancelButton);
        cancelButton.addActionListener(new CancelAction());
        settingPanel.add(buttonPanel);
        
        
        getContentPane().add(settingPanel, BorderLayout.SOUTH);
        
        pack();
    }
    public void insertText()
    {
            if (! boundingBox.isSelected() && !anchorBox.isSelected())JOptionPane.showMessageDialog(null,"Please select anchor point or bounding box");
            else
            {
                if (textArea.getText().equals(""))JOptionPane.showMessageDialog(null,"Please insert text");
                else 
                {
                    int just = jDVPSTextJustification.DVPSX_left;
                    if (right.isSelected()) just = jDVPSTextJustification.DVPSX_right;
                    if (center.isSelected()) just = jDVPSTextJustification.DVPSX_center;
                    
                    int rotation = 0;
                    if (rot90.isSelected()) rotation = 1;
                    if (rot180.isSelected()) rotation = 2;
                    if (rot270.isSelected()) rotation = 3;
                    screenImageHandler.setNewText(textArea.getText(),
                                                boundingBox.isSelected(),
                                                anchorBox.isSelected(),
                                                !anchorUnitCheckBox.isSelected(),
                                                !boundingBoxUnitCheckBox.isSelected(),
                                                anchorVisibleCheckBox.isSelected(),
                                                just,
                                                rotation);
                    setVisible(false);
                }
            }
        
    }
    public void editText()
    {
            if (textArea.getText().equals(""))JOptionPane.showMessageDialog(null,"Please insert text");
            else
            {
                    int just = jDVPSTextJustification.DVPSX_left;
                    if (right.isSelected()) just = jDVPSTextJustification.DVPSX_right;
                    if (center.isSelected()) just = jDVPSTextJustification.DVPSX_center;
                    
                    int rotation = 0;
                    if (rot90.isSelected()) rotation = 1;
                    if (rot180.isSelected()) rotation = 2;
                    if (rot270.isSelected()) rotation = 3;
                    screenImageHandler.setNewTextValue(textArea.getText(), just, rotation);
                    setVisible(false);
         }
        
    }
    class OKAction extends AbstractAction
    {
        public OKAction()
        {
            super();
        }
        public void actionPerformed(ActionEvent e)
        {
            if (!edit) insertText();
            else editText();
        }
    }
    class CancelAction extends AbstractAction
    {
        public CancelAction()
        {
            super();
        }
        public void actionPerformed(ActionEvent e)
        {
            setVisible(false);
        }
    }
    
}
/*
 *  CVS Log
 *  $Log: TextAnnotationDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

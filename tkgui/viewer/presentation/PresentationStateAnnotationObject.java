/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.presentation;

import J2Ci.*;
import java.awt.*;
import java.util.*;
import java.awt.geom.*;

/**
 * This class is the superclass for all objects which handles
 * the GUI based functions of annotations. There are two types of annotations:
 * text annotations and graphic annotations. Both can have image and display relative
 * parts.
 * 
 * The data are stored in objects of the  c++ interface. 
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PresentationStateLayerObject
 * @see PresentationStateGraphicObject
 * @see PresentationStateTextObject
 */
public abstract class PresentationStateAnnotationObject extends PresentationStateLayerObject
{
    
    /**
    * Contains size for drawing the image.This is important for calculating
    * the display relative annotations
    *
    * @since 30.04.1999
    */
    Dimension screenSize;
    
    
    
    /**
    * Creates a new object.
    *
    * @since 30.04.1999
    */
    public PresentationStateAnnotationObject()
    {
        super();
        screenSize = new Dimension();
    }
    
    
    /**
    * Handles the changing of the display size of the image.
    * If the size for displaying the image is changing the display relative annotations must be
    * rebuild.
    *
    * @param The new size for displaying the image.
    * @since 30.04.1999
    */
    public abstract void setScreenSize(Dimension screenSize);
    
    public abstract void convertDisplayedAreas(float offsetX, float offsetY,float transX, float transY);
    
    
} 


/*
 *  CVS Log
 *  $Log: PresentationStateAnnotationObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

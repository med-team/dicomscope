/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.presentation;

import J2Ci.*;
import main.*;

import java.awt.*;
import java.util.*;
import java.awt.geom.*;
import java.awt.image.*;

/**
 * This class manages the overlay data of one overlay. The data 
 * structure is encapsulate in the jDVPrStateParam_GetOverlayData.
 * <br>
 * The handling of the overlays is inconsistent with the handling of the
 * annotations. First the overlays will be rotated and flipped in the interface, 
 * the annoations not. Therefore they need an other transformation. Secondly the 
 * overlays have less functionality than the annotations. Especially this 
 * programm can only show overlays, if exist. This programm can not create overlays.
 *
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see J2Ci.jDVPrStateParam_GetOverlayData
 * @see PresentationStateOverlayObject
 */
public  class PresentationStateOverlayObject 
{
    
    /**
     * Contains the index layer of the overlay.
     * 
     * @since 30.04.1999
     */
   int layerIndex;
    
    /**
     * Contains the index of the overlay in the layer with the index layerIndex.
     * 
     * @since 30.04.1999
     */
   int index;
    /**
     * Contains data structure of the presentation state in the c++ interface.
     * 
     * @since 30.04.1999
     * @see J2Ci.jDVPresentationState
     */
   public jDVPresentationState ps;
   
    /**
     * Contains data structure of the overlay in the c++ interface.
     * 
     * @since 30.04.1999
     * @see J2Ci.jDVPrStateParam_GetOverlayData
     */
   jDVPrStateParam_GetOverlayData overlayData;
    
    /**
     * Contains the number of bits used for printing the overlay
     */
    public int bits;
    
    /**
     * Contains the geometric representation of the overlay.
     * 
     * @since 30.04.1999
     */
   BufferedImage overlayImage;
    
    /**
     * Constructs a new object form the specified parameters.
     * 
     * @param ps Contains the current jDVPresentationState
     * @param layerIndex Contains the layer index of the overlay
     * @param imageIndex Contains the index of the overlay in the specified layer.
     * @since 30.04.1999
     */
   public PresentationStateOverlayObject(jDVPresentationState ps, int layerIndex, int imageIndex, int bits)
   {
        this.ps = ps;
        this.bits = bits;
        this.layerIndex = layerIndex;
        this.index = imageIndex;
        getData();
   }
    
    /**
     * Draws the overlay in the specified Graphics2D context.
     * 
     * @param g2 Contains the Graphics2D object
     * @param aff Contains applying transformations
     * @param imageIndex Contains the index of the overlay in the specified layer.
     * @since 30.04.1999
     */
    public void draw(Graphics2D g2, AffineTransform aff)
    {
        //System.out.println("Aff: "+ aff);
        g2.setTransform(aff);
        g2.drawImage(overlayImage,overlayData.left,overlayData.top ,null);
        //g2.drawImage(overlayImage, new AffineTransform(1f,0f,0f,1f,overlayData.left,overlayData.top), null);       
    }
    
    /**
     * Gets the overlay data from the c++ interface and create the 
     * geometric representation.
     *
     * @since 30.04.1999
     * @see J2Ci.jDVPrStateParam_GetOverlayData
     */
    public void getData()
    {
        //Contains the DataBuffer
        DataBuffer overlayBuffer = null;
        
        //Contains the WritableRaster
        WritableRaster wr ;
        
        //Contains the offset
        int bandOffsets[] = {0};
        
        overlayData = new jDVPrStateParam_GetOverlayData();
        overlayData.layer = layerIndex;
        overlayData.idx = index;
        overlayData.bits = bits;
        int status = ps.getOverlayData(overlayData);
        
        IndexColorModel overlayColorModel = null;
        //Creates the BufferedImage
        if (bits == 8) 
        {
            overlayBuffer =  new DataBufferByte(overlayData.overlayData,overlayData.width* overlayData.height);
            System.out.println("overlay trans: " + overlayData.foreground);
            overlayColorModel = GrayColorIndex.getOverlayGrayColorModel(overlayData.foreground);
            //GrayColorIndex.countValues(overlayData.overlayData);
        }
        else 
        {
            overlayBuffer =  new DataBufferUShort(overlayData.overlayData12,overlayData.width* overlayData.height);
            overlayColorModel = GrayColorIndex.getOverlayGray12ColorModel(overlayData.foreground);
            //GrayColorIndex.countValues(overlayData.overlayData12);
            
        }
         
         wr = Raster.createInterleavedRaster(overlayBuffer,
                                            overlayData.width,
                                            overlayData.height,
                                            overlayData.width,
                                            1, bandOffsets,null);
        
       
        overlayImage = new BufferedImage(overlayColorModel, wr, overlayColorModel.isAlphaPremultiplied(), null);
    }
} 



/*
 *  CVS Log
 *  $Log: PresentationStateOverlayObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 10:15:38 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
 */

package viewer.presentation;

import J2Ci.*;

import java.awt.*;
import java.util.*;
import java.awt.geom.*;

/**
 * This class handles the graphic annoations.
 * This class  implementing only the GUI-based methods,
 * the data are stored in the c++ interface jDVPSGraphicObject. Every
 * PresentationStateGraphicObject must have one jDVPSGraphicObject which stores
 * the data of a graphic annotation.
 *<BR>
 * Each jDVPSGraphicObject has an index in the jDVPresentationState,
 * this index must be the same as the index in this class ( defined
 * in the super class PresentationStateLayerObject).
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PresentationStateAnnotatonObject
 * @see PresentationStateLayerObject
 * @see J2Ci.jDVPSGraphicObject
 */
public class PresentationStateGraphicObject extends PresentationStateAnnotationObject {
    
    
    /**
     * Contains the geometric representation of a
     * PresentationStateGraphicObject as a Shape, which can be drawn in any
     * Graphics2D object. If the PresentationStateGraphicObject is display relative, the Shape
     * can be drawn without transformations. If the PresentationStateGraphicObject is
     * image relative the shape must be transform.
     *
     * @since 30.04.1999
     */
    Shape thisShape = null;
    
    
    /**
     * Contains the data of a graphic annotation in a c++ interface.
     *
     * @since 30.04.1999
     */
    jDVPSGraphicObject graphicObject;
    
    DisplayArea da;
    /**
     * Constructs a new object.
     *
     * @since 30.04.1999
     */
    private PresentationStateGraphicObject() {
        super();
        
    }
    
    /**
     * Constructs a new PresentationStateGraphicObject and sets the  specified attributs
     * in the jDVPSGraphicObject.
     *
     * @param graphicObject Contains the data of a graphic annotation in a c++ interface.
     * @param index Contains the index of the PresentationStateGraphicObject.
     * @param type Contains the new graphic type of the jDVPSGraphicObject.
     * @param pointList Contains the new points of the  jDVPSGraphicObject.
     * @param annotationUnits Contains the new annotation units.
     * @param isFilled True if the jDVPSGraphicObject shuold be set to filled.
     * @param screenSize Contains the size for displaying.
     * @param aff Contains the applying transformations.
     * @since 30.04.1999
     */
    public PresentationStateGraphicObject(  jDVPSGraphicObject graphicObject,
    int index,
    int type,
    float[] pointList,
    int annotationUnits,
    boolean isFilled,
    Dimension screenSize,
    AffineTransform aff) {
        this();
        this.index = index;
        this.graphicObject = graphicObject;
        this.screenSize= screenSize;
        this.aff = aff;
        
        graphicObject.setFilled(isFilled);
        graphicObject.setGraphicType(type);
        
        if (annotationUnits == 1) {
            Point2D.Float point = new Point2D.Float();
            for (int i = 0; i < pointList.length/2;i++) {
                aff.transform( new Point2D.Float(pointList[2*i]-1,pointList[2*i+1]-1),point);
                pointList[2*i] = point.x/screenSize.width;
                pointList[2*i+1] = point.y/screenSize.height;
                
            }
            
        }
        graphicObject.setData(pointList.length/2, pointList, annotationUnits);
        buildShape();
        
    }
    /**
     * Constructs a PresentationStateGraphicObject form the specified jDVPSGraphicObject
     * with the specified index
     *
     * @param graphicObject Contains the data of a graphic annotation in a c++ interface.
     * @param index Contains the index of the PresentationStateGraphicObject.
     * @since 30.04.1999
     */
    public PresentationStateGraphicObject(jDVPSGraphicObject graphicObject, DisplayArea da,int index) {
        this();
        this.da = da;
        this.index = index;
        this.graphicObject = graphicObject;
        //Builds the shape, if pixel relative
        if (getAnnotationUnits() ==0) buildShape();
    }
    
    /**
     * Checks if the PresentationStateGraphicObject is closed. An PresentationStateGraphicObject is cloed if the fist and the last
     * point are equal and the PresentationStateGraphicObject has more than 2 points.
     *
     * @return True if the annotation is closed.
     * @since 30.04.1999
     */
    
    public boolean isClosed() {
        if (getGraphicType() == jDVPSGraphicType.DVPST_ellipse) return true;
        if (getGraphicType() == jDVPSGraphicType.DVPST_circle) return true;
        //if (getGraphicType() == jDVPSGraphicType.DVPST_interpolated) return false;
        if (getNumberOfPoints() < 3) return false;
        else return getPoint(0).equals(getPoint(getNumberOfPoints()-1));
        
    }
    
    /**
     * Moves the PresentationStateGraphicObject analog to the specified points.
     * The first point is the start point and the secound point is the point where the
     * first point is moved to. The PresentationStateGraphicObject will be moved
     * analog to the result vector.
     *
     * @param moveAnn Start moving point.
     * @param point Stop moving point.
     * @since 30.04.1999
     */
    public void moveTo(Point2D.Float moveAnn,Point2D.Float point ) {
        
        
        float[] newPixels = new float[getNumberOfPoints()*2];
        Point2D.Float translatedPoint1 = new Point2D.Float();
        Point2D.Float translatedPoint2 = new Point2D.Float();
        Point2D.Float translatedPoint = new Point2D.Float();
        
        //image relative
        if (getAnnotationUnits() == 0) {
            translatedPoint1 = getInverseTransformedPoint(moveAnn,aff);
            translatedPoint2 = getInverseTransformedPoint(point,aff);
            translatedPoint.x = translatedPoint2.x-translatedPoint1.x;
            translatedPoint.y = translatedPoint2.y-translatedPoint1.y;
            
            for (int i = 0; i < getNumberOfPoints(); i++) {
                newPixels[2*i]  = getPoint(i).x+translatedPoint.x;
                newPixels[2*i+1]  = getPoint(i).y +translatedPoint.y;
            }
            
            graphicObject.setData(newPixels.length/2, newPixels,0);
            
        }
        //Display relative
        else {
            Point2D.Float diff = new Point2D.Float();
            diff.x = point.x-moveAnn.x;
            diff.y = point.y-moveAnn.y;
            
            for (int i = 0; i < getNumberOfPoints(); i++) {
                newPixels[2*i]  = getPoint(i).x+diff.x/screenSize.width;
                newPixels[2*i+1]  =getPoint(i).y+ diff.y/screenSize.height;
                
            }
            
            graphicObject.setData(getNumberOfPoints(), newPixels, 1);
            
        }
        buildShape();
        
    }
    
    
    /**
     * Rebuilds the display relative annotations if the size for displaying the image
     * has changed.
     *
     * @param The new screensize of the annotation.
     * @since 30.04.1999
     */
    public void setScreenSize(Dimension screenSize) {
        this.screenSize = screenSize;
        if (getAnnotationUnits() ==jDVPSannotationUnit.DVPSA_display) if (screenSize!= null)buildShape();
        
    }
    
    /*
     * Builds the shape form the jDVPSGraphicObject
     *
     * @since 30.04.1999
     */
    public void buildShape() {
        
        //Contructs Polyline
        if (getGraphicType() == jDVPSGraphicType.DVPST_polyline) thisShape = buildPolyline();
        
        //Contructs Interpolated??
        else if (getGraphicType() == jDVPSGraphicType.DVPST_interpolated)thisShape = buildInterpolated();
        
        //Contructs Circle
        else if (getGraphicType() == jDVPSGraphicType.DVPST_circle)thisShape = buildCircle();
        
        else if (getGraphicType() == jDVPSGraphicType.DVPST_ellipse)thisShape = buildEllipse();
        
        else if (getGraphicType() == jDVPSGraphicType.DVPST_point)thisShape = buildPoint();
    }
    
    
    
    
    /**
     * Draws the shape of the graphics annotation with the specified
     * transformation in the specified Graphics2d object
     *
     * @param g2 The context in which this object will be drawn.
     * @param aff Contains the applying transformation.
     * @since 30.04.1999
     */
    public  void draw(Graphics2D g2,AffineTransform aff, boolean scale) {
        
        System.out.println(getInfo());
        this.aff = aff;
        if ((g2!=null) && (thisShape!= null)) {
            //System.out.println("**********draw Image Rel*************************");
            if (getAnnotationUnits() ==0) {
                
                g2.setTransform(aff);
                g2.draw((thisShape));
                
                if ((isfilled()&& isClosed())||getGraphicType() == jDVPSGraphicType.DVPST_point)  g2.fill(thisShape);
                if (!isActive())  drawBounding(g2,aff);
            }
            else {
                //System.out.println("**********draw Display rel*************************");
                g2.setTransform(new AffineTransform());
                // g2.setStroke(graphicStroke);
                g2.draw(thisShape);
                if ((isfilled()&& isClosed())||getGraphicType() == jDVPSGraphicType.DVPST_point)  g2.fill(thisShape);
                
            }
        }
    }
    public  void convertDisplayedAreas(float offsetX, float offsetY,float transX, float transY) {
        System.out.println("transY: "+transY);
        System.out.println("transX: "+transX);
        System.out.println("offsetY: "+offsetY);
        System.out.println("offsetX: "+offsetX);
        
        if (getAnnotationUnits() ==jDVPSannotationUnit.DVPSA_display) {
            int number = getNumberOfPoints();
            float[] points = new float[number*2];
            Point2D.Float p = new Point2D.Float();
            for (int i = 0; i< number; i++) {
                p = getPoint(i);
                System.out.println("p: "+p);
                
                points[2*i] = offsetX + p.x*transX;
                points[2*i+1] = offsetY + p.y*transY;
                
            }
            graphicObject.setData(number, points, jDVPSannotationUnit.DVPSA_display);
            buildShape();
        }
        
    }
    
    /**
     * Marked the PresentationStateGraphicObject with Rectangles in the specified
     * Graphics2D context.
     *
     * @param g2 The context in which this object will be merked.
     * @param aff Contains the applying transformation.
     * @since 30.04.1999
     */
    public void drawBounding(Graphics2D g2,AffineTransform aff) {
        Rectangle2D boundingRect;
        if (getAnnotationUnits()==0) {
            boundingRect = aff.createTransformedShape(thisShape.getBounds2D()).getBounds2D();
        }
        else {
            boundingRect = thisShape.getBounds2D();
            
        }
        
        g2.setTransform(new AffineTransform());
        
        g2.draw(new Rectangle2D.Double(boundingRect.getX()- 6,boundingRect.getY()-6,5,5));
        g2.draw(new Rectangle2D.Double(boundingRect.getX()+boundingRect.getWidth()+1,boundingRect.getY()-6,5,5));
        g2.draw(new Rectangle2D.Double(boundingRect.getX()-6,boundingRect.getY()+boundingRect.getHeight()+1,5,5));
        g2.draw(new Rectangle2D.Double(boundingRect.getX()+boundingRect.getWidth()+1,boundingRect.getY()+boundingRect.getHeight()+1,5,5));
        
    }
    
    
    /**
     * Checks if the graphic annotation is filled.
     *
     * @return True if graphic is filled.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#isFilled
     */
    public boolean isfilled() {
        return graphicObject.isFilled();
    }
    
    
    /**
     * Gets the graphic annotation units.
     *
     * @return graphic annotation units.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#getAnnotationUnits
     */
    public int getAnnotationUnits() {
        return graphicObject.getAnnotationUnits();
    }
    
    
    /**
     * Gets the graphic type of the graphic object.
     *
     * @return graphic type of the graphic object.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#getGraphicType
     */
    int getGraphicType() {
        return graphicObject.getGraphicType();
    }
    
    /**
     * Gets the point at the specified index.
     *
     * @param index Specifies the index.
     * @return The point at the specified index.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#getPoint
     */
    Point2D.Float getPoint(int index) {
        jDoubleByRef x = new jDoubleByRef();
        jDoubleByRef y  = new jDoubleByRef();
        graphicObject.getPoint(index,x,y);
        return new Point2D.Float((float)x.value, (float) y.value);
    }
    /**
     * Gets the point at the specified index.
     *
     * @param index Specifies the index.
     * @return The point at the specified index.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#getPoint
     */
    Point2D.Double getPointDouble(int index) {
        jDoubleByRef x = new jDoubleByRef();
        jDoubleByRef y  = new jDoubleByRef();
        graphicObject.getPoint(index,x,y);
        return new Point2D.Double(x.value,  y.value);
    }
    
    /**
     * Gets the number of points in the annotation.
     *
     * @return The number of points in the annotation.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#getNumberOfPoints
     */
    int getNumberOfPoints() {
        return graphicObject.getNumberOfPoints();
    }
    
    
    /**
     * Caluclates an new shape form a polyline graphic annotation.
     *
     * @return The shape of the annotation.
     * @since 30.04.1999
     */
    Shape buildPolyline() {
        //Shape object
        GeneralPath generalPath= new GeneralPath();
        
        //Image realtive annotation
        if ( getAnnotationUnits() == 0) {
            generalPath.moveTo((getPoint(0).x-1), (getPoint(0).y-1));
            for (int i = 1; i< getNumberOfPoints(); i++) generalPath.lineTo(getPoint(i).x-1, getPoint(i).y-1);
        }
        //Display realtive annotation
        else {
            generalPath.moveTo(((getPoint(0).x)*(screenSize.width)),(getPoint(0).y*screenSize.height));
            for (int i = 1; i< getNumberOfPoints(); i++) generalPath.lineTo(getPoint(i).x*screenSize.width, getPoint(i).y*screenSize.height);
            
        }
        return generalPath;
        
    }
    
    /**
     * Caluclates an new shape form a interpolated graphic annotation.
     *
     * @return The shape of the annotation.
     * @since 30.04.1999
     */
    Shape buildInterpolated() {
        GeneralPath generalPath= null;
        
        //Image realtive annotation
        if ( getAnnotationUnits() == 0) {
            int size = getNumberOfPoints();
            Point2D.Double[] pts = new Point2D.Double[size];
            Point2D.Double p;
            for (int i = 0; i< size; i++) {
                p = getPointDouble(i);
                pts[i] = new Point2D.Double(p.x-1,p.y-1);
            }
            generalPath = viewer.paint.GeometryTool.getCubicSpline(pts, 1d);
            
            
        }
        //Display realtive annotation
        else {
            
            int size = getNumberOfPoints();
            Point2D.Double[] pts = new Point2D.Double[size];
            Point2D.Double p;
            for (int i = 0; i< size; i++) {
                p = getPointDouble(i);
                pts[i] = new Point2D.Double(p.x*screenSize.width,p.y*screenSize.height);
            }
            generalPath = viewer.paint.GeometryTool.getCubicSpline(pts, 1d);
            
            
        }
        return generalPath;
        
    }
    
    
    /**
     * Caluclates an new shape form a ellipse graphic annotation.
     *
     * @return The shape of the annotation.
     * @since 30.04.1999
     */
    Shape buildEllipse() {
        System.out.println("zzzzz");
        //Shape object
        Ellipse2D.Float ellipse;
        
        //TLHC of the circle
        float majMinX;
        float majMinY;
        float minMinX;
        float minMinY;
        
        //Calculates letfx
        if (getPoint(0).x< getPoint(1).x) {
            majMinX = getPoint(0).x  ;
        }
        else {
            majMinX = getPoint(1).x ;
        }
        
        //calculates lefty
        if (getPoint(0).y< getPoint(1).y) {
            majMinY = getPoint(0).y;
        }
        else {
            majMinY = getPoint(1).y;
        }
        //Calculates letfx
        if (getPoint(2).x< getPoint(3).x) {
            minMinX = getPoint(2).x  ;
        }
        else {
            minMinX = getPoint(3).x ;
        }
        
        //calculates lefty
        if (getPoint(2).y< getPoint(3).y) {
            minMinY = getPoint(2).y;
        }
        else {
            minMinY = getPoint(3).y;
        }
        
        float leftx;
        float lefty;
        float height;
        float width;
        if (minMinY<majMinY) {
            
            height =    Math.abs(getPoint(0).x-getPoint(1).x);
            width=      Math.abs(getPoint(2).y-getPoint(3).y);
            leftx=majMinX;
            lefty=minMinY;
            /*
              System.err.println("getPoint(0)" +getPoint(0));
            System.err.println("getPoint(1)" +getPoint(1));
            System.err.println("getPoint(2)" +getPoint(2));
            System.err.println("getPoint(3)" +getPoint(3));
            System.err.println("11 height "+ height+"; width: " +width+", leftx" +leftx+", leftx: " +lefty);
             */
        } else {
            
            
            width =    Math.abs(getPoint(0).y-getPoint(1).y);
            height=      Math.abs(getPoint(2).x-getPoint(3).x);
            leftx=minMinX;
            lefty=majMinY;
            /*
            System.err.println("getPoint(0)" +getPoint(0));
            System.err.println("getPoint(1)" +getPoint(1));
            System.err.println("getPoint(2)" +getPoint(2));
            System.err.println("getPoint(3)" +getPoint(3));
            System.err.println("11 height "+ height+"; width: " +width+", leftx" +leftx+", leftx: " +lefty);
             */
        }
        
        
        
        
        if ( getAnnotationUnits() == 0) {
            
            ellipse =  new Ellipse2D.Float(leftx-1,lefty-1,height-1,width-1);
        }
        else {
            
            ellipse =  new Ellipse2D.Float(  leftx*screenSize.width,
            lefty*screenSize.height,
            height*screenSize.width,
            width*screenSize.height);
             System.out.println("Ellipse:" +leftx+ ", " +screenSize.toString());
        }
         
        return ellipse;
    }
    
    
    /**
     * Caluclates an new shape form a circle graphic annotation.
     *
     * @return The shape of the annotation.
     * @since 30.04.1999
     */
    Shape buildCircle() {
        Ellipse2D.Float circle;
        float leftx;
        float lefty;
        float radius;
        Point2D.Float pointCenter;
        Point2D.Float pointAtCirc;
        
        
        if ( getAnnotationUnits() == 0) {
            //System.out.println(" Image relative Annotation");
            pointCenter = new Point2D.Float(getPoint(0).x-1f,getPoint(0).y-1f);
            pointAtCirc = new Point2D.Float(getPoint(1).x-1f,getPoint(1).y-1f);
            
        }
        else {
            //System.out.println(" Display relative Annotation");
            pointCenter = new Point2D.Float(getPoint(0).x*screenSize.width,getPoint(0).y*screenSize.height);
            pointAtCirc = new Point2D.Float(getPoint(1).x*screenSize.width,getPoint(1).y*screenSize.height);
            
        }
        
        //Calculates radius
        radius =  (float)Math.sqrt( (pointCenter.x-pointAtCirc.x)*
        (pointCenter.x-pointAtCirc.x) +
        (pointCenter.y-pointAtCirc.y)*
        (pointCenter.y-pointAtCirc.y));
        
        leftx = pointCenter.x-radius;
        lefty = pointCenter.y-radius;
        circle = new Ellipse2D.Float(leftx, lefty, 2*radius-1,2*radius-1);
        
        //System.out.println("Circle: Annotation " + leftx + ", " +lefty + ", " + radius);
        return circle;
    }
    /**
     * Caluclates an new shape form a circle graphic annotation.
     *
     * @return The shape of the annotation.
     * @since 30.04.1999
     */
    Shape buildPoint() {
        System.out.println("-----------------------------buildPoint");
        Ellipse2D.Float circle;
        float leftx;
        float lefty;
        float radius;
        Point2D.Float pointCenter;
        Point2D.Float pointAtCirc;
        
        
        if ( getAnnotationUnits() == 0) {
            //System.out.println(" Image relative Annotation");
            pointCenter = new Point2D.Float(getPoint(0).x-1f,getPoint(0).y-1f);
            
        }
        else {
            //System.out.println(" Display relative Annotation");
            pointCenter = new Point2D.Float(getPoint(0).x*screenSize.width,getPoint(0).y*screenSize.height);
            
        }
        
        //Calculates radius
        radius =  5;
        
        leftx = pointCenter.x-radius;
        lefty = pointCenter.y-radius;
        circle = new Ellipse2D.Float(leftx, lefty, 2*radius-1,2*radius-1);
        
        //System.out.println("Circle: Annotation " + leftx + ", " +lefty + ", " + radius);
        return circle;
    }
    
    
    /**
     * Returns a String representing this object.The string contains the graphic
     * type of the annotation and the annatation unit.
     *
     * @return A String representing this object.
     * @since 30.04.1999
     */
    public String getListText() {
        String returnString = new String();
        if (getAnnotationUnits()== 0)returnString = returnString.concat("image rel. graphic:");
        else returnString = returnString.concat("display rel. graphic: ");
        if (getGraphicType()== 1)returnString = returnString.concat("polyline");
        if (getGraphicType()== 2)returnString = returnString.concat("interpol.");
        if (getGraphicType()== 3)returnString = returnString.concat("circle");
        if (getGraphicType()== 4)returnString = returnString.concat("ellipse");
        if (getGraphicType()== 0)returnString = returnString.concat("point");
        
        return returnString;
    }
    
    
    /**
     * Returns a String representing this object.
     *
     * @return A String representing this object
     * @since 30.04.1999
     */
    public String getInfo() {
        
        String returnString;
        
        //fill attributes
        returnString = new String(  "    PresenationStateGraphicsObject" +  "\n"+"\n"+
        "      isfilled: "+   isfilled() + "\n" +
        "      getAnnotationUnits: "+   getAnnotationUnits() + "\n" +
        "      getGraphicType: "+   getGraphicType() + "\n" );
        
        //fill points
        for (int i = 0; i < getNumberOfPoints(); i++) {
            returnString = returnString.concat("        Point " + i  + ": X = " + getPoint(i).getX() + ", Y = " + getPoint(i).getY()+"\n");
        }
        
        return returnString;
    }
    
    
    /**
     * Deletes the object.
     *
     * @since 30.04.1999
     */
    public void deleteAll() {
        
        graphicObject = null;
        thisShape = null;
    }
    
    /**
     * Filles/unfilles the annotation.
     *
     * @param filled True if the annatation shuold be filled.
     * @since 30.04.1999
     * @see J2Ci.jDVPSGraphicObject#setFilled
     */
    public void setFilled(boolean filled) {
        graphicObject.setFilled(filled);
    }
    
    
    /**
     * Check if this object contains the specified point.
     *
     * @return true if this object contains the specified point.
     * @since 30.04.1999
     */
    public boolean contains(Point2D.Float point) {
        if (getNumberOfPoints() <= 2) {
            Rectangle2D boundsRect;
            boundsRect = thisShape.getBounds2D();
            if (getAnnotationUnits() == 0)
                return (new Rectangle2D.Double(boundsRect.getX()-1f,boundsRect.getY()-1f,boundsRect.getWidth()+2d, boundsRect.getHeight()+2d  )).contains(getInverseTransformedPoint(point,aff));
            else
                return (new Rectangle2D.Double(boundsRect.getX()-1f,boundsRect.getY()-1f,boundsRect.getWidth()+2d, boundsRect.getHeight()+2d  )).contains(point);
        }
        if (getAnnotationUnits() == 0) return thisShape.contains(getInverseTransformedPoint(point,aff));
        else return thisShape.contains(point);
    }
    
    
    /**
     * If the annotation units is display relative this function
     * converts the annotation to image relative and vice versa.
     *
     * @since 30.04.1999
     */
    public void convert() {
        float[] newPixels = new float[getNumberOfPoints()*2];
        Point2D.Float translatedPoint = new Point2D.Float();
        //Convert image relative to display relative
        if (getAnnotationUnits() == 0) {
            for (int i = 0; i < getNumberOfPoints(); i++) {
                aff.transform(new Point2D.Float(getPoint(i).x-1 ,getPoint(i).y-1),translatedPoint);
                newPixels[2*i]  = translatedPoint.x/screenSize.width;
                newPixels[2*i+1]  = translatedPoint.y/screenSize.height;
                
            }
            for (int i = 0; i< newPixels.length; i++) System.out.println("klausnewPoint: " +i + ", : " + newPixels[i]);
            
            graphicObject.setData(getNumberOfPoints(), newPixels, 1);
        }
        //Convert display relative to image relative.
        else {
            for (int i = 0; i < getNumberOfPoints(); i++) {
                try {
                    aff.inverseTransform(new Point2D.Float(getPoint(i).x*screenSize.width,getPoint(i).y*screenSize.height),translatedPoint);
                }
                catch(NoninvertibleTransformException e)
                {}
                newPixels[2*i]  = translatedPoint.x+1;
                newPixels[2*i+1]  = translatedPoint.y+1;
            }
            
            graphicObject.setData(newPixels.length/2, newPixels,0);
            
        }
        buildShape();
    }
    
}



/*
 *  CVS Log
 *  $Log: PresentationStateGraphicObject.java,v $
 *  Revision 1.2  2003/09/08 10:15:38  kleber
 *  Bugfix: The first two points of Ellipse are definesd as the major axis
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 */

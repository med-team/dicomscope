/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.presentation;

import java.awt.geom.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;

import main.*;
import J2Ci.*;

/**
 * This class contains and gets the current image pixels from 
 * the C++ interface. The visible part of the image and the 
 * shutter can be drawn in a BufferedImage. The visible part of the 
 * image will be zoomed with java functions on the specified size.
 * 
 * @author Klaus Kleber.
 * @since 30.04.1999
 * @see TransformationConverter
 */
public class JavaTransformationConverter extends TransformationConverter
{
    /**
     * Contains the current image.
     * 
     * @since 30.04.1999
     */
     BufferedImage dicomImage;
    
    /**
     * Contains the current image pixels.
     * 
     * @since 30.04.1999
     */
    DataBufferByte dbb; 
    
    
    /**
     * Constructs an new Object whit an copy of the image data.
     * 
     * @param presentationStateGraphicsHandler Contains the current PresentationStateGraphicsHandler
     * @since 30.04.1999
     */
    public JavaTransformationConverter(PresentationStateGraphicsHandler presentationStateGraphicsHandler)
    {
        super(presentationStateGraphicsHandler);
    }
    
    /**
     * Gets the pixel data form the c++ part if newPixels = ture, draws the 
     * image into the specified BufferedImage which will be drawn on the screenand calculates the transformations for the
     * Annotations, Shutters, Overlays usw. The Shutter will be cilpped to the
     * BufferedImage.
     * 
     * @param bufferedImage Contains the BufferedImage which will be drawn on the screen.
     * @param newPixels true if the pixel should be request form the c++ part.
     * @since 30.04.1999
     */
    public void draw(BufferedImage bufferedImage, boolean newPixels, boolean newBackground)
    {
       
       //Load pixeldata if changed
       if (newPixels) getNewPixels();
	   
	   //Clearing Background of image whit the specified ShutterColor
        byte colorback = presentationStateGraphicsHandler.shutterList.getGreyColorByte();
        byte[] byteArray = ((DataBufferByte)(bufferedImage.getRaster().getDataBuffer())).getData();
        
        int size = displayDimension.width*displayDimension.height;
        
        // cost a lost of time
        // we have to optimize it.
        if (newBackground) 
        {
            for (int i=0; i< size; i++) byteArray[i] = colorback;
        }
        //Paint in Screen
        BufferedImage subBufferedImage = null;
        subBufferedImage = bufferedImage.getSubimage(displayPixelTLHC.x,displayPixelTLHC.y,bufferedImage.getWidth()-displayPixelTLHC.x,bufferedImage.getHeight()-displayPixelTLHC.y);
        BufferedImage subDicomImage = null;
        subDicomImage = dicomImage.getSubimage(sourcePixelTLHC.x,sourcePixelTLHC.y,dicomImage.getWidth()-sourcePixelTLHC.x,dicomImage.getHeight()-sourcePixelTLHC.y);
        
        Graphics2D g2 = (Graphics2D)subBufferedImage.getGraphics();
        
        if (g2!= null)
        {
            try
            {
                
                //Sets the vector shutters as a clipping area.
                AffineTransform shutterAff = new AffineTransform();
                shutterAff.translate(-displayPixelTLHC.x, -displayPixelTLHC.y);
                shutterAff.concatenate(aff);
                
                
                //Sets the clipping area
                presentationStateGraphicsHandler.shutterList.setClip(g2,shutterAff);
                
                
                g2.setTransform(new AffineTransform());
                
                AffineTransform zoomAff = new AffineTransform();
                zoomAff.scale(zoomValue*presentationStateGraphicsHandler.getCurrentScalingX(), zoomValue*presentationStateGraphicsHandler.getCurrentScalingY());
                
                //Draws and zooms the image
                //int gray = (int)(colorback & 0xff);
                //g2.setColor(new Color(gray,gray,gray));
               // g2.setBackground(new Color(gray,gray,gray));
                g2.drawRenderedImage(subDicomImage, zoomAff);
                
                
	            //removes any clipping Area
	            g2.setClip(null);
                
            }
            finally
            {
            g2.dispose();
            }
            
        }
    }
    
    public void printImage()
    {
    }
    
	/**
	 * Gets the image pixel data from the C++ part of the code.
	 * 
	 * @since 30.04.1999
	 */
	public void getNewPixels()
	{
        
        WritableRaster wr ;
        int bandOffsets[] = {0};
        
	    //System.out.println("setPixels");
	    int status = jE_Condition.EC_Normal;
	    if ((dbb == null) || (dbb.getSize() !=presentationStateGraphicsHandler.getImageWidth()*presentationStateGraphicsHandler.getImageHeight()))
        {
            if (dbb != null)
            {
                dbb = null;
                System.gc();
            }
            dbb = new DataBufferByte(new byte[presentationStateGraphicsHandler.getImageWidth()*presentationStateGraphicsHandler.getImageHeight()],
                                    presentationStateGraphicsHandler.getImageWidth()*presentationStateGraphicsHandler.getImageHeight());

        }
	  
	    //gets the pixel
	    status = presentationStateGraphicsHandler.ps.getPixelData(dbb.getData(), (long) dbb.getSize());
	    
	    if (status != jE_Condition.EC_Normal) System.out.println("Fehler bei ps.getPixelData");
        
        //Create new Image
        wr = Raster.createInterleavedRaster(dbb,
                                            presentationStateGraphicsHandler.getCurrentImageWidth(),
                                            presentationStateGraphicsHandler.getCurrentImageHeight(),
                                            presentationStateGraphicsHandler.getCurrentImageWidth(),
                                            1, bandOffsets,null);
        dicomImage = new BufferedImage(MainContext.instance().getIndexColorModel(), wr, MainContext.instance().getIndexColorModel().isAlphaPremultiplied(), null);
          /*  byte[] help = dbb.getData();
            for (int i = 0; i<presentationStateGraphicsHandler.getImageWidth(); i++)
                System.out.println("I : " + i + ", : " + help[i]);
	   */ 
	}

}



/*
 *  CVS Log
 *  $Log: JavaTransformationConverter.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

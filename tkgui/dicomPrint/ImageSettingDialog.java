/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package dicomPrint;

import java.util.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import jToolkit.gui.*;
import viewer.gui.*;
import J2Ci.*;

/**
 * This class contains the dialog for 
 * setting the image print options.
 * 
 * @author Klaus Kleber
 * @since 01.10.99
 */
public class ImageSettingDialog extends JDialog implements CommandButtonListener
{

    
    //Ids for handling the action Events
    static final int ID_OK =0;
    static final int ID_DEFAULT =1;
    static final int ID_DELETE =2;
    static final int ID_IMAGEMAGNIFICATION = 200;
    static final int ID_IMAGESMOOTHING = 201;
    static final int ID_IMAGECONFIGURATION = 202;
    
    /**
     * Contains the delete Button
     */
    CommandJButton deleteButton;
    
    /**
     * Contains the ob Button
     */
    CommandJButton okButton;
    
    /**
     * Contains the default Button
     */
    CommandJButton defaultButton;
     
    /**
     * Contains current jDVPSStoredPrint object with informations about the print settings of the print job
     */
    jDVPSStoredPrint storedPrint;
    
    /**
     * Contains the DVInterface.
     */
    jDVInterface dvi ;
     
    /**
     * Contains the ComboBox for changing the magnification type
     */
    CommandJComboBox imageMagnificationCombo;

    /**
     * Contains the ComboBox for changing the smoothing type
     */
    CommandJComboBox imageSmoothingCombo;

    /**
     * Contains the ComboBox for changing the configuration
     */
    CommandJComboBox imageConfigurationCombo;
    
    /**
     * Contains the  printImagePreview of the changing image.
     */
    PrintImagePreview printImagePreview;
    
    /**
     * Contains the  current PrintPanel
     */
    PrintPanel printPanel;
    
    /**
     * Index oft the changing image. 
     */
    int index;
    boolean activeListener = false;
     
     /**
      * Constructor
      * 
      * @param printImagePreview Contains the  printImagePreview of the changing image.
      * @param printPanel Contains the current PrintPanel
      */
     public ImageSettingDialog(PrintImagePreview printImagePreview,PrintPanel printPanel)
     {
          super();
          setLocationRelativeTo(printImagePreview);
          setModal(true);
          getContentPane().setLayout(new BorderLayout(10,10));
          
          //Init
          this.printImagePreview =  printImagePreview;
          this.printPanel = printPanel;
          dvi = printPanel.getDvi();
          storedPrint = printPanel.getStored();
          index = printImagePreview.getIndex();
          
          setTitle("Print Layout Options for Image " +(int)(index +1));
          
          //Buttons
          okButton = new CommandJButton("OK",this,ID_OK);
          defaultButton = new CommandJButton("Default",this,ID_DEFAULT);
          deleteButton = new CommandJButton("Delete",this,ID_DELETE);
          JPanel  buttonPanel = new JPanel();
          buttonPanel.add(okButton);
          //buttonPanel.add(defaultButton);
          buttonPanel.add(deleteButton);
          
          JPanel addLeftPanel = new JPanel(new GridBagLayout());
          addLeftPanel.setBorder(new TitledBorder("Print Settings"));
          GridBagConstraints gbc = new GridBagConstraints();
        
            //Magnification
            gbc.fill = GridBagConstraints.NONE;
            gbc.weightx = 0.0;
            gbc.gridwidth = 1;
            addLeftPanel.add(new JLabel("Magnification"),gbc);
            addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                    
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.weightx = 1.0;
            imageMagnificationCombo = new CommandJComboBox(this, ID_IMAGEMAGNIFICATION);
            addLeftPanel.add(imageMagnificationCombo,gbc);
        
            //Smoothing
            gbc.fill = GridBagConstraints.NONE;
            gbc.weightx = 0.0;
            gbc.gridwidth = 1;
            addLeftPanel.add(new JLabel("Smoothing"),gbc);
            addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                    
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.weightx = 1.0;
            imageSmoothingCombo = new CommandJComboBox(this, ID_IMAGESMOOTHING);
            addLeftPanel.add(imageSmoothingCombo,gbc);
                
            //Configuration
            gbc.fill = GridBagConstraints.NONE;
            gbc.weightx = 0.0;
            gbc.gridwidth = 1;
            addLeftPanel.add(new JLabel("Configuration"),gbc);
            addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                    
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.weightx = 1.0;
            imageConfigurationCombo = new CommandJComboBox(this, ID_IMAGECONFIGURATION);
            addLeftPanel.add(imageConfigurationCombo,gbc);
          
            getContentPane().add(addLeftPanel, BorderLayout.CENTER);
          
            getContentPane().add(buttonPanel,BorderLayout.SOUTH);
            pack();
            
            init();
            activeListener = true;
            
     }

    /**
     * Initializes the widgets with values form the jDVIInterface
     */
     public void init()
     {
        /////////////////////////////////////7
        //Magnification
        ////////////////////////////////
        
        Vector magnificationList = new Vector();
        int sizeFor = dvi.getTargetPrinterNumberOfMagnificationTypes(dvi.getCurrentPrinter());
        for (int i = 0; i < sizeFor;i++)
        {
            String t = dvi.getTargetPrinterMagnificationType(dvi.getCurrentPrinter(), i );
            magnificationList.addElement(t);
        }
        magnificationList.addElement(new String("Default"));
        imageMagnificationCombo.setModel(new DefaultComboBoxModel(magnificationList));
        
        if (storedPrint.getImageMagnificationType(printImagePreview.getIndex()) != null) imageMagnificationCombo.setSelectedItem(storedPrint.getImageMagnificationType(printImagePreview.getIndex()));
        else imageMagnificationCombo.setSelectedIndex(imageMagnificationCombo.getItemCount()-1);
        
        insertSmoothing();
        
        ///////////////////////////////
        //Configuration
        /////////////////////////////////
        Vector configurationList = new Vector();
       
        sizeFor = dvi.getTargetPrinterNumberOfConfigurationSettings(dvi.getCurrentPrinter());
        for (int i = 0; i < sizeFor;i++)
        {
            String t = dvi.getTargetPrinterConfigurationSetting(dvi.getCurrentPrinter(), i );
            configurationList.addElement(t);
        }
        configurationList.addElement(new String("Default"));
        imageConfigurationCombo.setModel(new DefaultComboBoxModel(configurationList));
        
        if (storedPrint.getImageConfigurationInformation(printImagePreview.getIndex()) != null) imageConfigurationCombo.setSelectedItem(storedPrint.getImageConfigurationInformation(printImagePreview.getIndex()));
        else imageConfigurationCombo.setSelectedIndex(imageConfigurationCombo.getItemCount()-1);
        
     }

    /**
     * Initialize the imageSmoothing.
     */
    public void insertSmoothing()
    {
        
        Vector smoothingList = new Vector();
        if (((storedPrint.getImageMagnificationType(printImagePreview.getIndex()) != null) &&(storedPrint.getImageMagnificationType(printImagePreview.getIndex()).equals("CUBIC"))))
        {
            int sizeFor = dvi.getTargetPrinterNumberOfSmoothingTypes(dvi.getCurrentPrinter());
            for (int i = 0; i < sizeFor;i++)
            {
                String t = dvi.getTargetPrinterSmoothingType(dvi.getCurrentPrinter(), i );
                smoothingList.addElement(t);
                
            }
            
        }
        smoothingList.addElement(new String("Default"));
        imageSmoothingCombo.setModel(new DefaultComboBoxModel(smoothingList));
        if (storedPrint.getImageSmoothingType(printImagePreview.getIndex()) != null) imageSmoothingCombo.setSelectedItem(storedPrint.getImageSmoothingType(printImagePreview.getIndex()));
        else imageSmoothingCombo.setSelectedIndex(imageSmoothingCombo.getItemCount()-1);
            
    }
     
     
     
    /**
     * Handle the ActionEvents 
     * 
     * @param id ID of the fireing Component
     */
    public void buttonClicked (int id)
    {
        if (activeListener)
        {
            switch (id)
            {
                case ID_DELETE:
                       setVisible (false);
                       printPanel.deleteImage(printImagePreview.getIndex());
                       break;
                
                case ID_OK: 
                        if ( (storedPrint.getImageConfigurationInformation(index) ==null)&&
                                (storedPrint.getImageSmoothingType(index)== null)&&
                                (storedPrint.getImageMagnificationType(index)== null)) printImagePreview.setParameter(false);
                        else printImagePreview.setParameter(true);
                        setVisible (false);
                        break;
                case ID_DEFAULT: 
                            
                            //Changing the values in the ComboBoxes results in calling the ActionEvents
                            //This results in calling buttonClicked(id);
                            imageMagnificationCombo.setSelectedIndex(imageMagnificationCombo.getItemCount()-1);
                            imageSmoothingCombo.setSelectedIndex(imageSmoothingCombo.getItemCount()-1);
                            imageConfigurationCombo.setSelectedIndex(imageConfigurationCombo.getItemCount()-1);
                            //setVisible (false);
                            
                            printImagePreview.setParameter(false);
                           
                            
                            break;
            
                case  ID_IMAGEMAGNIFICATION:
                            if ( imageMagnificationCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMagnificationTypes(dvi.getCurrentPrinter())) storedPrint.setImageMagnificationType(index,new String(""));
                            else storedPrint.setImageMagnificationType(index, (String)imageMagnificationCombo.getSelectedItem());
                            insertSmoothing();
                            break;
                            
                
                case  ID_IMAGESMOOTHING:
                            if ( imageSmoothingCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfSmoothingTypes(dvi.getCurrentPrinter())) storedPrint.setImageSmoothingType(index,new String(""));
                            else storedPrint.setImageSmoothingType(index,(String)imageSmoothingCombo.getSelectedItem());
                            break;
                case  ID_IMAGECONFIGURATION:
                            if ( imageConfigurationCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfConfigurationSettings(dvi.getCurrentPrinter())) storedPrint.setImageConfigurationInformation(index,new String(""));
                            else storedPrint.setImageConfigurationInformation(index,(String)imageConfigurationCombo.getSelectedItem());
                            break;
            }
        }
        
        
    }
    
        
}
/*
 *  CVS Log
 *  $Log: ImageSettingDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

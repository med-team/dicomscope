/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
/**
* Contains a document for DICOM Long String (SH) values.
* A character string that may be padded with
* leading and/or trailing spaces. The character
* code 05CH (the BACKSLASH �\� in ISO-IR 6)
* shall not be present, as it is used as the
* delimiter between values for multiple data
* elements. The string shall not have Control
* Characters except ESC.
* <p>
* Default Character Repertoire and/or
* as defined by (0008,0005).
* <p>
* At this time only the DICOM Character Repertoire can be used.
* @author Klaus Kleber
* @since 01.01.2001
*/
public class SHDocument extends PlainDocument
{
    
    /**
    * Size of the Document
    */
    private int maxSize = 16;
    
    /**
    * If true only the DICOM Character Repertoire will be used
    */
    private boolean onlyDICOMCharacterRepertoire = false;
    
    /**
    * Contstructor.
    */
    public SHDocument()
    {
        this(true);
    }
    
    /**
    * Contstructor. 
    * @param onlyDICOMCharacterRepertoire If true only the DICOM Character Repertoire will be used.
    * false is not supported at this time.
    */
    public SHDocument(boolean onlyDICOMCharacterRepertoire)
    {
        super();
    }
    
    
    /**
    * Inserts some content into the document.
    * <p>
    * The content will be checked. 
    * <p>
    * Inserting content causes a write lock to be held while the actual changes are taking place, 
    * followed by notification to the observers on the thread that grabbed the write lock. 
    * <p>
    * This method is thread safe, although most Swing methods are not.
    * Please see Threads and Swing for more information.
    * @param offs The starting offset >= 0 
    * @param str The string to insert; does nothing with null/empty strings
    * @param a The attributes for the inserted content 
    * @exception BadLocationException The given insert position is not a valid position within the document
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        char c;
        if (s == null) return;
        
        int size = s.length();
        
        for (int j = 0; j < size; j++)
        {
            if (offset+j >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            try   
            {
                
                c = s.charAt(j);
                if (!(DICOMDocumentUtils.isLineFeed(c)||
                    DICOMDocumentUtils.isFormFeed(c)||
                    DICOMDocumentUtils.isCarriageReturn(c)||
                    DICOMDocumentUtils.isBackslash(c)||
                    DICOMDocumentUtils.isDefaultCharacter(c))) 
                {
                    Toolkit.getDefaultToolkit().beep();
                            return;
                }
                
            }
            catch(Exception e)
            {
                
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            super.insertString(offset+j, new Character(c).toString(), attributeSet);
        }
        
    }
    
    
}
/*
 *  CVS Log
 *  $Log: SHDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

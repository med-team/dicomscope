/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.text.*;
/**
* Contains a document for DICOM Date (TM) values:
* <p>
* A string of characters of the format
* hhmmss.frac; where hh contains hours (range
* "00" - "23"), mm contains minutes (range "00" -
* "59"), ss contains seconds (range "00" -
* "59"), and frac contains a fractional part of a
* second as small as 1 millionth of a second
* (range �000000� - �999999�). A 24 hour clock
* is assumed. Midnight can be represented by
* only �0000� since �2400� would violate the hour
* range. The string may be padded with trailing
* spaces. Leading and embedded spaces are
* not allowed. One or more of the components
* mm, ss, or frac may be unspecified as long as
* every component to the right of an unspecified
* component is also unspecified. If frac is
* unspecified the preceding �.� may not be
* included. Frac shall be held to six decimal
* places or less to ensure its format conforms to
* the ANSI HISPP MSDS Time common data
* type.
* Examples:
* 1. �070907.0705 � represents a time of
* 7 hours, 9 minutes and 7.0705 seconds.
* <br>
* 2. �1010� represents a time of 10 hours,and 10 minutes.
* <br>
* 3. �021 � is an invalid value.
* <p>
* Notes: 1. For reasons of backward
* compatibility with versions of this
* standard prior to V3.0, it is
* recommended that implementations
* also support a string of characters of
* the format hh:mm:ss.frac for this VR.
* @author Klaus Kleber
*/
public class TMDocument extends PlainDocument
{
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss.SSSSSS");
    private static final SimpleDateFormat timeFormatShort = new SimpleDateFormat("HHmmss");
    private int maxSize = 13;
    /**
    * Contstructor.
    */
    public TMDocument()
    {
        super();
    }
    /**
    * Insert a String and checks if the String is a valid input.
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        //Deleting the leading and trailing spaces
        s = s.trim();
        char c;
        if (s == null) return;
        
        int size = s.length();
        int pos = offset;
        for (int j = 0; j < size; j++)
        {
            if (pos >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            c = s.charAt(j);
            
            //Ignores :
            if (c == ':') {
                if (isSeperatorPosition(pos)) continue;
                else {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
                
            }
            //seperator
            if (c == '.') {
                if (pos != 6){
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
                
            }
            else if (!isValid(c,pos)){
                Toolkit.getDefaultToolkit().beep();
                return;
            }
            
            
            super.insertString(pos, new Character(c).toString(), attributeSet);
            pos++;
        }
        
    }
    
    /**
    * Checks if the specified position is a place where a 
    * seperator can be placed.
    * @param pos The position
    * @return True a seperator can be placed on the specified postion
    */
    private boolean isSeperatorPosition(int pos)
    {
        if (pos == 2 || pos == 5) return true;
        else return false;
    }
    private boolean isValid(char c, int pos)
    
    {
       //Only digits allowed
       if (! Character.isDigit(c)) return false;
       
       //Checks Hour
       if (pos == 0 && c >'2') return false;
       
       
       //Checks Minuts
       if (pos == 2 && c >'5') return false;
       
       //Checks Secounds
       if (pos == 4 && c >'5') return false;
       
       
       
       
       
       return true; 
    }
    
    /**
    * Checks if the value if a valid date
    * @return True if the value is a valid date.
    */
    public  boolean check ()
    {
        try
        {
            java.util.Date d;
            if (getLength()==6)
            {
                timeFormatShort.setLenient(false);
                d =timeFormatShort.parse(getText(0,getLength()), new ParsePosition(0));
            }
            else
            {
                timeFormat.setLenient(false);
                d =timeFormat.parse(getText(0,getLength()), new ParsePosition(0));
                
            }
            if (d == null) return false;
            else return true;
        }
        catch(Exception e)
        {
            
            return false;
        }
        
    }
}
/*
 *  CVS Log
 *  $Log: TMDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

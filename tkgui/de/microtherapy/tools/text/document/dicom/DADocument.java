/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.text.*;
/**
* Contains a document for DICOM Date (DA) values:
* <p>
* A string of characters of the format yyyymmdd;
* where yyyy shall contain year, mm shall
* contain the month, and dd shall contain the
* day. This conforms to the ANSI HISPP MSDSDate common data type.
* <p>
* Example:�19930822� would represent August 22,1993.
* <p>
* Notes: 1. For reasons of backward
* compatibility with versions of this
* standard prior to V3.0, it is
* recommended that implementations
* also support a string of characters of
* the format yyyy.mm.dd for this VR.
* <p>
* This document is able to handle inputs
* with a length of 8. If the char '." is inserted it will be ignored 
* @author Klaus Kleber
*/
public class DADocument extends PlainDocument
{
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private int maxSize = 8;
    /**
    * Contstructor.
    */
    public DADocument()
    {
        super();
    }
    /**
    * Insert a String and checks if the String is a valid input.
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        char c;
        if (s == null) return;
        
        int size = s.length();
        int pos = offset;
        for (int j = 0; j < size; j++)
        {
            if (pos >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            c = s.charAt(j);
            
            //Ignores seperator
            if (c == '.') {
                if (isSeperatorPosition(pos)) continue;
                else {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
                
            }
               
            if (!isValid(c,pos)){
                Toolkit.getDefaultToolkit().beep();
                return;
            }
            
            
            super.insertString(pos, new Character(c).toString(), attributeSet);
            pos++;
        }
        
    }
    
    /**
    * Checks if the specified position is a place where a 
    * seperator can be placed.
    * @param pos The position
    * @return True a seperator can be placed on the specified postion
    */
    private boolean isSeperatorPosition(int pos)
    {
        if (pos == 4 || pos == 7) return true;
        else return false;
    }
    private boolean isValid(char c, int pos)
    
    {
       //Only digits allowed
       if (! Character.isDigit(c)) return false;
       
       //Checks Month
       if (pos == 4 && c >'1') return false;
       
       
       //Checks Day
       if (pos == 6 && c >'3') return false;
       
       
       
       
       
       return true; 
    }
    
    /**
    * Checks if the value if a valid date
    * @return True if the value is a valid date.
    */
    public  boolean check ()
    {
        //if (getLength()<8) return false;
        try
        {
            dateFormat.setLenient(false);
            java.util.Date d =dateFormat.parse(getText(0,getLength()), new ParsePosition(0));
            if (d == null) return false;
            else return true;
        }
        catch(Exception e)
        {
            
            return false;
        }
        
    }
}
/*
 *  CVS Log
 *  $Log: DADocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

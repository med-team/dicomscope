/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;

/**
* Contains a document for DICOM Decimal String (DS) values.
* <p>
* A string of characters representing either a
* fixed point number or a floating point number.
* A fixed point number shall contain only the
* characters 0-9 with an optional leading "+" or "-"
* and an optional "." to mark the decimal point.
* A floating point number shall be conveyed as
* defined in ANSI X3.9, with an "E" or "e" to
* indicate the start of the exponent. Decimal
* Strings may be padded with leading or trailing
* spaces. Embedded spaces are not allowed.
* <p>
* �0�-�9�, �+�, �-�,�E�, �e�, �." of Default Character Repertoire
* <p>
* Max size: 16
* @author Klaus Kleber
*/
public class DSDocument extends PlainDocument
{
    
    /**
    * Max size
    */
    private int maxSize = 16;
    
    /**
    * Contstructor. 
    */
    public DSDocument()
    {
        super();
    }
    
    
    /**
    * Inserts some content into the document.
    * <p>
    * <p>
    * Inserting content causes a write lock to be held while the actual changes are taking place, 
    * followed by notification to the observers on the thread that grabbed the write lock. 
    * <p>
    * This method is thread safe, although most Swing methods are not.
    * Please see Threads and Swing for more information.
    * @param offs The starting offset >= 0 
    * @param str The string to insert; does nothing with null/empty strings
    * @param a The attributes for the inserted content 
    * @exception BadLocationException The given insert position is not a valid position within the document
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        if (s == null) return;
        
        int size = s.length();
        
        int pos = offset; 
         
        for (int j = 0; j < size; j++)
        {
            if (pos >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            char c = s.charAt(j);
            try   
            {
                if (    (c !=    '+') &&
                        (c !=    '-') && 
                        (c !=    'E') && 
                        (c !=    ' ') && 
                        (c !=    'e') && 
                        (c !=    '.') && 
                        !DICOMDocumentUtils.isDigit(c))
                {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                    
                }
            }
            catch(Exception e)
            {
                
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            super.insertString(pos, new Character(c).toString(), attributeSet);
            pos++;
        }
    }
    
}
/*
 *  CVS Log
 *  $Log: DSDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

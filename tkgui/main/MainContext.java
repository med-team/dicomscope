/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 10:08:20 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
*/


package main;

import J2Ci.*;
import java.io.*;
import java.awt.*;
import java.util.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.font.*;
import java.awt.color.*;

/**
 * This class manages the context of DICOMscope. 
 * Some global variables of the programm are available by 
 * methods of this class.
 * It implements the singleton pattern.
 * 
 * @author Klaus Kleber
 * @author Andreas Schroeter
 * @since 30.04.1999
 */

public class MainContext extends java.lang.Object
{   
    //import static library
    static
    {
        System.loadLibrary ("jInterface");
    }

    /**
     * The only instance of this class.
     * 
     * @since 30.04.1999
     */
    private static MainContext theInstance = null;    
    
    

     public static Hashtable profiles = new Hashtable();
    
    
     /**
     * True if the hight quality monitor with the dome board is used.
     * 
     * @since 30.04.1999
     */
    public boolean highQualitySystem = false;
    
    
    
    
    /**
    * Application Name
    * 
    * @since 30.04.1999
    */
		
    //String title = new String ("DICOMscope 3.0");
    String title = new String ("DICOMscope 3.6.0");
    
    
    
    public static final String iconPath = "icons/";
    public static  Font logFont;
    
    /**
    * Contains the ColorModel for drawing the images. The ColorModel depends 
    * from the display system. If the display system the low quality system then 
    * a 8 bit IndexColorModel will used. If the display system is the high 
    * quality system 8 Bit ColorModel loaded from de GraphicsConfiguration will be 
    * used.
    *
    * @since 30.04.1999
    */
    ColorModel usedColorModel;
    
    //static Dimension screenSize;
    
    /**
    * Outputstream for log file
    *
    * @since 30.04.1999
    */
    
    
    public static  Hashtable configurationProps;
    jDVInterface dvi;
    
    /**
     * Return the only instance of a MainContext object. If there is no instance 
     * this methode create this instance.
     * <br> 
     * status: tested
     * 
     * @return The only intance of this class.
     * @since 30.04.1999
     */
    public static MainContext instance()
    {
        if (theInstance == null) theInstance = new MainContext();
        return theInstance;
    }

    public static SRCodeList codeList;
    
    /**
     * Constructor. This class implements the singleton pattern. 
     * This constructor should only called from the 
     * instance() methode of this class.
     * 
     * @since 30.04.1999
     * @see MainContext.instance()
     */
    
    private MainContext()
    {
        //System.out.println("MainContext.iconPath " +MainContext.iconPath );
        //Load deviceColorModell
        loadDeviceInformation();
        codeList = new SRCodeList("codes.dic");
    }
    
    
    
    /**
    * Loads and constructs the color model from the  device.
    *
    * @since 30.04.1999
    */
    private void loadDeviceInformation()
    {
            
        //
        GraphicsEnvironment  gr;
        GraphicsDevice gd;
        ColorModel cm;
        GraphicsConfiguration gc;
        
        
        gr = GraphicsEnvironment.getLocalGraphicsEnvironment();
        gd = gr.getDefaultScreenDevice();
        gc = gd.getDefaultConfiguration();
        cm = gc.getColorModel();
        usedColorModel = cm;
       
        
        GrayColorIndex.fillIndexColorModel();
        
        
    }       
    
    
    /**
     * Returns a 8 Bit IndexColorModel
     * 
     * @param A 8 Bit IndexColorModel
     * @since 30.04.1999
     */
    public ColorModel getIndexColorModel()
    {
         return GrayColorIndex.getGrayColorModel();
    }
    
    
    /**
     * Returns the ColorModel used for displaying images. Depends
     * from the system.
     * 
     * @return The ColorModel used for displaying images.
     * @since 30.04.1999
     */
    public ColorModel getUsedColorModel()
    {
        if (highQualitySystem)return usedColorModel;
        else return GrayColorIndex.getGrayColorModel();
    }
    
    
    /**
     * Returns the font for displaying PresentationStateTextObjects.
     * Depends from the system.
     * 
     * @return The  font for displaying PresentationStateTextObjects.
     * @since 30.04.1999
     */
    public Font getFontTextAnnotations()
    {
         return  (Font) configurationProps.get("TextFont");    
    }
    public void initConfiguration(jDVInterface dvi)
    {
        //Browser
        this.dvi = dvi;
		//highQualitySystem = true;
		
        configurationProps= new Hashtable();
	    String fontNameLog =  dvi.getGUIConfigEntry("FONTNAMELOG");
	    String fontSizeLog =  dvi.getGUIConfigEntry("FONTSIZELOG");
	    
        //Fonts
	    String fontSize =  dvi.getGUIConfigEntry("FONTSIZE");
	    String fontName =  dvi.getGUIConfigEntry("FONTNAME");
	    String fontNameText =  dvi.getGUIConfigEntry("FONTNAMETEXT");
	    String fontSizeText =  dvi.getGUIConfigEntry("FONTSIZETEXT");
	    
	    String functionPanelPlacement =  dvi.getGUIConfigEntry("FUNCTIONPANELPLACEMENT");
	    String paintPanelPlacement =  dvi.getGUIConfigEntry("PAINTPANELPLACEMENT");
	    
	    String navigationPanelOn =  dvi.getGUIConfigEntry("NAVIGATIONPANELON");
	    if (navigationPanelOn == null)configurationProps.put("NavigationPanelOn", new Boolean(true));
	    else configurationProps.put("NavigationPanelOn", new Boolean(navigationPanelOn));
	    
	    String signWarning = dvi.getGUIConfigEntry("SIGNWARNING");
	    if (signWarning == null)signWarning = "true";
	    configurationProps.put("SignWarning",new Boolean(signWarning));
	    
	    
	    String autoUpdateStudyBrowser = dvi.getGUIConfigEntry("AUTOUPDATESTUDYBROWSER");
	    if (autoUpdateStudyBrowser == null)autoUpdateStudyBrowser = "true";
	    configurationProps.put("AutoUpdateStudyBrowser",new Boolean(autoUpdateStudyBrowser));
	    String backgroundColor = dvi.getGUIConfigEntry("BACKGROUNDCOLOR");
	    if (backgroundColor!= null) 
	    {
	        try
	        {
	            StringTokenizer st = new StringTokenizer(backgroundColor, "\\");
	            Color color = new Color(new Integer(st.nextToken()).intValue(),
	                                              new Integer(st.nextToken()).intValue(),
	                                              new Integer(st.nextToken()).intValue());
	            configurationProps.put("BrowserBackgroundColor",color);
	                                    
	            
	        }
	        catch (NoSuchElementException  e)
	        {
	            System.out.println("colorn: ");
	            
	        }
	    }
	    
	    
	    
	    if (fontName == null) fontName = "SansSerif";
	    if (fontNameText == null) fontNameText = "SansSerif";
	    if (fontNameLog == null) fontNameLog = "Monospaced";
	    
	    if (dvi.getGUIConfigEntryBool("HIGHRESOLUTIONGRAPHICS", false) == false)
	    {
	        //Default settings fpr font
	        if (fontSize == null) fontSize = "12";
	        if (fontSizeText == null) fontSizeText = "16";
	        if (fontSizeLog == null) fontSizeLog = "12";
	        
	        //Default settings for function panel
	        if (functionPanelPlacement == null)configurationProps.put("FunctionPanelPlacement", "East");
	        else configurationProps.put("FunctionPanelPlacement", functionPanelPlacement);
	        
	        if (paintPanelPlacement == null)configurationProps.put("PaintPanelPlacement", "Integrated");
	        else configurationProps.put("PaintPanelPlacement", paintPanelPlacement);
	        
	        configurationProps.put("ProcessingPanelOn",  new Boolean(true));
	        configurationProps.put("PSPanelOn",  new Boolean(false));
	        
	        
	        configurationProps.put("HighEndSystem", new Boolean(false));
	        
	    }
	    else
	    {
	        if (fontSize == null) fontSize = "30";
	        if (fontSizeText == null) fontSizeText = "48";
	        if (fontSizeLog == null) fontSizeLog = "20";
	        
	        //Default settings for function panel
	        if (functionPanelPlacement == null)configurationProps.put("FunctionPanelPlacement", "North");
	        else configurationProps.put("FunctionPanelPlacement", functionPanelPlacement);
	        
	        if (paintPanelPlacement == null)configurationProps.put("PaintPanelPlacement", "Integrated");
	        else configurationProps.put("PaintPanelPlacement", paintPanelPlacement);
	        configurationProps.put("ProcessingPanelOn",  new Boolean(true));
	        configurationProps.put("PSPanelOn",  new Boolean(false));
	        
	        
	        configurationProps.put("HighEndSystem", new Boolean(true));
	        highQualitySystem = true;
	    }
	    
	    
	    
	    
	    configurationProps.put("GUIFont", new Font(fontName,0,new Integer(fontSize).intValue()));
	    configurationProps.put("TextFont", new Font(fontNameText,0,new Integer(fontSizeText).intValue()));
	    configurationProps.put("LogFont", new Font(fontNameLog,0,new Integer(fontSizeLog).intValue()));
	    logFont = new Font(fontNameLog,0,new Integer(fontSizeLog).intValue());
	    
	    //StudyBrowser
	    configurationProps.put("UID", new Boolean(false));
	    
	    configurationProps.put("Description",  new Boolean(true));
	    configurationProps.put("PatientInfo", new Boolean(true));
	    configurationProps.put("Modality",  new Boolean(true));
	    configurationProps.put("DateTime",  new Boolean(false));
	    configurationProps.put("NewItems",  new Boolean(true));
	    configurationProps.put("Filename",  new Boolean(false));
	    configurationProps.put("Label",  new Boolean(true));
	    configurationProps.put("ButtonOn",  new Boolean(false));
	    configurationProps.put("WarnUnsignedSrDouments",  new Boolean(false));
	    //Viewer
	    String warnUnsignedSrDouments = dvi.getGUIConfigEntry("WARNUNSIGNEDOBJECTSINSR");
	    if (warnUnsignedSrDouments == null)warnUnsignedSrDouments = "true";
	    configurationProps.put("WarnUnsignedSrDouments",new Boolean(warnUnsignedSrDouments));
	    
	    
	        
	    //Settings for TabbedPanel
	    String tabPlacement =  dvi.getGUIConfigEntry("TABPLACEMENT");
	    if (tabPlacement == null)configurationProps.put("TabbedPlacement", "1");
	    else 
	    {
	        if (tabPlacement.equals("North") ) configurationProps.put("TabbedPlacement", "1");
	        if (tabPlacement.equals("East") ) configurationProps.put("TabbedPlacement", "2");
	        if (tabPlacement.equals("South") ) configurationProps.put("TabbedPlacement", "3");
	        if (tabPlacement.equals("West") ) configurationProps.put("TabbedPlacement", "4");
	    }
	    
	    jDoubleByRef ambientValue = new jDoubleByRef();   
	    if (dvi.getAmbientLightValue(ambientValue) == 0)configurationProps.put("AmbientLight", new Double(ambientValue.value).toString());
	    
	    
	    double monitorHeight = dvi.getMonitorPixelHeight();
	    double monitorWidth = dvi.getMonitorPixelWidth();
	    
	    if ( monitorHeight == 0d) monitorHeight = 1d;
	    if ( monitorWidth == 0d) monitorWidth = 1d;
	    configurationProps.put("PixelSizeX",  new Double(monitorWidth).toString());
	    configurationProps.put("PixelSizeY",  new Double(monitorHeight).toString());
        
        configurationProps.put("Title", title);
        
        
	
		if (highQualitySystem)dvi.writeLogMessage(jDVPSLogMessageLevel.DVPSM_informational,"GUI","Very High Resolution Graphics" );
		else dvi.writeLogMessage(jDVPSLogMessageLevel.DVPSM_informational,"GUI","Standard Resolution Graphics" );
	    
        Hashtable srProfile = (Hashtable)configurationProps.clone();   
	    srProfile.put("FunctionPanelPlacement", "None");
	    srProfile.put("PaintPanelPlacement", "Toolbar");
	    srProfile.put("ProcessingPanelOn",  new Boolean(true));
	    srProfile.put("PSPanelOn",  new Boolean(false));
        Hashtable imageProfile = (Hashtable)configurationProps.clone();   
	    imageProfile.put("FunctionPanelPlacement", "East");
	    imageProfile.put("PaintPanelPlacement", "Integrated");
	    imageProfile.put("ProcessingPanelOn",  new Boolean(true));
	    imageProfile.put("PSPanelOn",  new Boolean(true));
	    
          profiles.put("Configuration Profile",(Hashtable)configurationProps.clone());
          profiles.put("Image Profile",imageProfile);
          profiles.put("Structured Report Profile",srProfile);
        
    }
    public Hashtable getConfiguration()
    {
        return configurationProps;
    }
}//FontRenderContext

/*
 *  CVS Log
 *  $Log: MainContext.java,v $
 *  Revision 1.2  2003/09/08 10:08:20  kleber
 *  New version number
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

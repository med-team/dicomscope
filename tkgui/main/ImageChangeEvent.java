/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
/**
* Event for changing the zoomValue
*/
public  class ImageChangeEvent extends DSEvent
{
    public int type;
    public boolean selected;
    public Double newDouble;
    public Double newDouble1;
    public String s;
    public int intValue;
    public ImageChangeEvent( Object o, int type)
    {
        super(o);
        this.type = type;
    }
    
    public ImageChangeEvent(   Object o, int type, boolean selected)
    {
        super(o);
        this.type = type;
        this.selected = selected;
    }
    public ImageChangeEvent(   Object o, int type, Double newDouble,Double newDouble1 )
    {
        super(o);
        this.type = type;
        this.newDouble = newDouble;
        this.newDouble1 = newDouble1;
    }
    public ImageChangeEvent(   Object o, int type, Double newDouble )
    {
        super(o);
        this.type = type;
        this.newDouble = newDouble;
    }
    public ImageChangeEvent(   Object o, int type,String s )
    {
        super(o);
        this.type = type;
        this.s = s;
    }
    public ImageChangeEvent(   Object o, int type,int intValue )
    {
        super(o);
        this.type = type;
        this.intValue = intValue;
    }
    
}
/*
 *  CVS Log
 *  $Log: ImageChangeEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

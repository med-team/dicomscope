/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
/**
* Event for the status line
*/
public  class SignedStatusEvent extends DSEvent
{
    public int combinedImagePStateSignatureStatus;
    public int reportSignatureStatus;
    
    public int actionType = 0;
    
    public static final int LOADSR = 0;
    public static final int LOADIMAGE = 1;
    public static final int LOADPS = 2;
    
    public boolean notifyUser = true;
    public SignedStatusEvent(Object o, int actionType,int combinedImagePStateSignatureStatus, int reportSignatureStatus)
    {
        super(o);
        this.actionType = actionType;
        this.combinedImagePStateSignatureStatus = combinedImagePStateSignatureStatus;
        this.reportSignatureStatus = reportSignatureStatus;
    }
    public SignedStatusEvent(   Object o, 
                                int actionType,
                                int combinedImagePStateSignatureStatus, 
                                int reportSignatureStatus,
                                boolean notifyUser)
    {
        super(o);
        this.actionType = actionType;
        this.combinedImagePStateSignatureStatus = combinedImagePStateSignatureStatus;
        this.reportSignatureStatus = reportSignatureStatus;
        this.notifyUser = notifyUser;
    }
    
    
}
/*
 *  CVS Log
 *  $Log: SignedStatusEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

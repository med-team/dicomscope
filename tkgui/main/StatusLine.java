/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.*;
import J2Ci.*;
/**
* Contains the GUI for the status line.
* The is context sensitve. For each DSComponentType
* There is a seperate view.
*/
public class StatusLine extends JPanel implements StatusListener, MainListener
{
    
    private String dsComponentType = new String();
    public String browserDescription = new String("Browser");
    public String printDescription= new String("Print");
    public String viewerDescription= new String("Viewer");
    public String aboutDescription= new String("About");
    
    private JLabel infoLabel = new JLabel("");
    private JLabel l1 = new JLabel("");
    private JLabel l2 = new JLabel("");
    private JLabel l3 = new JLabel("");
    private JLabel l4 = new JLabel("");
    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();
    private JPanel p3 = new JPanel();
    private SignAction reportSignAction;
    private SignAction imageSignAction;
     viewer.gui.SignatureOverview signatureOverview ;
     GridBagConstraints gbc = new GridBagConstraints();
    /**
     * The interface to the Dicom Toolkit
     */
    private jDVInterface dvi = null;
    boolean signWarning;
    public StatusLine(String dsComponentType, 
                        jDVInterface dvi,
                        boolean signWarning)
    {
        super();
        this.signWarning = signWarning;
        Controller.instance().addStatusListener(this);
        Controller.instance().addMainListener(this);
        
        setLayout(new GridBagLayout());
        gbc.anchor = GridBagConstraints.WEST;
        gbc.weightx = 2.0;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.BOTH;
        //setBorder(BorderFactory.createLineBorder(Color.gray));
        
        p1.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        p2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        p3.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));


        JLabel sizeLabel = new JLabel("M");
        JPanel p =new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));
        p.setBorder(BorderFactory.createTitledBorder("Info"));
        infoLabel.setHorizontalTextPosition(JLabel.LEFT);
        p.add(infoLabel);
        add(p,gbc);
        
        sizeLabel = new JLabel("c/w: 99999999/99999999");
        l1.setPreferredSize(sizeLabel.getPreferredSize());
        p1.setBorder(BorderFactory.createTitledBorder("Window/Level"));
        p1.add(l1);
        
        sizeLabel = new JLabel("Z: MM.MM, da: (+999/-999)/(+999/-999)");
        l2.setPreferredSize(sizeLabel.getPreferredSize());
        p2.setBorder(BorderFactory.createTitledBorder("Displayed Area"));
        p2.add(l2);
        
        //sizeLabel = new JLabel("MMMMMMMMMMMM");
        
        //l3.setPreferredSize(sizeLabel.getPreferredSize());
        p3.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        jToolkit.io.IconRetriever ir = new jToolkit.io.IconRetriever();
        
        signatureOverview = new viewer.gui.SignatureOverview(dvi);
        
      reportSignAction = new  SignAction(signatureOverview, SignAction.REPORT, 
                                                    ir.getIcon(MainContext.iconPath + "reportNoSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "reportInvalidSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "reportValidSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "reportUntrustedSignature.gif"));
        
        JButton reportSign = new JButton(reportSignAction);
        jToolkit.gui.CommandJButton.setButton(reportSign);
        reportSign.setMargin(new Insets(0,0,0,0));
        reportSign.setBorderPainted(false);
        p3.add(reportSign);
    
        imageSignAction = new  SignAction( signatureOverview,SignAction.COMBINATION, 
                                                    ir.getIcon(MainContext.iconPath + "imageNoSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "imageInvalidSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "imageValidSignature.gif"), 
                                                    ir.getIcon(MainContext.iconPath + "imageUntrustedSignature.gif"));
       
        JButton imageSign = new JButton(imageSignAction) ;
        jToolkit.gui.CommandJButton.setButton(imageSign);
        imageSign.setMargin(new Insets(0,0,0,0));
        imageSign.setBorderPainted(false);
        p3.add(imageSign);
        
        
        p3.setBorder(BorderFactory.createTitledBorder("Signature"));
        p3.add(l3);
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.VERTICAL;
        
        add(p3,gbc);
        
        if (dsComponentType.equals(DSComponentType.VIEWER)) setViewerInfo();
        else changeView(dsComponentType);
        
        
    }
    private void setViewerInfo()
    {
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.fill = GridBagConstraints.VERTICAL;
        
        remove(p3);
        add(p1,gbc);
        add(p2,gbc);
        add(p3,gbc);
        infoLabel.setText(viewerDescription);
        
        
    }
    public void changeView(String dsComponentType)
    {
        if (!dsComponentType.equals(this.dsComponentType))
        {
            if (this.dsComponentType.equals(DSComponentType.VIEWER))
            {
                remove(p1);
                remove(p2);
                repaint();
            }
            if (dsComponentType.equals(DSComponentType.VIEWER))setViewerInfo();
            else if (dsComponentType.equals(DSComponentType.BROWSER))infoLabel.setText(browserDescription);
            else if (dsComponentType.equals(DSComponentType.ABOUT))infoLabel.setText(aboutDescription);
            else if (dsComponentType.equals(DSComponentType.PRINT))infoLabel.setText(printDescription);
        }
        this.dsComponentType= dsComponentType;
    }
    public boolean processStatus (DSEvent e)
    {
        
        if (e instanceof SignedStatusEvent)
        {
            SignedStatusEvent sse = (SignedStatusEvent)e;
            reportSignAction.changeStatus(sse.reportSignatureStatus);
            imageSignAction.changeStatus(sse.combinedImagePStateSignatureStatus);
            if(sse.actionType == SignedStatusEvent.LOADSR)
            {
                //imageSignAction.changeStatus(jDVPSSignatureStatus.DVPSW_unsigned);
                if (signWarning)
                {
                    if (sse.reportSignatureStatus ==jDVPSSignatureStatus.DVPSW_signed_corrupt&&sse.notifyUser) openErrorPanel(0,true); 
                    else if (   sse.reportSignatureStatus ==jDVPSSignatureStatus.DVPSW_signed_unknownCA&&sse.notifyUser)  openErrorPanel(0,false); 
                          
                }
            }
            else
            {
                
                if (signWarning)
                {
                    if (sse.combinedImagePStateSignatureStatus ==jDVPSSignatureStatus.DVPSW_signed_corrupt&&sse.notifyUser) openErrorPanel(1, true);
                     if (   sse.combinedImagePStateSignatureStatus ==jDVPSSignatureStatus.DVPSW_signed_unknownCA&&sse.notifyUser) openErrorPanel(1, false);
                       
                }
                
           }
        }
        if (e instanceof StatusLineEvent)
        {
            StatusLineEvent ste =(StatusLineEvent)e;
            infoLabel.setText(ste.description);
            if (ste.dsComponentType ==DSComponentType.VIEWER) 
            {
                viewerDescription = ste.description;
                if (ste.text1 != null) l1.setText(ste.text1);
                if (ste.text2 != null) l2.setText(ste.text2);
                if (ste.text3 != null) l3.setText(ste.text3);
                if (ste.text4 != null) l4.setText(ste.text4);
                
            }
            if (ste.dsComponentType ==DSComponentType.BROWSER) browserDescription = ste.description;
            if (ste.dsComponentType ==DSComponentType.ABOUT) aboutDescription = ste.description;
            if (ste.dsComponentType ==DSComponentType.PRINT) printDescription = ste.description;
            return true;
        }
        
        return false;
    }
	public boolean processEvent (DSEvent e)
	{
	    if (e instanceof TabbedChangeEvent)
	    {
	        changeView(((TabbedChangeEvent)e).getTabbedTitle());
	    }
	    return false;
	}
    private void openErrorPanel(int type, boolean invalid)
    {
            String msg;
            if (type == 0)
            {
                if (invalid) msg = "Structured Report contains invalid digital signature(s)";
                else msg = "Structured Report contains untrustworthy digital signature(s)";
            }
            else 
            {
                if (invalid) msg = "Image/Presentation State contains invalid digital signature(s)";
                else  msg = "Image/Presentation State contains untrustworthy digital signature(s)";
            }
            Object[] possibleValues = { "Ignore", "Detail" }; 
            int selectedValue = JOptionPane.showOptionDialog(    null, 
                                                                    msg, 
                                                                    "Invalid Signature", 
                                                                    JOptionPane.YES_NO_OPTION, 
                                                                    JOptionPane.ERROR_MESSAGE, 
                                                                    null, 
                                                                    possibleValues, 
                                                                    possibleValues[0]); 
            if (selectedValue != 0) 
            {
                signatureOverview.setVisible(true);
            }
        
    }
        
}
/*
 *  CVS Log
 *  $Log: StatusLine.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import  de.microtherapy.tools.text.document.dicom.*;
import  de.microtherapy.tools.text.document.general.*;
import javax.swing.*;
import javax.swing.event.*;
import jToolkit.gui.*;
import jToolkit.io.*;
import J2Ci.*;
import java.awt.*;
import viewer.gui.*;
import java.io.*;
/**
* This class handles all GUI componentes which can be on different places
*/
public class GuiComponents implements CommandButtonListener, CommandToggleButtonListener,MainListener,GuiComponentListener, CommandTextListener
{
    
    public static final int ID_QUIT=0;
    public static final int ID_OPTIONS = 1;
    
    public static final int ID_LOAD = 10;
    public static final int ID_LOAD_PS = 11;
    public static final int ID_LOAD_IMAGE = 12;
    public static final int ID_LOAD_SR = 13;
    
    public static final int ID_SAVE = 20;
    public static final int ID_SAVEAS = 21;
    public static final int ID_SAVESCREEN = 22;
    
    public static final int ID_DELETE = 30;
    public static final int ID_SENDTO = 31;
    public static final int ID_REFRESH = 32;
    public static final int ID_CHECKIOD = 33;
    
    
    
    public static final int ID_ROT = 40;
    public static final int ID_ROT0 = 41;
    public static final int ID_ROT90 = 42;
    public static final int ID_ROT180 = 43;
    public static final int ID_ROT270 = 44;
    
    public static final int ID_INVERT = 50;
    public static final int ID_FLIP = 51;
    public static final int ID_RESET = 52;
    
    public static final int ID_PS = 60;
    public static final int ID_PRESENTATIONLUT = 61;
    
    public static final int ID_VOI = 62;
    public static final int ID_LUT = 63;
    public static final int ID_WININFRAME = 64;
    
    public static final int ID_WINDOW = 70;
    public static final int ID_CENTER = 71;
    public static final int ID_VOILUT = 72;
   
    
    public static final int ID_ZOOM = 80;
    public static final int ID_ZOOMINPUT = 81;
    public static final int ID_ZOOMFIT = 82;
    public static final int ID_ZOOM1TO1 = 83;
    
    
    public static final int ID_TRUESIZE = 90;
    public static final int ID_SCALETOFIT = 91;
    public static final int ID_MAGNIFY = 92;
    
    
    public static final int ID_IMAGERELATIVE = 100;
    public static final int ID_FILLEDBUTTON = 101;
    public static final int ID_IMAGERELATIVEBUTTON = 102;
    public static final int ID_FILLED = 103;
    
    public static final int ID_EDIT_LAYER = 110;
    public static final int ID_EDIT = 111;
    public static final int ID_LAYERLIST = 112;
    public static final int ID_SELECTLAYER = 113;
    
    public static final int ID_RECT = 200;
    public static final int ID_CIRCLE = 201;
    public static final int ID_ELLIPSE = 202;
    public static final int ID_LINE = 203;
    public static final int ID_POINT = 204;
    public static final int ID_POLY = 205;
    public static final int ID_TEXT = 206;
    public static final int ID_INTERPOLATED = 207;
    
    public static final int ID_SET_RECTSHUTTER = 210;
    public static final int ID_SET_POLYSHUTTER = 211;
    public static final int ID_SET_CIRCLESHUTTER = 212;
    public static final int ID_SET_BMPSHUTTER = 213;
    public static final int ID_SET_OPTSHUTTER = 214;
    
    public static final int ID_NAME = 400;
    public static final int ID_CREATOR = 401;
    public static final int ID_DESCRIPTION = 402;
    
    public static final int ID_PRINT = 1000;
    public static final int ID_PRINTHC = 1001;
    public static final int ID_PRINTSTTOPRINT = 1002;
    public static final int ID_PRINTST = 1003;
    //public static final int  = 1003;
    public static final int ID_INFO = 2000;
    public static final int ID_DUMP = 2001;
    
    public static final int ID_STACKIMAGE_LEFT = 5000;
    public static final int ID_STACKIMAGE_RIGHT = 5001;
    public static final int ID_STACKIMAGE_FIRST = 5002;
    public static final int ID_STACKIMAGE_LAST = 5003;
    public static final int ID_STACKMAXIMAGE = 5004;
    public static final int ID_STACKCURRENTIMAGE = 5005;
    
    
    public static final int ID_STACKFRAME_LEFT = 5010;
    public static final int ID_STACKFRAME_RIGHT = 5011;
    public static final int ID_STACKFRAME_FIRST = 5012;
    public static final int ID_STACKFRAME_LAST = 5013;
    public static final int ID_STACKMAXFRAME = 5014;
    public static final int ID_STACKCURRENTFRAME = 5015;
    
    public static final int ID_APPLYFRAME = 5020;
    public static final int ID_APPLYIMAGE = 5021;
    public static final int ID_APPLYALL = 5022;
    
    private boolean fireEvent = true;
    public CommandJButton stackImageLeftButton;
    public CommandJButton stackImageRightButton;
    public CommandJButton stackLastImageButton;
    public CommandJButton stackFirstImageButton;
    public CommandJButton stackFrameLeftButton;
    public CommandJButton stackFrameRightButton;
    public CommandJButton stackLastFrameButton;
    public CommandJButton stackFirstFrameButton;
    public CommandJRadioButton applyFrameRadio;
    public CommandJRadioButton applyImageRadio;
    public CommandJRadioButton applyAllRadio;
    
    public CommandJTextField maxImageTextField;
    public CommandJTextField currentImageTextField;
    
    public CommandJTextField maxFrameTextField;
    public CommandJTextField currentFrameTextField;
    
    //General
    public CommandJButton printButton;
    public CommandJButton printHcButton;
    public CommandJButton printStToPrinterButton;
    public CommandJButton printStButton;
    public CommandJButton dumpButton;
    
    
    public CommandJButton quitButton;
    public CommandJButton optionButton;
    public CommandJButton infoButton;
    
    public JButton srEditButton;
    public JButton srRevisedButton;
    public JButton srCodeEditButton;
    public JButton srCodeNewButton;
    public JButton srOkButton;
    public JButton srViewUpButton;
    public JButton srCompleteButton;
    public JButton srSaveButton;
    public JButton srResetButton;
    public JButton srVerificationButton;
    public JButton srViewDownButton;
    public JButton srViewBeginButton;
    public JButton srViewEndButton;
    public JButton srViewCopyButton;
    
    /**
     * Contains the ComboBox for changing the presentation lut
     */
    public CommandJToggleButton presentationLutButton;
    
    //DB
    public CommandJButton loadImageButton;
    public CommandJButton loadSRButton;
    public CommandJButton loadPSButton;
    public CommandJButton loadButton;
    public CommandJButton deleteButton;
    public CommandJButton sendToButton;
    public CommandJButton refreshButton;
    public CommandJButton checkIODButton;
    
    public CommandJButton saveButton;
    public CommandJButton saveAsButton;
    public CommandJButton saveScreenButton;
    
    //image processing
    public CommandJToggleButton psButton;
    public CommandJToggleButton lutButton;
    public CommandJButton resetButton;
    public CommandJToggleButton invertButton;
    public CommandJToggleButton flipButton;
    public CommandJButton rotButton;
    
    public CommandJButton zoomButton;
    public CommandJButton zoomFitButton;
    public CommandJButton zoom1to1Button;
    public CommandJButton voiButton;
    public CommandJButton winInFrameButton;
    
    
    
    
    //Paint
    public CommandJButton lineButton;
    public CommandJButton rectButton;
    public CommandJButton pointButton;
    public CommandJButton circleButton;
    public CommandJButton ellipseButton;
    public CommandJButton polyButton;
    public CommandJButton textButton;
    public CommandJButton interpolatedButton;
    public CommandJButton editButton;
    public CommandJButton editLayerButton;
    public CommandJButton selectLayerButton;
    
    
    
    Icon imageRelative;
    Icon displayRelative;
    public CommandJButton imageDisplayButton;
    public CommandJToggleButton filledButton;
    public CommandJToggleButton setLayerButton;
    
    //Shutter
    public CommandJToggleButton rectShButton;
    public CommandJToggleButton circleShButton;
    public CommandJToggleButton polyShButton;
    public CommandJToggleButton bmpShButton;
    public CommandJButton optShButton;
    
    
    Icon rotIcon;
    Icon rotIcon0;
    Icon rotIcon90;
    Icon rotIcon180;
    Icon rotIcon270;
    
    
    public CommandJComboBox layerComboBox;
    public JCheckBox filledCheckBox;
    public JCheckBox imageRelCheckBox;
    public CommandJTextField zoomTextField;
    public CommandJTextField windowTextField;
    public CommandJTextField centerTextField;
    public JTextField nameTextField;
    public JTextField creatorTextField;
    public JTextArea descrTextArea;
    public JTextArea infoTextArea;
    public CommandJRadioButton magnifyRadio;
    public CommandJRadioButton scaleRadio;
    public CommandJRadioButton trueSizeRadio;
    
    
    public static  GuiComponents instance;
    
    public static GuiComponents getInstance()
    {
        if (instance == null) instance = new GuiComponents();
        return instance;
    }
    
    private GuiComponents ()
    {
        Controller.instance().addGuiComponentListener(this);
        Controller.instance().addMainListener(this);
        filledCheckBox = new CommandJCheckBox("Filled",false,this,ID_FILLED);
        filledCheckBox.setAlignmentX(0.5f);
        filledCheckBox.setAlignmentY(0.5f);
        zoomTextField = new CommandJTextField(5,this, ID_ZOOMINPUT);
        zoomTextField.setDocument(new DoubleUnsignedDocument());
        
        centerTextField = new CommandJTextField(5,this, ID_CENTER);
        centerTextField.setDocument(new DoubleDocument());
        windowTextField = new CommandJTextField(5,this,ID_WINDOW);
        windowTextField.setDocument(new DoubleDocument());
        nameTextField = new CommandJTextField(15,this,ID_NAME);
        nameTextField.setDocument(new CSDocument());
        nameTextField.setColumns(5);
        nameTextField.setScrollOffset(0);
        
        
        creatorTextField = new CommandJTextField(15,this,ID_CREATOR);
        creatorTextField.setDocument(new PNDocument());
        
        
        descrTextArea= new JTextArea();
        imageRelCheckBox = new CommandJCheckBox("Image Relative",true,this,ID_IMAGERELATIVE);
        imageRelCheckBox.setAlignmentX(0.5f);
        imageRelCheckBox.setAlignmentY(0.5f);
        layerComboBox = new CommandJComboBox(this,ID_LAYERLIST);
        
        infoTextArea = new JTextArea();
        ButtonGroup presentationMode = new ButtonGroup();
        magnifyRadio = new CommandJRadioButton("Magnify", false, this, ID_MAGNIFY);
        scaleRadio = new CommandJRadioButton("Scale", false, this, ID_SCALETOFIT);
        trueSizeRadio = new CommandJRadioButton("True Size", false, this, ID_TRUESIZE);
        presentationMode.add(magnifyRadio);
        presentationMode.add(scaleRadio);
        presentationMode.add(trueSizeRadio);
	  
        ButtonGroup applyMode = new ButtonGroup();
        applyFrameRadio = new CommandJRadioButton("Frame", false, this, ID_APPLYFRAME);
        applyImageRadio = new CommandJRadioButton("Image", false, this, ID_APPLYIMAGE);
        applyAllRadio = new CommandJRadioButton("All", true, this, ID_APPLYALL);
        applyMode.add(applyFrameRadio);
        applyMode.add(applyImageRadio);
        applyMode.add(applyAllRadio);
        
        maxImageTextField = new CommandJTextField(3,this,ID_STACKMAXIMAGE);
        maxImageTextField.setEnabled(false);
        currentImageTextField = new CommandJTextField(3,this,ID_STACKCURRENTIMAGE);
        currentImageTextField.setDocument(new IntegerDocument(4));
        
        maxFrameTextField = new CommandJTextField(3,this,ID_STACKMAXFRAME);
        maxFrameTextField.setEnabled(false);
        
        currentFrameTextField = new CommandJTextField(3,this,ID_STACKCURRENTFRAME);
        currentFrameTextField.setDocument(new IntegerDocument(4));
	  
	  
	  
	  IconRetriever ir = new IconRetriever ();
        if (MainContext.instance().highQualitySystem)
        {
                srEditButton = new JButton(ir.getIcon(MainContext.iconPath + "newsrsop32.gif"));
                srCodeEditButton = new JButton(ir.getIcon(MainContext.iconPath + "modcode32.gif"));
                srCodeNewButton = new JButton(ir.getIcon(MainContext.iconPath + "newcode32.gif"));
                srViewUpButton= new JButton(ir.getIcon(MainContext.iconPath + "up32.gif"));
                srSaveButton= new JButton(ir.getIcon(MainContext.iconPath + "saveDB32.gif"));
                srResetButton= new JButton(ir.getIcon(MainContext.iconPath + "reset32.gif"));
                srViewDownButton = new JButton(ir.getIcon(MainContext.iconPath + "down32.gif"));
                srViewBeginButton=new JButton(ir.getIcon(MainContext.iconPath + "tobegin32.gif")); 
                srViewEndButton= new JButton(ir.getIcon(MainContext.iconPath + "toend32.gif"));
                srViewCopyButton=new JButton(ir.getIcon(MainContext.iconPath + "copy32.gif"));
                srCompleteButton=new JButton(ir.getIcon(MainContext.iconPath + "complete32.gif"));
                srOkButton=new JButton(ir.getIcon(MainContext.iconPath + "ok32.gif"));
                srVerificationButton=new JButton(ir.getIcon(MainContext.iconPath + "verify32.gif"));
                srRevisedButton = new JButton(ir.getIcon(MainContext.iconPath + "revised32.gif"));
            
            presentationLutButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath + "pLUT32.gif"),false,this,ID_PRESENTATIONLUT);
            
            checkIODButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "check32.gif"),this,ID_CHECKIOD);
            dumpButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "dump32.gif"),this,ID_DUMP);
            printButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus32.gif"),this,ID_PRINT);
            printHcButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus32.gif"),this,ID_PRINTHC);
            printStButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus32.gif"),this,ID_PRINTST);
            printStToPrinterButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "print32.gif"),this,ID_PRINTSTTOPRINT);
            quitButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "quit32.gif"),this,ID_QUIT);
            loadImageButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "openIMAG32.gif"),this,ID_LOAD_IMAGE);
            loadSRButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "openSr32.gif"),this,ID_LOAD_SR);
            loadPSButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"openSP32.gif"),this,ID_LOAD_PS);
            
            loadButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "loadDB32.gif"),this,ID_LOAD);
            deleteButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"delete32.gif"),this,ID_DELETE);
            sendToButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"send32.gif"),this,ID_SENDTO);
            refreshButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"refresh32.gif"),this,ID_REFRESH);
            
            lineButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintLINE32.gif"),this,ID_LINE);
            rectButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintRECT32.gif"),this,ID_RECT);
            polyButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintPOLY32.gif"),this,ID_POLY);
            textButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintTEXT32.gif"),this,ID_TEXT);
            circleButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintCIRC32.gif"),this,ID_CIRCLE);
            ellipseButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintELIP32.gif"),this,ID_ELLIPSE);
            
            pointButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintPOIN32.gif"),this,ID_POINT);
            interpolatedButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintINTE32.gif"),this,ID_INTERPOLATED);
            editButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"edit32.gif"),this,ID_EDIT);
            
            rectShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterRECT32.gif"),false,this,ID_SET_RECTSHUTTER);
            circleShButton = new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"shutterCIRC32.gif"),false,this,ID_SET_CIRCLESHUTTER);
            polyShButton = new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"shutterPOLY32.gif"),false,this,ID_SET_POLYSHUTTER);
            bmpShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterIMAG32.gif"),false,this,ID_SET_BMPSHUTTER);
            optShButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"shuttercolor_big.gif"),this,ID_SET_OPTSHUTTER);
           
            imageRelative =  ir.getIcon(MainContext.iconPath +"rtoimage32.gif");
            displayRelative =  ir.getIcon(MainContext.iconPath +"rtodisplay32.gif");
            imageDisplayButton = new CommandJButton(imageRelative,this,ID_IMAGERELATIVEBUTTON);
            filledButton = new CommandJToggleButton( ir.getIcon(MainContext.iconPath +"pipette32.GIF"),false,this,ID_FILLEDBUTTON);
            
            
    
            selectLayerButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"selectlayer32.gif"),this,ID_SELECTLAYER);
            
            editLayerButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"layer32.gif"),this,ID_EDIT_LAYER);
            invertButton = new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"invert32.gif"),false,this,ID_INVERT);
            flipButton= new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"fliphorizontally32.gif"),false,this,ID_FLIP);
            rotIcon0 =  ir.getIcon(MainContext.iconPath +"rotate32.gif");
            rotIcon90 =  ir.getIcon(MainContext.iconPath +"rotateR9032.gif");
            rotIcon180 =  ir.getIcon(MainContext.iconPath +"rotateR18032.gif");
            rotIcon270 =  ir.getIcon(MainContext.iconPath +"rotateR27032.gif");
            rotButton = new CommandJButton(rotIcon0,this,ID_ROT);
            zoomButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"zoom32.gif"),this,ID_ZOOM);
            zoomFitButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"2themax32.gif"),this,ID_ZOOMFIT);
            zoom1to1Button = new CommandJButton( ir.getIcon(MainContext.iconPath +"1zu132.gif"),this,ID_ZOOM1TO1);
            
            voiButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"voi132.gif"),this,ID_VOI);
            winInFrameButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"cw32.gif"),this,ID_WININFRAME);
            saveButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"saveDB32.gif"),this,ID_SAVE);
            saveAsButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"saveIMAG32.gif"),this,ID_SAVEAS);
            saveScreenButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"savescreen32.gif"),this,ID_SAVESCREEN);
            psButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"ps32.gif"),false,this,ID_PS);
            lutButton = new CommandJToggleButton( ir.getIcon(MainContext.iconPath +"lut32.gif"),false,this,ID_LUT);
            resetButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"reset32.gif"),this,ID_RESET);
            optionButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"options32.gif"),this,ID_OPTIONS);
    
            stackImageLeftButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"forw32a.gif"),this,ID_STACKIMAGE_LEFT);
            stackImageRightButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"back32a.gif"),this,ID_STACKIMAGE_RIGHT);
            stackLastImageButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"end32a.gif"),this,ID_STACKIMAGE_FIRST);
            stackFirstImageButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"begin32a.gif"),this,ID_STACKIMAGE_LAST);
            stackFrameLeftButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"forw32a.gif"),this,ID_STACKFRAME_LEFT);
            stackFrameRightButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"back32a.gif"),this,ID_STACKFRAME_RIGHT);
            stackLastFrameButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"end32a.gif"),this,ID_STACKFRAME_FIRST);
            stackFirstFrameButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"begin32a.gif"),this,ID_STACKFRAME_LAST);
            
            
        
        
        }
        else 
        {
            presentationLutButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath + "pLUT.gif"),false,this,ID_PRESENTATIONLUT);
            
            srEditButton = new JButton(ir.getIcon(MainContext.iconPath + "newsrsop.gif"));
            srCodeEditButton = new JButton(ir.getIcon(MainContext.iconPath + "modcode.gif"));
            srCodeNewButton = new JButton(ir.getIcon(MainContext.iconPath + "newcode.gif"));
            srViewUpButton= new JButton(ir.getIcon(MainContext.iconPath + "up.gif"));
            srResetButton= new JButton(ir.getIcon(MainContext.iconPath + "reset.gif"));
            srViewDownButton = new JButton(ir.getIcon(MainContext.iconPath + "down.gif"));
            srViewBeginButton=new JButton(ir.getIcon(MainContext.iconPath + "tobegin.gif")); 
            srViewEndButton= new JButton(ir.getIcon(MainContext.iconPath + "toend.gif"));
            srViewCopyButton=new JButton(ir.getIcon(MainContext.iconPath + "copy.gif"));
            srOkButton=new JButton(ir.getIcon(MainContext.iconPath + "ok.gif"));
                srRevisedButton = new JButton(ir.getIcon(MainContext.iconPath + "revised.gif"));
            
            srCompleteButton=new JButton(ir.getIcon(MainContext.iconPath + "complete.gif"));
            srVerificationButton=new JButton(ir.getIcon(MainContext.iconPath + "verify.gif"));
            srSaveButton= new JButton(ir.getIcon(MainContext.iconPath + "save2DB.gif"));
            
            checkIODButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "check.gif"),this,ID_CHECKIOD);
            dumpButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "dump.gif"),this,ID_DUMP);
            printButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus.gif"),this,ID_PRINT);
            printStToPrinterButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "print.gif"),this,ID_PRINTSTTOPRINT);
            printHcButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus.gif"),this,ID_PRINTHC);
            printStButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "printPlus.gif"),this,ID_PRINTST);
            quitButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"quit.gif"),this,ID_QUIT);
            loadImageButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"openIMAG.gif"),this,ID_LOAD_IMAGE);
            loadSRButton = new CommandJButton(ir.getIcon(MainContext.iconPath + "openSr.gif"),this,ID_LOAD_SR);
            loadPSButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"openSP.gif"),this,ID_LOAD_PS);
            
            loadButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"loadDB.gif"),this,ID_LOAD);
            deleteButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"delete.gif"),this,ID_DELETE);
            sendToButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"send.gif"),this,ID_SENDTO);
            refreshButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"refresh.gif"),this,ID_REFRESH);
            
            lineButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintLINE.gif"),this,ID_LINE);
            rectButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintRECT.gif"),this,ID_RECT);
            polyButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintPOLY.gif"),this,ID_POLY);
            textButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintTEXT.gif"),this,ID_TEXT);
            circleButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintCIRC.gif"),this,ID_CIRCLE);
            ellipseButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintELIP.gif"),this,ID_ELLIPSE);
            
            pointButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintPOIN.gif"),this,ID_POINT);
            interpolatedButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"paintINTE.gif"),this,ID_INTERPOLATED);
            editButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"edit.gif"),this,ID_EDIT);
            
            imageRelative =  ir.getIcon(MainContext.iconPath +"rtoimage.gif");
            displayRelative =  ir.getIcon(MainContext.iconPath +"rtodisplay.gif");
            imageDisplayButton = new CommandJButton(imageRelative,this,ID_IMAGERELATIVEBUTTON);
            
            rectShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterRECT.gif"),false,this,ID_SET_RECTSHUTTER);
            circleShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterCIRC.gif"),false,this,ID_SET_CIRCLESHUTTER);
            polyShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterPOLY.gif"),false,this,ID_SET_POLYSHUTTER);
            bmpShButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"shutterIMAG.gif"),false,this,ID_SET_BMPSHUTTER);
            optShButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"shuttercolor_small.gif"),this,ID_SET_OPTSHUTTER);
            
            selectLayerButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"selectlayer.gif"),this,ID_SELECTLAYER);
            editLayerButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"layer.gif"),this,ID_EDIT_LAYER);
            invertButton = new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"invert.gif"),false,this,ID_INVERT);
            flipButton= new CommandJToggleButton(ir.getIcon( MainContext.iconPath +"fliphorizontally.gif"),false,this,ID_FLIP);
            rotIcon0 =  ir.getIcon(MainContext.iconPath +"rotate.gif");
            rotIcon90 =  ir.getIcon(MainContext.iconPath +"rotateR90.gif");
            rotIcon180 =  ir.getIcon(MainContext.iconPath +"rotateR180.gif");
            rotIcon270 =  ir.getIcon(MainContext.iconPath +"rotateR270.gif");
            rotButton = new CommandJButton(rotIcon0,this,ID_ROT);
            imageRelative =  ir.getIcon(MainContext.iconPath +"rtoimage.gif");
            displayRelative =  ir.getIcon(MainContext.iconPath +"rtodisplay.gif");
            imageDisplayButton = new CommandJButton(imageRelative,this,ID_IMAGERELATIVEBUTTON);
            
            zoomButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"zoom.gif"),this,ID_ZOOM);
            zoomFitButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"2themax.gif"),this,ID_ZOOMFIT);
            zoom1to1Button = new CommandJButton( ir.getIcon(MainContext.iconPath +"1zu1.gif"),this,ID_ZOOM1TO1);
            
            voiButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"voi1.gif"),this,ID_VOI);
            winInFrameButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"cw.gif"),this,ID_WININFRAME);
            saveButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"save2DB.gif"),this,ID_SAVE);
            saveAsButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"saveIMAG.gif"),this,ID_SAVEAS);
            saveScreenButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"savescreen.gif"),this,ID_SAVESCREEN);
            psButton = new CommandJToggleButton( ir.getIcon(MainContext.iconPath +"ps.gif"),false,this,ID_PS);
            lutButton = new CommandJToggleButton(ir.getIcon(MainContext.iconPath +"lut.gif"),false,this,ID_LUT);
            resetButton = new CommandJButton(ir.getIcon(MainContext.iconPath +"reset.gif"),this,ID_RESET);
            optionButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"options.gif"),this,ID_OPTIONS);
            filledButton = new CommandJToggleButton( ir.getIcon(MainContext.iconPath +"pipette.gif"),false,this,ID_FILLEDBUTTON);
        
        
            stackImageLeftButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"forwa.gif"),this,ID_STACKIMAGE_LEFT);
            stackImageRightButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"backa.gif"),this,ID_STACKIMAGE_RIGHT);
            stackLastImageButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"enda.gif"),this,ID_STACKIMAGE_FIRST);
            stackFirstImageButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"begina.gif"),this,ID_STACKIMAGE_LAST);
            stackFrameLeftButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"forwa.gif"),this,ID_STACKFRAME_LEFT);
            stackFrameRightButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"backa.gif"),this,ID_STACKFRAME_RIGHT);
            stackLastFrameButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"enda.gif"),this,ID_STACKFRAME_FIRST);
            stackFirstFrameButton = new CommandJButton( ir.getIcon(MainContext.iconPath +"begina.gif"),this,ID_STACKFRAME_LAST);
            
        }
        rotIcon  = rotIcon90;
        
        CommandJButton.setButton(srEditButton);
        CommandJButton.setButton(srCodeNewButton);
        CommandJButton.setButton(srViewUpButton);
        CommandJButton.setButton(srViewDownButton);
        CommandJButton.setButton(srViewBeginButton);
        CommandJButton.setButton(srViewEndButton);
        CommandJButton.setButton(srViewCopyButton);
        CommandJButton.setButton(srCompleteButton);
        CommandJButton.setButton(srVerificationButton);
        CommandJButton.setButton(srSaveButton);
        CommandJButton.setButton(srResetButton);
        CommandJButton.setButton(srOkButton);
        CommandJButton.setButton(srCodeEditButton);
        CommandJButton.setButton(srRevisedButton);
        
        
        stackImageLeftButton.setToolTipText("Next Image");
        stackImageRightButton.setToolTipText("Previous Image");
        stackLastImageButton.setToolTipText("First Image");
        stackFirstImageButton.setToolTipText("Last Image");
        stackFrameLeftButton.setToolTipText("Next Frame");
        stackFrameRightButton.setToolTipText("Previous Frame");
        stackLastFrameButton.setToolTipText("First Frame");
        stackFirstFrameButton.setToolTipText("Last Frame");
        
        checkIODButton.setToolTipText("Check IOD");
        dumpButton.setToolTipText("Create Dump");
        presentationLutButton.setToolTipText("Presentation LUT");
        quitButton.setToolTipText("Exit");
        loadImageButton.setToolTipText( "Load Image File" );
        loadSRButton.setToolTipText( "Load SR File" );
        loadPSButton.setToolTipText( "Apply Presentation State File" );
        loadButton.setToolTipText( "Open" );
        deleteButton.setToolTipText( "Delete" );
        sendToButton.setToolTipText( "Send" );
        refreshButton.setToolTipText( "Refresh" );
        imageDisplayButton.setToolTipText("Annotation Unit");
        rectShButton.setToolTipText( "Rectangular Shutter" );
        circleShButton.setToolTipText( "Circular Shutter" );
        polyShButton.setToolTipText( "Polygonal Shutter" );
        bmpShButton.setToolTipText( "Bitmapped Shutter" );
        optShButton.setToolTipText( "Shutter Color" );
        editLayerButton.setToolTipText( "Layer" );
        invertButton.setToolTipText( "Invert" );
        flipButton.setToolTipText( "Flip" );
        rotButton.setToolTipText( "Rotate" );
        zoomButton.setToolTipText( "Zoom" );
        zoomFitButton.setToolTipText( "Fit Image" );
        zoom1to1Button.setToolTipText( "Zoom Image 1:1" );
        
        voiButton.setToolTipText( "Presets" );
        winInFrameButton.setToolTipText( "Interactive" );
        saveButton.setToolTipText( "Save to Database" );
        saveAsButton.setToolTipText( "Save to File" );
        saveScreenButton.setToolTipText( "Save Screen to File" );
        lutButton.setToolTipText( "GSDF/CIE-lab" );
        psButton.setToolTipText( "Presentation State" );
        resetButton.setToolTipText( "Reset" );
        optionButton.setToolTipText( "Options" );
        pointButton.setToolTipText( "Point" );
        lineButton.setToolTipText( "Line" );
        rectButton.setToolTipText( "Rectangle" );
        polyButton.setToolTipText( "Polyline" );
        circleButton.setToolTipText( "Circle" );
        ellipseButton.setToolTipText( "Ellipse" );
        textButton.setToolTipText( "Text" );
        interpolatedButton.setToolTipText( "Interpolated Line" );
        editButton.setToolTipText( "Edit" );
        printButton.setToolTipText( "Add to Print" );
        printStToPrinterButton.setToolTipText( "Print Stored Print Object" );
        printHcButton.setToolTipText( "Add to Print" );
        printStButton.setToolTipText( "Add to Print" );
        filledButton.setToolTipText( "Filled" );
        selectLayerButton.setToolTipText( "Select Layer" );
    }
    public static JLabel getSeperator() { return new JLabel ("   ");}
    public void buttonClicked (int ID)
    {
        if (fireEvent)
        {
            if (ID == ID_QUIT) System.exit(0);
            
            if (ID == ID_LOAD_IMAGE) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.LOAD_IM));
            if (ID == ID_LOAD_SR) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.LOAD_SR));
            if (ID == ID_LOAD) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.LOAD_DB));
            if (ID == ID_LOAD_PS) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.LOAD_PS));
            if (ID == ID_SAVE) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.SAVE_DB));
            if (ID == ID_SAVEAS) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.SAVE_AS));
            if (ID == ID_SAVESCREEN) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.SAVE_SCREEN));
            if (ID == ID_REFRESH) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.REFRESH));
            if (ID == ID_DELETE) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.DELETE));
            if (ID == ID_SENDTO) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.SENDTO));
            if (ID == ID_DUMP) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.DUMP));
            if (ID == ID_CHECKIOD) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.CHECKIOD));
            if (ID == ID_PRINTHC) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.PRINTHC));
            if (ID == ID_PRINTST) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.PRINTST));
            if (ID == ID_PRINTSTTOPRINT) Controller.instance().fireEvent(new DbActionEvent(this,DbActionEvent.PRINTSTTOPRINTER));
            
            
            if (ID == ID_SET_OPTSHUTTER) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_EDITSHUTTER));
            if (ID == ID_ROT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_ROT));
            if (ID == ID_RESET) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_RESET));
            if (ID == ID_OPTIONS) (new OptionDialog(optionButton,MainContext.instance().getConfiguration())).setVisible(true);
            if (ID == ID_ZOOM) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_ZOOM));
            if (ID == ID_ZOOMFIT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_ZOOMFIT));
            if (ID == ID_ZOOM1TO1) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_ZOOM1TO1));
            if (ID == ID_VOI) Controller.instance().fireEvent(new ImageActionEvent(voiButton,ImageActionEvent.ACTION_VOI));
            if (ID == ID_WININFRAME) Controller.instance().fireEvent(new ImageActionEvent(winInFrameButton,ImageActionEvent.ACTION_WININFRAME));
            
            if (ID == ID_RECT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_RECT));
            if (ID == ID_TEXT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_TEXT));
            if (ID == ID_CIRCLE) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_CIRCLE));
            if (ID == ID_LINE) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_LINE));
            if (ID == ID_ELLIPSE) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_ELLIPSE));
            if (ID == ID_INTERPOLATED) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_INTERPOLATED));
            if (ID == ID_POLY) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_POLYLINE));
            if (ID == ID_POINT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_POINT));
            if (ID == ID_EDIT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PAINT_EDIT));
            if (ID == ID_PRINT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PRINT));
            if (ID == ID_LAYERLIST) Controller.instance().fireEvent(new ImageChangeEvent(this,ID_LAYERLIST,layerComboBox.getSelectedIndex()));
            
            if (ID == ID_EDIT_LAYER) Controller.instance().fireEvent(new ImageActionEvent(editLayerButton,ImageActionEvent.ACTION_EDITLAYER));
            if (ID == ID_SELECTLAYER) Controller.instance().fireEvent(new ImageActionEvent(selectLayerButton,ImageActionEvent.ACTION_SELECTLAYER));
            if (ID == ID_IMAGERELATIVE) 
            {
                Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.SET_IMAGERELATIVE));
                
            }
            if (ID == ID_STACKIMAGE_LEFT) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.IMAGE_NEXT));
            if (ID == ID_STACKIMAGE_RIGHT) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.IMAGE_PREVIOUS));
            if (ID == ID_STACKIMAGE_FIRST) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.IMAGE_FIRST));
            if (ID == ID_STACKIMAGE_LAST) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.IMAGE_LAST));
            
            if (ID == ID_STACKFRAME_LEFT) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.FRAME_NEXT));
            if (ID == ID_STACKFRAME_RIGHT) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.FRAME_PREVIOUS));
            if (ID == ID_STACKFRAME_FIRST) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.FRAME_FIRST));
            if (ID == ID_STACKFRAME_LAST) Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.FRAME_LAST));
    
            
            
            if (ID == ID_IMAGERELATIVEBUTTON) 
            {
                Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.SET_IMAGERELATIVE));
                imageRelCheckBox.setSelected(!imageRelCheckBox.isSelected());
                if (imageRelCheckBox.isSelected()) imageDisplayButton.setIcon(imageRelative);
                else imageDisplayButton.setIcon(displayRelative);
                
            }
            
        }
    }
    public void setText(int ID, String text)
    {
        if ( ID==ID_WINDOW) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_WINDOW, new Double(text)));
        if ( ID==ID_CENTER) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_CENTER, new Double(text)));
        if ( ID==ID_ZOOMINPUT) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_ZOOM, new Double(text)));
        if ( ID==ID_NAME) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_NAME, text));
        if ( ID==ID_CREATOR) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_CREATOR, text));
        if ( ID==ID_DESCRIPTION) Controller.instance().fireEvent(new ImageChangeEvent(this, ID_DESCRIPTION, text));
        if (ID == ID_STACKCURRENTIMAGE) 
        {
            Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.IMAGE_SET, new Integer(text).intValue()));
        }
        if (ID == ID_STACKCURRENTFRAME) 
        {
            Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.FRAME_SET, new Integer(text).intValue()));
        }
        
    }
    
    public void buttonClicked (int ID, boolean state)
    {
            if (ID == ID_PRESENTATIONLUT) 
            {
                presentationLutButton.setSelected(!presentationLutButton.isSelected());
                Controller.instance().fireEvent(new ImageActionEvent(presentationLutButton,ImageActionEvent.ACTION_PRESENTATIONLUT));
            }
            if (ID == ID_LUT) 
            {
                lutButton.setSelected(!lutButton.isSelected());
                Controller.instance().fireEvent(new ImageActionEvent(lutButton,ImageActionEvent.ACTION_BARTEN));
            }
            
            if (ID == ID_SET_RECTSHUTTER) 
            {
                if (rectShButton.isSelected())rectShButton.setSelected(false);
                Controller.instance().fireEvent(new SetShutterEvent(this,SetShutterEvent.RECT,  state));
                
            }
            if (ID == ID_SET_POLYSHUTTER) 
            {
                if (polyShButton.isSelected())polyShButton.setSelected(false);
                Controller.instance().fireEvent(new SetShutterEvent(this,SetShutterEvent.POLY,  state));
            }
            if (ID == ID_SET_CIRCLESHUTTER) 
            {
                if (circleShButton.isSelected())circleShButton.setSelected(false);
                Controller.instance().fireEvent(new SetShutterEvent(this,SetShutterEvent.CIRCLE,  state));
            }
            if (ID == ID_SET_BMPSHUTTER) Controller.instance().fireEvent(new SetShutterEvent(bmpShButton,SetShutterEvent.BMP,  state));
            
            if (ID == ID_INVERT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_INVERT));
            if (ID == ID_FLIP) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_FLIP));
            if (ID == ID_PS) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.ACTION_PS));
            
            
            if (ID == ID_FILLED) 
            {
                Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.SET_FILLED));
                filledButton.setSelected(filledCheckBox.isSelected());
            }
            if (ID == ID_FILLEDBUTTON) 
            {
                Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.SET_FILLED));
                filledCheckBox.setSelected(filledButton.isSelected());
            }
            if (ID == ID_IMAGERELATIVE) 
            {
                Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.SET_IMAGERELATIVE));
                if (imageRelCheckBox.isSelected()) imageDisplayButton.setIcon(imageRelative);
                else imageDisplayButton.setIcon(displayRelative);
            }
            if (ID == ID_MAGNIFY) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.MODE_MAGNIFY));
            if (ID == ID_SCALETOFIT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.MODE_SCALE));
            if (ID == ID_TRUESIZE) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.MODE_TRUESIZE));
            if (ID == ID_PRINT) Controller.instance().fireEvent(new ImageActionEvent(this,ImageActionEvent.PRINT));
            
            if (ID == ID_APPLYFRAME)
               Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.APPLYFRAME));
            
            if (ID == ID_APPLYIMAGE)Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.APPLYIMAGE));
            if (ID == ID_APPLYALL)Controller.instance().fireEvent(new MoveImageEvent(this,MoveImageEvent.APPLYALL));
            
        
    }
    public void itemStateChanged (int ID, boolean state,Object o)
    {
        if (fireEvent)
        {
        }
    }
  /*  public void setSelected (int ID, int index)
    {
        //if (ID == ID_LIST); 
    }*/
	public boolean processEvent (DSEvent e)
	{
	     if (e instanceof ImageActionEvent)
	    {
	        ImageActionEvent ae = (ImageActionEvent)e;
	        {
	            switch (ae.type)
	            {
	                    case ImageActionEvent.MODE_MAGNIFY:
	                                zoomButton.setEnabled(true);
	                                zoomFitButton.setEnabled(true);
	                                zoomTextField.setEnabled(true);
	                                zoom1to1Button.setEnabled(true);
        	                        
	                                return true;
	                    case ImageActionEvent.MODE_SCALE:
	                                zoomButton.setEnabled(true);
	                                zoomFitButton.setEnabled(true);
	                                zoomTextField.setEnabled(true);
	                                zoom1to1Button.setEnabled(true);
	                                return true;
	                    case ImageActionEvent.MODE_TRUESIZE:
	                                zoomButton.setEnabled(false);
	                                zoomFitButton.setEnabled(false);
	                                zoomTextField.setText("");
	                                zoomTextField.setEnabled(false);
	                                zoom1to1Button.setEnabled(false);
	                                return true;
	                        
	                }
	        
	        }
	    }
	        return false;
	}
    public  boolean processComponents (DSEvent e)
    {
	    fireEvent = false;
	    if (e instanceof PresentationStateFixInfoEvent)
	    {
	        
	        PresentationStateFixInfoEvent sp = (PresentationStateFixInfoEvent)e;
                rectShButton.setSelected(sp.haveRectShutter);
                circleShButton.setSelected(sp.haveCircShutter);
                polyShButton.setSelected(sp.havePolyShutter);
                bmpShButton.setSelected(sp.haveBmpShutter);
	        invertButton.setSelected(sp.isInvert);
	        flipButton.setSelected(sp.isFlip);
	        if (sp.rot == 0) rotButton.setIcon(rotIcon0);
	        if (sp.rot == 1) rotButton.setIcon(rotIcon90);
	        if (sp.rot == 2) rotButton.setIcon(rotIcon180);
	        if (sp.rot == 3) rotButton.setIcon(rotIcon270);
	        psButton.setSelected(sp.psOn);
	        if (sp.lutOn);
	        else
	        {
	            
	            centerTextField.setText(sp.lev.toString());
                windowTextField.setText(sp.win.toString());
	        }
	        
	        double newValue = sp.zoom.doubleValue();
	        newValue = (Math.round(newValue*100d)/100d);
	        zoomTextField.setText(new Double (newValue).toString());
	        
	        fireEvent = true;
	        nameTextField.setText(sp.name);
	        creatorTextField.setText(sp.creator);
	        descrTextArea.setText(sp.description);
	        String mode = null;
	        trueSizeRadio.setEnabled(sp.canUseDisplayedAreaTrueSize);
	        if (sp.presentationSizeMode ==  jDVPSPresentationSizeMode.DVPSD_magnify) 
	        {
	            mode =  "magnify";
                  magnifyRadio.setSelected(true);
	        }
	        if (sp.presentationSizeMode ==  jDVPSPresentationSizeMode.DVPSD_scaleToFit)
	        {
	            mode =  "scale to fit";
                    scaleRadio.setSelected(true);
	        }
	        if (sp.presentationSizeMode ==  jDVPSPresentationSizeMode.DVPSD_trueSize) 
	        {
	            mode =  "true size";
	            zoomButton.setEnabled(false);
	            zoomTextField.setEnabled(false);
	            
                trueSizeRadio.setSelected(true);
	        }
	        String d = new String("rows = " + sp.rows + ", cols = " + sp.cols+
	                             "\nmode = " + mode+
	                             "\n" + sp.displayArea);
	        if (sp.presentationSizeMode ==  jDVPSPresentationSizeMode.DVPSD_magnify) d = d.concat("\nmangn. ratio" + sp.presentationPixelMagnificationRatio);
	        if (sp.haveDisplayedAreaPresentationPixelSpacing )d = d.concat("\npixel spacing: (" + sp.presentationPixelSpacing_x + "/" + sp.presentationPixelSpacing_y+ ")");
	        else d = d.concat("\naspect ratio: " + sp.presentationPixelAspectRatio);
	        infoTextArea.setLineWrap(true);
	        infoTextArea.setText(d);
            maxImageTextField.setText(new Integer(sp.maxNumberOfImages).toString());
            currentImageTextField.setText(new Integer(sp.currentImageNumber).toString());
            maxFrameTextField.setText(new Integer(sp.maxNumberOfFramesInCurrentImage).toString());
            currentFrameTextField.setText(new Integer(sp.currentNumberOfFrames).toString());
	    //applyAllRadio.setSelected(true);                    
	        
	        if (sp.applyTo == jDVPSObjectApplicability.DVPSB_allImages) applyAllRadio.setSelected(true);  
	        else if (sp.applyTo ==  jDVPSObjectApplicability.DVPSB_currentFrame)applyFrameRadio.setSelected(true);  
	             
	            
	        else if (sp.applyTo ==  jDVPSObjectApplicability.DVPSB_currentImage) applyImageRadio.setSelected(true);  
	            
	    }
	    if (e instanceof SetPresentationStateEvent)
	    {
	        
	        SetPresentationStateEvent sp = (SetPresentationStateEvent)e;
                rectShButton.setSelected(sp.haveRectShutter);
                circleShButton.setSelected(sp.haveCircShutter);
                polyShButton.setSelected(sp.havePolyShutter);
                bmpShButton.setSelected(sp.haveBmpShutter);
	        invertButton.setSelected(sp.isInvert);
	        flipButton.setSelected(sp.isFlip);
	        if (sp.rot == 0) rotButton.setIcon(rotIcon0);
	        if (sp.rot == 1) rotButton.setIcon(rotIcon90);
	        if (sp.rot == 2) rotButton.setIcon(rotIcon180);
	        if (sp.rot == 3) rotButton.setIcon(rotIcon270);
	        psButton.setSelected(sp.psOn);
	        if (sp.curve == jDVPSDisplayTransform.DVPSD_none)lutButton.setSelected(false);
	        else lutButton.setSelected(true);
	        
	        if (sp.psLut ==0)presentationLutButton.setSelected(false);
	        else presentationLutButton.setSelected(true);
	                             
	                             
	    }
	    fireEvent = true;
	    if (e instanceof ImageChangeEvent)
	    {
	        ImageChangeEvent ie = (ImageChangeEvent)e;
	        
	        switch (ie.type)
	        {
	            case ID_INVERT:
	                            invertButton.setSelected(ie.selected);
	                            break;
	            case ID_FLIP:
	                            flipButton.setSelected(ie.selected);
	                            break;
	            case ID_PS:
	                            psButton.setSelected(ie.selected);
	                            break;
	            case ID_LUT:
	                            lutButton.setSelected(ie.selected);
	                            break;
	            case ID_ROT0:
	                            rotButton.setIcon(rotIcon0);
	                            break;
	            case ID_ROT90:
	                            rotButton.setIcon(rotIcon90);
	                            break;
	            case ID_ROT180:
	                            rotButton.setIcon(rotIcon180);
	                            break;
	            case ID_ROT270:
	                            rotButton.setIcon(rotIcon270);
	                            break;
	                            
	            case ID_ZOOMINPUT:
	                            double newValue = ie.newDouble.doubleValue();
	                            newValue = (Math.round(newValue*100d)/100d);
	                            zoomTextField.setText(new Double (newValue).toString());
	                            break;
	            case ID_WINDOW:
	                            windowTextField.setText(ie.newDouble.toString());
	                            centerTextField.setText(ie.newDouble1.toString());
	                            break;
	            case ID_MAGNIFY:
	                            zoomButton.setEnabled(true);
	                            zoomFitButton.setEnabled(true);
	                             zoomTextField.setEnabled(true);
	                            zoom1to1Button.setEnabled(true);
	                            magnifyRadio.setSelected(true);
	                            break;
	            case ID_SCALETOFIT:
	                            zoomButton.setEnabled(true);
	                            zoomFitButton.setEnabled(true);
	                            zoom1to1Button.setEnabled(true);
	                            zoomTextField.setEnabled(true);
	                            scaleRadio.setSelected(true);
	                            break;
	            case ID_TRUESIZE:
	                            zoomFitButton.setEnabled(false);
	                            zoom1to1Button.setEnabled(false);
	                            zoomButton.setEnabled(false);
	                            zoomTextField.setEnabled(false);
	                            trueSizeRadio.setSelected(true);
	                            break;
	            case ID_VOILUT:
	                            windowTextField.setText("");
	                            centerTextField.setText("");
	                            break;
	           /* case ID_STACKCURRENTFRAME:
	                            currentFrameTextField.setText(new Integer(ie.intValue).toString());
	                            break;
	            case ID_STACKCURRENTIMAGE:
	                            currentImageTextField.setText(new Integer(ie.intValue).toString());
	                            break;
	                            */
	        }
	    }
	    if (e instanceof SendLayerEvent)
	    {
	        SendLayerEvent sle = (SendLayerEvent)e;
	        DefaultComboBoxModel m = new DefaultComboBoxModel(sle.layers);
	        layerComboBox.setModel(m);
	        layerComboBox.setSelectedIndex(sle.selectedIndex);
	        
        }
      return false;
    }
	
}
/*
 *  CVS Log
 *  $Log: GuiComponents.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;

import java.util.*;

/**
 * This class contains the names and the uids of DICOM class UIDs
 * @author Klaus Kleber
 * @since 08.11.2000
 */
public class DICOMSOPClassUID 
{
    
    
    /**
    * PS UID
    */
    public static final String PS_UID = "1.2.840.10008.5.1.4.1.1.11.1";
    
    /**
    * PS Name
    */
    public static final String PS_UID_NAME = "Presentation State Storage Service Class";

    
    /**
    * UID  Basic Text SR 
    */
    public static final String BASIC_TEXT_SR_UID = "1.2.840.10008.5.1.4.1.1.88.11";
    
    /**
    * UID  Enhanced SR 
    */
    public static final String ENHANCED_SR_UID = "1.2.840.10008.5.1.4.1.1.88.22"; 
    
    /**
    * UID  Comprehensive SR 
    */
    public static final String COMPREHENSIVE_SR_UID = "1.2.840.10008.5.1.4.1.1.88.33"; 
    
    /**
    * Name  Basic Text SR 
    */
    public static final String BASIC_TEXT_SR_NAME= "Basic Text SR";
    
    /**
    * Name  Enhanced SR 
    */
    public static final String ENHANCED_SR_UID_NAME = "Enhanced SR"; 
    
    /**
    * Name  Comprehensive SR 
    */
    public static final String COMPREHENSIVE_SR_UID_NAME = "Comprehensive SR"; 
    
    /**
    *Contains a dictionary form DICOM Uids to names
    */
    private static Hashtable uids = null;
    static
    {
        uids = new Hashtable();
        uids.put(PS_UID,PS_UID_NAME);
        uids.put(BASIC_TEXT_SR_UID,BASIC_TEXT_SR_NAME);
        uids.put(ENHANCED_SR_UID,ENHANCED_SR_UID_NAME);
        uids.put(COMPREHENSIVE_SR_UID,COMPREHENSIVE_SR_UID_NAME);
    }
    
    /**
    * Returns a name of a given UID
    */
    private String getName(String uid)
    {
        return (String)uids.get(uid);
    }
}
/*
 *  CVS Log
 *  $Log: DICOMSOPClassUID.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

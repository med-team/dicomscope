/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import de.microtherapy.tools.text.document.general.*;
import javax.swing.*;
import jToolkit.gui.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.*;
import viewer.gui.*;
import java.awt.event.*;
/**
 * This class contains the GUI for the OptionDialog.
 * 
 * @author Klaus Kleber
 */
public class OptionDialog extends JDialog implements  CommandButtonListener
{
     public static final int ID_OK =0;
     public static final int ID_CANCEL =1;
     public static final int ID_APPLY =2;
     private boolean changeListenerOn= true;
     /**
      * Contains the okButton
      */
     CommandJButton okButton;

     /**
      * Contains the applyButton
      */
     CommandJButton applyButton;

     /**
      * Contains the cancelButton
      */
     CommandJButton cancelButton;

     /**
      * Contains the Tabpanel of the OptionPanel
      */
     JTabbedPane tb = new JTabbedPane();

     /**
      * GUI of the Series Browser
      */
     JCheckBox uidCb = new JCheckBox("UID", false);
     JCheckBox descCb = new JCheckBox("Description", false);
     JCheckBox patiCb = new JCheckBox("Patient Information", false);
     JCheckBox modaCb = new JCheckBox("Modality", false);
     JCheckBox timeCb = new JCheckBox("Date & Time", false);
     JCheckBox newiCb = new JCheckBox("New Items", false);
     JCheckBox fileCb = new JCheckBox("Filename", false);
     JCheckBox lablCb = new JCheckBox("Label", false);
     
     JCheckBox studyButtonOn = new JCheckBox("Buttons on", false);
     
     /**
      * General GUI Elements
      */
     
     JRadioButton fLeft = new JRadioButton("Left");
     JRadioButton fRight = new JRadioButton("Right");
     JRadioButton fTop = new JRadioButton("Top");
     JRadioButton fDown = new JRadioButton("Down");
     JRadioButton fNone = new JRadioButton("None");
     
     JTextField ambientLight = new JTextField(5);
     JTextField pixelSizeX = new JTextField(10);
     JTextField pixelSizeY  = new JTextField(10);
     JComboBox uiComboBox;
     JComboBox profilesComboBox = new JComboBox(); 
     /**
      * GUI of the Viewer
      */
     JComboBox paintCombo = new JComboBox(); 
     JCheckBox processingPanelOn = new JCheckBox("Image Processing Functions", false);
     JCheckBox psPanelOn = new JCheckBox("Presentation State Panel", false);
     JCheckBox navigationPanelOn = new JCheckBox("Navigation Panel ", true);
     
     JRadioButton pLeft = new JRadioButton("Left");
     JRadioButton pRight = new JRadioButton("Right");
     JRadioButton pTop = new JRadioButton("Top");
     JRadioButton pDown = new JRadioButton("Down");
     
     
     /**
      * Contains the configuaration of DICOMscope
      */
     Hashtable configurationProperties;
    Font font ;
     /**
      * Constructor. Builds the GUI.
      * 
      * @param parent Parent Component of the Dialog
      * @param configurationProperties Contains the configuaration of DICOMscope.
      */
     
     public OptionDialog(JComponent parent, Hashtable configurationProperties)
     {
          super();
       ActionListener l = new ChangedActionListener();   
          
          
          
          
          
          
          
          
          
          this.configurationProperties = configurationProperties;
          if (configurationProperties.containsKey("GUIFont"))font = (Font)configurationProperties.get("GUIFont");
          getContentPane().setFont(font);
          //Inits
          setModal(true);
          setSize(300,400);
          setLocationRelativeTo(parent);
          setTitle("DICOMscope Options");
          getContentPane().setLayout(new BorderLayout(10,10));
          getRootPane().setFont(font);
          getRootPane().getContentPane().setFont(font);
          //GetFont
          
          
          //Buttons
          okButton = new CommandJButton("OK",this,ID_OK);
          cancelButton = new CommandJButton("Cancel",this,ID_CANCEL);
          applyButton = new CommandJButton("Apply",this,ID_APPLY);
          JPanel  buttonPanel = new JPanel();
          buttonPanel.add(okButton);
          buttonPanel.add(applyButton);
          buttonPanel.add(cancelButton);
          getContentPane().add(buttonPanel,BorderLayout.SOUTH);
          
          //TabbedPane
          JPanel studyOptions = new JPanel();
          JPanel viewerOptions = new JPanel();
          JPanel printOptions = new JPanel();
          JPanel generalOptions = new JPanel();
          tb.add("Browser",studyOptions);
          tb.add("Viewer",viewerOptions);
         // tb.add("Print",printOptions);
          tb.add("General",generalOptions);
          getContentPane().add(tb,BorderLayout.CENTER);
          
          ///////////////////////////////////////////////
          //General
          ///////////////////////////////////////////////////
          
          generalOptions.setLayout(new BoxLayout(generalOptions,BoxLayout.Y_AXIS));
          JPanel pPanel = new JPanel();
          pPanel.setBorder(new TitledBorder("Set Tabbed Placement"));
          
          ButtonGroup pGroup = new ButtonGroup();
          pPanel.add(pLeft);
          pPanel.add(pRight);
          pPanel.add(pTop);
          pPanel.add(pDown);
          pGroup.add(pLeft);
          pGroup.add(pRight);
          pGroup.add(pTop);
          pGroup.add(pDown);
          pLeft.setSelected(true);
          //generalOptions.add(pPanel);
          
          JPanel profilesPanel = new JPanel();
          profilesPanel.setBorder(new TitledBorder("Profiles"));
      
          profilesComboBox = new JComboBox();
      
          Hashtable profiles = MainContext.profiles;
           profilesComboBox.addItem( "User defined" );
           
           for (Enumeration e = profiles.keys() ; e.hasMoreElements() ;) {
                profilesComboBox.addItem( e.nextElement() ); 
            }
          profilesPanel.add(profilesComboBox);
          
          profilesComboBox.addActionListener(new ProfilesActionListener());
          generalOptions.add(profilesPanel);
          
          
          JPanel guiPanel = new JPanel();
          guiPanel.setBorder(new TitledBorder("Look and Feel"));
      
          uiComboBox = new JComboBox();
      
            // Menu for the look and feels (lnfs).
           UIManager.LookAndFeelInfo[] lnfs = UIManager.getInstalledLookAndFeels();
      
            for( int i = 0; i < lnfs.length; i++ ) 
            {
                uiComboBox.addItem( lnfs[i].getName() ); 
	 
	            if ( UIManager.getLookAndFeel().getName().equals( lnfs[i].getName() ) ) uiComboBox.setSelectedIndex(i);
            }
          guiPanel.add(uiComboBox);
          generalOptions.add(guiPanel);
          
          ///////////////////////////////////////////////
          //Viewer
          ///////////////////////////////////////////////////
          viewerOptions.setLayout(new BoxLayout(viewerOptions,BoxLayout.Y_AXIS));
          JPanel fPanel = new JPanel();
          fPanel.setBorder(new TitledBorder("Function Panel Placement"));
          
          ButtonGroup fGroup = new ButtonGroup();
          fPanel.add(fLeft);
          fPanel.add(fRight);
          fPanel.add(fTop);
          fPanel.add(fDown);
          fPanel.add(fNone);
          fGroup.add(fLeft);
          fGroup.add(fRight);
          fGroup.add(fTop);
          fGroup.add(fDown);
          fGroup.add(fNone);
          fLeft.setSelected(true);
          viewerOptions.add(fPanel);
          
          
          
          JPanel iPanel = new JPanel();
          iPanel.setBorder(new TitledBorder("Processing"));
          iPanel.setLayout(new GridLayout(3,1));
          iPanel.add(processingPanelOn);
          iPanel.add(psPanelOn);
          iPanel.add(navigationPanelOn);
          viewerOptions.add(iPanel);
          
          JPanel paintPanel = new JPanel();
          paintPanel.setBorder(new TitledBorder("Paint Functions"));
          paintCombo.addItem("North");
          paintCombo.addItem("South");
          paintCombo.addItem("West");
          paintCombo.addItem("East");
          paintCombo.addItem("Toolbar");
          paintCombo.addItem("Integrated");
          paintCombo.addItem("None");
          
          paintPanel.add(paintCombo);
          
          viewerOptions.add(paintPanel);
          
          
          JPanel aPanel = new JPanel();
          aPanel.setBorder(new TitledBorder("Ambient Light"));
            aPanel.add(new JLabel(" cd/m^2"));
          ambientLight.setDocument(new DoubleDocument());
          aPanel.add(ambientLight);
          viewerOptions.add(aPanel);
          
          JPanel sizePanel = new JPanel();
          sizePanel.setBorder(new TitledBorder("Screen Pixel Resolution in mm"));
            sizePanel.add(new JLabel(" X"));
          pixelSizeX.setDocument(new DoubleDocument());
            sizePanel.add(pixelSizeX);
            sizePanel.add(new JLabel(" Y"));
          pixelSizeY.setDocument(new DoubleDocument());
            sizePanel.add(pixelSizeY);
          viewerOptions.add(sizePanel);
            
          
          ///////////////////////////////////////////////
          //StudyMan
          ///////////////////////////////////////////////////
          studyOptions.setLayout(new BoxLayout(studyOptions,BoxLayout.Y_AXIS));
          
          
          //ViewOptions
          JPanel viewOptions = new JPanel();
          viewOptions.setBorder(new TitledBorder("Show Dicom Attributes"));
          viewOptions.setLayout(new GridLayout(1,1));
          JPanel centerPanel = new JPanel();
          centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
          
          centerPanel.add(uidCb);
          centerPanel.add(descCb);
          centerPanel.add(patiCb);
          centerPanel.add(modaCb);
          centerPanel.add(timeCb);
          centerPanel.add(newiCb);
          centerPanel.add(fileCb);
          centerPanel.add(lablCb);
          viewOptions.add(centerPanel);
          studyOptions.add(viewOptions);
          
          
          setProperties(configurationProperties);
      uidCb.addActionListener(l);
      descCb.addActionListener(l);
      patiCb.addActionListener(l);
      modaCb.addActionListener(l);
      timeCb.addActionListener(l);
      newiCb.addActionListener(l);
      fileCb.addActionListener(l);
      lablCb.addActionListener(l);
     
      studyButtonOn.addActionListener(l);
     
     /**
      * General GUI Elements
      */
     
      fLeft.addActionListener(l);
      fRight.addActionListener(l);
      fTop.addActionListener(l);
      fDown.addActionListener(l);
      fNone.addActionListener(l);
     
      ambientLight.addActionListener(l);
      pixelSizeX.addActionListener(l);
      pixelSizeY.addActionListener(l);
      uiComboBox.addActionListener(l);
     /**
      * GUI of the Viewer
      */
      paintCombo.addActionListener(l);
      processingPanelOn.addActionListener(l);
      psPanelOn.addActionListener(l);
      navigationPanelOn.addActionListener(l);
     
      pLeft.addActionListener(l);
      pRight.addActionListener(l);
      pTop.addActionListener(l);
      pDown.addActionListener(l);
          
          pack();
          
     }

     /**
      * Sets the value of the widgets from the specified Hashtable
      * 
      * @param prop Contains the values of the widgets
      */
     public void setProperties(Hashtable prop)
     {
        if (prop == null) return;
        //Series Browser
        if (prop.containsKey("UID"))uidCb.setSelected(((Boolean)(prop.get("UID"))).booleanValue());
        if (prop.containsKey("Description"))descCb.setSelected(((Boolean)(prop.get("Description"))).booleanValue());
        if (prop.containsKey("PatientInfo"))patiCb.setSelected(((Boolean)(prop.get("PatientInfo"))).booleanValue());
        if (prop.containsKey("Modality"))modaCb.setSelected(((Boolean)(prop.get("Modality"))).booleanValue());
        if (prop.containsKey("DateTime"))timeCb.setSelected(((Boolean)(prop.get("DateTime"))).booleanValue());
        if (prop.containsKey("NewItems"))newiCb.setSelected(((Boolean)(prop.get("NewItems"))).booleanValue());
        if (prop.containsKey("Filename"))fileCb.setSelected(((Boolean)(prop.get("Filename"))).booleanValue());
        if (prop.containsKey("Label"))lablCb.setSelected(((Boolean)(prop.get("Label"))).booleanValue());
        
        if (prop.containsKey("ButtonOn"))studyButtonOn.setSelected(((Boolean)(prop.get("ButtonOn"))).booleanValue());
        
        
        //Viewer
        if (prop.containsKey("PaintPanelPlacement"))paintCombo.setSelectedItem((String)(prop.get("PaintPanelPlacement")));
        if (prop.containsKey("NavigationPanelOn"))navigationPanelOn.setSelected(((Boolean)(prop.get("NavigationPanelOn"))).booleanValue());
        if (prop.containsKey("ProcessingPanelOn"))processingPanelOn.setSelected(((Boolean)(prop.get("ProcessingPanelOn"))).booleanValue());
        if (prop.containsKey("PSPanelOn"))psPanelOn.setSelected(((Boolean)(prop.get("PSPanelOn"))).booleanValue());
        if (prop.containsKey("AmbientLight")) ambientLight.setText((String)(prop.get("AmbientLight")));
        else ambientLight.setEnabled(false);
        if (prop.containsKey("PixelSizeX"))pixelSizeX.setText((String)(prop.get("PixelSizeX")));
        if (prop.containsKey("PixelSizeY"))pixelSizeY.setText((String)(prop.get("PixelSizeY")));
            
        
        
        if (prop.containsKey("TabbedPlacement"))
        {
            String placement  = (String)prop.get("TabbedPlacement");
            if (placement.equals("1")) pTop.setSelected(true);
            if (placement.equals("2")) pLeft.setSelected(true);
            if (placement.equals("3")) pDown.setSelected(true);
            if (placement.equals("4")) pRight.setSelected(true);
        }
        
        
        
        if (prop.containsKey("FunctionPanelPlacement"))
        {
            String orient  = (String)prop.get("FunctionPanelPlacement");
            if (orient.equals("North")) fTop.setSelected(true);
            if (orient.equals("West")) fLeft.setSelected(true);
            if (orient.equals("South")) fDown.setSelected(true);
            if (orient.equals("East")) fRight.setSelected(true);
            if (orient.equals("None")) fNone.setSelected(true);
        }
        
     }

    /**
     * q
     * 
     * @param ID q
     */
     
     
    public void buttonClicked (int ID)
    {
        if (ID == ID_OK || ID == ID_APPLY) 
        {
            applyChanges();
        }
        if ((ID == ID_OK || ID == ID_CANCEL)) 
        {
            setVisible (false);
        }
        
    }

    /**
     * q
     */
    public void applyChanges()
    {
        
	        
	        //Study Man
	        configurationProperties.put("UID", new Boolean(uidCb.isSelected()));
	        configurationProperties.put("Description", new Boolean(descCb.isSelected()));
	        configurationProperties.put("PatientInfo", new Boolean(patiCb.isSelected()));
	        configurationProperties.put("Modality", new Boolean(modaCb.isSelected()));
	        configurationProperties.put("DateTime", new Boolean(timeCb.isSelected()));
	        configurationProperties.put("NewItems", new Boolean(newiCb.isSelected()));
	        configurationProperties.put("Filename", new Boolean(fileCb.isSelected()));
	        configurationProperties.put("Label", new Boolean(lablCb.isSelected()));
	        
	        
	        configurationProperties.put("ButtonOn", new Boolean(studyButtonOn.isSelected()));
	        
	        //Viewer
	        configurationProperties.put("PaintPanelPlacement", paintCombo.getSelectedItem());
	        configurationProperties.put("ProcessingPanelOn", new Boolean(processingPanelOn.isSelected()));
	        configurationProperties.put("NavigationPanelOn", new Boolean(navigationPanelOn.isSelected()));
	        configurationProperties.put("PSPanelOn", new Boolean(psPanelOn.isSelected()));
	        configurationProperties.put("AmbientLight", ambientLight.getText());
	        configurationProperties.put("PixelSizeX", pixelSizeX.getText());
	        configurationProperties.put("PixelSizeY", pixelSizeY.getText());
	    
	        
	        int placement =0;
	        if (pTop.isSelected()) placement = 1;
	        if (pLeft.isSelected()) placement = 2;
	        if (pDown.isSelected()) placement = 3;
	        if (pRight.isSelected()) placement = 4;
	        configurationProperties.put("TabbedPlacement", new Integer(placement).toString());
	        
	        
	        //General
	        String orient = null;
	        if (fTop.isSelected()) orient = "North";
	        if (fLeft.isSelected()) orient = "West";
	        if (fDown.isSelected()) orient = "South";
	        if (fRight.isSelected()) orient = "East";
	        if (fNone.isSelected()) orient = "None";
	        configurationProperties.put("FunctionPanelPlacement", orient);
	        
            
            UIManager.LookAndFeelInfo[] lnfs = UIManager.getInstalledLookAndFeels();
		    UIManager.LookAndFeelInfo info =(UIManager.LookAndFeelInfo) lnfs[uiComboBox.getSelectedIndex()];
	        configurationProperties.put("UI", new String(info.getClassName()));
	        
	        Controller.instance().fireEvent(new ChangeOptionsEvent(this,configurationProperties));
    }
    
    public class ProfilesActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            changeListenerOn= false;
            String item = (String) profilesComboBox.getSelectedItem();
            if (!item.equals("User defined")) {
                setProperties((Hashtable)MainContext.profiles.get(item));
            }
            
            changeListenerOn= true;
        }
    }
    public class ChangedActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (changeListenerOn)profilesComboBox.setSelectedIndex(0);
            
        }
    }
    
}
/*
 *  CVS Log
 *  $Log: OptionDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

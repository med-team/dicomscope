/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
import viewer.presentation.*;
/**
* Contains all relevant information about a presentation state.
*/

public  class PresentationStateFixInfoEvent extends DSEvent
{
    
    public boolean haveRectShutter;
    public boolean haveCircShutter;
    public boolean havePolyShutter;
    public boolean haveBmpShutter;
    public boolean isFlip;
    public boolean isInvert;
    public int rot;
    public boolean psOn;
    public Double win;
    public Double lev;
    boolean lutOn = false;
    public Double zoom;
    public String name;
    public String creator;
    public String description;
    public int cols;
    public int rows;
    public DisplayArea displayArea;
    public boolean haveDisplayedAreaPresentationPixelSpacing;
    public boolean canUseDisplayedAreaTrueSize;
    public int presentationSizeMode;
    public double presentationPixelSpacing_x;
    public double presentationPixelSpacing_y;
    public double presentationPixelAspectRatio;
    public double presentationPixelMagnificationRatio;
    public int maxNumberOfImages = 1;
    public int currentImageNumber = 1;
    public int maxNumberOfFramesInCurrentImage = 1;
    public int currentNumberOfFrames= 1;
    public int applyTo;
    
    public PresentationStateFixInfoEvent(   Object o,
                                        boolean haveRectShutter,
                                        boolean haveCircShutter,
                                        boolean havePolyShutter,
                                        boolean haveBmpShutter,
                                        boolean isFlip,
                                        boolean isInvert,
                                        int rot,
                                        boolean psOn,
                                        Double win,
                                        Double lev, 
                                        boolean lutOn,
                                        Double zoom,
                                        String name,
                                        String creator,
                                        String description, 
                                        int cols, 
                                        int rows, 
                                        DisplayArea displayArea,
                                        boolean haveDisplayedAreaPresentationPixelSpacing,
                                        boolean canUseDisplayedAreaTrueSize,
                                        int presentationSizeMode,
                                         double presentationPixelSpacing_x,
                                         double presentationPixelSpacing_y,
                                         double presentationPixelAspectRatio,
                                         double presentationPixelMagnificationRatio,
                                        int maxNumberOfImages,
                                        int currentImageNumber ,
                                        int maxNumberOfFramesInCurrentImage,
                                         int currentNumberOfFrames,
                                         int applyTo
                                         )
    {
        super(o);
        this.applyTo = applyTo;
        this.havePolyShutter = havePolyShutter;
        this.haveRectShutter = haveRectShutter;
        this.haveCircShutter = haveCircShutter;
        this.haveBmpShutter = haveBmpShutter;
        this.isFlip = isFlip;
        this.isInvert = isInvert;
        this.rot = rot;
        this.psOn = psOn;
        this.win = win;
        this.lev = lev;
        this.lutOn = lutOn;
        this.zoom = zoom;
        this.name = name;
        this.creator = creator;
        this.description = description;
        this.cols = cols;
        this.rows = rows;
        this.displayArea = displayArea;
        this.presentationPixelAspectRatio = presentationPixelAspectRatio;
        this.presentationPixelMagnificationRatio = presentationPixelMagnificationRatio;
        this.presentationPixelAspectRatio = presentationPixelAspectRatio;
        this.presentationSizeMode = this.presentationSizeMode;
        this.presentationPixelSpacing_x = presentationPixelSpacing_x;
        this.presentationPixelSpacing_y = presentationPixelSpacing_y;
        this.haveDisplayedAreaPresentationPixelSpacing = haveDisplayedAreaPresentationPixelSpacing;
        this.canUseDisplayedAreaTrueSize = canUseDisplayedAreaTrueSize;
        this.maxNumberOfImages = maxNumberOfImages;
        this.currentImageNumber = currentImageNumber;
        this.maxNumberOfFramesInCurrentImage= maxNumberOfFramesInCurrentImage;
        this.currentNumberOfFrames = currentNumberOfFrames;
    }
}
/*
 *  CVS Log
 *  $Log: PresentationStateFixInfoEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

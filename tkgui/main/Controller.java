/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;

import java.util.*;
import javax.swing.event.*;
/**
 * The class Controller provides an event-based communication in
 * an application. Parts of the application can add itself to this
 * server to recieve incoming events. Those parts must implement the
 * "Listener"-interface.
 * Do add a listener you may use the following commands:
 *
 * class SomeListener implements Listener
 * {
 *  public SomeListener()
 *  {
 *      Controller es = Controller.instance().addMainListener(this);
 *  }
 *  
 *  public boolean processEvent (JiveEvent e)
 *  {
 *      if (e instanceof MySpecialEvent) doSomething();
 *      return true;
 *  }
 * }
*/

public class Controller
{
    // Singleton Pattern: 
    /** The one and only instance is stored here. */
    private static Controller theInstance = null;
    
    protected EventListenerList listenrList ;
    
    
    
    /**
     * The Constructor of this class. The one-and-only object of
     * this class can be created (or retrieved) with this method.
     *
     * @return an instance of this class.
    */
    public static Controller instance()
    {
        if (theInstance == null) theInstance = new Controller();
        
        return theInstance;
    }
    
    /**
     * The internal constructor of this class. Don't call direct.
    */
    protected Controller ()
    {
        // do not instanciate this way!
        
        listenrList = new EventListenerList();
    }
   

    public void addMainListener(MainListener l) 
    {
        listenrList.add(MainListener.class, l);
    }
    public void addGuiComponentListener(GuiComponentListener l) 
    {
        listenrList.add(GuiComponentListener.class, l);
    }

    public void removeGuiComponentListener(GuiComponentListener l) 
    {
        listenrList.remove(GuiComponentListener.class, l);
    }
    public void addStatusListener(StatusListener l) 
    {
        listenrList.add(StatusListener.class, l);
    }

    public void removeStatusListener(StatusListener l) 
    {
        listenrList.remove(StatusListener.class, l);
    }
    public void addPrintListener(PrintListener l) 
    {
        listenrList.add(PrintListener.class, l);
    }

    public void removePrintListener(PrintListener l) 
    {
        listenrList.remove(PrintListener.class, l);
    }

    // Notify all listeners that have registered interest for
    // notification on this event type.  The event instance 
    // is lazily created using the parameters passed into 
    // the fire method.

    public void fireEvent(DSEvent e) 
    {
        // Guaranteed to return a non-null array
        Object[] listeners = listenrList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length-2; i>=0; i-=2) 
        {
            if (listeners[i]==MainListener.class) 
            {
                // Lazily create the event:
                //if (jiveMessage == null) fooEvent = new FooEvent(this);
    
                ((MainListener)listeners[i+1]).processEvent(e);
            }
            
        }
    }
    // Notify all listeners that have registered interest for
    // notification on this event type.  The event instance 
    // is lazily created using the parameters passed into 
    // the fire method.

    public void fireNotification(DSEvent e) 
    {
        
        // Guaranteed to return a non-null array
        Object[] listeners = listenrList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length-2; i>=0; i-=2) 
        {
            if (listeners[i]==GuiComponentListener.class) 
            {
                // Lazily create the event:
                //if (jiveMessage == null) fooEvent = new FooEvent(this);
    
                ((GuiComponentListener)listeners[i+1]).processComponents(e);
            }
            
        }
    }	
    // Notify all listeners that have registered interest for
    // notification on this event type.  The event instance 
    // is lazily created using the parameters passed into 
    // the fire method.

    public void fireStatus(DSEvent e) 
    {
        
        // Guaranteed to return a non-null array
        Object[] listeners = listenrList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length-2; i>=0; i-=2) 
        {
            if (listeners[i]==StatusListener.class) 
            {
                // Lazily create the event:
                //if (jiveMessage == null) fooEvent = new FooEvent(this);
    
                ((StatusListener)listeners[i+1]).processStatus(e);
            }
            
        }
    }	
    // Notify all listeners that have registered interest for
    // notification on this event type.  The event instance 
    // is lazily created using the parameters passed into 
    // the fire method.

    public void firePrint(DSEvent e) 
    {
        
        // Guaranteed to return a non-null array
        Object[] listeners = listenrList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length-2; i>=0; i-=2) 
        {
            if (listeners[i]==PrintListener.class) 
            {
                // Lazily create the event:
                //if (jiveMessage == null) fooEvent = new FooEvent(this);
    
                ((PrintListener)listeners[i+1]).processPrint(e);
            }
            
        }
    }	
    
}
/*
 *  CVS Log
 *  $Log: Controller.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

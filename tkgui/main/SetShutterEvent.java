/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.io.*;
public class SetShutterEvent extends DSEvent 
{
     public static final int RECT = 0;
     public static final int CIRCLE = 1;
     public static final int POLY = 2;
     public static final int BMP = 3;
     public static final int EDIT = 10;
     
     int type;
     boolean selected;
     
     public SetShutterEvent(Object o,int type, boolean selected)
     {
       super(o);
        this.type = type;
        this.selected = selected;
     }
     public String toString()
     {
        return new String("SetShutterEvent: " + type + ", selected: " + selected);
     }
    public int getType()
    {
        return type;
    }
    public boolean getSelected()
    {
        return selected;
    }
}
/*
 *  CVS Log
 *  $Log: SetShutterEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

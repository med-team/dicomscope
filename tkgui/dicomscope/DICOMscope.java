/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 08:57:47 $
 *  Revision :    $Revision: 1.1 $
 *  State:        $State: Exp $
 */

package dicomscope;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

// Import all packages of the project:
import J2Ci.*;
import browser.*;
import main.MainContext;
import viewer.main.*;
import viewer.presentation.*;
import jToolkit.gui.*;
import jToolkit.io.*;
import about.*;
import main.*;
import viewer.gui.*;
import javax.swing.event.*;
import dicomPrint.*;
import processCommunication.*;

/**
 * This is the main class. This class initializes the study
 * browser, the viewer, the print panel  and the about box.
 * @author Andreas Schroeter
 * @since 30.03.
 */
public class DICOMscope extends JFrame implements MainListener {
    
    
    /**
     * The interface to the Dicom Toolkit
     */
    private jDVInterface dvi = null;
    
    /**
     * Contains the current PrintPanel
     */
    private PrintPanel printPanel;
    
    /**
     * Contains the main TabbedPane
     */
    private JTabbedPane	tabpane ;
    
    
    
    
    /**
     * Main Method. Initializes this class.
     * @param args Will be ignored
     */
    
    public static  void main(String[] args) {
        System.out.println("starting DICOMscope");
        System.out.println("please wait...");
        System.out.flush(); // force output right now!
        
        MainContext mc = MainContext.instance(); // create context
        
        DICOMscope sup = new DICOMscope();
        sup.setVisible(true);
    }
    
    /**
     * Constructor.
     */
    public DICOMscope() {
        Controller.instance().addMainListener(this);
        dvi = new jDVInterface("DICOMscope.cfg"); // change with configfile
        
        tabpane = new JTabbedPane(JTabbedPane.TOP);
        
        MainContext.instance().initConfiguration(dvi); // create context
        
        Hashtable config = MainContext.instance().getConfiguration();
        
        setConfiguration(config, true);
        
        
        
        getContentPane().setLayout(new BorderLayout(0,0));
        //getRoontPane().setFont
        java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        
        setSize(screen.width, screen.height);
        //setSize(300,300);
        SymWindow aSymWindow = new SymWindow();
        this.addWindowListener(aSymWindow);
        addComponentListener(new CmpAdapter());
        
        
        //Init toolbar
        DicomScopeToolBar toolbar;
        boolean print = false;
        if (dvi.getNumberOfTargets(jDVPSPeerType.DVPSE_printAny)>0 ) print = true;
        toolbar = new DicomScopeToolBar(config, print);
        getContentPane().add(toolbar,BorderLayout.NORTH);
        boolean signWarning=true;
        if (config.containsKey("SignWarning"))signWarning=((Boolean)config.get("SignWarning")).booleanValue();
        
        StatusLine sl = new StatusLine(DSComponentType.BROWSER,dvi,signWarning);
        getContentPane().add( sl, BorderLayout.SOUTH);
        
        
        StudyMan studyMan = new StudyMan(dvi, this,config);
        MainImageViewerPanel mImage = new MainImageViewerPanel(new PresentationStateGraphicsHandler(dvi),config,this);
        AboutBox about =  new AboutBox(config);
        tabpane.add(DSComponentType.BROWSER, studyMan);
        tabpane.add(DSComponentType.VIEWER, mImage);
        
        if (print) {
            
            printPanel = new  PrintPanel(dvi,config);
            tabpane.add(DSComponentType.PRINT,printPanel );
        }
        tabpane.add(DSComponentType.PROCESS,new ProcessComponent(this, MainContext.instance().logFont));
        
        tabpane.add(DSComponentType.ABOUT,about);
        
        getContentPane().add(tabpane, BorderLayout.CENTER);
        
        tabpane.addChangeListener(new ChangeToComponentListener() );
        
        
        Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.BROWSER,"Ready"));
        Controller.instance().fireEvent(new TabbedChangeEvent(DICOMscope.this,DSComponentType.BROWSER));
        
        ProcessCommunicationService pcs= new ProcessCommunicationService(dvi.getMessagePort(), true);
        
        dvi.startReceiver();
        dvi.startPrintSpooler();
        dvi.startPrintServer();
        dvi.startQueryRetrieveServer();
        System.setOut(new NullPrintStream());
        
        
        
    }
    
    /**
     * Closes the application and terminates the network receiver.
     */
    class SymWindow extends java.awt.event.WindowAdapter {
        public void windowClosing(java.awt.event.WindowEvent event) {
            Object object = event.getSource();
            if (object == DICOMscope.this)closing(event); {
            }
            
        }
    }
    
    /**
     * Closes the application and terminates the network receiver.
     * @param event The calling event. ignored.
     */
    public void closing(java.awt.event.WindowEvent event) {
        dvi.terminateReceiver();
        dvi.terminatePrintSpooler();
        dvi.terminatePrintServer();
        dvi.terminateQueryRetrieveServer();
        dvi.finalize();
        //setVisible(false);		 // hide the Frame
        System.exit(0);
    }
    
    /**
     * Handles the ChangeEvents.
     */
    private class ChangeToComponentListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JTabbedPane tb = (JTabbedPane)e.getSource();
            int index = tb.getSelectedIndex();
            String s = tb.getTitleAt(index);
            Controller.instance().fireEvent(new TabbedChangeEvent(DICOMscope.this,s));
        }
        
    }
    
    /**
     * Handles the ComponentEvents.
     */
    private class CmpAdapter extends ComponentAdapter {
        public void componentResized(ComponentEvent evt) {
            MainContext mc = MainContext.instance(); // create context
            
            Dimension d = getSize();
            
            if (mc.highQualitySystem) // doppelte Gr��e als Minimum
            {
                if (d.width < 1500) d.width = 1500;
                if (d.height < 1500) d.height = 1500;
            }
            else {
                if (d.width < 700) d.width =700;
                if (d.height < 600) d.height = 600;
            }
            
            setSize(d);
        }
    }
    
    /**
     * Toggles  tabpane to the viewer.
     *
     * @since 30.03
     */
    public void switchToViewer() {
        
        tabpane.setSelectedIndex(1);
        tabpane.repaint();
    }
    
    /**
     * Toggles the tabpane to the PrintPanel.
     *
     * @since 30.03
     */
    
    
    public void switchToPrinter() {
        if (printPanel != null) {
            tabpane.setSelectedComponent(printPanel);
            tabpane.repaint();
        }
    }
    
    
    
    
    /**
     * Handels the DSEvent fired by any Object of DICOMScope
     */
    public boolean processEvent(DSEvent e) {
        if (e instanceof ChangeOptionsEvent) {
            
            setConfiguration(((ChangeOptionsEvent)e).getConfig(), false);
        }
        
        return false;
    }
    
    /**
     * Sets the configuration of DICOMscope.
     * @param config Contains the configuration.
     * @param init should be true if this function is called the first time.
     */
    private void setConfiguration(Hashtable config, boolean init) {
        if (config.containsKey("Title"))setTitle((String)(config.get("Title")));
        
        if (config.containsKey("TabbedPlacement"))tabpane.setTabPlacement((new Integer((String)(config.get("TabbedPlacement"))).intValue()));
        if (config.containsKey("UI")) {
            try {
                UIManager.setLookAndFeel((String) config.get("UI") );
                // update the complete application's look & feel
                SwingUtilities.updateComponentTreeUI( DICOMscope.this);
            }
            catch( Exception f ) {
                System.err.println( " unable to set UI " + f.getMessage() );
            }
            
        }
        if (!init)tabpane.updateUI();
        if (init) {
            Font f = (Font) config.get("GUIFont");
            Font titledBorderFont = new Font(f.getName(), f.getStyle(),f.getSize()-f.getSize()/4);
            UIDefaults uiDefaults = UIManager.getDefaults();
            uiDefaults.put("MenuItem.font",f);
            uiDefaults.put("Label.font", f);
            uiDefaults.put("ComboBox.font",f);
            uiDefaults.put("Button.font", f);
            uiDefaults.put("CheckBox.font", f);
            uiDefaults.put("CheckBoxMenuItem.font", f);
            uiDefaults.put("RadioButtonMenuItem.font", f);
            uiDefaults.put("RadioButton.font", f);
            uiDefaults.put("TitledBorder.font", titledBorderFont);
            uiDefaults.put("TextField.font", f);
            uiDefaults.put("Dialog.font", f);
            uiDefaults.put("TabbedPane.font",f);
            uiDefaults.put("Frame.font", f);
            uiDefaults.put("Scrollbar.font", f);
            uiDefaults.put("Tree.font", f);
            uiDefaults.put("EditorPane.font", f);
            uiDefaults.put("TextArea.font", f);
            uiDefaults.put("List.font", f);
            uiDefaults.put("AbstractButton.font", f);
            uiDefaults.put("ToolTip.font", f);
            uiDefaults.put("Button", f);
            tabpane.updateUI();
        }
        
    }
    
}


/*
 *  CVS Log
 *  $Log: DICOMscope.java,v $
 *  Revision 1.1  2003/09/08 08:57:47  kleber
 *  move DICOMscope to folder dicomscope.
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 */

/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package jToolkit.io;

import javax.swing.*;
import java.io.*;
import java.net.*;
/**
* This class handles the loading of icons
*/
public class IconRetriever
{
    public ImageIcon getIcon (String filename)
    {
        byte[] imgStream = null;
        try
        {
            Class c = getClass();            
            ClassLoader cl = c.getClassLoader();
            URL u = cl.getResource (filename);
            if (u != null)
            {
                InputStream is = u.openStream();
                ByteArrayOutputStream bas = new ByteArrayOutputStream();
                int data;
                while ((data = is.read()) != -1)
                {
                    bas.write (data);
                }
                
                imgStream = bas.toByteArray();        
            }
            else // try normal way
            {                
                FileInputStream pf = new FileInputStream (filename);
                imgStream = new byte[pf.available()];
                pf.read (imgStream);                
            }
        }
        catch (Exception e)
        {
            System.out.println ("error!! " + e);
        }
        
        if (imgStream != null)
        {
            return new ImageIcon (imgStream);
        }
        else System.out.println ("Can't retrieve Icon");
        
        return null;
    }
}
/*
 *  CVS Log
 *  $Log: IconRetriever.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

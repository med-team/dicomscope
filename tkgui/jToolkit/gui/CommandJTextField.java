/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package jToolkit.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * The CommandJTextField extends the normal (Swing-)JTextField with an automatic command
 * call when the ActionEvent or the FocusEventis fired. 
 * @author Andreas Schroeter
 * @since 30.03.
*/
public class CommandJTextField extends JTextField implements ActionListener, FocusListener
{
    private int ID;
    private CommandTextListener ctl;
    public int getID(){ return ID;}


    public CommandJTextField(int col, CommandTextListener ctl, int ID)
    {
        super (col);
        this.ID = ID;
        this.ctl = ctl;
        addActionListener (this);
        addFocusListener(this);
        this.setMargin(new Insets(0,0,0,0));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this) ctl.setText (ID, getText());
    }
    public void focusGained(FocusEvent e)
    {
    }
    public void focusLost(FocusEvent e)
    {
        if (e.getSource() == this) ctl.setText (ID, getText());
    }
    
}
/*
 *  CVS Log
 *  $Log: CommandJTextField.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/

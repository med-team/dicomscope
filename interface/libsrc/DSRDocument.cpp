/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRDocument.h"
#include "jInterface.h"

#include "ofstream.h"


static inline DSRDocument *getAddressOfDSRDocument (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRDocument *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRDocument (JNIEnv *env, jobject obj, DSRDocument *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    isValid
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocument_isValid
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jboolean) doc->isValid();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    isFinalized
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocument_isFinalized
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jboolean) doc->isFinalized();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    renderHTML
 * Signature: (LJ2Ci/jStringByRef;I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_renderHTML
  (JNIEnv *env, jobject obj, jobject outputString, jint flags)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    /* create local string stream */
    OFOStringStream oss;
    OFCondition res = doc->renderHTML(oss, flags | DSRTypes::HF_version32Compatibility | DSRTypes::HF_renderFullData);
    OFSTRINGSTREAM_GETSTR(oss, tmpString)

    if (res == EC_Normal)
    {
        /* should never be NULL */
        if (tmpString != NULL)
        {
            jfieldID value = env->GetFieldID (env->GetObjectClass (outputString), "value", "Ljava/lang/String;");
            env->SetObjectField (outputString, value, env->NewStringUTF (tmpString));
        }
    }
    OFSTRINGSTREAM_FREESTR(tmpString)
    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getDocumentType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getDocumentType
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getDocumentType();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getTreeN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocument_getTreeN
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    DSRDocumentTree *tree = &(doc->getTree ());

    return (jlong) tree;
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSpecificCharacterSetType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getSpecificCharacterSetType
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getSpecificCharacterSetType();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setSpecificCharacterSetType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setSpecificCharacterSetType
  (JNIEnv *env, jobject obj, jint characterSet)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->setSpecificCharacterSetType((DSRTypes::E_CharacterSet)characterSet).status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getCompletionFlag
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getCompletionFlag
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getCompletionFlag();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getCompletionFlagDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getCompletionFlagDescription
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getCompletionFlagDescription();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getVerificationFlag
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getVerificationFlag
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getVerificationFlag();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getNumberOfVerifyingObservers
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getNumberOfVerifyingObservers
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getNumberOfVerifyingObservers();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getVerifyingObserver
 * Signature: (ILJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getVerifyingObserver__ILJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2
  (JNIEnv *env, jobject obj, jint idx, jobject dateTime, jobject obsName, jobject obsOrganization)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFString dtStr, onStr, ooStr;
    OFCondition res = doc->getVerifyingObserver((size_t)idx, dtStr, onStr, ooStr);

    jfieldID dtValue = env->GetFieldID (env->GetObjectClass(dateTime), "value", "Ljava/lang/String;");
    jfieldID onValue = env->GetFieldID (env->GetObjectClass(obsName), "value", "Ljava/lang/String;");
    jfieldID ooValue = env->GetFieldID (env->GetObjectClass(obsOrganization), "value", "Ljava/lang/String;");

    env->SetObjectField (dateTime, dtValue, env->NewStringUTF (dtStr.c_str()));
    env->SetObjectField (obsName, onValue, env->NewStringUTF (onStr.c_str()));
    env->SetObjectField (obsOrganization, ooValue, env->NewStringUTF (ooStr.c_str()));

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getVerifyingObserver
 * Signature: (ILJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;LJ2Ci/jStringByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getVerifyingObserver__ILJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2LJ2Ci_jStringByRef_2
  (JNIEnv *env, jobject obj, jint idx, jobject dateTime, jobject obsName, jobject obsCV, jobject obsCSD, jobject obsCSV, jobject obsCM, jobject obsOrganization)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFString dtStr, onStr, ooStr;
    DSRCodedEntryValue code;
    OFCondition res = doc->getVerifyingObserver((size_t)idx, dtStr, onStr, code, ooStr);

    jfieldID dtValue = env->GetFieldID (env->GetObjectClass(dateTime), "value", "Ljava/lang/String;");
    jfieldID onValue = env->GetFieldID (env->GetObjectClass(obsName), "value", "Ljava/lang/String;");
    jfieldID cvValue = env->GetFieldID (env->GetObjectClass(obsCV), "value", "Ljava/lang/String;");
    jfieldID csdValue = env->GetFieldID (env->GetObjectClass(obsCSD), "value", "Ljava/lang/String;");
    jfieldID csvValue = env->GetFieldID (env->GetObjectClass(obsCSV), "value", "Ljava/lang/String;");
    jfieldID cmValue = env->GetFieldID (env->GetObjectClass(obsCM), "value", "Ljava/lang/String;");
    jfieldID ooValue = env->GetFieldID (env->GetObjectClass(obsOrganization), "value", "Ljava/lang/String;");

    env->SetObjectField (dateTime, dtValue, env->NewStringUTF (dtStr.c_str()));
    env->SetObjectField (obsName, onValue, env->NewStringUTF (onStr.c_str()));
    env->SetObjectField (obsCV, cvValue, env->NewStringUTF (code.getCodeValue().c_str()));
    env->SetObjectField (obsCSD, csdValue, env->NewStringUTF (code.getCodingSchemeDesignator().c_str()));
    env->SetObjectField (obsCSV, csvValue, env->NewStringUTF (code.getCodingSchemeVersion().c_str()));
    env->SetObjectField (obsCM, cmValue, env->NewStringUTF (code.getCodeMeaning().c_str()));
    env->SetObjectField (obsOrganization, ooValue, env->NewStringUTF (ooStr.c_str()));

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getNumberOfPredecessorDocuments
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_getNumberOfPredecessorDocuments
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    return (jint) doc->getPredecessorDocuments().getNumberOfInstances();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getModality
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getModality
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getModality();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSOPClassUID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getStudyInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getStudyInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getStudyInstanceUID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSeriesInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSeriesInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSeriesInstanceUID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSOPInstanceUID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getInstanceCreatorUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getInstanceCreatorUID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getInstanceCreatorUID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSpecificCharacterSet
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSpecificCharacterSet
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSpecificCharacterSet();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getPatientsName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getPatientsName
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getPatientsName();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getPatientsBirthDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getPatientsBirthDate
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getPatientsBirthDate();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getPatientsSex
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getPatientsSex
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getPatientsSex();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getReferringPhysiciansName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getReferringPhysiciansName
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getReferringPhysiciansName();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getStudyDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getStudyDescription
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getStudyDescription();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSeriesDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSeriesDescription
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSeriesDescription();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getManufacturer
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getManufacturer
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getManufacturer();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getStudyDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getStudyDate
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getStudyDate();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getStudyTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getStudyTime
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getStudyTime();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getInstanceCreationDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getInstanceCreationDate
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getInstanceCreationDate();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getInstanceCreationTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getInstanceCreationTime
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getInstanceCreationTime();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getContentDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getContentDate
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getContentDate();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getContentTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getContentTime
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getContentTime();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getStudyID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getStudyID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getStudyID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getPatientID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getPatientID
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getPatientID();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getSeriesNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getSeriesNumber
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getSeriesNumber();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getInstanceNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getInstanceNumber
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getInstanceNumber();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    getAccessionNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocument_getAccessionNumber
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    const char *string = doc->getAccessionNumber();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setSpecificCharacterSet
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setSpecificCharacterSet
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setSpecificCharacterSet(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setCompletionFlagDescription
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setCompletionFlagDescription
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setCompletionFlagDescription(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setPatientsName
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setPatientsName
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setPatientsName(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setPatientsBirthDate
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setPatientsBirthDate
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setPatientsBirthDate(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setPatientsSex
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setPatientsSex
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setPatientsSex(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setReferringPhysiciansName
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setReferringPhysiciansName
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setReferringPhysiciansName(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setStudyDescription
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setStudyDescription
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setStudyDescription(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setSeriesDescription
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setSeriesDescription
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setSeriesDescription(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setManufacturer
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setManufacturer
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setManufacturer(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setStudyID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setStudyID
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setStudyID(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setPatientID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setPatientID
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->setPatientID(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setSeriesNumber
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setSeriesNumber
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setSeriesNumber(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setInstanceNumber
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setInstanceNumber
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setInstanceNumber(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    setAccessionNumber
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_setAccessionNumber
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = doc->setAccessionNumber(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewStudy
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocument_createNewStudy
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    doc->createNewStudy();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewSeries
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocument_createNewSeries
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    doc->createNewSeries();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewSeriesInStudy
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_createNewSeriesInStudy
  (JNIEnv *env, jobject obj, jstring studyUID)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    char *str = (studyUID) ? (char *) env->GetStringUTFChars (studyUID, 0) : NULL;

    OFCondition res = doc->createNewSeriesInStudy(str);

    env->ReleaseStringUTFChars (studyUID, str);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewSOPInstance
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocument_createNewSOPInstance
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    doc->createNewSOPInstance();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewDocument
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_createNewDocument__
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = doc->createNewDocument();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createNewDocument
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_createNewDocument__I
  (JNIEnv *env, jobject obj, jint type)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = doc->createNewDocument((DSRTypes::E_DocumentType)type);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    createRevisedVersion
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_createRevisedVersion
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = doc->createRevisedVersion();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    completeDocument
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_completeDocument__
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = doc->completeDocument();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    completeDocument
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_completeDocument__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring description)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(description, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = doc->completeDocument(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    verifyDocument
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_verifyDocument__Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring obsName, jstring obsOrganization)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jstring encoding = env->NewStringUTF(JAVA_ENCODING_STRING);
        jbyteArray array1 = (jbyteArray) env->CallObjectMethod(obsName, mid, encoding);
        jbyteArray array2 = (jbyteArray) env->CallObjectMethod(obsOrganization, mid, encoding);

        if (array1 && array2)
        {
            jbyte *string1 = env->GetByteArrayElements(array1, 0);
            jbyte *string2 = env->GetByteArrayElements(array2, 0);

            res = doc->verifyDocument(OFString((char *)string1, env->GetArrayLength(array1)),
                                      OFString((char *)string2, env->GetArrayLength(array2)));

            env->ReleaseByteArrayElements(array1, string1, 0);
            env->ReleaseByteArrayElements(array2, string2, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    verifyDocument
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocument_verifyDocument__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring obsName, jstring obsCV, jstring obsCSD, jstring obsCSV, jstring obsCM, jstring obsOrganization)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jstring encoding = env->NewStringUTF(JAVA_ENCODING_STRING);
        jbyteArray array1 = (jbyteArray) env->CallObjectMethod(obsName, mid, encoding);
        jbyteArray array2 = (jbyteArray) env->CallObjectMethod(obsCV, mid, encoding);
        jbyteArray array3 = (jbyteArray) env->CallObjectMethod(obsCSD, mid, encoding);
        jbyteArray array4 = (jbyteArray) env->CallObjectMethod(obsCSV, mid, encoding);
        jbyteArray array5 = (jbyteArray) env->CallObjectMethod(obsCM, mid, encoding);
        jbyteArray array6 = (jbyteArray) env->CallObjectMethod(obsOrganization, mid, encoding);

        if (array1 && array2 && array3 && array4 && array5 && array6)
        {
            jbyte *string1 = env->GetByteArrayElements(array1, 0);
            jbyte *string2 = env->GetByteArrayElements(array2, 0);
            jbyte *string3 = env->GetByteArrayElements(array3, 0);
            jbyte *string4 = env->GetByteArrayElements(array4, 0);
            jbyte *string5 = env->GetByteArrayElements(array5, 0);
            jbyte *string6 = env->GetByteArrayElements(array6, 0);

            DSRCodedEntryValue code(OFString((char *)string2, env->GetArrayLength(array2)),
                                    OFString((char *)string3, env->GetArrayLength(array3)),
                                    OFString((char *)string4, env->GetArrayLength(array4)),
                                    OFString((char *)string5, env->GetArrayLength(array5)));
            res = doc->verifyDocument(OFString((char *)string1, env->GetArrayLength(array1)), code,
                                      OFString((char *)string6, env->GetArrayLength(array6)));

            env->ReleaseByteArrayElements(array1, string1, 0);
            env->ReleaseByteArrayElements(array2, string2, 0);
            env->ReleaseByteArrayElements(array3, string3, 0);
            env->ReleaseByteArrayElements(array4, string4, 0);
            env->ReleaseByteArrayElements(array5, string5, 0);
            env->ReleaseByteArrayElements(array6, string6, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocument
 * Method:    removeVerification
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocument_removeVerification
  (JNIEnv *env, jobject obj)
{
    DSRDocument *doc = getAddressOfDSRDocument (env, obj);

    doc->removeVerification();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/

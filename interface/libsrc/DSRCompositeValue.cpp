/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRCompositeValue.h"
#include "jInterface.h"


static inline DSRCompositeReferenceValue *getAddressOfDSRCompositeReferenceValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRCompositeReferenceValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRCompositeReferenceValue (JNIEnv *env, jobject obj, DSRCompositeReferenceValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DSRCompositeReferenceValue *ref = getAddressOfDSRCompositeReferenceValue (env, obj);

    const char *string = ref->getSOPClassUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPClassName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPClassName
  (JNIEnv *env, jobject obj)
{
    DSRCompositeReferenceValue *ref = getAddressOfDSRCompositeReferenceValue (env, obj);

    const char *uid = ref->getSOPClassUID().c_str();
    if ((uid != NULL) && (strlen(uid) > 0))
    {
        const char *name = dcmFindNameOfUID(uid);
        if (name == NULL)
        {
            OFString string = "unknown SOP class";
            if (uid != NULL)
            {
                string += " (";
                string += uid;
                string += ")";
            }
            return env->NewStringUTF (string.c_str());
            return env->NewStringUTF (name);
        }
    }
    return env->NewStringUTF ("");
}


/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRCompositeReferenceValue *ref = getAddressOfDSRCompositeReferenceValue (env, obj);

    const char *string = ref->getSOPInstanceUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    setReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRCompositeValue_setReference
  (JNIEnv *env, jobject obj, jstring sopClassUID, jstring sopInstanceUID)
{
    DSRCompositeReferenceValue *ref = getAddressOfDSRCompositeReferenceValue (env, obj);

    char *string1 = (sopClassUID) ? (char *) env->GetStringUTFChars (sopClassUID, 0) : NULL;
    char *string2 = (sopInstanceUID) ? (char *) env->GetStringUTFChars (sopInstanceUID, 0) : NULL;

    OFCondition res = ref->setReference(string1, string2);

    env->ReleaseStringUTFChars (sopClassUID, string1);
    env->ReleaseStringUTFChars (sopInstanceUID, string2);

    return (jint) res.status();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/

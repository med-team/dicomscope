/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRCodeValue.h"
#include "jInterface.h"


static inline DSRCodedEntryValue *getAddressOfDSRCodedEntryValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRCodedEntryValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRCodedEntryValue (JNIEnv *env, jobject obj, DSRCodedEntryValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    clear
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRCodeValue_clear
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    cod->clear();
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    isValid
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRCodeValue_isValid
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    return (jint) cod->isValid();
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    isEmpty
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRCodeValue_isEmpty
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    return (jint) cod->isEmpty();
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    getCodeValue
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCodeValue_getCodeValue
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    const char *string = cod->getCodeValue().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    getCodingSchemeDesignator
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCodeValue_getCodingSchemeDesignator
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    const char *string = cod->getCodingSchemeDesignator().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    getCodingSchemeVersion
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCodeValue_getCodingSchemeVersion
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    const char *string = cod->getCodingSchemeVersion().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    getCodeMeaning
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCodeValue_getCodeMeaning
  (JNIEnv *env, jobject obj)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    const char *string = cod->getCodeMeaning().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRCodeValue
 * Method:    setCode
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRCodeValue_setCode
  (JNIEnv *env, jobject obj, jstring codeValue, jstring codingSchemeDesignator, jstring codingSchemeVersion, jstring codeMeaning)
{
    DSRCodedEntryValue *cod = getAddressOfDSRCodedEntryValue (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jstring encoding = env->NewStringUTF(JAVA_ENCODING_STRING);
        jbyteArray array1 = (jbyteArray) env->CallObjectMethod(codeValue, mid, encoding);
        jbyteArray array2 = (jbyteArray) env->CallObjectMethod(codingSchemeDesignator, mid, encoding);
        jbyteArray array3 = (jbyteArray) env->CallObjectMethod(codingSchemeVersion, mid, encoding);
        jbyteArray array4 = (jbyteArray) env->CallObjectMethod(codeMeaning, mid, encoding);

        if (array1 && array2 && array3 && array4)
        {
            jbyte *string1 = env->GetByteArrayElements(array1, 0);
            jbyte *string2 = env->GetByteArrayElements(array2, 0);
            jbyte *string3 = env->GetByteArrayElements(array3, 0);
            jbyte *string4 = env->GetByteArrayElements(array4, 0);

            res = cod->setCode(OFString((char *)string1, env->GetArrayLength(array1)),
                               OFString((char *)string2, env->GetArrayLength(array2)),
                               OFString((char *)string3, env->GetArrayLength(array3)),
                               OFString((char *)string4, env->GetArrayLength(array4)));

            env->ReleaseByteArrayElements(array1, string1, 0);
            env->ReleaseByteArrayElements(array2, string2, 0);
            env->ReleaseByteArrayElements(array3, string3, 0);
            env->ReleaseByteArrayElements(array4, string4, 0);
        }
    }

    return (jint) res.status();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/

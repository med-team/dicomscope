/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRDocumentTree.h"
#include "jInterface.h"


static inline DSRDocumentTree *getAddressOfDSRDocumentTree (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRDocumentTree *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRDocumentTree (JNIEnv *env, jobject obj, DSRDocumentTree *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    canAddContentItem
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_canAddContentItem
  (JNIEnv *env, jobject obj, jint relationshipType, jint valueType, jint addMode)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jboolean) tree->canAddContentItem((DSRTypes::E_RelationshipType)relationshipType,
        (DSRTypes::E_ValueType)valueType, (DSRTypes::E_AddMode)addMode);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    addContentItem
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_addContentItem
  (JNIEnv *env, jobject obj, jint relationshipType, jint valueType, jint addMode)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->addContentItem((DSRTypes::E_RelationshipType)relationshipType,
        (DSRTypes::E_ValueType)valueType, (DSRTypes::E_AddMode)addMode);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    removeCurrentContentItem
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_removeCurrentContentItem
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->removeCurrentContentItem();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    removeSignatures
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_removeSignatures
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    tree->removeSignatures();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    unmarkAllContentItems
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_unmarkAllContentItems
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    tree->unmarkAllContentItems();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoRootNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoRootNode
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->gotoRoot();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoPreviousNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoPreviousNode
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->gotoPrevious();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoNextNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoNextNode
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->gotoNext();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoParentNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoParentNode
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->goUp();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoFirstChildNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoFirstChildNode
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->goDown();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    iterateNodes
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_iterateNodes
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->iterate();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoNode
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoNode
  (JNIEnv *env, jobject obj, jint searchID)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->gotoNode((size_t)searchID);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    isCurrentItemValid
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_isCurrentItemValid
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jboolean) tree->getCurrentContentItem().isValid();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    isCurrentItemMarked
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_isCurrentItemMarked
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jboolean) tree->getCurrentContentItem().isMarked();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentItemMark
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentItemMark
  (JNIEnv *env, jobject obj, jboolean flag)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    tree->getCurrentContentItem().setMark(flag == JNI_TRUE ? OFTrue : OFFalse);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentValueType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentValueType
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getCurrentContentItem().getValueType();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentRelationshipType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentRelationshipType
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getCurrentContentItem().getRelationshipType();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentNodeID
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentNodeID
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getNodeID();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentReferencedNodeID
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentReferencedNodeID
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getCurrentContentItem().getReferencedNodeID();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentLevel
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentLevel
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getLevel();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentStringValue
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentStringValue
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    const char *str = tree->getCurrentContentItem().getStringValue().c_str();

    if (str == NULL) return NULL;
    return env->NewStringUTF (str);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentStringValue
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentStringValue
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = tree->getCurrentContentItem().setStringValue(OFString((char *)string, env->GetArrayLength(array)));

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentCodeValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentCodeValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRCodedEntryValue *ptr = tree->getCurrentContentItem().getCodeValuePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentNumValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentNumValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRNumericMeasurementValue *ptr = tree->getCurrentContentItem().getNumericValuePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentSCoordValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentSCoordValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRSpatialCoordinatesValue *ptr = tree->getCurrentContentItem().getSpatialCoordinatesPtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentTCoordValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentTCoordValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRTemporalCoordinatesValue *ptr = tree->getCurrentContentItem().getTemporalCoordinatesPtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentCompositeValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentCompositeValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRCompositeReferenceValue *ptr = tree->getCurrentContentItem().getCompositeReferencePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentImageValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentImageValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRImageReferenceValue *ptr = tree->getCurrentContentItem().getImageReferencePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentWaveformValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentWaveformValueN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRWaveformReferenceValue *ptr = tree->getCurrentContentItem().getWaveformReferencePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentContinuityOfContentFlag
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentContinuityOfContentFlag
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getCurrentContentItem().getContinuityOfContent();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentContinuityOfContentFlag
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentContinuityOfContentFlag
  (JNIEnv *env, jobject obj, jint flag)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    return (jint) tree->getCurrentContentItem().setContinuityOfContent((DSRTypes::E_ContinuityOfContent)flag).status();
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentConceptNameN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentConceptNameN
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    DSRCodedEntryValue *ptr = tree->getCurrentContentItem().getConceptNamePtr();

    return (jlong) ptr;
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentObservationDateTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentObservationDateTime
  (JNIEnv *env, jobject obj)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    const char *str = tree->getCurrentContentItem().getObservationDateTime().c_str();

    if (str == NULL) return NULL;
    return env->NewStringUTF (str);
}


/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentObservationDateTime
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentObservationDateTime
  (JNIEnv *env, jobject obj, jstring value)
{
    DSRDocumentTree *tree = getAddressOfDSRDocumentTree (env, obj);

    char *str = (value) ? (char *) env->GetStringUTFChars (value, 0) : NULL;

    OFCondition res = tree->getCurrentContentItem().setObservationDateTime(str);

    env->ReleaseStringUTFChars (value, str);

    return (jint) res.status();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/

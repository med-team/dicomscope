# Microsoft Developer Studio Project File - Name="jInterface" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=jInterface - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den Befehl
!MESSAGE 
!MESSAGE NMAKE /f "jInterface.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "jInterface.mak" CFG="jInterface - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "jInterface - Win32 Release" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "jInterface - Win32 Debug" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "jInterface - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "\jdk142\include" /I "\jdk142\include\win32" /I "..\..\openssl-0.9.6i\lib" /I "include" /I "..\..\dcmtk\dcmpstat\include" /I "..\..\dcmtk\config\include" /I "..\..\dcmtk\ofstd\include" /I "..\..\dcmtk\dcmdata\include" /I "..\..\dcmtk\dcmimgle\include" /I "..\..\dcmtk\imagectn\include" /I "..\..\dcmtk\dcmnet\include" /I "..\..\dcmtk\dcmsr\include" /I "..\..\dcmtk\dcmsign\include" /I "..\..\dcmtk\dcmjpeg\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib dcmpstat.lib dcmimgle.lib ofstd.lib dcmnet.lib dcmdata.lib imagedb.lib dcmsr.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 zlib_o.lib libeay32_o.lib ssleay32_o.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib netapi32.lib dcmpstat.lib dcmimgle.lib ofstd.lib dcmtls.lib dcmnet.lib dcmdata.lib imagedb.lib dcmsr.lib dcmdsig.lib dcmjpeg.lib ijg8.lib ijg12.lib ijg16.lib /nologo /subsystem:windows /dll /machine:I386 /libpath:"..\..\zlib-1.1.4\lib" /libpath:"..\..\openssl-0.9.6i\lib" /libpath:"..\..\dcmtk\dcmdata\libsrc\Release" /libpath:"..\..\dcmtk\ofstd\libsrc\Release" /libpath:"..\..\dcmtk\dcmimgle\libsrc\Release" /libpath:"..\..\dcmtk\dcmtls\libsrc\Release" /libpath:"..\..\dcmtk\dcmnet\libsrc\Release" /libpath:"..\..\dcmtk\dcmpstat\libsrc\Release" /libpath:"..\..\dcmtk\imagectn\libsrc\Release" /libpath:"..\..\dcmtk\dcmsr\libsrc\Release" /libpath:"..\..\dcmtk\dcmsign\libsrc\Release" /libpath:"..\..\dcmtk\dcmjpeg\libsrc\Release" /libpath:"..\..\dcmtk\dcmjpeg\libijg8\Release" /libpath:"..\..\dcmtk\dcmjpeg\libijg12\Release" /libpath:"..\..\dcmtk\dcmjpeg\libijg16\Release"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "jInterface - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "\jdk142\include" /I "\jdk142\include\win32" /I "..\..\openssl-0.9.6i\lib" /I "include" /I "..\..\dcmtk\dcmpstat\include" /I "..\..\dcmtk\config\include" /I "..\..\dcmtk\ofstd\include" /I "..\..\dcmtk\dcmdata\include" /I "..\..\dcmtk\dcmimgle\include" /I "..\..\dcmtk\imagectn\include" /I "..\..\dcmtk\dcmnet\include" /I "..\..\dcmtk\dcmsr\include" /I "..\..\dcmtk\dcmsign\include" /I "..\..\dcmtk\dcmjpeg\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib dcmpstat.lib dcmimgle.lib ofstd.lib dcmnet.lib dcmdata.lib imagedb.lib dcmsr.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 zlib_d.lib libeay32_d.lib ssleay32_d.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib netapi32.lib dcmpstat.lib dcmimgle.lib ofstd.lib dcmtls.lib dcmnet.lib dcmdata.lib imagedb.lib dcmsr.lib dcmdsig.lib dcmjpeg.lib ijg8.lib ijg12.lib ijg16.lib /nologo /subsystem:windows /dll /machine:I386 /libpath:"..\..\zlib-1.1.4\lib" /libpath:"..\..\openssl-0.9.6i\lib" /libpath:"..\..\dcmtk\dcmdata\libsrc\Debug" /libpath:"..\..\dcmtk\ofstd\libsrc\Debug" /libpath:"..\..\dcmtk\dcmimgle\libsrc\Debug" /libpath:"..\..\dcmtk\dcmtls\libsrc\Debug" /libpath:"..\..\dcmtk\dcmnet\libsrc\Debug" /libpath:"..\..\dcmtk\dcmpstat\libsrc\Debug" /libpath:"..\..\dcmtk\imagectn\libsrc\Debug" /libpath:"..\..\dcmtk\dcmsr\libsrc\Debug" /libpath:"..\..\dcmtk\dcmsign\libsrc\Debug" /libpath:"..\..\dcmtk\dcmjpeg\libsrc\Debug" /libpath:"..\..\dcmtk\dcmjpeg\libijg8\Debug" /libpath:"..\..\dcmtk\dcmjpeg\libijg12\Debug" /libpath:"..\..\dcmtk\dcmjpeg\libijg16\Debug"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "jInterface - Win32 Release"
# Name "jInterface - Win32 Debug"
# Begin Source File

SOURCE=.\libsrc\DSRCodeValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRCompositeValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRDocument.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRDocumentTree.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRImageValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRNumericValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRSCoordValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRTCoordValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DSRWaveformValue.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVInterface.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVPresentationState.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVPSCurve.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVPSGraphicObject.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVPSStoredPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\libsrc\DVPSTextObject.cpp
# End Source File
# End Target
# End Project

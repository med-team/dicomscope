/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class J2Ci_jDVPSTextObject */

#ifndef _Included_J2Ci_jDVPSTextObject
#define _Included_J2Ci_jDVPSTextObject
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    createObjOfDVPSTextObject
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_createObjOfDVPSTextObject
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    CopyConstructor
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_CopyConstructor
  (JNIEnv *, jobject, jlong);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    haveAnchorPoint
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_haveAnchorPoint
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    haveBoundingBox
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_haveBoundingBox
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setAnchorPoint
 * Signature: (DDIZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setAnchorPoint
  (JNIEnv *, jobject, jdouble, jdouble, jint, jboolean);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setBoundingBox
 * Signature: (DDDDII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setBoundingBox
  (JNIEnv *, jobject, jdouble, jdouble, jdouble, jdouble, jint, jint);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setText
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setText
  (JNIEnv *, jobject, jstring);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    removeAnchorPoint
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_removeAnchorPoint
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    removeBoundingBox
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_removeBoundingBox
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getText
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSTextObject_getText
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxTLHC_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxTLHC_1x
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxTLHC_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxTLHC_1y
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxBRHC_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxBRHC_1x
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxBRHC_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxBRHC_1y
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxAnnotationUnits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxAnnotationUnits
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPoint_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPoint_1x
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPoint_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPoint_1y
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    anchorPointIsVisible
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_anchorPointIsVisible
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPointAnnotationUnits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPointAnnotationUnits
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxHorizontalJustification
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxHorizontalJustification
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif

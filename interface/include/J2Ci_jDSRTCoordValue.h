/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class J2Ci_jDSRTCoordValue */

#ifndef _Included_J2Ci_jDSRTCoordValue
#define _Included_J2Ci_jDSRTCoordValue
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     J2Ci_jDSRTCoordValue
 * Method:    getTemporalRangeType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRTCoordValue_getTemporalRangeType
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRTCoordValue
 * Method:    setTemporalRangeType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRTCoordValue_setTemporalRangeType
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
